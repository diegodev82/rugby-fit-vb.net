Imports MPP
Imports BE

Public Class BLL_Categorias


    Public oMpp As New MPP.MPP_Categorias

    ''' 
    ''' <param name="objCategoria"></param>
    Public Function Crear(ByVal objCategoria As BE_Categoria) As BE_Categoria
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objCategoria"></param>
    Public Function Editar(ByVal objCategoria As BE_Categoria) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objCategoria"></param>
    Public Function Eliminar(ByVal objCategoria As BE_Categoria) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Categoria)
        Return oMpp.Listar(filtros)
    End Function


End Class ' BLL_Categorias

