Public Class BE_Evento

    Public Const CAMBIO_CONTRASENIA_CLIENTE = 1
    Public Const CONFIRMAR_REGISTRACION = 2
    Public Const RESET_CONTRASENIA_CLIENTE = 3
    Public Const EDITAR_CLIENTE = 4
    Public Const CREAR_CLIENTE = 5
    Public Const LOGIN_OK = 6
    Public Const NICKNAME_INCORRECTO = 7
    Public Const PASSWORD_INCORRECTO = 8
    Public Const USUARIO_BLOQUEADO = 9
    Public Const LOGOUT_OK = 10
    Public Const CREAR_VENTA_WEB = 11
    Public Const CREAR_FACTURA = 12
    Public Const GENERAR_BACKUP = 13
    Public Const RESTAURAR_BACKUP = 14
    Public Shared Property NombresCriticidad As New Dictionary(Of Integer, String)


    Public Property Criticidad As Integer
    Public Property IdEvento As Integer
    Public Property Nombre As String

    Public Sub New()
        If NombresCriticidad.Count = 0 Then
            NombresCriticidad.Add(1, "Muy critico")
            NombresCriticidad.Add(2, "Critico")
            NombresCriticidad.Add(3, "Moderado")
            NombresCriticidad.Add(4, "Leve")
        End If
        
    End Sub

End Class ' BE_Evento
