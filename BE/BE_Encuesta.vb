﻿Public Class BE_Encuesta
    Public Const TIPO_CONSULTA = "C"
    Public Const TIPO_SATISFACCION = "S"
    Public Property IdEncuesta As Integer
    Public Property Pregunta As String
    Public Property Tipo As String ' C = Consulta, S = satisfaccion
    Public Property FechaDesde As DateTime
    Public Property FechaHasta As DateTime
    Public Property Respuestas As New List(Of BE_RespuestaEncuesta)
End Class
