﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminEncuestasForm.aspx.vb" Inherits="TFI_RugbyFit.AdminEncuestaForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <asp:ScriptManager ID="ScriptManagerEncuestas" runat="server"></asp:ScriptManager>
     <% If Request.QueryString("guardado") = "ok" Then%>
           <div class="alert alert-success">
               <p><span class="glyphicon glyphicon-thumbs-up" ></span> La encuesta fue guardada con &eacute;xito</p>
           </div> 
        <% End If%>
    <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
        <div class="alert alert-danger">
            <asp:Literal ID="msjErrorDB" Visible="false" runat="server" Text="Se produjo un error en el sistema y el registro no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
        </div>
    </asp:PlaceHolder>
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <div class="form-group botonera clearfix">
        <a href="AdminEncuestasList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <a href="AdminEncuestasForm.aspx" class="btn btn-default pull-left">Nuevo</a>
        <asp:Button ID="btnEliminar" runat="server" CssClass="btn btn-danger pull-right" Visible="false" Text="Eliminar" />
    </div>
    <asp:HiddenField ID="iptCodigo" runat="server" />
      <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Tipo:</label>
            </div>
            <div class='col-md-11'>
                <asp:DropDownList ID="ddTipo" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="C">Encuesta</asp:ListItem>
                    <asp:ListItem Value="S">Ficha de Opini&oacute;n</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        </div>
        <!-- /row -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Pregunta:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptPregunta" MaxLength="50" runat="server" CssClass="form-control" ClientIDMode="Static" required="required"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_iptPregunta" runat="server" ControlToValidate="iptPregunta" ErrorMessage="Campo Requerido" ForeColor="red"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->

    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Fecha Desde:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptFechaDesde" runat="server" placeholder="dd/mm/yyyy" ClientIDMode="Static" required="required" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptFechaDesde" runat="server" ControlToValidate="iptFechaDesde" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderDesde" TargetControlID="iptFechaDesde" Format="dd/MM/yyyy" runat="server" />
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
       <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Fecha Hasta:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptFechaHasta" runat="server"  CssClass="form-control"  ClientIDMode="Static" required="required" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptFechaHasta" runat="server" ControlToValidate="iptFechaHasta" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="CalendarExtenderHasta" TargetControlID="iptFechaHasta" Format="dd/MM/yyyy" runat="server" />
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <asp:PlaceHolder ID="phRespuestas" Visible="false" runat="server">       
            <div class="row">
                <div class="col-md-6  col-md-offset-4">
                    <div class="col-md-12"><label>Respuestas</label></div>
                    <div class="col-md-9">
                        <asp:HiddenField ID="iptCodigoRta" Value="0" runat="server" />
                        <asp:TextBox ID="iptRespuesta" CssClass="form-control" ValidationGroup="vgAgregaRta" ClientIDMode="Static" MaxLength="30" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator_rta" runat="server" ForeColor="red" ErrorMessage="Campo Requerido" ControlToValidate="iptRespuesta" ValidationGroup="vgAgregaRta"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnAgregaRta" runat="server" CssClass="btn btn-sm btn-warning" Text="Guardar" ValidationGroup="vgAgregaRta" />
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-danger" runat="server" id="error_rta_duplicada" visible="false">
                            Ya existe una respuesta con el mismo valor
                        </div>
                        <div class="alert alert-danger" runat="server" id="error_db" visible="false">
                            Disculpe, hubo un error en el sistema y el registro no pudo ser guardado. Int&eacute;ntelo nuevamente, por favor.
                        </div>
                        <div class="alert alert-success" runat="server" id="guardado_ok" visible="false">
                            La respuesta se guard&oacute; con &eacute;xito
                        </div>
                         <asp:GridView ID="gdvRespuestas"  CssClass="table table-hover table-bordered" BorderStyle="None" runat="server" AutoGenerateColumns="False">

                            <Columns>
                                <asp:BoundField DataField="IdRespuesta" HeaderText="Codigo" ReadOnly="True" SortExpression="IdRespuesta"></asp:BoundField>
                                <asp:BoundField DataField="Respuesta"  HeaderText="Respuesta" ReadOnly="False" SortExpression="Respuesta"></asp:BoundField>
                                <%--<asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" ControlStyle-BorderStyle="None" HeaderText="Editar" />--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" CausesValidation="false" CssClass="btn btn-default  glyphicon-edit" Text='Editar' OnClick="btnEdit_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="Eliminar" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    <div class="form-group botonera clearfix">
        <a href="AdminEncuestasList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <input type='reset' class="btn btn-default  pull-left" name='guardar' value='Cancelar' />
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar" />
    </div>
</asp:Content>





        