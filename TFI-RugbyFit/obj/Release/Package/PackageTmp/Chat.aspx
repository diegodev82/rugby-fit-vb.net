﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Chat.aspx.vb" Inherits="TFI_RugbyFit.Chat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/chat.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <!-- Basado en: http://bootsnipp.com/snippets/featured/chat-widget -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-comment"></span> Comunicaci&oacute;n con un operador
                     <div class="btn-group pull-right">
                         <a href="Chat.aspx" class="btn btn-default btn-xs">
                             <span class="glyphicon glyphicon-refresh"></span>
                         </a>
                     </div>
                        <%-- <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </button>
                        <ul class="dropdown-menu slidedown">
                            <li><a href="#"><span class="glyphicon glyphicon-refresh">
                            </span>Refresh</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-ok-sign">
                            </span>Available</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-remove">
                            </span>Busy</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-time"></span>
                                Away</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><span class="glyphicon glyphicon-off"></span>
                                Sign Out</a></li>
                        </ul>
                    </div>--%>
                    </div>
                    <div class="panel-body">
                        <ul class="chat">
                            <asp:Repeater ID="rptChat" runat="server">
                                <ItemTemplate>
                                    
                                    <li class="left clearfix" runat="server" visible="<%# Not Container.DataItem.EsRespuesta %>">
                                        <span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff&text=Yo" class="img-circle" />
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">
                                                    <asp:Literal ID="ltlMiNombre" Text="<%# Container.DataItem.Emisor.Nombre %>" runat="server"></asp:Literal>
                                                </strong>
                                                <small class="pull-right text-muted">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                    <asp:Literal ID="ltlFechaHora" Text="<%# Container.DataItem.TiempoTranscurrido %>" runat="server"></asp:Literal>
                                                </small>
                                            </div>
                                            <p>
                                                <asp:Literal ID="ltlMensaje" runat="server" Text="<%# Container.DataItem.Mensaje %>"></asp:Literal></p>
                                        </div>
                                    </li>
                                    
                                    <li class="right clearfix"  runat="server" visible="<%# Container.DataItem.EsRespuesta %>">
                                        <span class="chat-img pull-right">
                                        <img src="assets/img/avatar-operador.png" width="50" height="50" alt="Operador" class="img-circle" />
                                    </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><%# Container.DataItem.TiempoTranscurrido %></small>
                                                <strong class="pull-right primary-font"><asp:Literal ID="Literal2" Text="<%# Container.DataItem.Emisor.Nombre %>" runat="server"></asp:Literal></strong>
                                            </div>
                                            <p><asp:Literal ID="Literal1" runat="server" Text="<%# Container.DataItem.Mensaje %>"></asp:Literal></p>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>

                           <%-- <li class="right clearfix"><span class="chat-img pull-right">
                                <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                            </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                        <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                    </div>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                    </p>
                                </div>
                            </li>
                            <li class="left clearfix"><span class="chat-img pull-left">
                                <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                            </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                            <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
                                    </div>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                    </p>
                                </div>
                            </li>
                            <li class="right clearfix"><span class="chat-img pull-right">
                                <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                            </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>15 mins ago</small>
                                        <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                    </div>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                    </p>
                                </div>
                            </li>--%>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <asp:HiddenField ID="iptIdChat" Value="0" runat="server" />
                            <asp:TextBox ID="iptMensaje" ValidationGroup="MsjChat" CssClass="form-control input-sm" ClientIDMode="Static" required="required" MaxLength="70" placeholder="Escriba su mensaje aquí..." runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptMsje" runat="server" ErrorMessage="Ingrese un mensaje" ControlToValidate="iptMensaje"></asp:RequiredFieldValidator>
                            <span class="input-group-btn">
                                <asp:Button ID="btnAgregarMensaje" CssClass="btn btn-warning btn-sm btnEnviarMensaje" ValidationGroup="MsjChat" runat="server" Text="Enviar" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
