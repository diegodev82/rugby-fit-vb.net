create table producto_comentario (
 id_comentario integer not null identity(1,1),
 autor varchar(200),
 comentario text,
 activo bit,
 PRIMARY KEY (id_comentario)
)

alter table dbo.producto_comentario alter column id_comentario integer not null

ALTER TABLE producto_comentario ADD PRIMARY KEY (id_comentario)


drop table producto_comentario
