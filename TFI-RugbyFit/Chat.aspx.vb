﻿Imports BLL
Imports BE
Public Class Chat
    Inherits ACL
    Private oBll As New BLL_Chat
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ValidarAccesoCliente()
            If Not IsPostBack Then
                Me.CargarChat()
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Public Sub CargarChat()
        Dim idUsu As Integer = Me.GetIdUsuario
        Dim chat As BE_Chat = oBll.GetChat(idUsu)
        If chat.IdChat > 0 Then
            iptIdChat.Value = chat.IdChat
        End If
        If chat.Mensajes.Count > 0 Then
            rptChat.DataSource = Nothing
            rptChat.DataMember = "BE_MensajeChat"
            rptChat.DataSource = chat.Mensajes
            rptChat.DataBind()
        End If
    End Sub

    Protected Sub btnAgregarMensaje_Click(sender As Object, e As EventArgs) Handles btnAgregarMensaje.Click
        Try
            Dim msj As New BE_MensajeChat
            msj.IdChat = iptIdChat.Value
            msj.Mensaje = iptMensaje.Text
            msj.Emisor = New BE_Usuario With {.IdUsuario = Me.GetIdUsuario()}
            Dim agregado As Boolean = oBll.AgregarMensaje(msj)
            If agregado = True Then
                Response.Redirect("Chat.aspx?send=ok")
            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub
End Class