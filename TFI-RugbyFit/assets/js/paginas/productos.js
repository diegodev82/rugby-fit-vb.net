﻿$(document).ready(function () {
    $("body").on('click.AgregarParaComparar', '.js_add_comparar_prd', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var idPrd = $(this).data("idprd");
        if (idPrd * 1 > 0) {
            if (puedeAgregarParaComparar()) {
                if (!productoYaAgregado(idPrd)) {
                    agregarParaComparar(idPrd);
                } else {
                    alert("El producto ya está agregado para comparar");
                }
            } else {
                alert("Ya agregó demasiados productos para comparar");
            }
        }
    });
    $("body").on('click.QuitarDeComparar', '.js-quitar-prd-comparacion', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var idPrd = $(this).data("idprd");
        if (idPrd * 1 > 0) {
            quitarDeComparacion(idPrd);
        }
    });
    
    renderBarra();
})


function getArrayProductosAgregados() {
    var items = localStorage.getItem("comparar");
    if (items != null) {
        return items.split(",");
    }
    return [];
}

function goToComparacion() {
    var items = getArrayProductosAgregados();
    location.href = "Comparacion.aspx?ids=" + items.toString();
}

function mostrarBarraComparacion() {
    var items = getArrayProductosAgregados();
    if (items.length > 0) {
        $("#barra_comparacion").show();
    } else {
        $("#barra_comparacion").hide();
    }
}

function agregarParaComparar(idPrd) {
    var aComp = getArrayProductosAgregados();
    aComp.push(idPrd);
    localStorage.setItem("comparar", aComp.toString());
    renderBarra();
}

function agregarAlaBarra(idPrd) {
    var titulo = $("#nombre_" + idPrd).html();
    var imgSrc = $("img[data-imgprd=" + idPrd + "]").prop("src");
    var div = '<span class="contenedor_prd_comparar" id="cont_' + idPrd + '">'
    + '<img width="50" height="50" src="' + imgSrc + '" class="img-responsive" /><br/>'
    + '<span class="ttl_prd_comparar">' + titulo  + '</span>'
    + '<a href="javascript:void(0);" class="text-danger js-quitar-prd-comparacion btn_quitar_comparacion" data-idprd="' + idPrd + '"><i class="glyphicon glyphicon-remove"></i></a></span>';
    $("#div_prd_agregados").append(div);
}

function puedeAgregarParaComparar() {
    var items = getArrayProductosAgregados();
    return items.length < 3;
    // return items.length < 3;
}

function quitarDeComparacion(idPrd) {
    var items = getArrayProductosAgregados();
    for (var index = 0; index < items.length; index++) {
        if (items[index] == idPrd) {
            items.splice(index, 1);
        }
    }
    if (items.length > 0) {
        localStorage.setItem("comparar", items.toString());
    } else {
        localStorage.removeItem("comparar");
    }
    
    renderBarra();
}

function productoYaAgregado(idPrd) {
    var items = getArrayProductosAgregados();
    for (var index = 0; index < items.length; index++) {
        if (items[index] == idPrd) {
            return true;
        }
    }
    return false
}

function renderBarra() {
    $("#div_prd_agregados").html('');
    var items = getArrayProductosAgregados();
    for (var index = 0; index < items.length; index++) {
        if (items[index] != null) {
            agregarAlaBarra(items[index]);
        }
    }
    mostrarBarraComparacion();
}

function limpiarComparacion() {
    localStorage.removeItem("comparar");
    renderBarra();
}