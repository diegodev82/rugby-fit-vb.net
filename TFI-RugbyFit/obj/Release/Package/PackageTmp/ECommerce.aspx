﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ECommerce.aspx.vb" Inherits="TFI_RugbyFit.ECommerce" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/css/ecommerce.css" rel="stylesheet" />
    <script src="assets/js/paginas/ecommerce.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
        <% If Request.QueryString("guardado") = "ok" Then%>
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-thumbs-up"></span> Los datos se han actualizado correctamente.
        </div> 
        <% end if %>
        <% If Request.QueryString("eliminado") = "ok" Then%>
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-thumbs-up"></span> El producto fue eliminado correctamente.
        </div> 
        <% end if %>
        <% If Request.QueryString("guardado") = "error" or Request.QueryString("eliminado") = "error" Then%>
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-thumbs-up"></span> Disculpe, hubo un error en el sistema y no pudimos completar la acci&oacute;n. Int&eacute;ntelo nuevamente, por favor.
        </div> 
        <% end if %>
        <table id="cart" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50%">Producto</th>
                    <th style="width: 10%">Precio</th>
                    <th style="width: 8%">Cantidad</th>
                    <th style="width: 22%" class="text-center">Subtotal</th>
                    <th style="width: 10%"></th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rptItemsCarrito" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2 hidden-xs">
                                        <img src="<%# Container.DataItem.Producto.LinkImagen%>" width="100" height="100" alt="..." class="img-responsive" />
                                    </div>
                                    <div class="col-sm-10">
                                        <h4 class="nomargin"><%# Container.DataItem.Producto.Nombre %></h4>
                                        <p><%# Container.DataItem.Producto.DescripcionCorta %></p>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">$<%# FormatNumber(Container.DataItem.Monto,2, TriState.True) %></td>
                            <td data-th="Quantity">
                                <input type="number" min="1" max="10" id="cant_<%# Container.DataItem.IdDetalleVenta %>" class="form-control text-center js-solo-numeros" value="<%# Container.DataItem.Cantidad %>" />                             
                            </td>
                            <td data-th="Subtotal" class="text-center ">$ <span class="js-subtotal" data-value="<%# Container.DataItem.Monto * Container.DataItem.Cantidad %>"><%# FormatNumber(Container.DataItem.Monto * Container.DataItem.Cantidad, 2,TriState.True) %></span></td>
                            <td class="actions" data-th="">
                                <small class="glyphicon glyphicon-info-sign" data-toggle="tooltip" title="Presione el botón 'actualizar' para actualizar la cantidad"></small>
                                <button class="btn btn-default btn-sm js-editar-item" type="button" data-idvtadet="<%# Container.DataItem.IdDetalleVenta %>" title="Actualizar"><i class="glyphicon glyphicon-refresh text-info"></i></button>                                
                                <button class="btn btn-danger btn-sm js-eliminar-item" data-idvtadet="<%# Container.DataItem.IdDetalleVenta %>"><i class="glyphicon glyphicon-trash"></i></button>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

            </tbody>
            <tfoot>
                <tr class="visible-xs">
                    <td class="text-center"><strong>Total 1.99</strong></td>
                </tr>
                <tr>
                    <td><a href="Productos.aspx" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continuar comprando</a></td>
                    <td colspan="2" class="hidden-xs"></td>
                    <td class="hidden-xs text-center"><strong>Total $<span id="total"></span></strong></td>
                    <td><a href="Pagar.aspx" class="btn btn-success btn-block">Comprar <i class="fa fa-angle-right"></i></a></td>
                </tr>
            </tfoot>
        </table>
    
</asp:Content>
