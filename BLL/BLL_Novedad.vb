﻿Imports BE
Imports MPP
Public Class BLL_Novedad
    Private oMiMPP As New MPP_Novedades



    Public Function GetPorId(ByVal id As Integer) As BE_Novedad
        Return oMiMPP.GetPorId(id)
    End Function

    ''' <param name="objNovedad"></param>
    Public Function Crear(ByVal objNovedad As BE_Novedad) As BE_Novedad
        If oMiMPP.ValidarExistente(objNovedad) = True Then
            Return Nothing
        End If
        Return oMiMPP.Crear(objNovedad)
    End Function

    ''' 
    ''' <param name="objNovedad"></param>
    Public Function Editar(ByVal objNovedad As BE_Novedad) As Boolean
        If oMiMPP.ValidarExistente(objNovedad) = True Then
            Return Nothing
        End If
        Return oMiMPP.Editar(objNovedad)
    End Function

    ''' 
    ''' <param name="objNovedad"></param>
    Public Function Eliminar(ByVal objNovedad As BE_Novedad) As Integer
        Dim resultado As Integer = oMiMPP.Eliminar(objNovedad)
        Return resultado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Novedad)
        Return oMiMpp.Listar(filtros)
    End Function

    Public Function SuscribirNewsletter(ByVal email As String) As Integer
        If oMiMPP.ValidarSuscripto(email) = True Then
            Return 2
        End If
        If oMiMPP.SuscribirNewsletter(email) = True Then
            Return 1
        End If
        Return 0
    End Function

End Class
