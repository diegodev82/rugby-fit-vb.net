Imports MPP
Imports BE
Public Class BLL_Permisos


    Public oMiMPP As New MPP.MPP_Permisos

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Permiso)
        Return oMiMPP.Listar(filtros)
    End Function


End Class ' BLL_Permisos

