
Imports BE
Imports DAL

Public Class MPP_Bitacora


    Public Function ListarEventos(ByVal filtros As Hashtable) As List(Of BE_Evento)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Evento)
        DS = oDatos.Leer("pa_GetEventos", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim unEvt As New BE_Evento With {.IdEvento = dr("id_evento"), .Nombre = dr("nombre")}
                listado.Add(unEvt)
            Next
        End If
        Return listado
    End Function


    Public Function ListarBitacora(ByVal filtros As Hashtable) As List(Of BE_Bitacora)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Bitacora)
        DS = oDatos.Leer("pa_GetBitacora", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim dataUsuario As BE_Usuario = Nothing
                If Not IsDBNull(dr("id_usuario")) And IsNumeric(dr("id_usuario")) And dr("id_usuario") > 0 Then
                    dataUsuario = New BE_Usuario With {.IdUsuario = dr("id_usuario"), .Nombre = IIf(IsDBNull(dr("nombre_usuario")), "", dr("nombre_usuario")), .Apellido = IIf(IsDBNull(dr("apellido_usuario")), "", dr("apellido_usuario"))}
                End If
                Dim unaBit As New BE_Bitacora With {
                    .IdBitacora = dr("id_bitacora"),
                    .Evento = New BE_Evento With {.IdEvento = dr("id_evento"), .Nombre = IIf(IsDBNull(dr("evento")), "", dr("evento")), .Criticidad = dr("criticidad")},
                    .Usuario = dataUsuario,
                    .FechaHora = CType(dr("fecha_hora"), DateTime),
                    .Observacion = IIf(IsDBNull(dr("observacion")), "", dr("observacion")),
                    .Impacto = GetNombreCriticidad(dr("criticidad"))
                    }
                listado.Add(unaBit)
            Next
        End If
        Return listado
    End Function
    ''' 
    ''' <param name="objEvento"></param>
    Public Function RegistrarEvento(ByVal objBitacora As BE_Bitacora) As Boolean
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        hdatos.Add("@IdEvento", objBitacora.Evento.IdEvento)
        hdatos.Add("@IdUsuario", objBitacora.Usuario.IdUsuario)
        hdatos.Add("@Observacion", objBitacora.Observacion)
        Dim idInsertado As Integer = oDatos.Insertar("pa_RegistrarBitacora", hdatos)
        Return idInsertado > 0
    End Function

    Public Shared Function GetNombreCriticidad(IdCriticidad) As String
        If BE_Evento.NombresCriticidad.ContainsKey(IdCriticidad) Then
            Return BE_Evento.NombresCriticidad.Item(IdCriticidad)
        End If
        Return ""
    End Function


End Class ' MPP_Bitacora

