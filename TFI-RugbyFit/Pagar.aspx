﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Pagar.aspx.vb" Inherits="TFI_RugbyFit.Pagar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
    <link href="assets/css/pagar.css" rel="stylesheet" />
    <script src="assets/js/paginas/ValidadorTC.js"></script>
    <script src="assets/js/paginas/pagar.js"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="row">
        <div class="col-md-12">
            <a href="javascript:void(0);" onclick="$('#tc_helper').toggle('slow');">Ver TC</a>
            <table class="table" id="tc_helper" style="display: none;">
                <tr>
                    <th>Tipo</th>
                    <th>Numero</th>
                    <th>Codigo</th>
                    <th>Fecha</th>
                    <th>&nbsp;</th>
                </tr>
                <asp:Repeater ID="rptTC" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="js-tipo-usar"><%# Container.DataItem.Tipo.Nombre%></td>
                            <td class="js-nro-usar"><%# Container.DataItem.Numero%></td>
                            <td class="js-codigo-usar"><%# Container.DataItem.CodigoDesencriptado%></td>
                            <td class="js-fecha-usar"><%# Container.DataItem.MesAnioExpiracion%></td>
                            <td><a href="javascript:void(0);" class="js-usar">Usar</a></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>

    </div>
    <asp:PlaceHolder ID="phErrorValidacionPago" Visible="false" runat="server">
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <div class="alert alert-danger">
                <p>
                    <asp:Literal ID="ltlMsjErrorPago" runat="server"></asp:Literal></p>
            </div>
        </div>
        </div>
        </asp:PlaceHolder>
    <div class="row">

        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <!-- <a href="javascript:testAjax()">Test ajax</a> -->
            <asp:HiddenField ID="iptCodigoVta" runat="server" />
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading ">
                    <h3 class="panel-title ">Detalles del pago </h3>
                    <span class="pull-right">Total <strong id="total">$<span id="montoVta"><asp:Literal ID="ltlTotalVta" runat="server"></asp:Literal></span></strong></span>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="phSaldoFavor" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading ">
                                <h3 class="panel-title ">
                                    <asp:CheckBox ID="chkUsarNC" CssClass="js-chkUsarNC" runat="server" />
                                    Usar saldo a favor </h3>
                                <%--<span class="pull-right">Total <strong id="Strong1">$</strong></span>--%>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="help-block">Usted cuenta con un monto a favor de: </p>
                                        <p class="lead">
                                            <strong class="text-success">$ 
                                                <span id="saldoFavor">
                                                    <asp:Literal ID="ltlSaldoFavor" runat="server"></asp:Literal>
                                                </span>
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <div class="panel panel-info">
                        <div class="panel-heading ">
                            <h3 class="panel-title ">
                                <asp:CheckBox ID="chkPagarConTC" CssClass="js-chkPagarConTC" runat="server" />
                                Pagar con Tarjeta de Cr&eacute;dito</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="radio-inline">
                                        <label>
                                            <asp:RadioButton ID="visa" GroupName="tipo_tc" CssClass="tc-visa js-tipo-tc" runat="server" />
                                            <i class="fa fa-cc-visa fa-3x"></i>
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <asp:RadioButton ID="mastercard" GroupName="tipo_tc" CssClass="tc-mastercard js-tipo-tc" runat="server" />
                                            <i class="fa fa-cc-mastercard fa-3x"></i>
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <asp:RadioButton ID="amex" GroupName="tipo_tc" CssClass="tc-amex js-tipo-tc" runat="server" />
                                            <i class="fa fa-cc-amex fa-3x"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="cardNumber">N&Uacute;MERO</label>
                                        <div class="input-group">
                                            <asp:TextBox ID="iptNumeroTC" AutoPostBack="false" CssClass="js-numero-tc form-control" runat="server"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                        </div>
                                        <p class="text-danger js-error js-error-nro" style="display: none">N&uacute;mero inv&aacute;lido</p>
                                        <p class="text-danger" runat="server" id="error_nro" visible="false">N&uacute;mero inv&aacute;lido</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-7 col-md-7">
                                    <div class="form-group">
                                        <label for="cardExpiry">FECHA DE EXPIRACI&Oacute;N</label>
                                        <asp:TextBox ID="iptFechaExpiracion" placeholder="MMYYYY" CssClass="form-control js-fecha-expiracion" runat="server"></asp:TextBox>
                                        <p class="text-danger js-error js-error-fecha" style="display: none">Fecha Inv&aacute;lida</p>
                                        <p class="text-danger" runat="server" id="error_fecha" visible="false">Fecha Inv&aacute;lida</p>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-5 pull-right">
                                    <div class="form-group">
                                        <label for="cardCVC">C&Oacute;DIGO SEGURIDAD</label>
                                        <asp:TextBox ID="iptCodigoSeg" CssClass="form-control js-codigo-seguridad" placeholder="Codigo Seguridad" runat="server"></asp:TextBox>
                                        <p class="text-danger js-error js-error-codigo" style="display: none">C&oacute;digo Inv&aacute;lido</p>
                                        <p class="text-danger" runat="server" id="error_codigo" visible="false">C&oacute;digo Inv&aacute;lido</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:Button ID="btnPagar" OnClick="btnPagar_Click" CssClass="btn btn-success btn-lg btn-block js-btn-pagar" runat="server" Text="Pagar" />
                        </div>
                    </div>

                </div>
            </div>
            <!-- CREDIT CARD FORM ENDS HERE -->
        </div>
    </div>

</asp:Content>
