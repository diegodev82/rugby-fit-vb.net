﻿Imports BLL
Imports BE
Public Class AdminVentasList
    Inherits ACL
    Private oBll As New BLL_Ventas
    Const COD_PERMISO = "MOD_VENTAS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.ValidarAcceso(COD_PERMISO)
            If Not IsPostBack Then
                Me.LlenarDgv()
            End If
        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
        
    End Sub

    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Venta) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count

        DgvVentas.AutoGenerateColumns = False
        DgvVentas.DataSource = registros
        DgvVentas.DataBind()
    End Sub
    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroTitulo.Text IsNot Nothing Then
            filtros.Add("@PalabraClave", filtroTitulo.Text)
        End If

        If IsNumeric(filtroCodigo.Text) Then
            Dim idVenta As Integer = CType(filtroCodigo.Text, Integer)
            If idVenta > 0 Then
                filtros.Add("@IdVenta", idVenta)
            End If
        End If

        Return filtros
    End Function
    Protected Sub DgvVentas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvVentas.PageIndexChanging
        Try
            DgvVentas.PageIndex = e.NewPageIndex
            Dim filtros As Hashtable = GetFiltros()
            LlenarDgv(filtros)
            Dim a As String = ""
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub DgvVentas_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvVentas.PageIndexChanged
        Try
            Dim filtros As Hashtable = GetFiltros()
            LlenarDgv(filtros)
            Dim b As String = ""
        Catch ex As Exception

        End Try
        
    End Sub
    Protected Sub DgvVentas_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvVentas.RowEditing
        Try
            Dim row As GridViewRow = DgvVentas.Rows(e.NewEditIndex)
            Dim IdVenta As String = row.Cells(0).Text

            Response.Redirect("/AdminVentasForm.aspx?id=" + IdVenta)
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub DgvVentas_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvVentas.RowUpdating
        Try
            Dim a As Integer = 0
        Catch ex As Exception

        End Try


    End Sub

End Class