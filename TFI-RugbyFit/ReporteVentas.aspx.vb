﻿Imports BLL
Imports Microsoft.Reporting.WebForms

Public Class ReporteVentas
    Inherits ACL
    Const COD_PERMISO = "MOD_ADM_FZAS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.RefrescarReporte()

        End If
    End Sub

    Protected Function GetData() As DsFacturacion
        Dim oBll As New BLL_Reportes
        Dim miDs As New DsFacturacion
        Dim filtros As Hashtable = Me.GetFiltros
        Dim dt As DataTable = oBll.GetReporteFacturacion(filtros)
        Dim fDesde As String = ""
        Dim fHasta As String = ""
        If IsDate(filtroDesde.Text) Then
            fDesde = filtroDesde.Text
        End If
        If IsDate(filtroHasta.Text) Then
            fHasta = filtroHasta.Text
        End If
        For Each dr As DataRow In dt.Rows
            Dim nombreMes As String = GetNombreMes(dr("mes"))
            miDs.DataTableFact.AddDataTableFactRow(dr("anio"), nombreMes, fDesde, fHasta, dr("Monto"))
        Next

        Return miDs
        '
    End Function

    Protected Function GetNombreMes(ByVal nro As Integer) As String
        If nro = "0" Then
            Return "Todos"
        End If
        If nro = "1" Then
            Return "01-Enero"
        End If
        If nro = "2" Then
            Return "02-Febrero"
        End If
        If nro = "3" Then
            Return "03-Marzo"
        End If
        If nro = "4" Then
            Return "04-Abril"
        End If
        If nro = "5" Then
            Return "05-Mayo"
        End If
        If nro = "6" Then
            Return "06-Junio"
        End If
        If nro = "7" Then
            Return "07-Julio"
        End If
        If nro = "8" Then
            Return "08-Agosto"
        End If
        If nro = "9" Then
            Return "09-Septiembre"
        End If
        If nro = "10" Then
            Return "10-Octubre"
        End If
        If nro = "11" Then
            Return "11-Noviembre"
        End If
        If nro = "12" Then
            Return "12-Diciembre"
        End If
    End Function

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Me.RefrescarReporte()
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        
        If IsDate(filtroDesde.Text) And IsDate(filtroHasta.Text) Then
            Dim fDesde As Date = CType(filtroDesde.Text, Date)
            Dim fHasta As Date = CType(filtroHasta.Text, Date)

            filtros.Add("@FechaDesde", fDesde)
            filtros.Add("@FechaHasta", fHasta)
        Else
            If Not ddAnio.SelectedValue = "0" Then
                filtros.Add("@Anio", ddAnio.SelectedValue)
            End If
            If Not ddMes.SelectedValue = "0" Then
                filtros.Add("@Mes", ddMes.SelectedValue)
            End If
        End If

        Return filtros
    End Function

    Protected Sub RefrescarReporte()
        rvGanancias.ProcessingMode = ProcessingMode.Local
        rvGanancias.LocalReport.ReportPath = Server.MapPath("~/ReporteFacturacion.rdlc")
        Dim dsMio As DsFacturacion = Me.GetData()
        Dim datasource As New ReportDataSource("DsFact", dsMio.Tables(0))
        rvGanancias.LocalReport.DataSources.Clear()
        rvGanancias.LocalReport.DataSources.Add(datasource)
    End Sub
End Class