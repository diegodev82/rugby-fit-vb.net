﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Contacto.aspx.vb" Inherits="TFI_RugbyFit.Contacto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">Cont&aacute;ctenos</h1>
                <p class="help-block">Dej&eacute;nos su consulta y un representante estar&aacute; comunic&aacute;ndose con usted a la brevedad.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="well well-sm">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control" id="name" placeholder="Ingrese su nombre" required="required" />
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                        </span>
                                        <input type="email" class="form-control" id="email" placeholder="Ingrese su email" required="required" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subject">Asunto</label>
                                    <select id="subject" name="subject" class="form-control" required="required">
                                        <option value="na" selected="">Seleccione una:</option>
                                        <option value="service">Consulta sobre productos</option>
                                        <option value="suggestions">Sugerencias</option>
                                        <option value="product">Consulta administrativa</option>
                                        <option value="soporte">Soporte E-Commerce</option>
                                        <option value="otros">Otros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Mensaje</label>
                                    <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                        placeholder="Deje aqu&iacute; su mensaje"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                    Enviar Consulta</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <form>
                    <legend><span class="glyphicon glyphicon-globe"></span> Showroom</legend>
                    <address>
                        <strong>Rugby Fit.</strong><br>
                        Av. Cnel. Roca 5601<br>
                        Villa Lugano, C1439DVG CABA<br>
                        <abbr title="Tel&eacute;fono">
                            T:</abbr>
                        (+5411) 4123-4567
                    </address>
                    <address>
                        <strong>Email</strong><br>
                        <a href="mailto:#">contacto@rugbyfit.com.ar</a>
                    </address>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

