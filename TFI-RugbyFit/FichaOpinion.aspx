﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="FichaOpinion.aspx.vb" Inherits="TFI_RugbyFit.FichaOpinion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/paginas/ficha_opinion.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <asp:HiddenField ID="iptIdFc" runat="server" ClientIDMode="Static" />
    <h2>Ficha de Opini&oacute;n</h2>
    <p class="help-block">A continuaci&oacute;n le haremos algunas preguntas acerca del proceso de compras con el fin de poder mejorarlo para futuras operaciones.</p>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <asp:Repeater ID="rptFichas" runat="server" OnItemDataBound="rptFichas_ItemDataBound">
                <ItemTemplate>
                    <div class=" js-contenedor-ficha">
                        <div class="panel panel-default" id="panelVotacion">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                    <%# Container.DataItem.Pregunta %>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <asp:Repeater ID="rptRespuestas" runat="server">
                                        <ItemTemplate>
                                            <li class="list-group-item">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio"class="rta_encuesta" name="respuesta" data-id_encuesta="<%# Container.DataItem.IdEncuesta %>"" value="<%# Container.DataItem.IdRespuesta %>" /> <%# Container.DataItem.Respuesta %>
                                                    </label>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                            <div class="panel-footer text-right">
                                <button class="btn btn-primary btn-sm btn_votar" value="Votar">Votar</button>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>

            </asp:Repeater>

        </div>
    </div>
</asp:Content>
