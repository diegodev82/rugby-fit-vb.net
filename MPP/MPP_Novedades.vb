﻿Imports DAL
Imports BE
Public Class MPP_Novedades

    Public Function SuscribirNewsletter(ByVal email As String) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Email", email)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_SuscribirNewsletter", hdatos)
        Return InsertedId > 0
    End Function

    Public Function GetPorId(ByVal id As Integer) As BE_Novedad
        Dim hdatos As New Hashtable
        hdatos.Add("@IdNovedad", id)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetNovedadPorId", hdatos)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                Dim unaNove As New BE_Novedad
                unaNove.IdNovedad = item("id_novedad")
                unaNove.Titulo = item("titulo")
                unaNove.Copete = item("copete")
                unaNove.Cuerpo = item("cuerpo")
                unaNove.LinkImagen = IIf(IsDBNull(item("link_imagen")) = True, "", item("link_imagen"))
                unaNove.FechaHora = item("fecha_hora")
                Return unaNove
            End If
        End If
        Return Nothing
    End Function
    ''' 
    ''' <param name="objNovedad"></param>
    Public Function Crear(ByVal objNovedad As BE_Novedad) As BE_Novedad
        Dim hdatos As New Hashtable

        hdatos.Add("@Titulo", objNovedad.Titulo)
        hdatos.Add("@Copete", objNovedad.Copete)
        hdatos.Add("@Cuerpo", objNovedad.Cuerpo)
        hdatos.Add("@LinkImagen", objNovedad.LinkImagen)
        hdatos.Add("@FechaHora", objNovedad.FechaHora)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearNovedad", hdatos)
        If InsertedId > 0 = True Then
            objNovedad.IdNovedad = InsertedId
        End If
        Return objNovedad
    End Function

    ''' 
    ''' <param name="objNovedad"></param>
    Public Function Editar(ByVal objNovedad As BE_Novedad) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdNovedad", objNovedad.IdNovedad)
        hdatos.Add("@Titulo", objNovedad.Titulo)
        hdatos.Add("@Copete", objNovedad.Copete)
        hdatos.Add("@Cuerpo", objNovedad.Cuerpo)
        hdatos.Add("@LinkImagen", objNovedad.LinkImagen)
        hdatos.Add("@FechaHora", objNovedad.FechaHora)
        

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ActualizarNovedad", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objNovedad"></param>
    Public Function Eliminar(ByVal objNovedad As BE_Novedad) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        hdatos.Add("@IdNovedad", objNovedad.IdNovedad)
        Dim resultado As Object = oDatos.Eliminar("pa_EliminarNovedad", hdatos)
        Return resultado
    End Function

    ' <summary>Devuelve True si ya está suscripto, FALSE si no</summary>
    Public Function ValidarSuscripto(ByVal email As String) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Email", email)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarEmailSuscripto", hdatos)

        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0 
        End If
        Return False
    End Function

    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Novedad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Novedad)
        DS = oDatos.Leer("pa_GetNovedades", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim unaNov As New BE_Novedad
                unaNov.IdNovedad = dr("id_novedad")
                unaNov.Titulo = dr("titulo")
                unaNov.Copete = IIf(IsDBNull(dr("copete")), "", dr("copete"))
                unaNov.Cuerpo = dr("cuerpo")
                unaNov.LinkImagen = IIf(IsDBNull(dr("link_imagen")), "", dr("link_imagen"))
                unaNov.FechaHora = dr("fecha_hora")
                listado.Add(unaNov)
            Next
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objNovedad"></param>
    Public Function ValidarExistente(ByVal objNovedad As BE_Novedad) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdNovedad", objNovedad.IdNovedad)
        hdatos.Add("@Titulo", objNovedad.Titulo)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarNovedadExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function
End Class
