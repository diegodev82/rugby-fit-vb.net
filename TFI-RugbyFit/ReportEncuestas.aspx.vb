﻿Imports BLL
Imports BE
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Public Class ReportEncuestas
    Inherits ACL
    Const COD_PERMISO = "MOD_ADM_FZAS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        ltlTitulo.Text = IIf(ddTipo.SelectedValue = "C", "Encuestas", "Fichas de Opinión")
        If Not IsPostBack Then
            Me.RefrescarReporte()

        End If
    End Sub

    Protected Function GetData() As DsEncuestas
        Dim oBll As New BLL_Reportes
        Dim miDs As New DsEncuestas
        Dim filtros As Hashtable = Me.GetFiltros
        Dim dt As DataTable = oBll.GetReporteEncuestas(filtros)
        For Each dr As DataRow In dt.Rows
            miDs.DataTableEnc.AddDataTableEncRow(dr("Codigo"), dr("Pregunta"), dr("CantidadVotos"), dr("FechaDesde"), dr("FechaHasta"))
        Next

        Return miDs
        '
    End Function

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Me.RefrescarReporte()
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If IsDate(filtroDesde.Text) Then
            Dim fDesde As Date = CType(filtroDesde.Text, Date)
            filtros.Add("@FechaDesde", fDesde)
        End If
        If IsDate(filtroHasta.Text) Then
            Dim fHasta As Date = CType(filtroHasta.Text, Date)
            filtros.Add("@FechaHasta", fHasta)
        End If
        filtros.Add("@Tipo", ddTipo.SelectedValue)
        Return filtros
    End Function

    Protected Sub RefrescarReporte()
        rvEncuestas.ProcessingMode = ProcessingMode.Local
        rvEncuestas.LocalReport.ReportPath = Server.MapPath("~/ReportEncuesta.rdlc")
        Dim dsMio As DsEncuestas = Me.GetData()
        Dim datasource As New ReportDataSource("DsEncuestas", dsMio.Tables(0))
        rvEncuestas.LocalReport.DataSources.Clear()
        rvEncuestas.LocalReport.DataSources.Add(datasource)
    End Sub
End Class