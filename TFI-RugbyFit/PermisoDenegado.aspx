﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="PermisoDenegado.aspx.vb" Inherits="TFI_RugbyFit.PermisoDenegado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <div class="alert alert-danger">
        Usted no tiene permiso para acceder a este m&oacute;dulo.
    </div>
</asp:Content>
