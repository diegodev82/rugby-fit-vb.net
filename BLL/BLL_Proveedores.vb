Imports BE
Imports MPP

Public Class BLL_Proveedores


    Public oMpp As New MPP.MPP_Proveedores

    ''' 
    ''' <param name="objProv"></param>
    Public Function Crear(ByVal objProv As BE_Proveedor) As BE.BE_Proveedor
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objProv"></param>
    Public Function Editar(ByVal objProv As BE_Proveedor) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objProv"></param>
    Public Function Eliminar(ByVal objProv As BE_Proveedor) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Proveedor)
        Return oMpp.Listar(filtros)
    End Function


End Class ' BLL_Proveedores
