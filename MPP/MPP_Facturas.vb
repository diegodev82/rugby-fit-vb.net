Imports BE
Imports DAL

Public Class MPP_Facturas
    Private oDal As SQLHelper = SQLHelper.GetInstance

    Public Function GetPagosPorCliente(ByVal idCliente As Integer) As List(Of BE_Pago)
        Dim listado As New List(Of BE_Pago)
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", idCliente)

        Dim ds As DataSet = oDal.Leer("pa_GetPagosPorCliente", hdatos)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each item As DataRow In ds.Tables(0).Rows
                Dim pago As New BE_Pago
                pago.IdPago = item("id_pago")
                pago.Monto = item("monto")
                pago.IdCliente = item("id_cliente")
                pago.Fecha = item("fecha")
                pago.TipoPago = item("tipo_pago")
                pago.TipoMovimiento = item("tipo_movimiento")
                pago.IdFacturaCancela = item("id_factura_cancela")
                listado.Add(pago)
            Next
            Return listado
        End If
        Return listado
    End Function

    Public Function AgregarPago(ByVal pago As BE_Pago) As BE_Pago
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFacturaCancela", pago.IdFacturaCancela)
        hdatos.Add("@IdFacturaUsa", pago.IdFacturaUsa)
        hdatos.Add("@IdCliente", pago.IdCliente)
        hdatos.Add("@Monto", pago.Monto)
        hdatos.Add("@Fecha", Date.Now)
        hdatos.Add("@TipoPago", pago.TipoPago)
        hdatos.Add("@TipoMov", pago.TipoMovimiento)


        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_AgregarPago", hdatos)
        If InsertedId > 0 = True Then
            pago.IdPago = InsertedId
        End If
        Return pago
    End Function

    Public Function GetPorId(ByVal idFactura As Integer) As BE_Factura
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFactura", idFactura)

        Dim ds As DataSet = oDal.Leer("pa_GetFacturaPorId", hdatos)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim item As DataRow = ds.Tables(0).Rows(0)
            Dim fc As New BE_Factura
            fc.IdFactura = item("id_factura")
            fc.Estado = item("estado")
            fc.IdFacturaRel = item("id_factura_rel")
            fc.FormaPago = item("id_forma_pago")
            fc.Fecha = item("fecha")
            fc.Tipo = item("tipo")
            fc.IdVenta = item("id_venta")
            fc.Domicilio = item("domicilio")
            fc.Monto = IIf(IsDBNull(item("monto_total")), 0, item("monto_total"))
            fc.Ciudad = New BE_Ciudad With {
                .IdCiudad = IIf(IsDBNull(item("id_ciudad")) = True, 0, item("id_ciudad")),
                .Nombre = IIf(IsDBNull(item("ciu_nombre")) = True, "", item("ciu_nombre")),
                .Provincia = New BE_Provincia With {
                    .IdProvincia = IIf(IsDBNull(item("ciu_id_provincia")), 0, item("ciu_id_provincia")),
                    .Nombre = IIf(IsDBNull(item("pro_nombre")), "", item("pro_nombre"))
                    }
            }
            fc.Cliente = New BE_Cliente With {
                .IdCliente = item("id_cliente"),
                .Nombre = item("cli_nombre"),
                .Apellido = item("cli_apellido"),
                .DNI = item("cli_dni"),
                .CUIT = item("cli_cuit"),
                .Email = item("cli_email"),
                .Domicilio = item("cli_domicilio")
            }
            fc.Items = Me.GetItems(fc)
            Return fc
        End If
        Return Nothing
    End Function

    ''' 
    ''' <param name="objDetFc"></param>
    Public Function AgregarItem(ByVal objDetFc As BE_Detalle_Factura) As BE.BE_Detalle_Factura
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFactura", objDetFc.IdFactura)
        hdatos.Add("@IdProducto", objDetFc.Producto.IdProducto)
        hdatos.Add("@Cantidad", objDetFc.Cantidad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_AgregarFacturaDetalle", hdatos)
        If InsertedId > 0 = True Then
            objDetFc.IdDetalleFactura = InsertedId
        End If
        Return objDetFc
    End Function

    ''' 
    ''' <param name="facturas"></param>
    Public Function Anular(ByVal factura As BE_Factura) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFactura", factura.IdFactura)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim anulada As Boolean = oDatos.Escribir("pa_AnularFactura", hdatos)
        Return anulada
    End Function

    Public Function CrearNc(ByVal nc As BE_Factura) As BE_Factura
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFacRel", nc.IdFacturaRel)
        hdatos.Add("@IdVenta", nc.IdVenta)
        hdatos.Add("@IdCliente", nc.Cliente.IdCliente)
        hdatos.Add("@IdCiudad", nc.Ciudad.IdCiudad)
        hdatos.Add("@Domicilio", nc.Domicilio)
        hdatos.Add("@Estado", nc.Estado)
        hdatos.Add("@Tipo", nc.Tipo)
        hdatos.Add("@IdFormaPago", nc.FormaPago)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearFactura", hdatos)
        If InsertedId > 0 = True Then
            nc.IdFactura = InsertedId
        End If
        Return nc
    End Function

    ''' 
    ''' <param name="objFactura"></param>
    Public Function Crear(ByVal objFactura As BE_Factura) As BE.BE_Factura
        If ValidarFacturaExistente(objFactura.IdVenta) = True Then
            objFactura.IdFactura = 0
            Return objFactura
        End If
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFacRel", objFactura.IdFacturaRel)
        hdatos.Add("@IdVenta", objFactura.IdVenta)
        hdatos.Add("@IdCliente", objFactura.Cliente.IdCliente)
        hdatos.Add("@IdCiudad", objFactura.Ciudad.IdCiudad)
        hdatos.Add("@Domicilio", objFactura.Domicilio)
        hdatos.Add("@Estado", objFactura.Estado)
        hdatos.Add("@Tipo", objFactura.Tipo)
        hdatos.Add("@IdFormaPago", objFactura.FormaPago)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearFactura", hdatos)
        If InsertedId > 0 = True Then
            objFactura.IdFactura = InsertedId
        End If
        Return objFactura
    End Function

    ''' 
    ''' <param name="objDetFc"></param>
    Public Function EliminarItem(ByVal objDetFc As BE_Detalle_Factura) As Boolean
        EliminarItem = False
    End Function

    ''' 
    ''' <param name="objNC"></param>
    Public Function GenerarNotaCredito(ByVal objNC As BE_Factura) As BE.BE_Factura
        GenerarNotaCredito = Nothing
    End Function

    ''' 
    ''' <param name="objFactura"></param>
    Public Function GetItems(ByVal objFactura As BE_Factura) As List(Of BE_Detalle_Factura)
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFactura", objFactura.IdFactura)
        Dim listado As New List(Of BE_Detalle_Factura)
        Dim ds As DataSet = oDal.Leer("pa_GetFacturaDetalles", hdatos)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each item As DataRow In ds.Tables(0).Rows
                Dim dfc As New BE_Detalle_Factura
                dfc.IdDetalleFactura = item("id_factura_detalle")
                dfc.IdFactura = item("id_factura")
                dfc.Monto = item("precio_unitario")
                dfc.Producto = New BE_Producto With {.IdProducto = item("id_producto"), .Nombre = item("prd_nombre")}
                dfc.Cantidad = item("cantidad")
                listado.Add(dfc)
            Next
            Return listado
        End If
        Return New List(Of BE_Detalle_Factura)
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarFacturas(ByVal filtros As Hashtable) As List(Of BE_Factura)
        ListarFacturas = Nothing
    End Function

    Public Function ValidarFacturaExistente(ByVal IdVenta As Integer)
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVenta", IdVenta)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarFacturaExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function

End Class ' MPP_Facturas

