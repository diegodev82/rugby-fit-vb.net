﻿Imports MPP
Imports BE
Public Class BLL_Chat
    Private oMiMpp As New MPP_Chat

    Public Function AgregarMensaje(ByVal msj As BE_MensajeChat) As Boolean
        If msj.IdChat = 0 Then
            Dim nuevoChat As New BE_Chat
            nuevoChat.Emisor = New BE_Usuario With {.IdUsuario = msj.Emisor.IdUsuario}
            Dim idChatcreado As Integer = oMiMpp.IniciarChat(nuevoChat)
            If idChatcreado <= 0 Then
                Return Nothing
            End If
            msj.IdChat = idChatcreado
        End If
        Dim IdMensaje As Integer = oMiMpp.AgregarMensaje(msj)
        Return IdMensaje > 0
        'Return False
    End Function

    Public Function GetChat(ByVal IdUsuario As Integer) As BE_Chat
        Dim chat As BE_Chat = oMiMpp.GetChat(IdUsuario)
        Return chat
    End Function

    Public Function GetChats() As List(Of BE_Chat)
        Dim chats As List(Of BE_Chat) = oMiMpp.GetChats()
        Return chats
    End Function

End Class
