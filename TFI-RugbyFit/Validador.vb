﻿Public Class Validador
    Public Shared Function EmailValido(ByVal email As String) As Boolean
        Try
            Dim a As New System.Net.Mail.MailAddress(email)
        Catch
            Return False
        End Try
        Return True
    End Function

    Public Shared Function EstaVacio(ByVal value As String) As Boolean
        Return String.IsNullOrEmpty(value)
    End Function
    Public Shared Function SuperaMaxLength(ByVal value As String, Optional ByVal length As Integer = 50) As Boolean
        If value.Length > length Then
            Return True
        End If
        Return False
    End Function
    Public Shared Function EstaVacio_MaxLength(ByVal value As String, Optional ByVal length As Integer = 50) As Boolean
        If Validador.EstaVacio(value) = True Or SuperaMaxLength(value, length) = True Then
            Return True
        End If
        Return False
    End Function
End Class
