Imports BE
Imports DAL

Public Class MPP_Familias



    Public Function GetPorId(ByVal id As Integer) As BE_Familia
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFamilia", id)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetFamiliaPorId", hdatos)
        Dim familia As BE_Familia = Nothing
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                familia = New BE_Familia With {.IdFamilia = item("id_familia"), .Nombre = item("nombre")}
                familia.Permisos = _GetPermisosFamilia(id)
            End If
        End If
        Return familia
    End Function

    Private Function _GetPermisosFamilia(ByVal idFamilia As Integer) As List(Of BE_Permiso)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFamilia", idFamilia)

        ds = oDatos.Leer("pa_GetPermisosDeFamilia", hdatos)
        Dim listadoPerm As New List(Of BE_Permiso)
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim permiso As New BE.BE_Permiso
            permiso.IdPermiso = dr.Item("id_permiso")
            permiso.Descripcion = dr.Item("descripcion")
            permiso.Tipo = 1 'siempre son permitidos, para denegar el permiso directamente se quita o no se agrega
            permiso.Codigo = dr.Item("codigo")
            listadoPerm.Add(permiso)
        Next
        Return listadoPerm
    End Function
    ''' 
    
    Public Function AgregarPermiso(ByVal objPerm As BE_Permiso, ByVal IdFamilia As Integer) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@IdFamilia", IdFamilia)
        hdatos.Add("@IdPermiso", objPerm.IdPermiso)


        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim IdInsertado As Object = oDatos.Insertar("pa_AgregarFamiliaPermiso", hdatos)
        If IsNumeric(IdInsertado) = True Then
            Return IdInsertado
        End If
        Return 0
    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function Crear(ByVal objFam As BE_Familia) As Object
        Dim hdatos As New Hashtable

        hdatos.Add("@Nombre", objFam.Nombre)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim IdInsertado As Object = oDatos.Insertar("pa_CrearFamilia", hdatos)
        If IsNumeric(IdInsertado) = True Then
            Return IdInsertado
        End If
        Return 0
    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function Editar(ByVal objFam As BE_Familia) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdFamilia", objFam.IdFamilia)
        hdatos.Add("@Nombre", objFam.Nombre)
        
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ModificarFamilia", hdatos)
        Return guardado

    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function Eliminar(ByVal objFam As BE_Familia) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFamilia", objFam.IdFamilia)
        Dim resultado As Object = oDatos.Eliminar("pa_EliminarFamilia", hdatos)
        Return resultado
    End Function

    
    Public Function QuitarPermiso(ByVal IdFamilia As Integer, ByVal IdPermiso As Integer) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdFamilia", IdFamilia)
        hdatos.Add("@IdPermiso", IdPermiso)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_QuitarFamiliaPermiso", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Familia)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Familia)
        DS = oDatos.Leer("pa_GetFamilias", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim unaFam As New BE_Familia With {.IdFamilia = dr("id_familia"), .Nombre = dr("nombre")}
                listado.Add(unaFam)
            Next
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function ValidarExistente(ByVal objFam As BE_Familia) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Nombre", objFam.Nombre)
        hdatos.Add("@IdFamilia", objFam.IdFamilia)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarFamiliaExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function


End Class ' MPP_Familias

