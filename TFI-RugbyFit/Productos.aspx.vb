﻿Imports BLL
Imports BE

Public Class Productos
    Inherits System.Web.UI.Page
    Private oBll As New BLL_Productos
    Private oBllCat As New BLL_Categorias
    Private categorias As New List(Of BE_Categoria)
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        


        If Not IsPostBack Then
            Me.CargarProductos()
            If Not Request.QueryString("idCat") Is Nothing Then
                Me.FiltrarPorCategoria()
            End If

            Me.categorias = oBllCat.Listar()
            Me.CargaCategoriasSideBar()
            Me.CargarDDCategorias()
        End If

    End Sub


    Public Sub CargarProductos()
        prdRepeater.DataSource = Nothing
        prdRepeater.DataBind()
        Dim filtros As Hashtable = Me.GetFiltros()
        Dim productos As List(Of BE_Producto) = oBll.Listar(filtros)
        MsjNoRtdos.Visible = False
        If productos.Count = 0 Then
            MsjNoRtdos.Visible = True
        Else
            prdRepeater.DataSource = productos
            prdRepeater.ItemType = "BE_Producto"
            prdRepeater.DataBind()
        End If
        
    End Sub

    Public Sub CargarDDCategorias()
        ddFiltroCategoria.DataMember = "BE_Categoria"
        ddFiltroCategoria.DataValueField = "IdCategoria"
        ddFiltroCategoria.DataTextField = "Nombre"
        ddFiltroCategoria.DataSource = Me.categorias
        ddFiltroCategoria.DataBind()
    End Sub

    Public Sub CargaCategoriasSideBar()
        rptCategorias.DataMember = "BE_Categoria"
        rptCategorias.DataSource = Me.categorias
        rptCategorias.DataBind()
    End Sub

    'Protected Sub facetedCategorias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles facetedCategorias.SelectedIndexChanged
    '    ' Dim a As Object = facetedCategorias.SelectedItem.Value
    ''    Dim b As String = ""
    'End Sub

    Protected Function GetIdsCategoriasSeleccionadas(sender As Object, e As EventArgs) As Collection
        Dim Ids As New Collection
        'For Each oItem As ListItem In facetedCategorias.Items
        '    If oItem.Selected Then
        '        Ids.Add(oItem.Value)
        '    End If
        'Next
        Return Ids
    End Function

    'Protected Sub prdRepeater_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles prdRepeater.ItemCommand
    '    Dim idProducto As Integer = CType(e.CommandArgument, Integer)
    '    If PuedeAgregarParaComparar() Then
    '        If YaAgregadoParaComparar(idProducto) = False Then
    '            If AgregarParaComparar(idProducto) Then
    '                'Mostar en barra de comparación
    '            End If
    '        End If
    '    End If
    'End Sub

    'Private Sub MostrarBarraComparacion()

    'End Sub


    'Private Function PuedeAgregarParaComparar() As Boolean

    'End Function

    'Private Function AgregarParaComparar(ByVal idPrd As Integer) As Boolean

    'End Function

    'Private Function EliminarDeComparacion(ByVal idPrd As Integer) As Boolean

    'End Function

    'Private Function YaAgregadoParaComparar(ByVal idPrd As Integer) As Boolean

    'End Function

    Protected Sub FiltrarPorCategoria()
        Dim filtros As New Hashtable
        filtros.Add("@Activo", 1)
        If Not Request.QueryString("idCat") Is Nothing And IsNumeric(Request.QueryString("idCat")) Then
            Dim idCat As Integer = CType(Request.QueryString("idCat"), Integer)
            filtros.Add("@IdCategoria", idCat)
            ddFiltroCategoria.SelectedValue = idCat
        End If

        
        prdRepeater.DataSource = Nothing
        prdRepeater.DataBind()
        Dim productos As List(Of BE_Producto) = oBll.Listar(filtros)
        prdRepeater.DataSource = productos
        prdRepeater.ItemType = "BE_Producto"
        prdRepeater.DataBind()

    End Sub

    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        
    End Sub

    Public Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        filtros.Add("@Activo", 1)
        If filtroNombre.Text IsNot Nothing Then
            filtros.Add("@NombreProducto", filtroNombre.Text)
        End If
        Dim idCat As Integer = CType(ddFiltroCategoria.SelectedValue, Integer)
        If idCat > 0 Then

            filtros.Add("@IdCategoria", idCat)
        End If
        If IsNumeric(filtroCodigo.Text) Then
            Dim idProducto As Integer = CType(filtroCodigo.Text, Integer)
            If idProducto > 0 Then
                filtros.Add("@IdProducto", idProducto)
            End If
        End If

        Return filtros
    End Function

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs)
        Me.CargarProductos()
    End Sub

    
    Protected Sub btnBuscador_Click(sender As Object, e As EventArgs)
        Me.CargarProductos()
    End Sub
End Class