Imports BE
Imports MPP

Public Class BLL_Publicidades


    Public m_MPP_Publicidades As MPP_Publicidades

    ''' 
    ''' <param name="objPubl"></param>
    Public Function Crear(ByVal objPubl As BE_Publicidad) As BE_Publicidad
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objPubl"></param>
    Public Function Editar(ByVal objPubl As BE_Publicidad) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objPubl"></param>
    Public Function Eliminar(ByVal objPubl As BE_Publicidad) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Publicidad)
        Listar = Nothing
    End Function


End Class ' BLL_Publicidades

