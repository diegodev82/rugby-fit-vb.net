﻿Imports BLL
Imports BE
Public Class AdminMiPerfil
    Inherits ACL
    Const COD_PERMISO = "MOD_PERFIL"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAcceso()

        If Not IsPostBack Then
            Try
                Me._CargarDatosUsuario()
            Catch ex As Exception
                TryCast(Me.Master, Backend).MostrarErrorSistema()
            End Try

            'Me.CargarProvincias()
        End If
    End Sub
    'Protected Sub CargarCiudades(idProvincia)
    '    Dim oBllCiudad As New BLL_Ciudades
    '    Dim hDatos As New Hashtable()
    '    hDatos.Add("@IdProvincia", idProvincia)
    '    Dim ciudades As List(Of BE_Ciudad) = oBllCiudad.Listar(hDatos)

    '    ddCiudad.DataTextField = "Nombre"
    '    ddCiudad.DataValueField = "IdCiudad"
    '    ddCiudad.DataMember = "BE_Ciudad"
    '    ddCiudad.DataSource = ciudades
    '    ddCiudad.DataBind()
    'End Sub

    'Protected Sub CargarProvincias()
    '    Dim oBLLProv As New BLL_Provincias()
    '    ddProvincia.DataTextField = "nombre"
    '    ddProvincia.DataValueField = "id_provincia"
    '    ddProvincia.DataSource = oBLLProv.Listar()
    '    ddProvincia.DataBind()
    'End Sub

    Protected Sub _CargarDatosUsuario()
        Dim oBllUsuario As New BLL_Usuarios
        Dim id As Integer = MyBase.GetIdUsuario()
        Dim usuario As BE_Usuario = oBllUsuario.GetPorId(id)
        If usuario Is Nothing Then
            Response.Write("Error! No hay datos del usuario")
        Else
            'If usuario.Ciudad.Provincia.IdProvincia > 0 Then
            '    CargarCiudades(usuario.Ciudad.Provincia.IdProvincia)
            'End If
            iptIdUsuario.Value = usuario.IdUsuario
            iptApellido.Text = usuario.Apellido
            iptNickname.Text = usuario.Nickname
            iptNombre.Text = usuario.Nombre
            iptEmail.Text = usuario.Email
            iptTelefono.Text = usuario.Telefono
            iptDNI.Text = usuario.DNI

            'iptDomicilio.Text = usuario.Domicilio
            'ddCiudad.SelectedValue = usuario.Ciudad.IdCiudad
            'ddProvincia.SelectedValue = usuario.Ciudad.Provincia.IdProvincia
        End If
    End Sub

    Protected Sub btnModificarDatos_Click(sender As Object, e As EventArgs) Handles btnModificarDatos.Click
        Try

        
        Dim oBll As New BLL_Usuarios
        Dim oBllSeg As New BLL_Seguridad
        Dim oBEUsuario As New BE_Usuario
        'Dim oCiudad As New BE_Ciudad With {.IdCiudad = 0}
        'Dim idCiudadRequest As String = Request(ddCiudad.UniqueID)
        'If Not idCiudadRequest Is Nothing Then
        '    oCiudad.IdCiudad = idCiudadRequest
        'End If
      
            oBEUsuario.IdUsuario = Me.GetIdUsuario
        oBEUsuario.Nombre = iptNombre.Text
        oBEUsuario.Apellido = iptApellido.Text
            oBEUsuario.DNI = IIf(IsNumeric(iptDNI.Text) = False, 0, iptDNI.Text)
        'oBEUsuario.Domicilio = iptDomicilio.Text
        oBEUsuario.Email = iptEmail.Text
            oBEUsuario.Telefono = iptTelefono.Text
            oBEUsuario.Activo = True
        'oBEUsuario.Ciudad = oCiudad

        Dim guardado As Boolean = oBll.Editar(oBEUsuario)
        If guardado = False Then
            MensajeRespuesta.Text = "Se produjo un error desconocido y la registación no pudo realizarse. Inténtelo nuevamente o pongase en <a href='Contacto.aspx'>contacto con nosotros</a>"
            PanelMensajeRespuesta.Visible = True
        Else
            Response.Redirect("AdminMiPerfil.aspx?guardado=ok")
            End If
        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
    End Sub
End Class