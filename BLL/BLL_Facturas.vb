Imports MPP
Imports BE
Public Class BLL_Facturas

    Private oBllBit As New BLL_Bitacora
    Private oMiMpp As New MPP_Facturas
    Public Const RUTA_FACTURAS_GENERADAS = "/assets/fc"

    Public Function GetPagosPorCliente(ByVal idCliente As Integer) As List(Of BE_Pago)
        Return oMiMpp.GetPagosPorCliente(idCliente)
    End Function

    Public Function AgregarPago(ByVal pago As BE_Pago) As Boolean
        Dim pagoAgregado As BE_Pago = oMiMpp.AgregarPago(pago)
        If pagoAgregado.IdPago > 0 Then
            Return True
        End If
        Return False
    End Function


    Public Function GetPorId(ByVal idFactura As Integer) As BE_Factura
        Dim fc As BE_Factura = oMiMpp.GetPorId(idFactura)
        Return fc
    End Function


    ''' <param name="facturas"></param>
    Public Function Anular(ByVal facturas As List(Of BE_Factura)) As Boolean
        Dim bllCli As New BLL_Clientes
        Dim oMppCliente As New MPP_Clientes
        For Each fc As BE_Factura In facturas
            If fc.Estado = BE_Factura.ESTADO_PAGADA Then
                Dim anulada As Boolean = oMiMpp.Anular(fc)
                If anulada = True Then
                    Dim nc As BE_Factura = New BE_Factura
                    nc.Cliente = New BE_Cliente
                    'nc = fc
                    nc.IdFactura = 0
                    nc.IdFacturaRel = fc.IdFactura
                    nc.Cliente = fc.Cliente
                    nc.Domicilio = fc.Domicilio
                    nc.Ciudad = fc.Ciudad
                    nc.Fecha = Date.Now
                    nc.FormaPago = 1
                    nc.IdVenta = fc.IdVenta
                    nc.Monto = fc.Monto
                    nc.Tipo = BE_Factura.TIPO_NC
                    nc.Estado = BE_Factura.ESTADO_NC_SIN_USAR

                    Dim ncCreada As BE_Factura = oMiMpp.CrearNc(nc)
                    If ncCreada.IdFactura > 0 Then
                        oMppCliente.IncrementarSaldoFavor(fc.Cliente.IdCliente, fc.Monto)

                        Dim pago As New BE_Pago
                        pago.IdCliente = fc.Cliente.IdCliente
                        pago.IdFacturaCancela = fc.IdFactura
                        pago.EsParcial = False
                        pago.Monto = fc.Monto
                        pago.TipoPago = BE_Pago.PAGO_CON_NC
                        pago.TipoMovimiento = BE_Pago.ENTRADA
                        Me.AgregarPago(pago)
                        Return True
                    End If

                End If
            End If
        Next
        Return False
    End Function

    Private Function CalcularTotal(ByVal items As List(Of BE_Detalle_Factura)) As Double
        Dim total As Double = 0
        For Each item As BE_Detalle_Factura In items
            total = total + (item.Cantidad * item.Monto)
        Next
        Return total
    End Function

    Private Function EnviarEmail() As Boolean
        EnviarEmail = False
    End Function

    ''' 
    ''' <param name="ventas"></param>
    Public Function Facturar(ByVal ventas As List(Of BE_Venta)) As Integer
        Dim bllUsu As New BLL_Usuarios
        Dim bllCli As New BLL_Clientes
        Dim oMppVta As New MPP_Ventas
        Dim oMppPrd As New MPP_Productos
        Dim oMppCliente As New MPP_Clientes
        For Each vta As BE_Venta In ventas
            Dim cliente As BE_Cliente = bllCli.GetPorId(vta.IdCliente)
            Dim fc As New BE_Factura
            If vta.Origen = "W" Then
                fc.Estado = BE_Factura.ESTADO_PAGADA
                fc.FormaPago = 1
                fc.IdVenta = vta.IdVenta
                fc.Cliente = cliente
                fc.Domicilio = cliente.Domicilio
                fc.Ciudad = cliente.Ciudad
                fc.IdFacturaRel = 0
                fc.Tipo = BE_Factura.TIPO_FC
                fc.Items = New List(Of BE_Detalle_Factura)
                Dim fcCreada As BE_Factura = oMiMpp.Crear(fc)
                If fcCreada.IdFactura > 0 Then
                    Dim evento As New BE_Evento
                    Dim oBllBit As New BLL_Bitacora
                    evento.IdEvento = BE_Evento.CREAR_FACTURA
                    Dim observacion As String = "Factura creada. # Vta: " + vta.IdVenta.ToString
                    oBllBit.RegistrarEvento(evento, observacion)
                    Dim detalles As List(Of BE_Detalle_Venta) = oMppVta.GetDetalleVenta(vta)
                    Dim cantDetalleVtas As Integer = detalles.Count
                    Dim contador As Integer = 0
                    For Each item As BE_Detalle_Venta In detalles
                        Dim stock As Integer = oMppPrd.ValidarStock(item.Producto)
                        If stock > item.Cantidad Then
                            Dim dfc As BE_Detalle_Factura = New BE_Detalle_Factura
                            dfc.Cantidad = item.Cantidad
                            dfc.IdFactura = fc.IdFactura
                            dfc.Monto = item.Monto
                            dfc.Producto = item.Producto
                            Dim dfcAgregado As BE_Detalle_Factura = oMiMpp.AgregarItem(dfc)
                            If dfcAgregado.IdDetalleFactura > 0 Then
                                fc.Items.Add(dfcAgregado)
                                oMppPrd.DescontarStock(dfc.Cantidad, dfc.Producto)
                                Dim puntosOtorgados As Integer = oMppPrd.PuntosOtorgados(dfc.Producto)
                                If puntosOtorgados > 0 Then
                                    oMppCliente.IncremetarPuntosBeneficio(vta.IdCliente, puntosOtorgados)
                                    Dim objPtoCliente As BE_Punto_Cliente = New BE_Punto_Cliente
                                    objPtoCliente.Cantidad = puntosOtorgados * dfc.Cantidad
                                    objPtoCliente.Cliente = New BE_Cliente With {.IdCliente = vta.IdCliente}
                                    objPtoCliente.Factura = fc
                                    objPtoCliente.Producto = item.Producto
                                    oMppCliente.RegistrarAcumulacionPuntos(objPtoCliente)
                                End If
                            End If
                            '@Todo ac� habr�a que agregar un else para hacer alguna acci�n para avisar que no hab�a stock
                        End If
                    Next
                    Dim total As Double = Me.CalcularTotal(fc.Items)
                    fc.Monto = total

                    Dim generado As Boolean = Me.GenerarPDF(fc)
                    Return fc.IdFactura
                End If
            End If
        Next


    End Function

    Public Function GenerarPDF(ByVal fc As BE_Factura) As Boolean
        Dim ruta As String = RUTA_FACTURAS_GENERADAS + "/" + fc.Cliente.IdCliente.ToString

    End Function


    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarFacturas(ByVal filtros As Hashtable) As List(Of BE_Factura)
        ListarFacturas = Nothing
    End Function

    Private Sub RegistrarFaltaStock()

    End Sub


End Class ' BLL_Facturas
