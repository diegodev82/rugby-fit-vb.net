USE [master]
GO

/****** Object:  StoredProcedure [dbo].[pa_BackupRugbyFit]    Script Date: 24/10/2016 02:35:31 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_BackupRugbyFit]
	@RutaGenerada nvarchar(400)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BACKUP DATABASE RugbyFit TO DISK = @RutaGenerada WITH FORMAT;
END



GO

