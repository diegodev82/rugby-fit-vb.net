﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Mision.aspx.vb" Inherits="TFI_RugbyFit.Mision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="container">
        <div class="col-md-6">
            <h2>Nuestra Misi&oacute;n</h2>
            <div>
                <p>
                    Somos Rugby Fit: una empresa joven en pleno crecimiento que traslada los valores del rugby a la organización. 
                </p>
                <p>
                    Queremos ser parte del equipo que empuje al rugby argentino al mejor nivel. Junto con los clubes de Buenos Aires y con la Unión de Rugby Argentina, conformar una unidad de excelencia con el sólo fin de mejorar la disciplina. 
                </p>
                <p>
                    Nos dedicaremos a la comercialización y venta de equipamiento de entrenamiento de rugby. Nuestra intención es formar parte del equipo de nuestros clientes y juntos perfeccionar los entrenamientos y alcanzar mejores logros.
                </p>
                <p>
                    Solidaridad, respeto y compromiso son los valores que el deporte inyectó en nuestros mejores jugadores -  los empleados -  y que trasladamos a nuestros clientes, al medio ambiente y al deporte.
                </p>
                <p>
                    Con la fuerza de un scrum empujaremos para equiparar la desigualdad social, brindando colaboración para que el rugby social continúe creciendo, integrando y conteniendo a los más necesitados, para que desarrollen un sentimiento de pertenencia e inclusión social. 
                </p>
            </div>
            <h2>Nuestra Visi&oacute;n</h2>
            <div>
                <p>
                    Ser la empresa líder en Argentina en comercialización y distribución de equipamiento de entrenamiento de rugby, identificados como la empresa más innovadora y vanguardista del mercado.
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <img src="assets/img/Logo-RugbyFit.png" class="img-responsive center-block" alt="Rugby Fit" />
        </div>
    </div>
</asp:Content>
