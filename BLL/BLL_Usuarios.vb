Imports BE
Imports MPP


Public Class BLL_Usuarios

    Public Const ERROR_USUARIO_EXISTENTE = 1
    Public Const ERROR_DB = 2

    Private oBllBit As New BLL_Bitacora
    Public oMiMpp As New MPP_Usuarios


    Public Function GetPorId(ByVal id As Integer) As BE_Usuario
        Return oMiMpp.GetPorId(id)
    End Function
    
    ''' 
    ''' <param name="IdUsuario"></param>
    ''' <param name="IdFamilia"></param>
    Public Function AgregarFamilia(ByVal IdFamilia As Integer, ByVal IdUsuario As Integer) As Boolean
        Dim agregada As Integer = oMiMpp.AgregarFamilia(IdFamilia, IdUsuario)
        Return agregada > 0
    End Function

    ''' 
    ''' <param name="IdUsuario"></param>
    ''' <param name="objPermiso"></param>
    Public Function AgregarPermiso(ByVal IdUsuario As Integer, ByVal objPermiso As BE_Permiso) As Boolean
        Dim agregado As Integer = oMiMpp.AgregarPermiso(IdUsuario, objPermiso)
        Return agregado > 0
        'AgregarPermiso = False
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Crear(ByVal objUsuario As BE_Usuario) As BE.BE_Usuario
        If oMiMpp.ValidarExistente(objUsuario) = True Then
            Return Nothing
        End If
        objUsuario.IdUsuario = 0 'Asumo que va a fallar
        Dim IdInsertado As Integer = oMiMpp.Crear(objUsuario)
        If IdInsertado > 0 Then ' Si no falla, sobrescribo el valor por def.
            objUsuario.IdUsuario = IdInsertado
        End If
        Return objUsuario
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Desbloquear(ByVal objUsuario As BE_Usuario) As Boolean
        Desbloquear = False
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Editar(ByVal objUsuario As BE_Usuario) As Boolean
        If oMiMpp.ValidarExistente(objUsuario) = True Then
            Return Nothing
        End If
        Dim actualizado As Boolean = oMiMpp.Editar(objUsuario)
        Return actualizado
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Eliminar(ByVal objUsuario As BE_Usuario) As Integer
        Dim resultado As Integer = 0
        If oMiMpp.PuedeEliminarUsuario(objUsuario) = True Then
            resultado = oMiMpp.Eliminar(objUsuario)
        Else
            resultado = -2 'Es super admin y es el �ltimo
        End If
        Return resultado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Usuario)
        Return oMiMpp.Listar(filtros)
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function ModificarContrasenia(ByVal objUsuario As BE_Usuario) As Boolean
        Dim bllSeguridad As New BLL_Seguridad
        'objUsuario.Contrasenia = bllSeguridad.Encriptar(objUsuario.Contrasenia) ya deber�a venir encriptada
        Dim mppUsu As New MPP_Usuarios
        Dim modificado As Boolean = mppUsu.ModificarContrasenia(objUsuario)
        If modificado = True Then
            Dim evento As New BE_Evento
            evento.IdEvento = BE_Evento.CAMBIO_CONTRASENIA_CLIENTE
            oBllBit.RegistrarEvento(evento, "Modificado por el usuario")
        End If
        Return modificado
    End Function

    ''' 
    Public Function QuitarFamilia(ByVal IdUsuario As Integer, ByVal idFamilia As Integer) As Boolean
        Dim eliminado As Boolean = oMiMpp.QuitarFamilia(IdUsuario, idFamilia)
        Return eliminado
    End Function

    ''' 
    ''' <param name="IdUsuarioPermiso"></param>
    Public Function QuitarPermiso(ByVal IdUsuario As Integer, ByVal IdPermiso As Integer) As Boolean
        Dim eliminado As Boolean = oMiMpp.QuitarPermiso(IdUsuario, IdPermiso)
        Return eliminado
    End Function


End Class ' BLL_Usuarios
