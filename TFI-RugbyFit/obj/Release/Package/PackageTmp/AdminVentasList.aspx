﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminVentasList.aspx.vb" Inherits="TFI_RugbyFit.AdminVentasList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <div class="content-header">
        <div class="header-section">
            <h3>
                <strong>Ventas</strong>
                <a href="AdminVentasForm.aspx" class="btn btn-primary pull-right">Nuevo</a><br />
            </h3>
        </div>
    </div>

    <div class="block full">
        <div class="row">
            <div id="filter-panel" class="filter-panel">
                <div class="panel panel-default bg-panel">
                    <div class="panel-body">
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">C&oacute;digo</div>
                                <asp:TextBox ID="filtroCodigo" TextMode="Number" ClientIDMode="Static" MaxLength="3" Max="100" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Cliente</div>
                                <asp:TextBox ID="filtroTitulo" runat="server" MaxLength="70" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <!-- form group [rows] -->
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-info" />
                        <a href="/AdminVentasList.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;Limpiar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block full">
        <div class="table-responsive">
            <span class="lead">Se han encontrado <strong class="text-danger">
                <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> registros
            </span>
            <span class="help-block pull-right">Mostrando 15 registros por p&aacute;gina</span>
            <div style="border-top: 1px solid black;"></div>
            <asp:GridView ID="DgvVentas" runat="server" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-striped table-hover" AllowPaging="True" GridLines="None" PagerStyle-CssClass="pagination-dgv " PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="DgvVentas_PageIndexChanging" OnPageIndexChanged="DgvVentas_PageIndexChanged" PageSize="15">
                <Columns>
                    <asp:BoundField DataField="IdVenta" HeaderText="Codigo" />
                    <asp:BoundField DataField="NombreCliente" HeaderText="Cliente" />
                    <asp:BoundField DataField="MontoTotal" HeaderText="Monto" />
                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                    <asp:BoundField DataField="Estado" HeaderText="Estado" ControlStyle-CssClass="label label-default" />
                    <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="Editar" />

                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
