﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="ReporteVentas.aspx.vb" Inherits="TFI_RugbyFit.ReporteVentas" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <h2>Ventas <a href="AdminReportes.aspx" class="btn btn-default pull-right">Volver al Panel de Reportes</a></h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
        <div id="filter-panel" class="filter-panel">
            <div class="panel panel-default bg-panel">
                <div class="panel-body">
                    
                        <div class="col-md-2">
                            <div class="input-group">
                                <div class="input-group-addon">Anio</div>
                                <asp:DropDownList ID="ddAnio" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Todos</asp:ListItem>
                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <div class="input-group-addon">Mes</div>
                                <asp:DropDownList ID="ddMes" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Todos</asp:ListItem>
                                    <asp:ListItem Value="1">Enero</asp:ListItem>
                                    <asp:ListItem Value="2">Febrero</asp:ListItem>
                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                    <asp:ListItem Value="4">Abril</asp:ListItem>
                                    <asp:ListItem Value="5">Mayo</asp:ListItem>
                                    <asp:ListItem Value="6">Junio</asp:ListItem>
                                    <asp:ListItem Value="7">Julio</asp:ListItem>
                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                    <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                    <asp:ListItem Value="10">Octubre</asp:ListItem>
                                    <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                    <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Desde</div>
                                <asp:TextBox ID="filtroDesde" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderDesde" TargetControlID="filtroDesde" Format="dd-MM-yyyy" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Hasta</div>
                                <asp:TextBox ID="filtroHasta" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderHasta" TargetControlID="filtroHasta" Format="dd-MM-yyyy" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- form group [rows] -->
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-info" />
                            <a href="/ReporteVentas.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></a>
                        </div>




                </div>
            </div>
        
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <rsweb:ReportViewer ID="rvGanancias" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" Width="100%" WaitMessageFont-Size="14pt" SizeToReportContent="true">
                <LocalReport ReportEmbeddedResource="TFI_RugbyFit.ReporteFacturacion.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DsFact" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="DsFacturacionTableAdapters."></asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
