﻿Imports BLL
Imports BE
Public Class MiPerfilCliente
    Inherits ACL
    Private oBllCliente As New BLL_Clientes

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAccesoCliente()

        If Not IsPostBack Then
            Me._CargarDatosCliente()
            Me.CargarProvincias()
        End If
    End Sub

    Protected Sub CargarCiudades(idProvincia)
        Dim oBllCiudad As New BLL_Ciudades
        Dim hDatos As New Hashtable()
        hDatos.Add("@IdProvincia", idProvincia)
        Dim ciudades As List(Of BE_Ciudad) = oBllCiudad.Listar(hDatos)

        ddCiudad.DataTextField = "Nombre"
        ddCiudad.DataValueField = "IdCiudad"
        ddCiudad.DataMember = "BE_Ciudad"
        ddCiudad.DataSource = ciudades
        ddCiudad.DataBind()
    End Sub

    Protected Sub CargarProvincias()
        Dim oBLLProv As New BLL_Provincias()
        ddProvincia.DataTextField = "nombre"
        ddProvincia.DataValueField = "id_provincia"
        ddProvincia.DataSource = oBLLProv.Listar()
        ddProvincia.DataBind()
    End Sub

    Protected Sub _CargarDatosCliente()
        Dim id As Integer = MyBase.GetIdCliente()
        Dim cliente As BE_Cliente = oBllCliente.GetPorId(id)
        If cliente Is Nothing Then
            Response.Write("Error! No hay datos del cliente")
        Else
            If cliente.Ciudad.Provincia.IdProvincia > 0 Then
                CargarCiudades(cliente.Ciudad.Provincia.IdProvincia)
            End If
            iptIdCliente.Value = cliente.IdCliente
            iptApellido.Text = cliente.Apellido
            iptNickname.Text = cliente.Nickname
            iptNombre.Text = cliente.Nombre
            iptEmail.Text = cliente.Email
            iptTelefono.Text = cliente.Telefono
            iptDNI.Text = cliente.DNI
            iptCUIT.Text = cliente.CUIT
            iptDomicilio.Text = cliente.Domicilio
            ddCiudad.SelectedValue = cliente.Ciudad.IdCiudad
            ddProvincia.SelectedValue = cliente.Ciudad.Provincia.IdProvincia
        End If
        
    End Sub

    Protected Sub btnModificarDatos_Click(sender As Object, e As EventArgs) Handles btnModificarDatos.Click

        Dim FormValido As Boolean = Me.ValidarForm()
        If FormValido = True Then
            Try
                Dim oBll As New BLL_Clientes
                Dim oBllSeg As New BLL_Seguridad
                Dim oBECliente As New BE_Cliente
                Dim oCiudad As New BE_Ciudad With {.IdCiudad = 0}
                Dim idCiudadRequest As String = Request(ddCiudad.UniqueID)
                If Not idCiudadRequest Is Nothing Then
                    oCiudad.IdCiudad = idCiudadRequest
                End If
                oBECliente.IdCliente = iptIdCliente.Value
                oBECliente.Nombre = iptNombre.Text
                oBECliente.Apellido = iptApellido.Text
                oBECliente.DNI = IIf(Not IsNumeric(iptDNI.Text), 0, CType(iptDNI.Text, Integer))
                oBECliente.CUIT = iptCUIT.Text
                oBECliente.Domicilio = iptDomicilio.Text
                oBECliente.Email = iptEmail.Text
                oBECliente.Telefono = iptTelefono.Text
                oBECliente.Ciudad = oCiudad

                Dim guardado As Boolean = oBll.Editar(oBECliente)
                If guardado = False Then
                    MensajeRespuesta.Text = "Se produjo un error desconocido y la registación no pudo realizarse. Inténtelo nuevamente o pongase en <a href='Contacto.aspx'>contacto con nosotros</a>"
                    PanelMensajeRespuesta.Visible = True
                Else
                    Response.Redirect("MiPerfilCliente.aspx?guardado=ok")
                End If
            Catch ex As Exception

            End Try
        End If
       
    End Sub

    Protected Function ValidarForm() As Boolean
        Dim huboError As Boolean = False

        EmailRequerido.Visible = False
        regexEmailValid.Visible = False
      
        NombreRequerido.Visible = False
        ApellidoRequerido.Visible = False


        If Validador.EstaVacio(iptEmail.Text) Then
            huboError = True
            EmailRequerido.Visible = True
        End If
        If Not Validador.EmailValido(iptEmail.Text) Then
            huboError = True
            regexEmailValid.Visible = True
        End If
        
        
        
        If Validador.EstaVacio_MaxLength(iptNombre.Text, 10) Then

            huboError = True
            NombreRequerido.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            NombreRequerido.Visible = True
        End If
        If Validador.EstaVacio_MaxLength(iptApellido.Text, 10) Then
            huboError = True
            ApellidoRequerido.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            ApellidoRequerido.Visible = True
        End If
       
        If huboError = False Then
            Return True
        End If
        Return False
    End Function
End Class