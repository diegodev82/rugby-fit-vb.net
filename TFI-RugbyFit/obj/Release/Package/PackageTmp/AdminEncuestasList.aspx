﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminEncuestasList.aspx.vb" Inherits="TFI_RugbyFit.AdminEncuestaList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <div class="content-header">
            <% If Request.QueryString("eliminado") = "ok" Then%>
            <div class="block full">
                <div class="alert alert-success">
                    <p>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                        <asp:Literal ID="msjEliminadoOk" runat="server" Text="La encuesta fue eliminada con éxito"></asp:Literal>
                    </p>
                </div>
            </div>
            <% End If%>
            <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
                <div class="alert alert-danger">
                    <asp:Literal ID="msjErrorDB" Visible="false" runat="server" Text="Se produjo un error en el sistema y el encuesta no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
                </div>
            </asp:PlaceHolder>
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="Literal1" runat="server" Text="Encuestas"></asp:Literal></strong>
                    <a href="AdminEncuestasForm.aspx" class="btn btn-primary pull-right">
                        <asp:Literal ID="Literal2" runat="server" Text="Nuevo"></asp:Literal></a><br />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="row">
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default bg-panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">C&oacute;digo</div>
                                        <asp:TextBox ID="filtroCodigo" runat="server" ClientIDMode="Static" TextMode="Number" MaxLength="2"  max="50"  CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="col-md-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">Tipo</div>
                                        <asp:DropDownList ID="ddTipo" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
                                            <asp:ListItem Value="C">Encuesta</asp:ListItem>
                                            <asp:ListItem Value="S">Ficha de opinión</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon">Pregunta</div>
                                        <asp:TextBox ID="filtroNombre" runat="server" MaxLength="20" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">Desde</div>
                                        <asp:TextBox ID="filtroDesde" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderDesde" TargetControlID="filtroDesde" Format="dd-MM-yyyy" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <div class="input-group-addon">Hasta</div>
                                        <asp:TextBox ID="filtroHasta" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderHasta" TargetControlID="filtroHasta" Format="dd-MM-yyyy" runat="server" />
                                    </div>
                                </div>
                            </div>
                             <div class="row botones-buscador">
                                <div class="col-md-3 col-md-offset-10">
                                    <!-- form group [rows] -->
                                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-info" />
                                    <a href="/AdminEncuestasList.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;Limpiar</a>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="block full">
            <div class="table-responsive">
                <span class="lead">Se han encontrado <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> registros
                </span>
                <span class="help-block pull-right">Mostrando 15 registros por p&aacute;gina</span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvEncuestas" runat="server" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-striped table-hover" AllowPaging="True" GridLines="None" PagerStyle-CssClass="pagination-dgv " PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="DgvEncuestas_PageIndexChanging" OnPageIndexChanged="DgvEncuestas_PageIndexChanged" PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="IdEncuesta" HeaderText="Codigo" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                        <asp:BoundField DataField="Pregunta" HeaderText="Nombre" />
                        <asp:BoundField DataField="FechaDesde" HeaderText="Desde" />
                        <asp:BoundField DataField="FechaHasta" HeaderText="Hasta" />
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="Editar" />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="Eliminar" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
