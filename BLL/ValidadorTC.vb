﻿Imports BE
Public Class ValidadorTC
    Private Numero As Int64
    Private TipoTc As String
    Private DataTC As BE_TarjetaCredito

    Public Const VISA = "visa"
    Public Const MASTERCARD = "mastercard"
    Public Const AMEX = "amex"

    Public Sub New()

    End Sub

    Public Sub SetTipoTcSeleccionada(ByVal tipo As String)
        Me.TipoTc = tipo
    End Sub

    Public Function ValidarNumero(ByVal nro As Int64) As Boolean
        Me.Numero = nro
        If Me.TipoTc = "visa" And Not Me.EsVisa() Then
            Return False
        End If
        If Me.TipoTc = "mastercard" And Not Me.EsMastercard() Then

            Return False
        End If
        If Me.TipoTc = "amex" And Not Me.EsAmex() Then
            Return False
        End If
        If Not Me.EsNumeroValido() Then
            Return False
        End If
        Me.DataTC = Me.GetDataTC(nro)
        If Me.DataTC Is Nothing Then
            Return False
        End If
        Return True
    End Function

    Public Function ValidarCodigo(ByVal codigoIngresado As Integer) As Boolean
        Return Me.DataTC.Codigo = codigoIngresado
    End Function

    Public Function ValidarFechaExpiracion(ByVal fechaIngresada As String) As Boolean
        Return Me.DataTC.MesAnioExpiracion = fechaIngresada
    End Function
    
    Public Function GetDataTC(ByVal nro As Int64) As BE_TarjetaCredito
        Dim oBll As New BLL_Ventas()
        Return oBll.ValidarTCExistente(nro)
    End Function

    Public Function EsNumeroValido() As Boolean
        Dim numString As String = Me.Numero.ToString
        Dim charCount As Integer = numString.Length
        If charCount < 13 Or charCount > 16 Then
            Return False
        End If

        Dim dv As Char = numString.ElementAt(charCount - 1)
        Dim numArr As New ArrayList
        Dim pos As Integer = 0
        Dim digit As Integer
        Dim nro As Integer
        For index = charCount - 2 To 0 Step -1
            digit = Char2Int(numString.ElementAt(index))
            If pos Mod 2 = 0 Then
                nro = digit * 2
                nro = Me.ToSingle(nro)
            Else
                nro = digit
            End If
            numArr.Add(nro)
            pos = pos + 1
        Next
        Dim sumTotal As Integer = 0
        For index = 0 To numArr.Count - 1
            sumTotal = sumTotal + numArr(index)
        Next
        Dim resultado As Integer = sumTotal * 9
        Dim strRtdo As String = resultado.ToString
        Dim digitoCalculado As Integer = Char2Int(strRtdo.ElementAt(strRtdo.Length - 1))
        If digitoCalculado = Char2Int(dv) Then
            Return True
        End If
        Return False
    End Function

    Public Function Char2Int(ByVal ch As Char) As Integer
        Dim int As Integer = Convert.ToInt32(ch) - 48 ' este hack hay que hacerlo porque devuelve el ascii (me parece)
        Return int
    End Function

    Public Function ToSingle(ByVal digit As Integer) As Integer
        If digit > 9 Then
            Dim tmp = digit.ToString
            Dim d1 As Integer = Char2Int(tmp.ElementAt(0))
            Dim d2 As Integer = Char2Int(tmp.ElementAt(1))
            Return d1 + d2
        Else
            Return digit
        End If
    End Function

    Public Function EsVisa() As Boolean
        Dim num = Me.Numero.ToString().Substring(0, 1)
        If num = "4" Then
            Return True
        End If
        Return False
    End Function

    Public Function EsMastercard() As Boolean
        Dim num = Me.Numero.ToString().Substring(0, 2)
        If num = "51" Or num = "52" Or num = "53" Or num = "55" Then
            Return True
        End If
        Return False
    End Function

    Public Function EsAmex() As Boolean
        Dim num = Me.Numero.ToString().Substring(0, 2)
        If num = "34" Or num = "37" Then
            Return True
        End If
        Return False
    End Function
End Class
