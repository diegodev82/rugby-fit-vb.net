﻿Imports BLL
Imports BE
Imports System.Web.Script.Serialization

Public Class ajax
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("action") Is Nothing Then
            Dim action As String = Request.QueryString("action")
            If action = "GetCiudadesPorProvincia" Then
                
                Dim id_provincia As String = Request.QueryString("id_provincia")
                Dim ciudades As List(Of BE_Ciudad) = GetCiudadesPorProvincia(id_provincia)
                'Response.Write(ciudades)
                Dim serializer As New JavaScriptSerializer()
                Dim arrayJson As String = serializer.Serialize(ciudades)
                Response.Headers.Add("Content-type", "application/json")
                Response.Write(arrayJson)
            End If

        End If

    End Sub

    Public Function GetCiudadesPorProvincia(ByVal id_provincia As Integer) As List(Of BE_Ciudad)
        Dim oBll As New BLL_Ciudades()
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProvincia", id_provincia)
        Return oBll.Listar(hdatos)
    End Function

End Class