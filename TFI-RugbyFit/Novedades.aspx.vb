﻿Imports BE
Imports BLL
Public Class Novedades
    Inherits System.Web.UI.Page
    Private oBll As New BLL_Novedad
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.CargarNovedades()
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try

    End Sub

    Public Sub CargarNovedades()
        Dim filtros As New Hashtable
        filtros.Add("@Limit", 9)
        Dim novedades As List(Of BE_Novedad) = oBll.Listar(filtros)
        rptNovedades.DataSource = novedades
        rptNovedades.ItemType = "BE_Novedad"
        rptNovedades.DataBind()
    End Sub

    Protected Sub btnSuscribirse_Click(sender As Object, e As EventArgs) Handles btnSuscribirse.Click
        Try
            Dim email As String = iptEmailNewsletter.Text
            If Validador.EmailValido(email) = False Then
                msjeRtaOK.Visible = False
                msjeRtaError.Visible = True
                msjeRtaError.InnerText = "Ingrese un email válido"
                Return
            End If
            Dim resultado As Integer = oBll.SuscribirNewsletter(email)
            If resultado = 1 Then
                iptEmailNewsletter.Text = ""
                msjeRtaOK.Visible = True
                msjeRtaError.Visible = False
            ElseIf resultado = 2 Then
                msjeRtaOK.Visible = False
                msjeRtaError.Visible = True
                msjeRtaError.InnerText = "Usted ya se encuentra suscripto al newsletter"
            Else
                msjeRtaOK.Visible = False
                msjeRtaError.Visible = True
                msjeRtaError.InnerText = "Disculpe, en estos momentos no podemos suscribirlo al newsletter. Por favor, inténtenlo nuevamente más tarde."
            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub
End Class