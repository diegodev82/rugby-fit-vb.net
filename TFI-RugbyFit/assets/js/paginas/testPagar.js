﻿
function validarIngresoFechaExpiracion(e) {
    var regexp = new RegExp("/^[0-9]$/");
    var value = $(".js-fecha-expiracion").val();
    if (regexp.test(value) == false) {
        return false;
    }
    if (e.charCode == 0 || e.keyCode == 8 || e.keyCode == 46) {
        return true;
    }
    return element.value.length != 6;
}


function testAjax() {
    var numero = $(".js-numero-tc").val();
    $.ajax({
        "url": "/AjaxUtils.asmx/TCExistente",
        data: "{Numero:" + numero + "}",
        contentType: 'application/json; utf-8',
        dataType: 'json',
        type: 'post',
        async: false,
        success: function (data) {
            console.info(data.d);
            if (data.d != null) {
                return data.d;
            }
            return null;
        },
    });
}


function testNroValido(numero) {
    var ccNum = numero;
    var charCount = numero.length
    if (charCount < 13 || charCount > 16) {
        return false;
    }
    var dv = ccNum[ccNum.length - 1];
    var numArr = [];
    var pos = 0;
    for (i = charCount - 2; i >= 0 ; i--) {
        var digit = parseInt(ccNum.charAt(i));
        var nro;

        if (pos % 2 == 0) {
            nro = digit * 2
            nro = this.toSingle(nro);
        } else {
            nro = digit
        }
        console.log(nro);
        numArr.push(nro)
        pos++;
    }
    sumTotal = 0;
    for (i = 0; i < numArr.length; i++) {
        sumTotal += numArr[i];
    }
    console.log(sumTotal);
    var control = sumTotal * 9;
    var strControl = control.toString()
    var digitoControl = strControl[strControl.length - 1] * 1;
    console.info(digitoControl == dv);
    return digitoControl == dv;
    //console.log(digitoControl, dv);

}

function tester() {
    var arr = ["372232196180003", "4929847779946197", "345519957313417", ];
    //var arr = ["4929847779946197" ];
    for (var i = 0; i < arr.length; i++) {
        testNroValido(arr[i]);
    }
}
