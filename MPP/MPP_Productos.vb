Imports BE
Imports DAL

Public Class MPP_Productos
    Public Function Valorar(ByVal idPrd As Integer, valoracion As Integer) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdProducto", idPrd)
        hdatos.Add("@Valoracion", valoracion)
        
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ValorarProducto", hdatos)
        Return guardado
    End Function


    Public Function AgregarComentario(ByVal objComen As BE_Comentario) As BE_Comentario
        Dim hdatos As New Hashtable

        hdatos.Add("@Autor", objComen.Autor)
        hdatos.Add("@Comentario", objComen.Comentario)
        hdatos.Add("@IdProducto", objComen.IdProducto)
        hdatos.Add("@Activo", True)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearComentario", hdatos)
        If InsertedId > 0 = True Then
            objComen.IdComentario = InsertedId
        End If
        Return objComen
    End Function
    ''' 
    ''' <param name="objPrd"></param>
    Public Function Crear(ByVal objPrd As BE_Producto) As BE.BE_Producto
        Dim hdatos As New Hashtable

        hdatos.Add("@Nombre", objPrd.Nombre)
        hdatos.Add("@Descripcion", objPrd.Descripcion)
        hdatos.Add("@DescripcionCorta", objPrd.DescripcionCorta)
        hdatos.Add("@IdCategoria", objPrd.Categoria.IdCategoria)
        hdatos.Add("@LinkImagen", objPrd.LinkImagen)
        hdatos.Add("@OfrecePuntos", objPrd.OfrecePuntos)
        hdatos.Add("@EsCanjeable", objPrd.EsCanjeable)
        hdatos.Add("@PuntosOfrecidos", objPrd.PuntosOfrecidos)
        hdatos.Add("@PuntosRequeridos", objPrd.PuntosRequeridos)
        hdatos.Add("@IdProveedor", objPrd.Proveedor.IdProveedor)
        hdatos.Add("@Stock", objPrd.Stock)
        hdatos.Add("@StockMinimo", objPrd.StockMinimo)
        hdatos.Add("@Precio", objPrd.Precio)
        hdatos.Add("@Activo", objPrd.Activo)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearProducto", hdatos)
        If InsertedId > 0 = True Then
            objPrd.IdProducto = InsertedId
        End If
        Return objPrd
    End Function

    ''' 
    ''' <param name="cant"></param>
    ''' <param name="objPrd"></param>
    Public Function DescontarStock(ByVal cant As Integer, ByVal objPrd As BE_Producto) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdProducto", objPrd.IdProducto)
        hdatos.Add("@Cantidad", cant)
        
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_DescontarStock", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    Public Function Editar(ByVal objPrd As BE_Producto) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdProducto", objPrd.IdProducto)
        hdatos.Add("@Nombre", objPrd.Nombre)
        hdatos.Add("@Descripcion", objPrd.Descripcion)
        hdatos.Add("@DescripcionCorta", objPrd.DescripcionCorta)
        hdatos.Add("@IdCategoria", objPrd.Categoria.IdCategoria)
        hdatos.Add("@LinkImagen", objPrd.LinkImagen)
        hdatos.Add("@OfrecePuntos", objPrd.OfrecePuntos)
        hdatos.Add("@EsCanjeable", objPrd.EsCanjeable)
        hdatos.Add("@PuntosOfrecidos", objPrd.PuntosOfrecidos)
        hdatos.Add("@PuntosRequeridos", objPrd.PuntosRequeridos)
        hdatos.Add("@IdProveedor", objPrd.Proveedor.IdProveedor)
        hdatos.Add("@StockMinimo", objPrd.StockMinimo)
        hdatos.Add("@Precio", objPrd.Precio)
        hdatos.Add("@Activo", objPrd.Activo)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ActualizarProducto", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    Public Function Eliminar(ByVal objPrd As BE_Producto) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", objPrd.IdProducto)
        Dim resultado As Object = oDatos.Eliminar("pa_EliminarProducto", hdatos)
        Return resultado
    End Function

    ''' 
    ''' <param name="cant"></param>
    ''' <param name="objPrd"></param>
    Public Function IncrementarStock(ByVal cant As Integer, ByVal objPrd As BE_Producto) As Boolean
        IncrementarStock = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Producto)
        Dim hdatos As New Hashtable
        'hdatos.Add("@Email", objCliente.Email)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetProductos", filtros)
        Dim listado As New List(Of BE_Producto)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unPrd As New BE_Producto
                    unPrd.IdProducto = item("id_producto")
                    unPrd.Nombre = item("nombre")
                    unPrd.Descripcion = item("descripcion")
                    unPrd.DescripcionCorta = item("descripcion_corta")
                    unPrd.EsCanjeable = item("es_canjeable")
                    unPrd.LinkImagen = IIf(IsDBNull(item("link_imagen")), "", item("link_imagen"))
                    unPrd.OfrecePuntos = item("ofrece_puntos")
                    unPrd.PuntosOfrecidos = item("puntos_ofrecidos")
                    unPrd.PuntosRequeridos = item("puntos_requeridos")
                    unPrd.Proveedor = New BE_Proveedor With {.Nombre = item("proveedor")}
                    unPrd.Categoria = New BE_Categoria With {.Nombre = item("categoria")}
                    unPrd.Precio = IIf(IsDBNull(item("precio")), 0, item("precio"))
                    unPrd.Activo = item("activo")
                    unPrd.Valoracion = item("valoracion")
                    unPrd.CantVotos = item("cant_votos")
                    If unPrd.CantVotos > 0 Then
                        unPrd.Rating = unPrd.Valoracion / unPrd.CantVotos
                    End If

                    listado.Add(unPrd)
                Next
            End If
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objProducto"></param>
    Public Function PuntosOtorgados(ByVal objProducto As BE_Producto) As Integer
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", objProducto.IdProducto)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetPuntosOtorgados", hdatos)
        If ds.Tables.Count > 0 Then
            Dim item As DataRow = ds.Tables(0).Rows(0)
            Return item("puntos_ofrecidos")
        End If
        Return 0
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    Public Function ValidarExistencia(ByVal objPrd As BE_Producto) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", objPrd.IdProducto)
        hdatos.Add("@Nombre", objPrd.Nombre)
        hdatos.Add("@IdCategoria", objPrd.Categoria.IdCategoria)
        hdatos.Add("@IdProveedor", objPrd.Proveedor.IdProveedor)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarProductoExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function

    ''' 
    ''' <param name="objProducto"></param>
    Public Function ValidarStock(ByVal objProducto As BE_Producto) As Integer
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", objProducto.IdProducto)
        
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarStock", hdatos)
        If ds.Tables.Count > 0 Then
            Dim item As DataRow = ds.Tables(0).Rows(0)
            Return item("stock")
        End If
        Return 0
    End Function

    Public Function GetPorId(ByVal Id As Integer) As BE_Producto
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", Id)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetProductoPorId", hdatos)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                Dim unPrd As New BE_Producto
                unPrd.IdProducto = item("id_producto")
                unPrd.Nombre = item("nombre")
                unPrd.Descripcion = item("descripcion")
                unPrd.DescripcionCorta = item("descripcion_corta")
                unPrd.EsCanjeable = item("es_canjeable")
                unPrd.LinkImagen = IIf(IsDBNull(item("link_imagen")), "/assets/img/img-vacia.png", item("link_imagen"))
                unPrd.OfrecePuntos = item("ofrece_puntos")
                unPrd.PuntosOfrecidos = item("puntos_ofrecidos")
                unPrd.PuntosRequeridos = item("puntos_requeridos")
                unPrd.Proveedor = New BE_Proveedor With {.IdProveedor = item("id_proveedor")}
                unPrd.Categoria = New BE_Categoria With {.IdCategoria = item("id_categoria"), .Nombre = IIf(IsDBNull(item("nombre_categoria")), "", item("nombre_categoria"))}
                unPrd.Precio = IIf(IsDBNull(item("precio")), 0, item("precio"))
                unPrd.Activo = item("activo")
                unPrd.Stock = IIf(IsDBNull(item("stock")), 0, item("stock"))
                unPrd.StockMinimo = item("stock_minimo")
                unPrd.Comentarios = GetComentariosPorProducto(unPrd.IdProducto)
                unPrd.Valoracion = item("valoracion")
                unPrd.CantVotos = item("cant_votos")
                If unPrd.CantVotos > 0 Then
                    unPrd.Rating = unPrd.Valoracion / unPrd.CantVotos
                End If
                Return unPrd
            End If
        End If
        Return Nothing
    End Function

    Public Function GetComentariosPorProducto(ByVal IdProducto As Integer) As List(Of BE_Comentario)
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", IdProducto)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetComentariosPorProducto", hdatos)
        Dim listado As New List(Of BE_Comentario)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unCom As New BE_Comentario
                    unCom.IdComentario = item("id_producto")
                    unCom.Autor = item("autor")
                    unCom.Comentario = item("comentario")
                    unCom.Activo = item("activo")
                    unCom.FechaHora = item("fecha_hora")
                    listado.Add(unCom)
                Next
            End If
        End If
        Return listado
    End Function

End Class ' MPP_Productos
