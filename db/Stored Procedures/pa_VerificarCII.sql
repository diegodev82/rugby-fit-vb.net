SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_VerificarCII
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TOP 1 cii FROM usuario WHERE id_usuario = @IdUsuario
END
GO
