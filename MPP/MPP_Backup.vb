Imports DAL
Imports BE

Public Class MPP_Backup

    Private oDatos As SQLHelper = SQLHelper.GetInstance
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Backup)
        Dim hdatos As New Hashtable
        Dim ds As New DataSet
        ds = oDatos.Leer("pa_GetBackups", filtros)
        Dim listado As New List(Of BE_Backup)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unBk As New BE_Backup
                    unBk.IdBackup = item("id_backup")
                    unBk.Fecha = item("fecha")
                    unBk.Ruta = item("ruta")
                    listado.Add(unBk)
                Next
            End If
        End If
        Return listado
    End Function
    ''' 
    ''' <param name="ruta"></param>
    Public Function RealizarBackup(ByVal rutaGenerada As String) As Boolean

        Dim hdatos As New Hashtable
        hdatos.Add("@RutaGenerada", rutaGenerada)
        Dim dbMaster As SQLHelper = SQLHelper.GetInstance("MASTER")
        Dim generado As Boolean = dbMaster.ExecuteNonQuery("EXEC [dbo].[pa_BackupRugbyFit] @RutaGenerada = N'" + rutaGenerada + "'")
        Return generado
        'Dim sql As String = "BACKUP DATABASE RugbyFit TO DISK = '" & rutaGenerada & "'"
        'Return SQLHelper.GetInstance("MASTER").ExecuteNonQuery(sql)

        '        USE [master]
        '        GO()
        '/****** Object:  StoredProcedure [dbo].[pa_BackupRugbyFit]    Script Date: 24/10/2016 02:18:09 p.m. ******/
        'SET ANSI_NULLS ON
        '        GO()
        'SET QUOTED_IDENTIFIER ON
        '        GO()
        '-- =============================================
        '-- Author:		Diego Olmedo
        '-- Create date: 
        '-- Description:	
        '-- =============================================
        'ALTER PROCEDURE [dbo].[pa_BackupRugbyFit]
        '	@RutaGenerada nvarchar(400)
        'AS
        '        BEGIN()
        '	-- SET NOCOUNT ON added to prevent extra result sets from
        '	-- interfering with SELECT statements.
        '	SET NOCOUNT ON;
        'BACKUP DATABASE RugbyFit TO DISK = @RutaGenerada WITH FORMAT;
        '        End


    End Function

    Public Function RegistrarBackup(ByVal ruta As String) As Boolean
        '
        Dim hdatos As New Hashtable
        hdatos.Add("@RutaGenerada", ruta)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim Guardado As Boolean = oDatos.Escribir("pa_RegistrarBackupRealizado", hdatos)
        Return Guardado
    End Function

    Public Function GetPorId(ByVal IdBackup As Integer) As BE_Backup
        Dim hdatos As New Hashtable
        hdatos.Add("@IdBackup", IdBackup)
        Dim ds As New DataSet
        ds = oDatos.Leer("pa_GetBackups", hdatos)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                Dim unBk As New BE_Backup
                unBk.IdBackup = item("id_backup")
                unBk.Fecha = item("fecha")
                unBk.Ruta = item("ruta")
                Return unBk
            End If
        End If
        Return New BE_Backup
    End Function

    ''' 
    ''' <param name="ruta"></param>
    Public Function RestaurarBackup(ByVal ruta As String) As Boolean
        Dim dbMaster As SQLHelper = SQLHelper.GetInstance("MASTER")
        If Not dbMaster Is Nothing Then
            If SQLHelper.GetInstance.isOpen() Then
                SQLHelper.GetInstance._disconnect()
            End If
            'Dim sql2 As String = "ALTER DATABASE ELECTROGESTION SET MULTI_USER WITH ROLLBACK IMMEDIATE"
            'Mata los procesos existentes que impiden realizar el restore
            Try
                Dim generado As Boolean = dbMaster.ExecuteNonQuery("EXEC [dbo].[pa_RestaurarBackupRugbyFit] @Ruta = N'" + ruta + "'")
                Return True
            Catch ex As Exception
                Return False
            End Try
        End If
        Return False
        '        USE [master]
        '        GO()
        '/****** Object:  StoredProcedure [dbo].[pa_RestaurarBackupRugbyFit]    Script Date: 24/10/2016 02:18:51 p.m. ******/
        'SET ANSI_NULLS ON
        '        GO()
        'SET QUOTED_IDENTIFIER ON
        '        GO()
        '-- =============================================
        '-- Author:		Diego Olmedo
        '-- Create date: 
        '-- Description:	
        '-- =============================================
        'ALTER PROCEDURE [dbo].[pa_RestaurarBackupRugbyFit]
        '	-- @IdBackup integer,
        '	@Ruta as nvarchar(300)
        'AS
        '        BEGIN()
        '	-- SET NOCOUNT ON added to prevent extra result sets from
        '	-- interfering with SELECT statements.
        '	-- declare @Ruta nvarchar(500)
        '	-- select @Ruta = ruta from backups where id_backup = @IdBackup;
        '	SET NOCOUNT ON;
        '	alter database RugbyFit set offline with rollback immediate;
        '	alter database RugbyFit set online;
        '	ALTER DATABASE RugbyFit SET MULTI_USER WITH ROLLBACK IMMEDIATE
        '	RESTORE DATABASE RugbyFit FROM DISK= @Ruta WITH REPLACE;
        '        End




    End Function


End Class ' MPP_Backup

