﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Comparacion.aspx.vb" Inherits="TFI_RugbyFit.Comparacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/comparacion.css" />
    <script type="text/javascript" src="assets/js/paginas/comparacion.js" ></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <a href="Productos.aspx" class="pull-right">Volver al cat&aacute;logo</a>
    <h1>Tabla de comparaci&oacute;n de productos </h1>
    
    <div class="alert alert-danger" runat="server" id="errorCant">Debe elegir al menos 2 productos para comparar y no m&aacute;s de 3</div>
<div class="row" runat="server" visible="false" id="tblComparacion">
    <div class="col-md-3 columna">
        
       <div class="header"><br /></div>
        <div class="foto">Foto</div>
        <div>Precio</div>
        <div>Categor&iacute;a</div>
        <div>Descripci&oacute;n</div>
        <div>Ofrece puntos</div>
        <div>Puntos ofrecidos</div>
        <div>Es canjeable</div>
    </div>
    <asp:Repeater ID="rptComparacion" runat="server">
        <ItemTemplate>
        <div class="col-md-3 columna">
        <div class="header">
            <%# Container.DataItem.Nombre %>
            <a href="#" class="js-eliminar-comparacion pull-right"><span class="glyphicon glyphicon-remove text-danger"></span></a>
        </div>
        <div  class="foto"><img src="<%# Container.DataItem.LinkImagen %>" class="img-responsive" width="150" />  </div>
            <div>$ <%# Container.DataItem.Precio %> </div>
        <div><%# Container.DataItem.Categoria.Nombre %></div>
        <div><%# Container.DataItem.DescripcionCorta %></div>
        <div><%# IIf(Container.DataItem.OfrecePuntos = True, "<span class='glyphicon glyphicon-ok text-success'></span>", "<span class='glyphicon glyphicon-remove text-danger'></span>")%></div>
        <div><%# Container.DataItem.PuntosOfrecidos %></div>
        <div><%# IIf(Container.DataItem.EsCanjeable = True, "<span class='glyphicon glyphicon-ok text-success'></span>", "<span class='glyphicon glyphicon-remove text-danger'></span>") %></div>
    </div>
            </ItemTemplate>
        </asp:Repeater>
    
    
</div>
</asp:Content>
