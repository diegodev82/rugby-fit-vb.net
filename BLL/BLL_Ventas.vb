Imports BE
Imports MPP

Public Class BLL_Ventas


    Private oMpp As New MPP_Ventas


    Public Function GetPorId(ByVal idVta As Integer) As BE_Venta
        Return oMpp.GetPorId(idVta)
    End Function
    Public Function ActualizarEstado(ByVal IdVta As Integer, ByVal estado As String) As Boolean
        Return oMpp.ActualizarEstado(IdVta, estado)
    End Function

    Public Function GetVentasPorCliente(ByVal idCliente As Integer) As List(Of BE_Venta)
        Return oMpp.GetVentasPorCliente(idCliente)
    End Function

    Public Function GetTC() As List(Of BE_TarjetaCredito)
        Return oMpp.GetTC()
    End Function

    Public Function ValidarTCExistente(ByVal numero As Int64) As BE_TarjetaCredito
        Return oMpp.ValidarTCExistente(numero)
    End Function

    ''' 
    ''' <param name="IdVenta"></param>
    ''' <param name="objProducto"></param>
    Public Function AgregarProducto(ByVal IdVenta As Integer, ByVal dv As BE_Detalle_Venta) As Boolean
        Dim oMppPrd As New MPP_Productos
        Dim stockSuficiente As Integer = oMppPrd.ValidarStock(dv.Producto)
        If stockSuficiente < dv.Cantidad Then
            Return Nothing
        End If
        Dim IdDteVta As Integer = oMpp.ProductoAgregado(dv)
        If IdDteVta > 0 Then
            dv.IdDetalleVenta = IdDteVta
            Return oMpp.IncrementarCantidad(dv)
        Else
            dv = oMpp.AgregarProducto(IdVenta, dv)
            Return dv.IdDetalleVenta > 0
        End If
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function AnularVenta(ByVal objVenta As BE_Venta) As Integer
        AnularVenta = 0
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function Editar(ByVal objDetVta As BE_Detalle_Venta) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function EditarItem(ByVal objDetVta As BE_Detalle_Venta) As Boolean
        Return oMpp.EditarItem(objDetVta)
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function Eliminar(ByVal objVenta As BE_Venta) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function EliminarItem(ByVal objDetVta As BE_Detalle_Venta) As Boolean
        Return oMpp.EliminarItem(objDetVta)
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function FinalizarVenta(ByVal objVenta As BE_Venta) As Integer
        Return oMpp.FinalizarVenta(objVenta)
    End Function

    ''' 
    ''' <param name="IdUsuario"></param> 'Conviene pasar el id_cliente para dar soporte a ventas locales que no tienen usuario
    Public Function GetCarrito(ByVal IdCliente As Integer) As BE.BE_Venta
        Dim oMpp As New MPP_Ventas
        Dim vta As BE_Venta
        vta = oMpp.VerificarCarritoAbierto(IdCliente)
        If vta Is Nothing Then
            Dim oBllBit As New BLL_Bitacora
            Dim cliente As New BE_Cliente With {.IdCliente = IdCliente}
            Dim evento As New BE.BE_Evento
            evento.IdEvento = BE_Evento.CREAR_VENTA_WEB
            oBllBit.RegistrarEvento(evento, "Cliente # " + IdCliente.ToString, IdCliente)
            vta = oMpp.CrearCarrito(cliente)
        End If
        Return vta
    End Function

    ''' 
    ''' <param name="objVta"></param>
    Public Function Guardar(ByVal objVta As BE_Venta) As BE.BE_Venta
        Guardar = Nothing
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Venta)
        Return oMpp.Listar(filtros)
    End Function


End Class ' BLL_Ventas