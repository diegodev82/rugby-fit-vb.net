﻿Imports BE
Public Class AdminDefault
    Inherits ACL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso()
        nombreEmpleado.Text = UsuarioLogueado.Apellido + ", " + UsuarioLogueado.Nombre
    End Sub

End Class