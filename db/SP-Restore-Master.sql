USE [master]
GO
/****** Object:  StoredProcedure [dbo].[pa_RestaurarBackupRugbyFit]    Script Date: 24/10/2016 02:35:39 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_RestaurarBackupRugbyFit]
	-- @IdBackup integer,
	@Ruta as nvarchar(300)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- declare @Ruta nvarchar(500)
	-- select @Ruta = ruta from backups where id_backup = @IdBackup;
	SET NOCOUNT ON;
	alter database RugbyFit set offline with rollback immediate;
	alter database RugbyFit set online;
	ALTER DATABASE RugbyFit SET MULTI_USER WITH ROLLBACK IMMEDIATE
	RESTORE DATABASE RugbyFit FROM DISK= @Ruta WITH REPLACE;
END


