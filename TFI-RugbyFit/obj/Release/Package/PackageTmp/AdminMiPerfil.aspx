﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminMiPerfil.aspx.vb" Inherits="TFI_RugbyFit.AdminMiPerfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
   <!-- <script src="assets/js/paginas/provincia_ciudad.js" type="text/javascript"></script> -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    
        <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
            <div class="alert alert-danger">
                <span class="glyphicon glyphicon-remove-circle"></span> 
                <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
        <% If Request.QueryString("guardado") = "ok" Then%>
       <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" >
                    <span class="glyphicon glyphicon-thumbs-up"></span> Los datos se guardaron correctamente
                </div>
            </div>
        </div>
    <% End If    %>
        <div class="row">
            <div class="col-md-3 col-md-offset-5">
                <h2>Mis Datos</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2"> 
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:HiddenField ID="iptIdUsuario" runat="server" />
                        <label for="iptEmail" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptEmail" class="form-control" ClientIDMode="Static" required="required" runat="server" MaxLength="320" TextMode="Email" PlaceHolder="example@example.com"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EmailRequerido" runat="server" ForeColor="red" ErrorMessage="Campo requerido" ControlToValidate="iptEmail"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ForeColor="red" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="iptEmail" ErrorMessage="Formato de Email inválido"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptEmail" class="col-sm-3 control-label">Usuario</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptNickname" CssClass="form-control" runat="server" MaxLength="50" PlaceHolder="usuario82" ReadOnly="True" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptContrasenia" class="col-sm-3 control-label">Contrase&ntilde;a *</label>
                        <div class="col-sm-9">
                           <a href="AdminModificarContrasenia.aspx" class="btn btn-warning">Cambiar Contrase&ntilde;a</a>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="iptNombre" class="col-sm-3 control-label">Nombre *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptNombre" ClientIDMode="Static" required="required" class="form-control" MaxLength="30" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="NombreRequerido" runat="server" ForeColor="red" ErrorMessage="Campo requerido" ControlToValidate="iptNombre"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptApellido" class="col-sm-3 control-label">Apellido *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptApellido" ClientIDMode="Static" required="required" class="form-control" MaxLength="30" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ApellidoRequerido" runat="server" ForeColor="red" ErrorMessage="Campo requerido" ControlToValidate="iptApellido"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptCuitDNI" class="col-sm-3 control-label">D.N.I.</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptDNI" class="form-control js-max-length js-solo-numeros" ClientIDMode="Static" Max="999999999"   runat="server" TextMode="Number" MaxLength="9"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptTelefono" class="col-sm-3 control-label">Tel&eacute;fono</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptTelefono" class="form-control js-max-length js-solo-numeros" runat="server" ClientIDMode="Static" Max="9999999999" TextMode="Number" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                   <%-- <div class="form-group">
                        <label for="iptDomicilio" class="col-sm-3 control-label">Domicilio</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptDomicilio" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ddProvincia" class="col-sm-3 control-label">Provincia</label>
                        <div class="col-sm-9">
                            <asp:DropDownList ID="ddProvincia" class="form-control slt-provincia" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem Value="0" Text="Seleccione..."></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ddCiudad" class="col-sm-3 control-label">Ciudad</label>
                        <div class="col-sm-9">
                            <asp:DropDownList ID="ddCiudad" class="form-control slt-ciudad" runat="server"></asp:DropDownList>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-6">
                            <asp:Button ID="btnModificarDatos" class="btn btn-success" runat="server" Text="Guardar" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
</asp:Content>
