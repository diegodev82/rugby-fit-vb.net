﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ResetContrasenia.aspx.vb" Inherits="TFI_RugbyFit.ResetContrasenia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <% 
        Dim rtaReset As String = Request.QueryString("rta")
        If Not rtaReset Is Nothing Then
            If rtaReset = "ok" Then%>
    <div class="alert alert-success">
        <p><span class="glyphicon glyphicon-thumbs-up"></span> Su contrase&ntilde;a ha sido actualizada correctamente.</p>
        <p>Hemos enviado un email a su casilla con la nueva contrase&ntilde;a generada.</p>
    </div>
    <div class="alert alert-warning">
        <p><span class="glyphicon glyphicon-info-sign"></span> Recuerde actualizar la nueva contraseña en el primer ingreso.</p>
    </div>
    <% Else%>
    <div class="alert alert-danger">
        <% 
            Dim mensaje As String = "Error desconocido"
            If rtaReset = "email-desconocido" Then
                mensaje = "Disculpe, ese email no se encuentra registrado en el sistema"
            End If
            If rtaReset = "error-db" Then
                mensaje = "Disculpe, se produjo un error desconocido y no pudo realizarse la acción. Inténtelo nuevamente o pongase en <a href='Contacto.aspx'>contacto con nosotros</a>"
            End If
        %>
        <p><%=mensaje %></p>
    </div>
    <% End If
    End If%>
    <h1 class="text-center">Olvid&eacute; mi contrase&ntilde;a</h1>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <p>Si olvid&oacute; su contrase&ntilde;a, puede restaurarla desde aqu&iacute;.</p>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox ID="email_resetPwd" class="form-control input-lg" placeholder="Ingrese su e-mail" runat="server" ClientIDMode="Static" required="required" TextMode="Email"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ResetPwdEmailRequired" runat="server" ErrorMessage="Ingrese un email válido" ValidationGroup="ResetPwdValidation" ControlToValidate="email_resetPwd"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email_resetPwd" ValidationGroup="ResetPwdValidation" ErrorMessage="Formato de Email inválido"></asp:RegularExpressionValidator>
                                </div>
                                <asp:Button class="btn btn-lg btn-primary btn-block" ID="btn_ResetPwd" runat="server" Text="Restaurar Contraseña" ValidationGroup="ResetPwdValidation" />
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





</asp:Content>
