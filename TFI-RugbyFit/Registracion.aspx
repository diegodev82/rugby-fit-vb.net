﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Registracion.aspx.vb" EnableEventValidation="false" Inherits="TFI_RugbyFit.Registracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/css/registracion.css" rel="stylesheet" />
    <script src="assets/js/paginas/registracion.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="container">
        <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
            <div class="alert alert-danger">
                <span class="glyphicon glyphicon-remove-circle"></span>
                <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
            </div>
        </asp:Panel>

        <div class="row">
            <div class="col-md-3 col-md-offset-5">
                <h2>Registraci&oacute;n</h2>
            </div>
        </div>
        <asp:PlaceHolder ID="phFallBack" runat="server" Visible="false">
            <p class="text-info">
                Fallback: Hubo un problema con el servidor SMTP. Este sería el link de confirmación de registración:
                <asp:Literal ID="ltlLinkConfirmacion" runat="server"></asp:Literal>
            </p>
        </asp:PlaceHolder>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="iptEmail" class="col-sm-3 control-label">Email *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptEmail" class="form-control" runat="server" MaxLength="320" TextMode="Email" PlaceHolder="example@example.com"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="EmailRequerido" runat="server" ErrorMessage="Campo requerido  (Max 320 caracteres)" ControlToValidate="iptEmail"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="iptEmail" ErrorMessage="Formato de Email inválido"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptEmail" class="col-sm-3 control-label">Usuario *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptNickname" class="form-control" runat="server" MaxLength="10" PlaceHolder="usuario82"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="NicknameRequerido" runat="server" ErrorMessage="Campo requerido  (Max 10 caracteres)" ControlToValidate="iptNickname"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexUsuarioValido" runat="server" ValidationExpression="^[a-zA-Z0-9]*$" ControlToValidate="iptNickname" ErrorMessage="Ingrese solo números  y/o letras"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptContrasenia" class="col-sm-3 control-label">Contrase&ntilde;a *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptContrasenia" class="form-control" runat="server" MaxLength="10" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="ContraseniaRequerida" runat="server" ErrorMessage="Campo requerido  (Max 10 caracteres)" ControlToValidate="iptContrasenia"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptPreguntaSeguridad" class="col-sm-3 control-label">Pregunta Seguridad *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptPreguntaSeguridad" class="form-control" runat="server" MaxLength="300" PlaceHolder="Nombre de mi hermano"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="preguntaRequerida" runat="server" ErrorMessage="Campo requerido  (Max 50 caracteres)" ControlToValidate="iptPreguntaSeguridad"></asp:RequiredFieldValidator>
                            <p class="help-block">Esta pregunta ser&aacute; usada en caso de solicitar cambiar la contrase&tilde;a.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptRtaPreguntaSeguridad" class="col-sm-3 control-label">Respuesta Seguridad *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptRtaPreguntaSeguridad" class="form-control" MaxLength="300" runat="server" PlaceHolder="Roberto"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="rtaRequerida" runat="server" ErrorMessage="Campo requerido  (Max 50 caracteres)" ControlToValidate="iptRtaPreguntaSeguridad"></asp:RequiredFieldValidator>
                            <p class="help-block">Respuesta a la pregunta anterior.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptNombre" class="col-sm-3 control-label">Nombre *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptNombre" class="form-control" MaxLength="50" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="NombreRequerido" runat="server" ErrorMessage="Campo requerido  (Max 10 caracteres)" ControlToValidate="iptNombre"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptApellido" class="col-sm-3 control-label">Apellido *</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptApellido" class="form-control" MaxLength="50" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ForeColor="Red"  ID="ApellidoRequerido" runat="server" ErrorMessage="Campo requerido  (Max 10 caracteres)" ControlToValidate="iptApellido"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptCuitDNI" class="col-sm-3 control-label">D.N.I.</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptDNI" class="form-control js-max-length" ClientIDMode="Static" runat="server"  MaxLength="9" Max="99999999" TextMode="Number"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptCuitDNI" class="col-sm-3 control-label">C.U.I.T.</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptCUIT" class="form-control  js-max-length"  ClientIDMode="Static" runat="server" MaxLength="11" Max="99999999999" TextMode="Number"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptTelefono" class="col-sm-3 control-label">Tel&eacute;fono</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptTelefono" class="form-control" runat="server" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iptDomicilio" class="col-sm-3 control-label">Domicilio</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="iptDomicilio" class="form-control" runat="server" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ddProvincia" class="col-sm-3 control-label">Provincia</label>
                        <div class="col-sm-9">
                            <asp:DropDownList ID="ddProvincia" class="form-control slt-provincia" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem Value="0" Text="Seleccione..."></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ddCiudad" class="col-sm-3 control-label">Ciudad</label>
                        <div class="col-sm-9">
                            <asp:DropDownList ID="ddCiudad" class="form-control slt-ciudad" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="checkbox">
                        
                        <div class="col-sm-6 col-sm-offset-3">
                            <label>
                                <asp:CheckBox ID="chkTyC" CssClass="checkboxTyC" runat="server" />
                                Acepto los <a href="#" class="js-ver-tyc">T&eacute;rminos y Condiciones</a>
                                <br />
                                <span runat="server" style="color:red" id="ErrorTyC" visible="false">Debe aceptar los t&eacute;rminos y condiciones.</span>
                                <asp:CustomValidator runat="server" ID="CheckBoxRequired" ForeColor="Red" EnableClientScript="true"
                                    ClientValidationFunction="CheckBoxRequired_ClientValidate">Debe aceptar los t&eacute;rminos y condiciones.</asp:CustomValidator>
                                
                                <br />
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-6">
                            <asp:Button ID="btnRegistrarme" class="btn btn-success" runat="server" Text="Registrarme" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
