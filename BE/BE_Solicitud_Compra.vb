Public Class BE_Solicitud_Compra


    ''' <summary>
    ''' Puede ser AUTORIZADO, RECHAZADO, PENDIENTE
    ''' </summary>
    Public Property Estado As String
    Public Property FechaSolicitud As DateTime
    Public Property IdSolicitudCompra As Integer
    Public Property ListadoDetalle As List(Of BE_Detalle_Solicitud_Compra)



End Class ' BE_Solicitud_Compra