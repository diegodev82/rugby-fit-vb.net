﻿Imports BLL
Imports BE
Public Class Pagar
    Inherits ACL
    Private oBll As New BLL_Ventas

    Private _vta As BE_Venta

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.ValidarAccesoCliente()
            Me.CargarCarrito()
            rptTC.DataSource = Me.GetTCRegistradas
            rptTC.DataBind()
            Me.MostrarSaldoFavor()
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try

    End Sub

    Protected Function GetTCRegistradas() As List(Of BE_TarjetaCredito)
        Dim listado As List(Of BE_TarjetaCredito) = oBll.GetTC
        Dim bllSeg As New BLL_Seguridad
        For Each tc As BE_TarjetaCredito In listado
            tc.CodigoDesencriptado = bllSeg.Desencriptar(tc.Codigo, True)
        Next
        Return listado
    End Function

    Protected Sub CargarCarrito()
        Try
            Me._vta = oBll.GetCarrito(MyBase.GetIdCliente)
            Dim total As Double = 0
            iptCodigoVta.Value = Me._vta.IdVenta
            For Each item As BE_Detalle_Venta In Me._vta.Items
                total = total + (item.Cantidad * item.Monto)
            Next
            If total <= 0 Then
                Response.Redirect("Productos.aspx")
            End If
            Me._vta.MontoTotal = total
            ltlTotalVta.Text = total.ToString
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub MostrarSaldoFavor()
        Try
            Dim bllCli As New BLL_Clientes
            Dim cliente As BE_Cliente = bllCli.GetPorId(MyBase.GetIdCliente())
            If cliente.SaldoFavor > 0 Then
                phSaldoFavor.Visible = True
                ltlSaldoFavor.Text = cliente.SaldoFavor.ToString
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub btnPagar_Click(sender As Object, e As EventArgs) Handles btnPagar.Click
        Try
            If Me._ValidarPago() = True Then
                Dim oBllFact As New BLL_Facturas
                Dim listadoVtas As New List(Of BE_Venta)
                ' Dim vtaActual As BE_Venta = oBll.GetCarrito(MyBase.GetIdCliente())
                Dim vtaActual As BE_Venta = Me._vta
                listadoVtas.Add(vtaActual)
                Dim idFactura As Integer = oBllFact.Facturar(listadoVtas)
                If idFactura > 0 Then
                    Me._ProcesarPago(idFactura)
                    Dim finalizada As Boolean = oBll.FinalizarVenta(vtaActual)
                    If finalizada Then
                        Response.Redirect("/FichaOpinion.aspx?idFc=" + idFactura.ToString + "&facturado=ok", False)
                        Return
                        'Response.Redirect("/Factura.aspx?id=" + idFactura.ToString + "&facturado=ok")
                    End If

                End If
            Else
                phErrorValidacionPago.Visible = True
            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try

    End Sub

    Protected Function _ProcesarPago(ByVal idFacturaCancelada As Integer) As Boolean
        Try
            phErrorValidacionPago.Visible = False
            If Me._ValidarPago = False Then
                phErrorValidacionPago.Visible = True
                Return False
            End If
            Dim bllFc As New BLL_Facturas

            Dim bllCli As New BLL_Clientes
            Dim cliente As BE_Cliente = bllCli.GetPorId(MyBase.GetIdCliente())

            'Validamos si va a usar el saldo a favor.
            If chkUsarNC.Checked = True Then


                If cliente.SaldoFavor >= Me._vta.MontoTotal Then
                    'Alcanza justo el saldo para pagar la compra. Cancelamos la factura con la NC
                    Dim pago As New BE_Pago
                    pago.IdCliente = cliente.IdCliente
                    pago.IdFacturaCancela = idFacturaCancelada
                    pago.EsParcial = False
                    pago.Monto = Me._vta.MontoTotal
                    pago.TipoPago = BE_Pago.PAGO_CON_NC
                    pago.TipoMovimiento = BE_Pago.SALIDA
                    bllFc.AgregarPago(pago)

                    'Actualizo el saldo a favor del cliente, si tenía justo queda en 0, si tenía más descuenta de lo que compró
                    Dim montoUsado As Decimal = Me._vta.MontoTotal
                    bllCli.DescontarSaldoFavor(cliente.IdCliente, montoUsado)
                Else
                    'No alcanza el saldo, entonces tenemos que usar la TC para pagar lo que falta
                    'Registro el pago con NC (lo que tenía a favor)
                    Dim pagoNC As New BE_Pago
                    pagoNC.IdCliente = cliente.IdCliente
                    pagoNC.IdFacturaCancela = idFacturaCancelada
                    pagoNC.EsParcial = True
                    pagoNC.Monto = cliente.SaldoFavor 'solo el saldo que tenía a favor
                    pagoNC.TipoPago = BE_Pago.PAGO_CON_NC
                    pagoNC.TipoMovimiento = BE_Pago.SALIDA
                    bllFc.AgregarPago(pagoNC)

                    'Actualizo el saldo a favor del cliente, si tenía justo queda en 0, si tenía más descuenta de lo que compró
                    Dim saldoUsado As Decimal = cliente.SaldoFavor
                    bllCli.DescontarSaldoFavor(cliente.IdCliente, saldoUsado)
                    'Registro el pago con TC
                    Dim pagoTC As New BE_Pago
                    pagoTC.IdCliente = cliente.IdCliente
                    pagoTC.IdFacturaCancela = idFacturaCancelada
                    pagoTC.EsParcial = True
                    pagoTC.Monto = Me._vta.MontoTotal - cliente.SaldoFavor 'lo que falta del monto total - el saldo a favor que usó
                    pagoTC.TipoPago = BE_Pago.PAGO_CON_TC
                    pagoTC.TipoMovimiento = BE_Pago.SALIDA
                    bllFc.AgregarPago(pagoTC)
                End If
            Else
                'Va a usar solo TC para pagar el total
                Dim pagoTC As New BE_Pago
                pagoTC.IdFacturaCancela = idFacturaCancelada
                pagoTC.IdCliente = cliente.IdCliente
                pagoTC.EsParcial = True
                pagoTC.Monto = Me._vta.MontoTotal 'El monto total de la venta
                pagoTC.TipoPago = BE_Pago.PAGO_CON_TC
                pagoTC.TipoMovimiento = BE_Pago.SALIDA
                bllFc.AgregarPago(pagoTC)
            End If
            Return True
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try

        Return True
    End Function


    Protected Function _ValidarPago() As Boolean
        Try
            Dim usaTC As Boolean = chkPagarConTC.Checked
            Dim usaSaldo As Boolean = chkUsarNC.Checked
            If usaTC = False And usaSaldo = False Then
                ltlMsjErrorPago.Text = "Seleccione un tipo de pago, por favor"
                Return False
            End If
            Dim bllCli As New BLL_Clientes
            Dim cliente As BE_Cliente = bllCli.GetPorId(MyBase.GetIdCliente())
            Dim saldoFavor As Decimal = cliente.SaldoFavor
            Dim montoVta As Decimal = Me._vta.MontoTotal
            If usaSaldo = True And usaTC = False Then
                If saldoFavor < montoVta Then
                    ltlMsjErrorPago.Text = "El monto a favor no es suficiente para saldar el monto de la venta"
                    Return False
                Else
                    'alcanza para pagar con el saldo a favor. Destildo la opcion del pago con tarjeta porque está al pedo
                    chkPagarConTC.Checked = False
                    usaTC = False
                End If
            End If
            If usaTC = True Then
                If Me._ValidarDatosTC = False Then
                    ltlMsjErrorPago.Text = "Verifique los datos de la tarjeta, por favor."
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Protected Function _ValidarDatosTC() As Boolean
        Try
            error_codigo.Visible = False
            error_nro.Visible = False
            error_fecha.Visible = False
            Dim oValidador As New ValidadorTC
            Dim TcValida As Boolean = True
            'Valido que estén cargados bien todos los datos
            If visa.Checked = False And mastercard.Checked = False And amex.Checked = False Then
                TcValida = False
            End If
            Dim datosCompletos As Boolean = True
            If iptCodigoSeg.Text.Length < 4 Then
                error_codigo.Visible = True
                datosCompletos = False
            End If
            If iptNumeroTC.Text.Length < 13 Or iptNumeroTC.Text.Length > 16 Then
                error_nro.Visible = True
                datosCompletos = False
            End If
            If iptFechaExpiracion.Text.Length < 6 Then
                error_fecha.Visible = True
                datosCompletos = False
            End If
            If datosCompletos = False Then
                Return False
            End If
            'Estan los datos completos, verifico la validez de los mismos
            If visa.Checked = True Then
                oValidador.SetTipoTcSeleccionada(ValidadorTC.VISA)
            End If
            If mastercard.Checked = True Then
                oValidador.SetTipoTcSeleccionada(ValidadorTC.MASTERCARD)
            End If
            If amex.Checked = True Then
                oValidador.SetTipoTcSeleccionada(ValidadorTC.AMEX)
            End If
            'Validar nro
            If oValidador.ValidarNumero(iptNumeroTC.Text) = False Then
                error_nro.Visible = True
                TcValida = False
            Else
                'Si el número es válido y está registrada la TC..
                'Validar codigo
                Dim bllSeg As New BLL_Seguridad
                Dim codEnc As String = bllSeg.EncriptarNumero(iptCodigoSeg.Text, True)
                If oValidador.ValidarCodigo(codEnc) = False Then
                    error_codigo.Visible = True
                    TcValida = False
                End If
                'Validar Fecha
                If oValidador.ValidarFechaExpiracion(iptFechaExpiracion.Text) = False Then
                    error_fecha.Visible = True
                    TcValida = False
                End If
            End If

            Return TcValida

        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

End Class