SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_CrearCliente
	@Nombre nchar(320),
	@Apellido nchar(320),
	@Email nchar(320),
	@Nickname nchar(320),
	@Contrasenia nchar(320),
	@Telefono nchar(320),
	@IdCiudad nchar(320),
	@Domicilio nchar(320),
	@Cuit nchar(320),	
	@DNI nchar(320),
	@Origen nchar(320),
	@HashRegistracion nchar (50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into dbo.cliente 
	(nombre, apellido, email, nickname, contrasenia, telefono, id_ciudad, domicilio, eliminado, cuit, dni, origen, puntos_acumulados, hash_registracion, activo)
	values
	(@Nombre, @Apellido, @Email,@Nickname,@Contrasenia, @Telefono, @IdCiudad,@Domicilio,0, @Cuit,@DNI, @Origen, 0, @HashRegistracion, 0);
	SELECT SCOPE_IDENTITY();
END
GO
