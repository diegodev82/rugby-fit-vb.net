﻿$(document).ready(function () {
    $("#btn_votar").on("click.VotarEncuesta", function (e) {
        e.preventDefault();
        //validar cookie
        if (sessionStorage.getItem("ya_voto") == "1") {
            alert("Disculpe, usted ya votó en esta encuesta. Sólo se permite un voto por persona");
            return false;
        }
        var rta_selecccionada = $(".rta_encuesta:checked");
        if (rta_selecccionada.length == 0) {
            alert("Debe seleccionar una respuesta")
            return false;
        }

        var voto = rta_selecccionada.val();
        var id_encuesta = rta_selecccionada.data("id_encuesta");
        $.ajax(
            {
                "url": "/AjaxUtils.asmx/Votar",
                data: "{'id_encuesta':'" + id_encuesta + "', 'voto':'" + voto + "'}",
                contentType: 'application/json; utf-8',
                dataType: 'json',
                type: 'post',
                success: function (data) {

                    if (data.d == true) {
                        sessionStorage.setItem("ya_voto", "1");

                        verResultados();
                        //Guardar cookie
                        //mostrar resultados
                    }
                },
            }
        );
    });
});

function verVotacion() {
    if (sessionStorage.getItem("ya_voto") == "1") {
        alert("Disculpe, usted ya votó en esta encuesta. Sólo se permite un voto por persona");
        return false;
    }
    $("#panelVotacion").show();
    $("#panelRtdos").hide();
}

function verResultados(idEnc) {
    $("#panelVotacion").hide();
    $("#panelRtdos").show();
    $.ajax(
    {
        "url": "/AjaxUtils.asmx/VerResultados",
        data: "{}",
        contentType: 'application/json; utf-8',
        dataType: 'json',
        type: 'post',
        success: function (data) {

            if (data.d != null) {
                var rtdos = data.d;
                
                for (var index in rtdos) {
                    var idRta = rtdos[index].IdRespuesta;
                    var porc = rtdos[index].Porcentaje.toFixed(2);
                    $("#porcentaje_"+idRta).html(porc +"%");
                    $("#porcentaje_" + idRta).css("width", Math.ceil(porc) + "%");
                    if (!puedeVotar()) {
                        $("#linkVotar").hide();
                    }
                    
                }
                
            }
        },
    }
);
}

function puedeVotar() {
return sessionStorage.getItem("ya_voto") != "1"
}