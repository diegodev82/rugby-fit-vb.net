﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminProductosForm.aspx.vb" Inherits="TFI_RugbyFit.AdminProductosForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
    <script>console.log("JS ACTIVADO");</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <% If Request.QueryString("guardado") = "ok" Then%>
           <div class="alert alert-success">
               <p><span class="glyphicon glyphicon-thumbs-up" ></span> El producto fue guardado con &eacute;xito</p>
           </div> 
        <% End If%>
    <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
        <div class="alert alert-danger">
            <asp:Literal ID="msjErrorFK" Visible="false"  runat="server" Text="El producto no puede ser eliminado ya que está asignado a una venta o a una compra"></asp:Literal>
            <asp:Literal ID="msjErrorDB" Visible="false"  runat="server" Text="Se produjo un error en el sistema y el producto no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
        </div> 
    </asp:PlaceHolder>
    <div class="form-group botonera clearfix">
        <a href="AdminProductosList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <a href="AdminProductosForm.aspx" class="btn btn-default pull-left">Nuevo</a>
        <asp:Button ID="btnEliminar" runat="server" CssClass="btn btn-danger pull-right" Visible="false" Text="Eliminar" />
    </div>
    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Nombre *:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptNombre" runat="server"  ClientIDMode="Static" required="required" CssClass="form-control" MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="iptNombre" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Descripci&oacute;n *:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptDescripcion" ClientIDMode="Static" required="required" runat="server" MaxLength="50" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="red" ControlToValidate="iptDescripcion" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Descripci&oacute;n corta *:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptDescripcionCorta" runat="server"  ClientIDMode="Static" required="required"  CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Categor&iacute;a *:</label>
            </div>
            <div class='col-md-11'>
                <asp:DropDownList ID="ddCategoria" runat="server" CssClass="form-control" ClientIDMode="Static" required="required" AutoPostBack="True" AppendDataBoundItems="True">
                    <asp:ListItem Value="">Seleccione una categor&iacute;a</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="red" ControlToValidate="ddCategoria" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Proveedor *:</label>
            </div>
            <div class='col-md-11'>
                <asp:DropDownList ID="ddProveedor" runat="server" CssClass="form-control" ClientIDMode="Static" required="required" AutoPostBack="True" AppendDataBoundItems="True">
                    <asp:ListItem Value="">Seleccione un proveedor</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="red" ControlToValidate="ddProveedor" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Ofrece puntos:</label>
            </div>
            <div class='col-md-11'>
                <asp:DropDownList ID="ddOfrecePuntos" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="1">Sí</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Es canjeable:</label>
            </div>
            <div class='col-md-11'>
                <asp:DropDownList ID="ddEsCanjeable" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="1">Sí</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Puntos ofrecidos *:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptPuntosOfrecidos" runat="server"  ClientIDMode="Static" required="required" Max="10000" CssClass="form-control" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="red" ControlToValidate="iptPuntosOfrecidos" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Puntos requeridos *:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptPuntosRequeridos" runat="server"  ClientIDMode="Static" required="required" Max="10000" CssClass="form-control" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="red" ControlToValidate="iptPuntosRequeridos" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Stock:</label>
            </div>
            <div class='col-md-11'>                
                <asp:TextBox ID="iptStock" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ForeColor="red" ControlToValidate="iptStock" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Stock m&iacute;nimo*:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptStockMinimo" runat="server" CssClass="form-control"  ClientIDMode="Static" required="required" Max="1000" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="red" ControlToValidate="iptStockMinimo" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Precio *:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptPrecio" runat="server" CssClass="form-control"  ClientIDMode="Static" required="required" Max="100000" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="red" ControlToValidate="iptPrecio" runat="server" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
     <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Foto:</label>
            </div>
            <div class='col-md-11'>
                <asp:FileUpload ID="iptFoto" CssClass="form-control" runat="server" />
                <asp:Image ID="imgFotoSubida" CssClass="img-responsive" Width="200" Height="200" Visible="false" runat="server" />
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Activo:</label>
            </div>
            <div class='col-md-11'>
                <asp:DropDownList ID="ddActivo" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="1">Sí</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class="form-group botonera clearfix">
        <a href="AdminProductosList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <input type='reset' class="btn btn-default  pull-left" name='guardar' value='Cancelar' />
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar" />
    </div>
</asp:Content>
