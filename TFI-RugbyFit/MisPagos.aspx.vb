﻿Imports BLL
Imports BE
Public Class MisPagos
    Inherits ACL
    Private oBll As New BLL_Facturas
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAccesoCliente()
        Me._CargarData()
    End Sub

    Protected Sub _CargarData()
        Dim pagos As List(Of BE_Pago) = oBll.GetPagosPorCliente(MyBase.GetIdCliente())
        rptPagos.DataMember = "BE_Pago"
        rptPagos.DataSource = pagos
        rptPagos.DataBind()
    End Sub

End Class