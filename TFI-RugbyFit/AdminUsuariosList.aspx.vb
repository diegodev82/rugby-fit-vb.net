﻿Imports BLL
Imports BE
Public Class AdminUsuariosList
    Inherits ACL
    Dim oBll As New BLL_Usuarios
    Const COD_PERMISO = "MOD_SEGURIDAD"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.LlenarFiltroFamilias()
            Me.LlenarDgv()
        End If
    End Sub
    Protected Sub LlenarFiltroFamilias()
        Dim oBllFam As New BLL_Familias
        Dim listadoFam As List(Of BE_Familia) = oBllFam.Listar()
        ddFiltroGrupo.DataTextField = "Nombre"
        ddFiltroGrupo.DataValueField = "IdFamilia"
        ddFiltroGrupo.DataSource = listadoFam
        ddFiltroGrupo.DataBind()
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Usuario) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count

        DgvUsuarios.AutoGenerateColumns = False
        DgvUsuarios.DataSource = registros
        DgvUsuarios.DataBind()
    End Sub



    Protected Sub DgvUsuarios_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvUsuarios.RowEditing
        Dim row As GridViewRow = DgvUsuarios.Rows(e.NewEditIndex)
        Dim IdProducto As String = row.Cells(0).Text

        Response.Redirect("/AdminUsuariosForm.aspx?id=" + IdProducto)
    End Sub

    Protected Sub DgvUsuarios_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvUsuarios.RowUpdating
        Dim a As Integer = 0

    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvUsuarios_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvUsuarios.PageIndexChanging
        DgvUsuarios.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvUsuarios_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvUsuarios.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroNombre.Text IsNot Nothing Then
            filtros.Add("@Nombre", filtroNombre.Text)
        End If
        Dim idCat As Integer = CType(ddFiltroGrupo.SelectedValue, Integer)
        If idCat > 0 Then
            filtros.Add("@IdFamilia", idCat)
        End If
        If IsNumeric(filtroCodigo.Text) Then
            Dim idUsuario As Integer = CType(filtroCodigo.Text, Integer)
            If idUsuario > 0 Then
                filtros.Add("@IdUsuario", idUsuario)
            End If
        End If

        Return filtros
    End Function

    Protected Sub DgvUsuarios_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles DgvUsuarios.RowDeleting
        Dim row As GridViewRow = DgvUsuarios.Rows(e.RowIndex)
        Dim idUsuario As String = row.Cells(0).Text
        If IsNumeric(idUsuario) Then
            If idUsuario = MyBase.GetIdUsuario Then
                RtaErrorEliminar.Visible = True
                msjErrorEsMiUsuario.Visible = True

                Return
            End If
            Dim objUsu As New BE_Usuario With {.IdUsuario = idUsuario}
            Dim eliminado As Integer = oBll.Eliminar(objUsu)
            If eliminado = 1 Then
                Response.Redirect("AdminUsuariosList.aspx?eliminado=ok")

            ElseIf eliminado = -1 Then
                RtaErrorEliminar.Visible = True
                msjErrorFK.Visible = True
            ElseIf eliminado = -2 Then
                RtaErrorEliminar.Visible = True
                msjErrorEsAdmin.Visible = True
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub DgvUsuarios_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles DgvUsuarios.RowDataBound
        Dim cssClass As String = ""
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim activo As String = e.Row.Cells(4).Text
            If activo = True Then
                e.Row.Cells(4).Text = "<span class='glyphicon glyphicon-ok text-success'></span>"
            Else
                e.Row.Cells(4).Text = "<span class='glyphicon glyphicon-remove text-danger'></span>"
            End If
            ' e.Row.CssClass = impac.ToLower

        End If
    End Sub

    Public Sub RenderActivo(e As GridViewRowEventArgs)

    End Sub
End Class