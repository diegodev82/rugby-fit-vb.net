SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_ValidarProductoExistente
	@Nombre nchar(320),
	@IdCategoria Integer,
	@IdProveedor Integer

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TOP 1 * FROM producto WHERE nombre =@Nombre AND id_categoria = @IdCategoria AND id_proveedor = @IdProveedor
END
GO
