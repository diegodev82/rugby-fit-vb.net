﻿
Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
'http://www.aspsnippets.com/Articles/Create-RDLC-Report-using-Stored-Procedure-in-ASPNet-with-C-and-VBNet.aspx
Public Class MiReporteNuevo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ReportViewer1.ProcessingMode = ProcessingMode.Local
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/MiReporte.rdlc")
            Dim dsMio As MiDataset = GetData()
            Dim datasource As New ReportDataSource("UnDs", dsMio.Tables(0))
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.DataSources.Add(datasource)
        End If
    End Sub

    Private Function GetData() As MiDataset
        Dim oBll As New BLL.BLL_Reportes
        Dim miDs As New MiDataset
        Dim dt As DataTable = oBll.GetReporteFacturacion
        For Each dr As DataRow In dt.Rows
            miDs.DataTable1.AddDataTable1Row(dr("IdFactura"), dr("Monto"), dr("Fecha"))
        Next

        Return miDs

        'Dim conString As String = "Data Source=TELAM\MSSQLSERVER2012;Initial Catalog=RugbyFit;Integrated Security=True"
        'Dim cmd As New SqlCommand("pa_ReporteFacturacion")
        'Using con As New SqlConnection(conString)
        '    Using sda As New SqlDataAdapter()
        '        cmd.Connection = con
        '        cmd.CommandType = CommandType.StoredProcedure
        '        sda.SelectCommand = cmd
        '        Using dsCustomers As New MiDataset()
        '            sda.Fill(dsCustomers, "DataTable1")
        '            Return dsCustomers
        '        End Using
        '    End Using
        'End Using
    End Function
End Class