Imports BE
Imports DAL


Public Class MPP_Ofertas


    ''' 
    ''' <param name="objOferta"></param>
    Public Function Crear(ByVal objOferta As BE_Oferta) As BE.BE_Oferta
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objOferta"></param>
    Public Function Editar(ByVal objOferta As BE_Oferta) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objOferta"></param>
    Public Function Eliminar(ByVal objOferta As BE_Oferta) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Oferta)
        Listar = Nothing
    End Function

    ''' 
    ''' <param name="objOferta"></param>
    Public Function ValidarExistente(ByVal objOferta As BE_Oferta) As Boolean
        ValidarExistente = False
    End Function


End Class ' MPP_Ofertas

