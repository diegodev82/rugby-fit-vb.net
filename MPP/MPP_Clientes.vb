Imports BE
Imports DAL
Imports System.Security.Cryptography
Imports System.Text

Public Class MPP_Clientes

    Const ID_FAMILIA_CLIENTE = 2
    Private oDatos As SQLHelper
    Private ds As DataSet
    Public Sub New()
        oDatos = SQLHelper.GetInstance
    End Sub

    Public Function DescontarSaldoFavor(ByVal idCliente As Integer, ByVal monto As Decimal) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", idCliente)
        hdatos.Add("@Monto", monto)
        Dim incrementado As Boolean = oDatos.Escribir("pa_DescontarSaldoFavor", hdatos)
        Return incrementado
    End Function

    Public Function IncrementarSaldoFavor(ByVal idCliente As Integer, ByVal monto As Decimal) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", idCliente)
        hdatos.Add("@Monto", monto)
        Dim incrementado As Boolean = oDatos.Escribir("pa_IncrementarSaldoFavor", hdatos)
        Return incrementado
    End Function

    Public Function GetPorId(ByVal id As Integer, Optional ByVal activo As Boolean = True) As BE_Cliente
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", id)
        hdatos.Add("@Activo", activo)
        ds = oDatos.Leer("pa_GetClientePorId", hdatos)
        Dim cliente As BE_Cliente = Nothing
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            cliente = New BE_Cliente
            cliente.IdCliente = dr("id_cliente")
            cliente.Nombre = dr("nombre")
            cliente.Apellido = dr("apellido")
            cliente.Email = dr("email")
            cliente.Nickname = dr("nickname")
            cliente.Contrasenia = dr("contrasenia")
            cliente.Telefono = dr("telefono")
            cliente.Domicilio = dr("domicilio")
            cliente.CUIT = dr("cuit")
            cliente.DNI = dr("dni")
            cliente.SaldoFavor = IIf(IsDBNull(dr("saldo_a_favor")), 0, dr("saldo_a_favor"))
            If IsDBNull(dr("ciudad")) Then
                cliente.Ciudad = New BE_Ciudad With {.IdCiudad = 0, .Nombre = "", .Provincia = New BE_Provincia With {.IdProvincia = 0}}
            Else
                cliente.Ciudad = New BE_Ciudad With {.IdCiudad = dr("id_ciudad"), .Nombre = dr("ciudad"), .Provincia = New BE_Provincia With {.IdProvincia = dr("id_provincia")}}
            End If

            cliente.PuntosAcumulados = dr("puntos_acumulados")
            cliente.HashRegistracion = dr("hash_registracion")
            cliente.PreguntaSeguridad = dr("pregunta_seguridad")
            cliente.RegistracionConfirmada = dr("registracion_confirmada")
            cliente.RespuestaSeguridad = dr("respuesta_seguridad")
            cliente.Origen = dr("origen")
            cliente.Activo = dr("activo")
        End If
        Return cliente
    End Function


    ''' 
    ''' <param name="IdCliente"></param>
    Public Function ActivarUsuario(ByVal IdCliente As Integer) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", IdCliente)
        Dim activado As Boolean = oDatos.Escribir("pa_ActivarCliente", hdatos)
        Return activado
    End Function

    Public Function ResetContrasenia(ByVal email As String, ByVal nuevaPwd As String) As Boolean

        Dim hdatos As New Hashtable
        hdatos.Add("@Email", email)
        hdatos.Add("@Contrasenia", nuevaPwd)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ResetearContraseniaUsuario", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objCliente"></param>
    Public Function Crear(ByVal objCliente As BE_Cliente) As Object
        Dim hdatos As New Hashtable
        objCliente.HashRegistracion = Me.GenerarHashUnico()
        hdatos.Add("@Nickname", objCliente.Nickname)
        hdatos.Add("@Email", objCliente.Email)
        hdatos.Add("@Nombre", objCliente.Nombre)
        hdatos.Add("@Apellido", objCliente.Apellido)
        hdatos.Add("@Domicilio", objCliente.Domicilio)
        hdatos.Add("@IdCiudad", objCliente.Ciudad.IdCiudad)
        hdatos.Add("@Contrasenia", objCliente.Contrasenia)
        hdatos.Add("@Telefono", objCliente.Telefono)
        hdatos.Add("@Cuit", objCliente.CUIT)
        hdatos.Add("@DNI", objCliente.DNI)
        hdatos.Add("@HashRegistracion", objCliente.HashRegistracion)
        hdatos.Add("@Origen", objCliente.Origen)
        hdatos.Add("@Activo", 0)


        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim IdCliente As Integer = oDatos.Insertar("pa_CrearCliente", hdatos)
        If IdCliente > 0 Then
            objCliente.IdCliente = IdCliente
            Dim IdUsuario As Integer = Me._CrearUsuario(objCliente)
            If IdUsuario > 0 Then
                AsignarFamilia(IdUsuario)
                Return objCliente
            End If

        End If
        Return False
    End Function

    Private Function AsignarFamilia(ByVal IdUsuario As Integer) As Integer
        Dim oMppUsuario As New MPP_Usuarios
        Dim IdInsertado As Integer = oMppUsuario.AgregarFamilia(ID_FAMILIA_CLIENTE, IdUsuario)
        Return IdInsertado
    End Function

    Private Function _CrearUsuario(ByVal objCliente As BE_Cliente) As Integer
        Dim hdatos As New Hashtable
        objCliente.HashRegistracion = Me.GenerarHashUnico()
        hdatos.Add("@Nombre", objCliente.Nombre)
        hdatos.Add("@Apellido", objCliente.Apellido)
        hdatos.Add("@Nickname", objCliente.Nickname)
        hdatos.Add("@Contrasenia", objCliente.Contrasenia)
        hdatos.Add("@Email", objCliente.Email)
        hdatos.Add("@Telefono", objCliente.Telefono)
        hdatos.Add("@DNI", objCliente.DNI)
        hdatos.Add("@cii", 0)
        hdatos.Add("@Activo", objCliente.Activo)
        hdatos.Add("@IdCliente", objCliente.IdCliente)
        hdatos.Add("@PreguntaSeguridad", objCliente.PreguntaSeguridad)
        hdatos.Add("@RespuestaSeguridad", objCliente.RespuestaSeguridad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim IdInsertado As Object = oDatos.Insertar("pa_CrearUsuario", hdatos)
        If IsNumeric(IdInsertado) = True Then
            Return IdInsertado
        End If
        Return 0
        Return False
    End Function

    ''' 
    ''' <param name="IdCliente"></param>
    ''' <param name="cant"></param>
    Public Function DescontarPuntosBeneficio(ByVal IdCliente As Integer, ByVal cant As Integer) As Integer
        DescontarPuntosBeneficio = 0
    End Function

    ''' 
    ''' <param name="objCliente"></param>
    Public Function Editar(ByVal objCliente As BE_Cliente) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdCliente", objCliente.IdCliente)
        hdatos.Add("@Nombre", objCliente.Nombre)
        hdatos.Add("@Apellido", objCliente.Apellido)
        hdatos.Add("@Email", objCliente.Email)
        hdatos.Add("@Telefono", objCliente.Telefono)
        hdatos.Add("@Domicilio", objCliente.Domicilio)
        hdatos.Add("@IdCiudad", objCliente.Ciudad.IdCiudad)

        hdatos.Add("@Cuit", objCliente.CUIT)
        hdatos.Add("@DNI", objCliente.DNI)
        

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ModificarCliente", hdatos)
        Return guardado

    End Function

    ''' 
    ''' <param name="objCliente"></param>
    Public Function Eliminar(ByVal objCliente As BE_Cliente) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="objPtoClie"></param>
    Public Function GetPuntosBeneficioObtenido(ByVal idCliente As Integer) As List(Of BE.BE_Punto_Cliente)
        Try
            Dim hdatos As New Hashtable
            hdatos.Add("@IdCliente", idCliente)
            Dim listado As New List(Of BE_Punto_Cliente)
            Dim ds As New DataSet
            Dim oDatos As SQLHelper = SQLHelper.GetInstance
            ds = oDatos.Leer("pa_GetPuntosCliente", hdatos)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        Dim pcli As New BE_Punto_Cliente
                        pcli.Cantidad = IIf(IsDBNull(dr("cantidad")), 0, dr("cantidad"))
                        pcli.Cliente = Nothing
                        pcli.Factura = New BE_Factura With {.IdFactura = IIf(IsDBNull(dr("id_factura")), 0, dr("id_factura"))}
                        pcli.FechaHora = IIf(IsDBNull(dr("fecha_hora")), "", dr("fecha_hora"))
                        pcli.IdPuntoCliente = IIf(IsDBNull(dr("id_punto_cliente")), 0, dr("id_punto_cliente"))
                        pcli.Producto = New BE_Producto With {.IdProducto = IIf(IsDBNull(dr("id_producto")), 0, dr("id_producto")), .Nombre = IIf(IsDBNull(dr("producto")), "producto #12", dr("producto"))}
                        listado.Add(pcli)
                    Next
                End If
                Return listado
            End If
        Catch ex As Exception
            Return Nothing
        End Try        
        Return Nothing
    End Function

    ''' 
    ''' <param name="IdCliente"></param>
    ''' <param name="cant"></param>
    Public Function IncremetarPuntosBeneficio(ByVal IdCliente As Integer, ByVal cant As Integer) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@IdCliente", IdCliente)
        hdatos.Add("@Cantidad", cant)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_IncrementarPuntosBeneficios", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Cliente)
        Listar = Nothing
    End Function

    ''' 
    ''' <param name="objPuntoCliente"></param>
    Public Function RegistrarAcumulacionPuntos(ByVal objPuntoCliente As BE_Punto_Cliente) As BE_Punto_Cliente
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProducto", objPuntoCliente.Producto.IdProducto)
        hdatos.Add("@IdCliente", objPuntoCliente.Cliente.IdCliente)
        hdatos.Add("@IdFactura", objPuntoCliente.Factura.IdFactura)
        hdatos.Add("@Cantidad", objPuntoCliente.Cantidad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_RegistrarAcumulacionPuntos", hdatos)
        If InsertedId > 0 = True Then
            objPuntoCliente.IdPuntoCliente = InsertedId
        End If
        Return objPuntoCliente
    End Function

    ''' 
    ''' <param name="objPtsClie"></param>
    Public Function RegistrarDescuentoPuntosBeneficio(ByVal objPtsClie As BE_Punto_Cliente) As Boolean
        RegistrarDescuentoPuntosBeneficio = False
    End Function

    ''' 
    ''' <param name="objCliente"></param>
    Public Function ValidarExistente(ByVal objCliente As BE_Cliente) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Nickname", objCliente.Nickname)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarUsuarioExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function

    ''' 
    ''' <param name="objCliente"></param>
    Public Function ValidarExistentePorMail(ByVal objCliente As BE_Cliente) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Email", objCliente.Email)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarUsuarioExistentePorMail", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function

    ''' 
    ''' <param name="linkConfirmacion"></param>
    Public Function ValidarLinkConfirmacion(ByVal hash As String, ByVal email As String) As Integer

        Dim hdatos As New Hashtable
        hdatos.Add("@Email", email)
        hdatos.Add("@HashRegistracion", hash)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarHashRegistracion", hdatos)
        If ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
            Dim item As DataRow = ds.Tables(0).Rows(0)
            Return item("id_cliente")
        End If
        Return 0
    End Function



    Public Function GenerarHashUnico() As String
        ' http://www.codeproject.com/Articles/14403/Generating-Unique-Keys-in-Net
        ' Return Guid.NewGuid().ToString().GetHashCode().ToString("x") 'Menos colisiones pero tarda mas
        Return DateTime.Now.ToString().GetHashCode().ToString("x")
    End Function

End Class
