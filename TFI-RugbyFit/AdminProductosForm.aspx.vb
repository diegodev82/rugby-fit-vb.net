﻿Imports BLL
Imports BE
Public Class AdminProductosForm
    Inherits ACL
    Const COD_PERMISO = "MOD_MKT"
    Dim oBllPrd As New BLL_Productos
    Const RUTA_FOTOS = "/assets/media/productos/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.CargarData()
            Me.LlenarDropdownCategoria()
            Me.LlenarDropdownProveedor()
        End If

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        Dim objProducto As New BE_Producto
        objProducto.IdProducto = IIf(iptCodigo.Value <> "", iptCodigo.Value, 0)
        objProducto.Nombre = iptNombre.Text
        objProducto.Descripcion = iptDescripcion.Text
        objProducto.DescripcionCorta = iptDescripcionCorta.Text
        objProducto.Categoria = New BE_Categoria With {.IdCategoria = ddCategoria.SelectedValue}
        objProducto.LinkImagen = "" '@TODO: implementar subida
        objProducto.EsCanjeable = ddEsCanjeable.SelectedValue
        objProducto.OfrecePuntos = ddOfrecePuntos.SelectedValue
        objProducto.PuntosOfrecidos = iptPuntosOfrecidos.Text
        objProducto.PuntosRequeridos = iptPuntosRequeridos.Text
        objProducto.Proveedor = New BE_Proveedor With {.IdProveedor = ddProveedor.SelectedValue}
        objProducto.Stock = iptStock.Text
        objProducto.StockMinimo = iptStockMinimo.Text
        objProducto.Precio = iptPrecio.Text
        objProducto.Activo = ddActivo.SelectedValue
        Dim ruta_foto As String = SubirArchivo()
        If Not ruta_foto Is Nothing Then
            objProducto.LinkImagen = ruta_foto
        Else
            objProducto.LinkImagen = imgFotoSubida.ImageUrl
        End If

        Dim productoGuardado As BE_Producto = Nothing
        If objProducto.IdProducto > 0 Then
            Dim guardado As Boolean = oBllPrd.Editar(objProducto)
            If guardado = True Then
                productoGuardado = objProducto
            End If
        Else
            productoGuardado = oBllPrd.Crear(objProducto)
        End If
        If productoGuardado Is Nothing Then
            Dim b As Integer = 0 'Mostrar mensaje de producto duplicado
        End If
        If productoGuardado.IdProducto > 0 Then
            Response.Redirect("AdminProductosForm.aspx?guardado=ok&id=" + productoGuardado.IdProducto.ToString)
        Else
            Dim b As Integer = 1 'Mostrar mensaje de error de creacion
        End If
    End Sub
    Protected Sub LlenarDropdownCategoria()
        Dim oBllCate As New BLL_Categorias
        Dim listadoCat As List(Of BE_Categoria) = oBllCate.Listar()
        ddCategoria.DataTextField = "Nombre"
        ddCategoria.DataValueField = "IdCategoria"
        ddCategoria.DataSource = listadoCat
        ddCategoria.DataBind()
    End Sub

    Protected Sub LlenarDropdownProveedor()
        Dim oBllProve As New BLL_Proveedores
        Dim listadoProve As List(Of BE_Proveedor) = oBllProve.Listar()
        ddProveedor.DataTextField = "Nombre"
        ddProveedor.DataValueField = "IdProveedor"
        ddProveedor.DataSource = listadoProve
        ddProveedor.DataBind()
    End Sub
    Protected Sub CargarData()
        'seteo el maxlength para el textarea porque no te deja hacerlo por html
        Try
            iptDescripcion.Attributes("MaxLength") = 300
            iptDescripcionCorta.Attributes("MaxLength") = 150
        Catch ex As Exception

        End Try

        If Request.QueryString("id") IsNot Nothing Then
            If Not IsNumeric(Request.QueryString("id")) Then
                Return
            End If
            Dim idProducto As Integer = CType(Request.QueryString("id"), Integer)
            Dim dataProducto As BE_Producto = oBllPrd.GetPorId(idProducto)
            If dataProducto IsNot Nothing Then
                iptCodigo.Value = dataProducto.IdProducto
                iptNombre.Text = dataProducto.Nombre
                iptDescripcion.Text = dataProducto.Descripcion
                iptDescripcionCorta.Text = dataProducto.DescripcionCorta
                iptPrecio.Text = dataProducto.Precio
                iptPuntosOfrecidos.Text = dataProducto.PuntosOfrecidos
                iptPuntosRequeridos.Text = dataProducto.PuntosRequeridos
                iptStockMinimo.Text = dataProducto.StockMinimo
                iptStock.Text = dataProducto.Stock
                iptStock.Enabled = False 'Si está editando no puede cambiar el stock porque se actualiza al vender o recibir
                ddActivo.SelectedValue = dataProducto.Activo
                ddEsCanjeable.SelectedValue = dataProducto.EsCanjeable
                ddOfrecePuntos.SelectedValue = dataProducto.OfrecePuntos
                ddCategoria.SelectedValue = dataProducto.Categoria.IdCategoria
                ddProveedor.SelectedValue = dataProducto.Proveedor.IdProveedor
                If dataProducto.LinkImagen IsNot Nothing And dataProducto.LinkImagen.Trim.Length > 0 Then
                    imgFotoSubida.Visible = True
                    imgFotoSubida.ImageUrl = dataProducto.LinkImagen
                End If
                btnEliminar.Visible = True
            End If
        End If
    End Sub

    Protected Function SubirArchivo() As String
        If iptFoto.HasFile Then
            Try
                iptFoto.SaveAs(MapPath(RUTA_FOTOS & iptFoto.FileName))
                Return RUTA_FOTOS & iptFoto.FileName
            Catch ex As Exception
                'Label1.Text = "ERROR: " & ex.Message.ToString()
            End Try
        End If
        Return Nothing
    End Function

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If IsNumeric(iptCodigo.Value) Then
            Dim idPrd As Integer = CType(iptCodigo.Value, Integer)
            Dim objPrd As New BE_Producto With {.IdProducto = idPrd}
            Dim eliminado As Integer = oBllPrd.Eliminar(objPrd)
            If eliminado = 1 Then
                Response.Redirect("AdminProductosList.aspx?eliminado=ok")

            ElseIf eliminado = -1 Then
                RtaErrorEliminar.Visible = True
                msjErrorFK.Visible = True
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If

    End Sub
End Class