﻿Imports BLL
Imports BE
'Imports iTextSharp
'Imports iTextSharp.text
'Imports iTextSharp.text.pdf
'Imports iTextSharp.text.html

Imports System.IO

Public Class ECommerce
    Inherits ACL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.ValidarAccesoCliente()
            Me.CargarDataCarrito()
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub
    Protected Sub CargarDataCarrito()
        Dim acl As New ACL
        If acl.GetIdCliente() > 0 Then
            Dim oBllVta As New BLL_Ventas
            Dim carrito As BE_Venta = oBllVta.GetCarrito(acl.GetIdCliente())
            If Not carrito Is Nothing Then
                ' ltlCantItems.Text = carrito.Items.Count
                rptItemsCarrito.DataSource = Nothing
                rptItemsCarrito.DataBind()
                rptItemsCarrito.DataMember = "BE_Detalle_Venta"
                rptItemsCarrito.DataSource = carrito.Items
                rptItemsCarrito.DataBind()
            Else

            End If
        Else

        End If

    End Sub

End Class