﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminReportes.aspx.vb" Inherits="TFI_RugbyFit.AdminReportes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
    <style>
        .btn-reporte {
            margin-left:15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <h2>Reportes</h2>
    
    <a href="ReportEncuestas.aspx" class="btn btn-info btn-lg "><span class="glyphicon glyphicon-check"></span> Comparaci&oacute;n Encuestas</a>
    <a href="ReporteVentas.aspx" class="btn btn-success btn-lg btn-reporte"><span class="glyphicon glyphicon-usd"></span> Ventas</a>
    <a href="ReporteComparacionGanancias.aspx" class="btn btn-default btn-lg btn-reporte"><span class="glyphicon glyphicon-usd"></span> Comparaci&oacute;n Facturaci&oacute;n</a>
</asp:Content>
