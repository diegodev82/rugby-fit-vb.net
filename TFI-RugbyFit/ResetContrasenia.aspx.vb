﻿Imports BLL
Imports BE
Public Class ResetContrasenia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btn_ResetPwd_Click(sender As Object, e As EventArgs) Handles btn_ResetPwd.Click
        Dim oBllClientes As New BLL_Clientes
        Dim oCliente As New BE_Cliente With {.Email = email_resetPwd.Text}
        Dim existe As Boolean = oBllClientes.ValidarUsuarioExistentePorMail(oCliente)
        If existe = False Then
            Response.Redirect("ResetContrasenia.aspx?rta=email-desconocido")
        Else
            Dim actualizado As Boolean = oBllClientes.ResetContrasenia(email_resetPwd.Text)
            If actualizado = True Then
                Response.Redirect("ResetContrasenia.aspx?rta=ok")
            Else
                Response.Redirect("ResetContrasenia.aspx?rta=error-db")
            End If
        End If
    End Sub
End Class