﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class SQLHelper
    Private Shared Property _instance() As New Dictionary(Of String, SQLHelper)
    Private Property _conn As SqlConnection
    Private Tranx As SqlTransaction
    Public Property ConnectionString() As New Dictionary(Of String, String)
    Const DB_NAME = "RugbyFit"
    Const DB_MASTER = "MASTER"

    Private Sub New(Optional ByVal database As String = DB_NAME)
        Me.ConnectionString.Add(DB_NAME, Me._GetConnectionString())
        Me.ConnectionString.Add(DB_MASTER, Me._GetConnectionString(True))
        If database = DB_MASTER Then
            Me._conn = New SqlConnection(Me.ConnectionString.Item(DB_MASTER))
        Else
            Me._conn = New SqlConnection(Me.ConnectionString.Item(DB_NAME))
        End If
    End Sub

    Public Function isOpen() As Boolean
        Return Me._conn.State = ConnectionState.Open
    End Function

    Public Shared Function GetInstance(Optional database As String = DB_NAME) As SQLHelper
        If Not _instance.ContainsKey(database) Then
            _instance.Add(database, New SQLHelper(database))
        End If
        Return _instance(database)
    End Function

#Region "Consultas SP"

    Public Function Leer(ByVal spName As String, ByVal hdatos As Hashtable) As DataSet
        Try
            Me._connect()
            Dim Ds As New DataSet
            Dim Cmd = New SqlCommand

            Cmd.Connection = Me._conn
            Cmd.CommandText = spName
            Cmd.CommandType = CommandType.StoredProcedure

            If Not hdatos Is Nothing Then

                'si la hashtable no esta vacia, y tiene el dato q busco 
                For Each dato As String In hdatos.Keys
                    'cargo los parametros que le estoy pasando con la Hash
                    Cmd.Parameters.AddWithValue(dato, hdatos(dato))
                Next
            End If

            Dim Adaptador As New SqlDataAdapter(Cmd)
            Adaptador.Fill(Ds)
            Return Ds
        Catch ex As Exception
            Dim dsVacio As New DataSet
            Dim dt As New DataTable
            dsVacio.Tables.Add(dt)
            Return dsVacio
        End Try
        

    End Function

    Public Function Eliminar(ByVal spName As String, ByVal hdatos As Hashtable) As Integer
        Me._connect()
        Try
            Tranx = Me._conn.BeginTransaction()
            Dim Cmd = New SqlCommand
            Cmd.Connection = Me._conn
            Cmd.CommandText = spName
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Transaction = Tranx

            If Not hdatos Is Nothing Then

                For Each dato As String In hdatos.Keys
                    'cargo los parametros que le estoy pasando con la Hash
                    Cmd.Parameters.AddWithValue(dato, hdatos(dato))
                Next
            End If

            Dim respuesta As Integer = Cmd.ExecuteNonQuery
            Tranx.Commit()
            Return 1

        Catch ex As SqlException
            Dim i As Integer
            For i = 0 To ex.Errors.Count - 1
                If ex.Errors(i).Number = 547 Then
                    Return -1
                End If
            Next i
            Tranx.Rollback()
            Me._disconnect()
            Return 0
        Finally
            Me._disconnect()
        End Try

    End Function

    Public Function Insertar(ByVal spName As String, ByVal hdatos As Hashtable) As Integer
        Me._connect()
        Try
            Tranx = Me._conn.BeginTransaction()
            Dim Cmd = New SqlCommand
            Cmd.Connection = Me._conn
            Cmd.CommandText = spName
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Transaction = Tranx

            If Not hdatos Is Nothing Then

                For Each dato As String In hdatos.Keys
                    'cargo los parametros que le estoy pasando con la Hash
                    Cmd.Parameters.AddWithValue(dato, hdatos(dato))
                Next
            End If
            Cmd.Parameters.Add("@InsertedID", SqlDbType.Int, 4).Direction = ParameterDirection.Output
            Dim respuesta As Integer = Cmd.ExecuteNonQuery
            Tranx.Commit()
            Return Cmd.Parameters("@InsertedID").Value

        Catch ex As Exception
            Tranx.Rollback()
            Me._disconnect()
            Return False
        Finally
            Me._disconnect()
        End Try
    End Function

    Public Function Escribir(ByVal spName As String, ByVal hdatos As Hashtable) As Boolean

        Me._connect()
        Try
            Tranx = Me._conn.BeginTransaction()
            Dim Cmd = New SqlCommand
            Cmd.Connection = Me._conn
            Cmd.CommandText = spName
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Transaction = Tranx

            If Not hdatos Is Nothing Then

                For Each dato As String In hdatos.Keys
                    'cargo los parametros que le estoy pasando con la Hash
                    Cmd.Parameters.AddWithValue(dato, hdatos(dato))
                Next
            End If

            Dim respuesta As Integer = Cmd.ExecuteNonQuery
            Tranx.Commit()
            Return True

        Catch ex As Exception
            Tranx.Rollback()
            Me._disconnect()
            Return False
        Finally
            Me._disconnect()
        End Try

    End Function

#End Region

#Region "Consultas Directas"

    Public Function ExecuteQuery(ByVal query) As Boolean
        Me._connect()
        Dim executed As Boolean = Me._execute(query)
        Me._disconnect()
        Return executed
    End Function

    Public Function ExecuteScalar(ByVal query) As Object
        Dim cmd As New SqlClient.SqlCommand
        Dim result As Object
        Try
            _connect()
            cmd.Connection = _conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = query
            result = cmd.ExecuteScalar()
            _disconnect()

        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
        Return result
    End Function

    Public Function GetRecordset(ByVal query) As DataSet
        Dim Da As New SqlClient.SqlDataAdapter
        Dim Ds As New DataSet("Listado")
        Try
            _connect()
            Da.SelectCommand = New SqlClient.SqlCommand
            Da.SelectCommand.Connection = _conn
            Da.SelectCommand.CommandType = CommandType.Text
            Da.SelectCommand.CommandText = query
            Da.Fill(Ds)
            _disconnect()

        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
        Return Ds
    End Function

    Public Function ExecuteNonQuery(ByVal sql As String) As Integer
        _connect()
        Dim cmd As New SqlClient.SqlCommand
        Dim result As Integer
        Try
            _connect()
            cmd.Connection = _conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            result = cmd.ExecuteNonQuery()
            _disconnect()

        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Throw ex
        End Try
        Return result
    End Function

    Public Function GetLastInsertId(nombreTabla As String) As Integer
        Dim sqlID As String = "SELECT IDENT_CURRENT('" & nombreTabla.ToLower.Trim & "')"
        Return SQLHelper.GetInstance.ExecuteScalar(sqlID)
    End Function

    Public Function Destroy()
        _instance = New Dictionary(Of String, SQLHelper)
    End Function

#End Region

    Private Function _execute(ByVal query) As Boolean
        Dim response As Boolean = True
        Dim sSQL As String = query
        Try
            Dim objCmd As New SqlCommand(sSQL, _conn)
            objCmd.CommandType = CommandType.Text
            objCmd.ExecuteNonQuery()
            objCmd.Dispose()
            Me._disconnect()
        Catch ex As Exception
            Console.WriteLine(sSQL)
            Console.WriteLine(ex.Message)
            Throw ex
            response = False
        Finally
            Me._disconnect()
        End Try
        Return response
    End Function

    Private Function _GetConnectionString(Optional EsdbMaster As Boolean = False) As String
        Dim sconfig As String = ""
        Static conn As New SqlClient.SqlConnection
        Dim connstr As String = ""
        If EsdbMaster Then
            'connstr = "Data Source=.\sqlexpress;Initial Catalog=master;Integrated Security=True"
            connstr = "Data Source=TELAM\MSSQLSERVER2012;Initial Catalog=master;Integrated Security=True"
            'Return My.Settings.connStringMASTER
        Else
            'connstr = "Data Source=.\sqlexpress;Initial Catalog=RugbyFit;Integrated Security=True"
            connstr = "Data Source=TELAM\MSSQLSERVER2012;Initial Catalog=RugbyFit;Integrated Security=True"
            'Return My.Settings.connStringRugbyFit
        End If
        Return connstr

    End Function

    Private Sub _connect()
        If Me._conn.State = ConnectionState.Open Then
            Me._conn.Close()
        End If
        Try
            Me._conn.Open()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Public Sub _disconnect()
        _conn.Close()
    End Sub
End Class
