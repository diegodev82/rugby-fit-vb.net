﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminNovedadesForm.aspx.vb" Inherits="TFI_RugbyFit.AdminNovedadesForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
    <script>console.log("JS ACTIVADO");</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <asp:ScriptManager ID="ScriptManagerNovedades" runat="server"></asp:ScriptManager>
     <% If Request.QueryString("guardado") = "ok" Then%>
           <div class="alert alert-success">
               <p><span class="glyphicon glyphicon-thumbs-up" ></span> El novedad fue guardado con &eacute;xito</p>
           </div> 
        <% End If%>
    <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
        <div class="alert alert-danger">
            <asp:Literal ID="msjErrorDB" Visible="false" runat="server" Text="Se produjo un error en el sistema y el registro no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
        </div>
    </asp:PlaceHolder>
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <div class="form-group botonera clearfix">
        <a href="AdminNovedadesList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <a href="AdminNovedadesForm.aspx" class="btn btn-default pull-left">Nuevo</a>
        <asp:Button ID="btnEliminar" runat="server" CssClass="btn btn-danger pull-right" Visible="false" Text="Eliminar" />
    </div>
    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>T&iacute;tulo:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptTitulo" ClientIDMode="Static" required="required"  MaxLength="70" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Copete:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptCopete" ClientIDMode="Static" required="required"  MaxLength="250" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Cuerpo:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptCuerpo" runat="server" ClientIDMode="Static" required="required"  CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    
    
     <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Foto:</label>
            </div>
            <div class='col-md-11'>
                <asp:FileUpload ID="iptFoto" CssClass="form-control" runat="server" /><br />
                <asp:Image ID="imgFotoSubida" CssClass="img-responsive" Width="300" Height="100" Visible="false" runat="server" />
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Fecha:</label>
            </div>
            <div class='col-md-11'>
                <asp:TextBox ID="iptFecha" runat="server" format="dd/mm/yyyy" ClientIDMode="Static" required="required"  TextMode="Date" CssClass="form-control"></asp:TextBox>

            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class="form-group botonera clearfix">
        <a href="AdminNovedadesList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <input type='reset' class="btn btn-default  pull-left" name='guardar' value='Cancelar' />
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar" />
    </div>
</asp:Content>
