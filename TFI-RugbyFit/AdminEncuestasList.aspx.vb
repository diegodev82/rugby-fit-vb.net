﻿Imports BLL
Imports BE
Public Class AdminEncuestaList
    Inherits ACL
    Private oBll As New BLL_Encuesta
    Const COD_PERMISO = "MOD_MKT"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            LlenarDgv()
        End If
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Encuesta) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count
        DgvEncuestas.AutoGenerateColumns = False
        DgvEncuestas.DataSource = registros
        DgvEncuestas.DataBind()
    End Sub

    Protected Sub DgvEncuestas_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvEncuestas.RowEditing
        Dim row As GridViewRow = DgvEncuestas.Rows(e.NewEditIndex)
        Dim IdFamilia As String = row.Cells(0).Text

        Response.Redirect("/AdminEncuestasForm.aspx?id=" + IdFamilia)
    End Sub

    Protected Sub DgvEncuestas_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvEncuestas.RowUpdating
        Dim a As Integer = 0

    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvEncuestas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvEncuestas.PageIndexChanging
        DgvEncuestas.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvEncuestas_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvEncuestas.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroNombre.Text IsNot Nothing Then
            filtros.Add("@Pregunta", filtroNombre.Text)
        End If

        If IsNumeric(filtroCodigo.Text) Then
            Dim idFamilia As Integer = CType(filtroCodigo.Text, Integer)
            If idFamilia > 0 Then
                filtros.Add("@IdEncuesta", idFamilia)
            End If
        End If

        If ddTipo.SelectedValue <> "0" Then
            filtros.Add("@Tipo", IIf(ddTipo.SelectedValue = "C", "C", "S"))
        End If

        If IsDate(filtroDesde.Text) Then
            Dim fDesde As DateTime = CType(filtroDesde.Text, DateTime)
            filtros.Add("@FechaDesde", fDesde)
        End If

        If IsDate(filtroHasta.Text) Then
            Dim fHasta As DateTime = CType(filtroHasta.Text, DateTime)
            filtros.Add("@FechaHasta", fHasta)
        End If

        Return filtros
    End Function

    Protected Sub DgvEncuestas_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles DgvEncuestas.RowDeleting
        Dim row As GridViewRow = DgvEncuestas.Rows(e.RowIndex)
        Dim idEnc As String = row.Cells(0).Text
        If IsNumeric(idEnc) Then
            Dim objEnc As New BE_Encuesta With {.IdEncuesta = idEnc}
            Dim eliminado As Integer = oBll.Eliminar(objEnc)
            If eliminado = 1 Then
                Response.Redirect("AdminEncuestasList.aspx?eliminado=ok")
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub


    Protected Sub DgvEncuestas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles DgvEncuestas.RowDataBound
        Dim cssClass As String = ""
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim tipo As String = e.Row.Cells(1).Text
            If tipo = "C" Then
                e.Row.Cells(1).Text = "Encuesta"
            Else
                e.Row.Cells(1).Text = "Ficha de opinión"
            End If
            ' e.Row.CssClass = impac.ToLower

        End If
    End Sub
End Class