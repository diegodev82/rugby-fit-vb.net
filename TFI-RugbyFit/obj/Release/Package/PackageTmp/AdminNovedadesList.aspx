﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminNovedadesList.aspx.vb" Inherits="TFI_RugbyFit.AdminNovedades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
     <div>
        <div class="content-header">
            <% If Request.QueryString("eliminado") = "ok" Then%>
            <div class="block full">
                <div class="alert alert-success">
                    <p>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                        <asp:Literal ID="msjEliminadoOk" runat="server" Text="El novedad fue eliminado con éxito"></asp:Literal>
                    </p>
                </div>
            </div>
            <% End If%>
                <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
        <div class="alert alert-danger">
            <asp:Literal ID="msjErrorDB" Visible="false"  runat="server" Text="Se produjo un error en el sistema y el novedad no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
        </div> 
    </asp:PlaceHolder>
            <div class="header-section">
                <h3>
                    <strong>
                        <asp:Literal ID="Literal1" runat="server" Text="Novedades"></asp:Literal></strong>
                    <a href="AdminNovedadesForm.aspx" class="btn btn-primary pull-right">
                        <asp:Literal ID="Literal2" runat="server" Text="Nuevo"></asp:Literal></a><br />
                </h3>
            </div>
        </div>

        <div class="block full">
            <div class="row">
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default bg-panel">
                        <div class="panel-body">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon">C&oacute;digo</div>
                                    <asp:TextBox ID="filtroCodigo" TextMode="Number" ClientIDMode="Static" MaxLength="3" max="100" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon">T&iacute;tulo</div>
                                    <asp:TextBox ID="filtroTitulo" runat="server" MaxLength="30" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <!-- form group [rows] -->
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-info" />
                            <a href="/AdminNovedadesList.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;Limpiar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block full">
            <div class="table-responsive">
                <span class="lead">Se han encontrado <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> registros
                </span>
                <span class="help-block pull-right">Mostrando 15 registros por p&aacute;gina</span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvNovedades" runat="server" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-striped table-hover" AllowPaging="True" GridLines="None" PagerStyle-CssClass="pagination-dgv " PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="DgvNovedades_PageIndexChanging" OnPageIndexChanged="DgvNovedades_PageIndexChanged" PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="IdNovedad" HeaderText="Codigo" />
                        <asp:BoundField DataField="Titulo" HeaderText="Titulo" />
                        <asp:BoundField DataField="FechaHora" HeaderText="Fecha" />
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-edit'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="Editar" />
                        <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger"  HeaderText="Eliminar" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
