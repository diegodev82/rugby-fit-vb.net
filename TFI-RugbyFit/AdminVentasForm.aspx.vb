﻿Imports BLL
Imports BE
Public Class AdminVentasForm
    Inherits ACL
    Private oBll As New BLL_Ventas
    Const COD_PERMISO = "MOD_VENTAS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            If Not Request.QueryString("id") Is Nothing Then
                If Not IsNumeric(Request.QueryString("id")) Then
                    Response.Redirect("AdminVentasList.aspx")
                    Return
                End If
                Me.CargarData(CType(Request.QueryString("id"), Integer))
            End If

        End If
    End Sub

    Protected Sub CargarData(ByVal idVta As Integer)
        Dim vta As BE_Venta = oBll.GetPorId(idVta)
        ltlCliente.Text = vta.NombreCliente
        ltlFecha.Text = vta.Fecha.ToString
        ltlMontoTotal.Text = vta.MontoTotal
        ddEstado.SelectedValue = vta.Estado
        iptCodigo.Value = vta.IdVenta

    End Sub


    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim idVta As Integer = iptCodigo.Value
        If oBll.ActualizarEstado(idVta, ddEstado.SelectedValue) = True Then
            Response.Redirect("AdminVentasForm.aspx?id=" + idVta.ToString + "&guardado=ok")
        End If
        Response.Redirect("AdminVentasForm.aspx?id=" + idVta.ToString + "&guardado=error-db")
    End Sub
End Class