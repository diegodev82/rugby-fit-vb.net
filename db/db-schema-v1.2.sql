USE [master]
GO
/****** Object:  Database [RugbyFit]    Script Date: 29/08/2016 20:36:47 ******/
CREATE DATABASE [RugbyFit]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RugbyFit', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\RugbyFit.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RugbyFit_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\RugbyFit_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RugbyFit] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RugbyFit].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RugbyFit] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RugbyFit] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RugbyFit] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RugbyFit] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RugbyFit] SET ARITHABORT OFF 
GO
ALTER DATABASE [RugbyFit] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RugbyFit] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [RugbyFit] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RugbyFit] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RugbyFit] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RugbyFit] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RugbyFit] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RugbyFit] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RugbyFit] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RugbyFit] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RugbyFit] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RugbyFit] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RugbyFit] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RugbyFit] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RugbyFit] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RugbyFit] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RugbyFit] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RugbyFit] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RugbyFit] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RugbyFit] SET  MULTI_USER 
GO
ALTER DATABASE [RugbyFit] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RugbyFit] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RugbyFit] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RugbyFit] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [RugbyFit]
GO
/****** Object:  StoredProcedure [dbo].[pa_AumentarCII]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_AumentarCII]
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE usuario SET cii = cii + 1  WHERE id_usuario = @IdUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[pa_EsAdministrador]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_EsAdministrador]
	@IdUsuario Integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT COUNT(*) AS cant FROM usuario_familia WHERE id_familia = 1 AND id_usuario = @IdUsuario

END

GO
/****** Object:  StoredProcedure [dbo].[pa_GetFamiliasPorUsuario]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_GetFamiliasPorUsuario]
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT f.id_familia, f.nombre FROM usuario_familia AS uf  
INNER JOIN familia AS f ON f.id_familia = uf.id_familia 
WHERE uf.id_usuario = @IdUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[pa_GetPermisosFamilias]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_GetPermisosFamilias]
	@IdsFamilias nchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT p.id_permiso, p.descripcion, p.codigo FROM permiso p
	 INNER JOIN familia_permiso pf ON pf.id_permiso = p.id_permiso 
	  WHERE pf.id_familia IN (@IdsFamilias)

END

GO
/****** Object:  StoredProcedure [dbo].[pa_GetPermisosHeredadosPorFamilias]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_GetPermisosHeredadosPorFamilias]
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT p.id_permiso, p.codigo FROM usuario_familia uf 
		INNER JOIN familia_permiso pf ON uf.id_familia = pf.id_familia 
		INNER JOIN permiso p ON p.id_permiso = pf.id_permiso  
		WHERE uf.id_usuario = @IdUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[pa_GetPermisosHeredadosPorUsuario]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_GetPermisosHeredadosPorUsuario]
	@IdUsuario nchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT p.id_permiso, p.descripcion, up.tipo, p.codigo FROM usuario_permiso AS up 
	  INNER JOIN permiso AS p ON p.id_permiso = up.id_permiso 
	  WHERE up.id_usuario = @IdUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[pa_Login]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_Login]
	@Nickname nchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TOP 1 * FROM usuario WHERE nickname = @Nickname
END

GO
/****** Object:  StoredProcedure [dbo].[pa_ResetearCII]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_ResetearCII]
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE usuario SET cii = 0  WHERE id_usuario = @IdUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[pa_VerificarCII]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pa_VerificarCII]
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TOP 1 cii FROM usuario WHERE id_usuario = @IdUsuario
END

GO
/****** Object:  Table [dbo].[bitacora]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bitacora](
	[id_bitacora] [int] IDENTITY(1,1) NOT NULL,
	[id_evento] [int] NULL,
	[id_usuario] [int] NULL,
	[fecha_hora] [datetime] NULL,
	[observacion] [varchar](255) NULL,
 CONSTRAINT [PK_bitacora] PRIMARY KEY CLUSTERED 
(
	[id_bitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cliente](
	[id_cliente] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[email] [varchar](320) NULL,
	[nickname] [varchar](50) NULL,
	[contrasenia] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
	[id_ciudad] [int] NULL,
	[id_provincia] [int] NULL,
	[domicilio] [varchar](50) NULL,
	[eliminado] [int] NOT NULL,
	[cuit] [varchar](13) NULL,
	[dni] [int] NULL,
	[origen] [varchar](50) NULL,
	[puntos_acumulados] [int] NULL,
 CONSTRAINT [PK_cliente] PRIMARY KEY CLUSTERED 
(
	[id_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[compra]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[compra](
	[id_compra] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NULL,
 CONSTRAINT [PK_compra] PRIMARY KEY CLUSTERED 
(
	[id_compra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[compra_detalle]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[compra_detalle](
	[id_compra_detalle] [int] IDENTITY(1,1) NOT NULL,
	[id_compra] [int] NULL,
	[id_producto] [int] NULL,
	[cantidad] [int] NULL,
	[precio_unitario] [float] NULL,
	[recibido] [int] NULL,
 CONSTRAINT [PK_compra_detalle] PRIMARY KEY CLUSTERED 
(
	[id_compra_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[detalle_factura]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalle_factura](
	[id_factura_detalle] [int] NOT NULL,
	[id_factura] [int] NULL,
	[id_producto] [int] NULL,
	[cantidad] [int] NULL,
	[precio_unitario] [float] NULL,
	[precio_final] [float] NULL,
 CONSTRAINT [PK_detalle_factura] PRIMARY KEY CLUSTERED 
(
	[id_factura_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[evento]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[evento](
	[id_evento] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NULL,
	[criticidad] [int] NULL,
 CONSTRAINT [PK_evento] PRIMARY KEY CLUSTERED 
(
	[id_evento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[factura]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[factura](
	[id_factura] [int] IDENTITY(1,1) NOT NULL,
	[id_cliente] [int] NULL,
	[fecha] [int] NULL,
	[domicilio] [varchar](50) NULL,
	[id_ciudad] [int] NULL,
	[id_provincia] [int] NULL,
	[tipo] [int] NULL,
	[id_forma_pago] [int] NULL,
	[estado] [varchar](50) NULL,
 CONSTRAINT [PK_factura] PRIMARY KEY CLUSTERED 
(
	[id_factura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[familia]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[familia](
	[id_familia] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[descripcion] [text] NULL,
 CONSTRAINT [PK_familia] PRIMARY KEY CLUSTERED 
(
	[id_familia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[familia_permiso]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[familia_permiso](
	[id_familia_permiso] [int] IDENTITY(1,1) NOT NULL,
	[id_familia] [int] NULL,
	[id_permiso] [int] NULL,
 CONSTRAINT [PK_familia_permiso] PRIMARY KEY CLUSTERED 
(
	[id_familia_permiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[medio]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[medio](
	[id_medio] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[id_rubro] [int] NULL,
 CONSTRAINT [PK_medio] PRIMARY KEY CLUSTERED 
(
	[id_medio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oferta]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oferta](
	[id_oferta] [int] IDENTITY(1,1) NOT NULL,
	[fecha_inicio] [datetime] NULL,
	[fecha_fin] [datetime] NULL,
	[descripcion] [text] NULL,
	[ubicacion_sitio] [varchar](50) NULL,
	[link_imagen] [varchar](50) NULL,
 CONSTRAINT [PK_ofertas] PRIMARY KEY CLUSTERED 
(
	[id_oferta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[permiso]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[permiso](
	[id_permiso] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](50) NULL,
	[descripcion] [text] NULL,
 CONSTRAINT [PK_permiso] PRIMARY KEY CLUSTERED 
(
	[id_permiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[producto]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[producto](
	[id_producto] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[descripcion] [text] NULL,
	[id_categoria] [int] NULL,
	[link_imagen] [varchar](320) NULL,
	[ofrece_puntos] [int] NULL,
	[es_canjeable] [int] NULL,
	[puntos_ofrecidos] [int] NULL,
	[puntos_requeridos] [int] NULL,
	[id_proveedor] [int] NULL,
	[stock] [int] NULL,
	[stock_minimo] [int] NULL,
 CONSTRAINT [PK_producto] PRIMARY KEY CLUSTERED 
(
	[id_producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[proveedor]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[proveedor](
	[id_proveedor] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[domicilio] [varchar](50) NULL,
	[email] [varchar](320) NULL,
	[telefono] [varchar](50) NULL,
	[id_ciudad] [int] NULL,
	[id_provincia] [int] NULL,
 CONSTRAINT [PK_proveedor] PRIMARY KEY CLUSTERED 
(
	[id_proveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[publicidad]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[publicidad](
	[id_publicidad] [int] IDENTITY(1,1) NOT NULL,
	[id_rubro] [int] NULL,
	[id_medio] [int] NULL,
	[fecha_inicio] [date] NULL,
	[fecha_fin] [date] NULL,
	[monto] [decimal](10, 2) NULL,
	[descripcion] [text] NULL,
 CONSTRAINT [PK_publicidad] PRIMARY KEY CLUSTERED 
(
	[id_publicidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[punto_cliente]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[punto_cliente](
	[id_punto_cliente] [int] IDENTITY(1,1) NOT NULL,
	[id_cliente] [int] NULL,
	[cantidad] [int] NULL,
	[fecha_hora] [datetime] NULL,
	[id_producto] [int] NULL,
 CONSTRAINT [PK_punto_cliente] PRIMARY KEY CLUSTERED 
(
	[id_punto_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[rubro]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rubro](
	[id_rubro] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
 CONSTRAINT [PK_rubro] PRIMARY KEY CLUSTERED 
(
	[id_rubro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[solicitud_compra]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[solicitud_compra](
	[id_solicitud_compra] [int] IDENTITY(1,1) NOT NULL,
	[fecha_solicitud] [datetime] NULL,
	[solicitado_por] [int] NULL,
	[resuelto_por] [int] NULL,
	[estado] [varchar](50) NULL,
	[fecha_resolucion] [datetime] NULL,
 CONSTRAINT [PK_solicitud_compra] PRIMARY KEY CLUSTERED 
(
	[id_solicitud_compra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[solicitud_compra_detalle]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[solicitud_compra_detalle](
	[id_solicitud_compra_detalle] [int] IDENTITY(1,1) NOT NULL,
	[id_solicitud_compra] [int] NULL,
	[id_producto] [int] NULL,
	[cantidad] [int] NULL,
	[precio_unitario] [decimal](10, 2) NULL,
 CONSTRAINT [PK_solicitud_compra_detalle] PRIMARY KEY CLUSTERED 
(
	[id_solicitud_compra_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[usuario]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuario](
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[nickname] [varchar](50) NULL,
	[contrasenia] [varchar](50) NULL,
	[email] [varchar](max) NULL,
	[telefono] [varchar](50) NULL,
	[dni] [int] NULL,
	[cii] [int] NULL,
	[activo] [int] NULL,
	[es_empleado] [int] NULL,
 CONSTRAINT [PK_usuario] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuario_familia]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario_familia](
	[id_usuario_familia] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[id_familia] [int] NULL,
 CONSTRAINT [PK_usuario_familia] PRIMARY KEY CLUSTERED 
(
	[id_usuario_familia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[usuario_permiso]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario_permiso](
	[id_usuario_permiso] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NULL,
	[id_permiso] [int] NULL,
	[tipo] [int] NULL,
 CONSTRAINT [PK_usuario_permiso] PRIMARY KEY CLUSTERED 
(
	[id_usuario_permiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[venta]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[venta](
	[id_venta] [int] IDENTITY(1,1) NOT NULL,
	[id_cliente] [int] NULL,
	[id_vendedor] [int] NULL,
	[fecha] [datetime] NULL,
	[origen] [int] NULL,
 CONSTRAINT [PK_venta] PRIMARY KEY CLUSTERED 
(
	[id_venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[venta_detalle]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[venta_detalle](
	[id_venta_detalle] [int] IDENTITY(1,1) NOT NULL,
	[id_venta] [int] NULL,
	[id_producto] [int] NULL,
	[cantidad] [int] NULL,
	[monto] [float] NULL,
 CONSTRAINT [PK_detalle_venta] PRIMARY KEY CLUSTERED 
(
	[id_venta_detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[voucher]    Script Date: 29/08/2016 20:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[voucher](
	[id_voucher] [int] IDENTITY(1,1) NOT NULL,
	[id_producto] [int] NULL,
	[fecha_generacion] [datetime] NULL,
	[fecha_canje] [datetime] NULL,
	[estado] [varchar](50) NULL,
	[puntos_canjeados] [int] NULL,
	[id_cliente] [int] NULL,
 CONSTRAINT [PK_Table1] PRIMARY KEY CLUSTERED 
(
	[id_voucher] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[cliente] ADD  DEFAULT ((0)) FOR [eliminado]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF_usuario_cii]  DEFAULT ((0)) FOR [cii]
GO
ALTER TABLE [dbo].[usuario] ADD  CONSTRAINT [DF_usuario_activo]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[bitacora]  WITH CHECK ADD  CONSTRAINT [FK_bitacora_evento] FOREIGN KEY([id_evento])
REFERENCES [dbo].[evento] ([id_evento])
GO
ALTER TABLE [dbo].[bitacora] CHECK CONSTRAINT [FK_bitacora_evento]
GO
ALTER TABLE [dbo].[compra_detalle]  WITH CHECK ADD  CONSTRAINT [FK_compra_detalle_compra] FOREIGN KEY([id_compra])
REFERENCES [dbo].[compra] ([id_compra])
GO
ALTER TABLE [dbo].[compra_detalle] CHECK CONSTRAINT [FK_compra_detalle_compra]
GO
ALTER TABLE [dbo].[compra_detalle]  WITH CHECK ADD  CONSTRAINT [FK_compra_detalle_producto] FOREIGN KEY([id_producto])
REFERENCES [dbo].[producto] ([id_producto])
GO
ALTER TABLE [dbo].[compra_detalle] CHECK CONSTRAINT [FK_compra_detalle_producto]
GO
ALTER TABLE [dbo].[detalle_factura]  WITH CHECK ADD  CONSTRAINT [FK_detalle_factura_factura] FOREIGN KEY([id_factura])
REFERENCES [dbo].[factura] ([id_factura])
GO
ALTER TABLE [dbo].[detalle_factura] CHECK CONSTRAINT [FK_detalle_factura_factura]
GO
ALTER TABLE [dbo].[detalle_factura]  WITH CHECK ADD  CONSTRAINT [FK_detalle_factura_producto] FOREIGN KEY([id_producto])
REFERENCES [dbo].[producto] ([id_producto])
GO
ALTER TABLE [dbo].[detalle_factura] CHECK CONSTRAINT [FK_detalle_factura_producto]
GO
ALTER TABLE [dbo].[factura]  WITH CHECK ADD  CONSTRAINT [FK_factura_cliente] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[cliente] ([id_cliente])
GO
ALTER TABLE [dbo].[factura] CHECK CONSTRAINT [FK_factura_cliente]
GO
ALTER TABLE [dbo].[familia_permiso]  WITH CHECK ADD  CONSTRAINT [FK_familia_permiso_familia] FOREIGN KEY([id_familia])
REFERENCES [dbo].[familia] ([id_familia])
GO
ALTER TABLE [dbo].[familia_permiso] CHECK CONSTRAINT [FK_familia_permiso_familia]
GO
ALTER TABLE [dbo].[familia_permiso]  WITH CHECK ADD  CONSTRAINT [FK_familia_permiso_permiso] FOREIGN KEY([id_permiso])
REFERENCES [dbo].[permiso] ([id_permiso])
GO
ALTER TABLE [dbo].[familia_permiso] CHECK CONSTRAINT [FK_familia_permiso_permiso]
GO
ALTER TABLE [dbo].[medio]  WITH CHECK ADD  CONSTRAINT [FK_medio_rubro] FOREIGN KEY([id_rubro])
REFERENCES [dbo].[rubro] ([id_rubro])
GO
ALTER TABLE [dbo].[medio] CHECK CONSTRAINT [FK_medio_rubro]
GO
ALTER TABLE [dbo].[producto]  WITH CHECK ADD  CONSTRAINT [FK_producto_proveedor] FOREIGN KEY([id_proveedor])
REFERENCES [dbo].[proveedor] ([id_proveedor])
GO
ALTER TABLE [dbo].[producto] CHECK CONSTRAINT [FK_producto_proveedor]
GO
ALTER TABLE [dbo].[publicidad]  WITH CHECK ADD  CONSTRAINT [FK_publicidad_medio] FOREIGN KEY([id_medio])
REFERENCES [dbo].[medio] ([id_medio])
GO
ALTER TABLE [dbo].[publicidad] CHECK CONSTRAINT [FK_publicidad_medio]
GO
ALTER TABLE [dbo].[publicidad]  WITH CHECK ADD  CONSTRAINT [FK_publicidad_rubro] FOREIGN KEY([id_rubro])
REFERENCES [dbo].[rubro] ([id_rubro])
GO
ALTER TABLE [dbo].[publicidad] CHECK CONSTRAINT [FK_publicidad_rubro]
GO
ALTER TABLE [dbo].[punto_cliente]  WITH CHECK ADD  CONSTRAINT [FK_punto_cliente_cliente] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[cliente] ([id_cliente])
GO
ALTER TABLE [dbo].[punto_cliente] CHECK CONSTRAINT [FK_punto_cliente_cliente]
GO
ALTER TABLE [dbo].[punto_cliente]  WITH CHECK ADD  CONSTRAINT [FK_punto_cliente_producto] FOREIGN KEY([id_producto])
REFERENCES [dbo].[producto] ([id_producto])
GO
ALTER TABLE [dbo].[punto_cliente] CHECK CONSTRAINT [FK_punto_cliente_producto]
GO
ALTER TABLE [dbo].[solicitud_compra]  WITH CHECK ADD  CONSTRAINT [FK_solicitud_compra_usuario] FOREIGN KEY([solicitado_por])
REFERENCES [dbo].[usuario] ([id_usuario])
GO
ALTER TABLE [dbo].[solicitud_compra] CHECK CONSTRAINT [FK_solicitud_compra_usuario]
GO
ALTER TABLE [dbo].[solicitud_compra]  WITH CHECK ADD  CONSTRAINT [FK_solicitud_compra_usuario_02] FOREIGN KEY([resuelto_por])
REFERENCES [dbo].[usuario] ([id_usuario])
GO
ALTER TABLE [dbo].[solicitud_compra] CHECK CONSTRAINT [FK_solicitud_compra_usuario_02]
GO
ALTER TABLE [dbo].[solicitud_compra_detalle]  WITH CHECK ADD  CONSTRAINT [FK_solicitud_compra_detalle_producto] FOREIGN KEY([id_producto])
REFERENCES [dbo].[producto] ([id_producto])
GO
ALTER TABLE [dbo].[solicitud_compra_detalle] CHECK CONSTRAINT [FK_solicitud_compra_detalle_producto]
GO
ALTER TABLE [dbo].[solicitud_compra_detalle]  WITH CHECK ADD  CONSTRAINT [FK_solicitud_compra_detalle_solicitud_compra] FOREIGN KEY([id_solicitud_compra])
REFERENCES [dbo].[solicitud_compra] ([id_solicitud_compra])
GO
ALTER TABLE [dbo].[solicitud_compra_detalle] CHECK CONSTRAINT [FK_solicitud_compra_detalle_solicitud_compra]
GO
ALTER TABLE [dbo].[usuario_familia]  WITH CHECK ADD  CONSTRAINT [FK_usuario_familia_familia] FOREIGN KEY([id_familia])
REFERENCES [dbo].[familia] ([id_familia])
GO
ALTER TABLE [dbo].[usuario_familia] CHECK CONSTRAINT [FK_usuario_familia_familia]
GO
ALTER TABLE [dbo].[usuario_familia]  WITH CHECK ADD  CONSTRAINT [FK_usuario_familia_usuario] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[usuario] ([id_usuario])
GO
ALTER TABLE [dbo].[usuario_familia] CHECK CONSTRAINT [FK_usuario_familia_usuario]
GO
ALTER TABLE [dbo].[usuario_permiso]  WITH CHECK ADD  CONSTRAINT [FK_usuario_permiso_permiso] FOREIGN KEY([id_permiso])
REFERENCES [dbo].[permiso] ([id_permiso])
GO
ALTER TABLE [dbo].[usuario_permiso] CHECK CONSTRAINT [FK_usuario_permiso_permiso]
GO
ALTER TABLE [dbo].[usuario_permiso]  WITH CHECK ADD  CONSTRAINT [FK_usuario_permiso_usuario] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[usuario] ([id_usuario])
GO
ALTER TABLE [dbo].[usuario_permiso] CHECK CONSTRAINT [FK_usuario_permiso_usuario]
GO
ALTER TABLE [dbo].[venta]  WITH CHECK ADD  CONSTRAINT [FK_venta_cliente] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[cliente] ([id_cliente])
GO
ALTER TABLE [dbo].[venta] CHECK CONSTRAINT [FK_venta_cliente]
GO
ALTER TABLE [dbo].[venta]  WITH CHECK ADD  CONSTRAINT [FK_venta_usuario] FOREIGN KEY([id_vendedor])
REFERENCES [dbo].[usuario] ([id_usuario])
GO
ALTER TABLE [dbo].[venta] CHECK CONSTRAINT [FK_venta_usuario]
GO
ALTER TABLE [dbo].[venta_detalle]  WITH CHECK ADD  CONSTRAINT [FK_detalle_venta_venta] FOREIGN KEY([id_venta])
REFERENCES [dbo].[venta] ([id_venta])
GO
ALTER TABLE [dbo].[venta_detalle] CHECK CONSTRAINT [FK_detalle_venta_venta]
GO
ALTER TABLE [dbo].[venta_detalle]  WITH CHECK ADD  CONSTRAINT [FK_venta_detalle_producto] FOREIGN KEY([id_producto])
REFERENCES [dbo].[producto] ([id_producto])
GO
ALTER TABLE [dbo].[venta_detalle] CHECK CONSTRAINT [FK_venta_detalle_producto]
GO
ALTER TABLE [dbo].[voucher]  WITH CHECK ADD  CONSTRAINT [FK_voucher_cliente] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[cliente] ([id_cliente])
GO
ALTER TABLE [dbo].[voucher] CHECK CONSTRAINT [FK_voucher_cliente]
GO
ALTER TABLE [dbo].[voucher]  WITH CHECK ADD  CONSTRAINT [FK_voucher_producto] FOREIGN KEY([id_producto])
REFERENCES [dbo].[producto] ([id_producto])
GO
ALTER TABLE [dbo].[voucher] CHECK CONSTRAINT [FK_voucher_producto]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'origen:  WEB: registrado a través de la web LOCAL: registrado a través del sitema de gestión local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cliente', @level2type=N'COLUMN',@level2name=N'origen'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Critico 2 = Severo 3 = Leve 4 = Marginal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'evento', @level2type=N'COLUMN',@level2name=N'criticidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo factura: 1 = FC 2 = NC 3 = ND' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'factura', @level2type=N'COLUMN',@level2name=N'tipo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'forma pago 1 = FT 2 = CC 3 = Transferencia 4 = Cheque 5...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'factura', @level2type=N'COLUMN',@level2name=N'id_forma_pago'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado: ANULADA ACTIVA ABONADA PENDIENTE ....' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'factura', @level2type=N'COLUMN',@level2name=N'estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Puede ser: TV, GRAFICO, RADIO, etc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'publicidad', @level2type=N'COLUMN',@level2name=N'id_rubro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Origen: 1 = Web 0 = Local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'venta', @level2type=N'COLUMN',@level2name=N'origen'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado: CANJEADO PENDIENTE ANULADO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'voucher', @level2type=N'COLUMN',@level2name=N'estado'
GO
USE [master]
GO
ALTER DATABASE [RugbyFit] SET  READ_WRITE 
GO
