﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="ReportEncuestas.aspx.vb" Inherits="TFI_RugbyFit.ReportEncuestas" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <h2><asp:Literal ID="ltlTitulo" runat="server">Encuestas</asp:Literal> <a href="AdminReportes.aspx" class="btn btn-default pull-right">Volver al Panel de Reportes</a></h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         
            
                <div id="filter-panel" class="filter-panel">
                    <div class="panel panel-default bg-panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon">Desde</div>
                                        <asp:TextBox ID="filtroDesde" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderDesde" TargetControlID="filtroDesde" Format="dd-MM-yyyy" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon">Hasta</div>
                                        <asp:TextBox ID="filtroHasta" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderHasta" TargetControlID="filtroHasta" Format="dd-MM-yyyy" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon">Tipo</div>
                                        <asp:DropDownList ID="ddTipo" runat="server" CssClass="form-control">
                                            <asp:ListItem Selected="True" Value="C" Text="Encuesta"></asp:ListItem>
                                            <asp:ListItem  Value="S" Text="Ficha de Opinión"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                 <div class="col-md-3 pull-right text-right">
                                    <!-- form group [rows] -->
                                    <asp:Button ID="btnBuscar" runat="server" Text="Filtrar" CssClass="btn btn-info" />
                                    <a href="/ReportEncuestas.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;Limpiar</a>
                                </div>
                            </div>
                             
                               
                             
                        </div>
                    </div>
                </div>
            
        
    <div class="row">
        <div class="col-md-9">
            <rsweb:ReportViewer ID="rvEncuestas" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" SizeToReportContent="True">
                <LocalReport ReportPath="ReportEncuesta.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DsEncuestas" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="TFI-RugbyFit.DsEncuestasTableAdapters."></asp:ObjectDataSource>
        </div>
    </div>

</asp:Content>
