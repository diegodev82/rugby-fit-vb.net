﻿$(document).ready(function () {
    var cantPreguntas = $(".js-contenedor-ficha").length;
    var cantVotos = 0;
    var idFactura = $("#iptIdFc").val();
    $(".btn_votar").on("click.VotarEncuesta", function (e) {
        e.preventDefault();
        var $btn = $(this);
        var $ficha = $btn.parents(".js-contenedor-ficha");
        //validar cookie
        
        var rta_selecccionada = $ficha.find(".rta_encuesta:checked");
        if (rta_selecccionada.length == 0) {
            alert("Debe seleccionar una respuesta")
            return false;
        }

        var voto = rta_selecccionada.val();
        var id_encuesta = rta_selecccionada.data("id_encuesta");
        $.ajax(
            {
                "url": "/AjaxUtils.asmx/Votar",
                data: "{'id_encuesta':'" + id_encuesta + "', 'voto':'" + voto + "'}",
                contentType: 'application/json; utf-8',
                dataType: 'json',
                type: 'post',
                success: function (data) {

                    if (data.d == true) {
                        $ficha.html("<div class='alert alert-success'><p>Gracias por colaborar</p></div>");
                        cantVotos++;
                        if (cantVotos == cantPreguntas) {
                            location.href = "Factura.aspx?id=" + idFactura+"&facturado=ok";
                        }
                        //Guardar cookie
                        //mostrar resultados
                    }
                },
            }
        );
    });
});