
Imports DAL
Imports BE

Public Class MPP_Reportes



    Public m_BE_Compra As BE.BE_Compra
    Public m_BE_Cliente As BE.BE_Cliente
    Public m_BE_Venta As BE.BE_Venta
    Public m_BE_Factura As BE.BE_Factura

    ''' 
    ''' <param name="filtros"></param>
    ''' <param name="nombre"></param>
    Public Function Listar(ByVal filtros As Hashtable, ByVal nombre As String) As List(Of Object)
        Listar = Nothing
    End Function

    Public Function GetReporteEncuestas(Optional ByVal filtros As Hashtable = Nothing) As DataTable
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Permiso)
        DS = oDatos.Leer("pa_ReporteEncuestas", filtros)
        If DS.Tables.Count = 1 Then
            Return DS.Tables(0)
        End If
        Return Nothing

    End Function
    Public Function GetReporteFacturacion(Optional ByVal filtros As Hashtable = Nothing) As DataTable
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Permiso)
        DS = oDatos.Leer("pa_ReporteFacturacion", filtros)
        If DS.Tables.Count = 1 Then
            Return DS.Tables(0)
        End If
        Return Nothing

    End Function
    Public Function GetReporteComparacionGanancias(Optional ByVal filtros As Hashtable = Nothing) As DataTable
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Permiso)
        DS = oDatos.Leer("pa_ReporteComparacionGcias", filtros)
        If DS.Tables.Count = 1 Then
            Return DS.Tables(0)
        End If
        Return Nothing

    End Function
    'pa_ReporteComparacionGcias
End Class ' MPP_Reportes

