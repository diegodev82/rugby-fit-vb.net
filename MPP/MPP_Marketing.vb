Imports BE
Imports DAL

Public Class MPP_Marketing




    ''' 
    ''' <param name="objProducto"></param>
    Public Function Canjear(ByVal objProducto As BE_Producto) As Boolean
        Canjear = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarProductosCanjeables(ByVal filtros As Hashtable) As List(Of BE_Producto)
        ListarProductosCanjeables = Nothing
    End Function

    ''' 
    ''' <param name="objVoucher"></param>
    Public Function RegistrarVoucher(ByVal objVoucher As BE_Voucher) As BE.BE_Voucher
        RegistrarVoucher = Nothing
    End Function

    ''' 
    ''' <param name="objProducto"></param>
    Public Function ValidarPuntaje(ByVal objProducto As BE_Producto) As Boolean
        ValidarPuntaje = False
    End Function

    ''' 
    ''' <param name="objProducto"></param>
    Public Function ValidarStock(ByVal objProducto As BE_Producto) As Boolean
        ValidarStock = False
    End Function

    ''' 
    ''' <param name="IdCliente"></param>
    Public Function VerMisProductosCanjeados(ByVal IdCliente As Integer) As List(Of BE_Voucher)
        VerMisProductosCanjeados = Nothing
    End Function

    ''' 
    ''' <param name="IdCliente"></param>
    Public Function VerMisPuntos(ByVal IdCliente As Integer) As List(Of BE_Punto_Cliente)
        VerMisPuntos = Nothing
    End Function


End Class ' MPP_Marketing

