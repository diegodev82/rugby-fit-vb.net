﻿Imports BLL
Imports BE
Imports SelectPdf

Public Class Factura
    Inherits ACL
    Dim oBll As New BLL_Facturas
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("imprimir") Is Nothing Then
                ValidarAccesoCliente()
            End If

            div_error.Visible = False
            div_fc.Visible = False
            If Not IsPostBack Then
                If Not Request.QueryString("id") Is Nothing And IsNumeric(Request.QueryString("id")) Then
                    Dim idFactura As Integer = CType(Request.QueryString("id"), Integer)
                    Me._CargarData(idFactura)
                End If

            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub


    Protected Sub _CargarData(ByVal idFactura As Integer)
        iptIdFactura.Value = idFactura
        Dim fc As BE_Factura = oBll.GetPorId(idFactura)
        If Request.QueryString("imprimir") Is Nothing And Me._ValidarFactura(fc) = False Then
            div_error.Visible = True
        Else
            div_fc.Visible = True
            ltlNroFc.Text = fc.IdFactura.ToString("000")
            ltlCiudad.Text = fc.Ciudad.Nombre.ToLower()
            ltlProvincia.Text = fc.Ciudad.Provincia.Nombre
            ltlDomicilioCliente.Text = fc.Domicilio
            'ltlTelefonoCliente.Text = fc.Cliente.Telefono
            ltlMailCliente.Text = fc.Cliente.Email
            ltlNombreCliente.Text = fc.Cliente.Nombre + " " + fc.Cliente.Apellido
            ltlMetodoPago.Text = BE_Factura.GetNombreFormaPago(fc.FormaPago)
            ltlFecha.Text = fc.Fecha.ToLongDateString
            ltlFecha2.Text = fc.Fecha.ToLongDateString
            btnAnular.Visible = fc.Estado = BE_Factura.ESTADO_PAGADA
            ltlTotal.Text = fc.Monto
            ltlMontoCab.Text = fc.Monto
            If fc.Items.Count > 0 Then
                Me._CargarItems(fc.Items)
            End If
        End If

    End Sub

    Protected Sub _CargarItems(ByVal items As List(Of BE_Detalle_Factura))
        rptItems.DataSource = Nothing
        rptItems.DataBind()

        rptItems.DataMember = "BE_Detalle_Factura"
        rptItems.DataSource = items
        rptItems.DataBind()
    End Sub

    Protected Function _ValidarFactura(fc As BE_Factura) As Boolean
        If fc Is Nothing Then
            msj_error.InnerText = "Disculpe, no existe la factura en el sistema"
            Return False
        End If
        If Not fc.Cliente.IdCliente = Me.GetIdCliente() Then
            msj_error.InnerText = "Disculpe, no puede ver esta factura"
            Return False
        End If
        Return True
    End Function


    Protected Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        If iptIdFactura.Value > 0 Then
            Dim oBllCli As New BLL_Clientes
            Dim idFactura As Integer = iptIdFactura.Value
            Dim fc As BE_Factura = oBll.GetPorId(idFactura)
            Dim facturas As New List(Of BE_Factura)
            facturas.Add(fc)
            Dim anulada As Boolean = oBll.Anular(facturas)
            If anulada = True Then
                Response.Redirect("MisCompras.aspx?anulado=ok")
            Else
                msj_error.InnerText = "Disculpe, no pudimos anular la factura solicitada"
                div_error.Visible = True
            End If

        End If

    End Sub

    Protected Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        ' read parameters from the webpage
        Dim idFac As String = iptIdFactura.Value.ToString
        Dim url As String = "http://localhost:53255/Factura.aspx?id=" + idFac + "&imprimir=1"

        Dim pageSize As PdfPageSize = PdfPageSize.A4
        Dim pdfOrientation As PdfPageOrientation = PdfPageOrientation.Portrait
        Dim webPageWidth As Integer = 1024
        Dim webPageHeight As Integer = 0
        

        ' instantiate a html to pdf converter object
        Dim converter As New HtmlToPdf()

        ' set converter options
        converter.Options.PdfPageSize = pageSize
        converter.Options.PdfPageOrientation = pdfOrientation
        converter.Options.WebPageWidth = webPageWidth
        converter.Options.WebPageHeight = webPageHeight

        ' create a new pdf document converting an url
        Dim doc As PdfDocument = converter.ConvertUrl(url)

        ' save pdf document
        doc.Save(Response, False, "Factura-000" + idFac + ".pdf")

        ' close pdf document
        doc.Close()
    End Sub


End Class