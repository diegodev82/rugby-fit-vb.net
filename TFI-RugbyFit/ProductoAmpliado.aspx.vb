﻿Imports BLL
Imports BE
Public Class ProductoAmpliado
    Inherits ACL
    Private oBll As New BLL_Productos

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Request.QueryString("id") Is Nothing And IsNumeric(Request.QueryString("id")) Then
                If Not IsPostBack Then
                    Try
                        Dim id As Integer = Request.QueryString("id")
                        Me.CargarDatos(id)
                    Catch ex As Exception

                    End Try
                End If
            Else
                Response.Redirect("Productos.aspx", False)
            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub

    Public Sub CargarDatos(id)
        Dim IdCliente As Integer = Me.GetIdCliente
        If IdCliente = 0 Then
            btnAgregarAlCarrito.Visible = False
        End If
        Dim producto As BE_Producto = oBll.GetPorId(id)
        iptCodigo.Value = id
        ltlTitulo.Text = producto.Nombre
        ltlDescripcion.Text = producto.Descripcion
        ltlDescCorta.Text = producto.DescripcionCorta
        ltlPrecio.Text = producto.Precio
        ' ratingPrd.CurrentRating = producto.Rating
        ' ratingPrd.Tag = producto.IdProducto
        ltlRating.Text = producto.Rating
        ltlCantVotos.Text = producto.CantVotos

        If producto.Stock > producto.StockMinimo Then
            divEnStock.Visible = True
        Else
            divSinStock.Visible = True
            btnAgregarAlCarrito.Visible = False

        End If
        imgPrd.ImageUrl = producto.LinkImagen
        If producto.Comentarios.Count > 0 Then
            rptComentarios.Visible = True
            rptComentarios.DataMember = "BE_Comentario"
            rptComentarios.DataSource = producto.Comentarios
            rptComentarios.DataBind()
        Else
            div5mentarios.Visible = True
        End If
        'ltlEnStock.Text =
    End Sub

    Protected Function ValidarForm() As Boolean
        Dim huboError As Boolean = False

        RegularExpressionValidator_txtComentario.Visible = False
        


        If Validador.EstaVacio_MaxLength(txtComentario.Text, 250) Then

            huboError = True
            'RegularExpressionValidator_txtComentario.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            RegularExpressionValidator_txtComentario.Visible = True
        End If
        
        If huboError = False Then
            Return True
        End If
        Return False
    End Function
    

    Protected Sub btnGuardarComentario_Click(sender As Object, e As EventArgs) Handles btnGuardarComentario.Click
        Try
            If ValidarForm() = True Then
                Dim coment As New BE_Comentario
                coment.Autor = IIf(String.IsNullOrEmpty(iptAutor.Text), "Anónimo", iptAutor.Text)
                coment.Comentario = txtComentario.Text
                coment.IdProducto = iptCodigo.Value
                coment.Activo = True
                Dim agregado As Boolean = oBll.AgregarComentario(coment)
                If agregado = True Then
                    Response.Redirect("/ProductoAmpliado.aspx?id=" + iptCodigo.Value.ToString + "&guardado=ok", False)
                    Return
                End If
            End If
            Response.Redirect("/ProductoAmpliado.aspx?id=" + iptCodigo.Value.ToString + "&guardado=error-db", False)
            Return
        Catch ex As Exception
            Response.Redirect("/ProductoAmpliado.aspx?id=" + iptCodigo.Value + "&guardado=error-db", False)
            Return
        End Try
    End Sub

    Protected Sub btnAgregarAlCarrito_Click(sender As Object, e As EventArgs) Handles btnAgregarAlCarrito.Click
        Try
            Dim usuario As BE_Usuario = Me.GetUsuarioLogueado()

            Dim oBllVta As New BLL_Ventas
            Dim vta As BE_Venta = oBllVta.GetCarrito(MyBase.GetIdCliente())
            Dim idVenta As Integer = vta.IdVenta
            Dim dv As New BE_Detalle_Venta
            dv.IdVenta = idVenta
            dv.Producto = New BE_Producto With {.IdProducto = iptCodigo.Value}
            dv.Cantidad = 1

            Dim agregado As Object = oBllVta.AgregarProducto(idVenta, dv)
            If agregado Is Nothing Then
                Response.Redirect("ProductoAmpliado.aspx?rtdo=error-stock&id=" + iptCodigo.Value, False)
                Return
            End If
            If agregado = True Then
                Response.Redirect("ProductoAmpliado.aspx?rtdo=ok&id=" + iptCodigo.Value, False)
                Return
            Else
                Response.Redirect("ProductoAmpliado.aspx?rtdo=error&id=" + iptCodigo.Value, False)
                Return
            End If
        Catch ex As Exception
            'Response.Redirect("ProductoAmpliado.aspx?rtdo=error&id=" + iptCodigo.Value, False)
        End Try
       
    End Sub
End Class