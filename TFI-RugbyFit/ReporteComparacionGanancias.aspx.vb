﻿Imports BLL
Imports Microsoft.Reporting.WebForms

Public Class ReporteComparacionGanancias
    Inherits ACL
    Const COD_PERMISO = "MOD_ADM_FZAS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.SetDropdownValues()
            Me.RefrescarReporte()
        End If
    End Sub

    Protected Sub SetDropdownValues()
        Dim AnioActual As Integer
        AnioActual = Convert.ToInt32(Now.ToString("yyyy"))
        Dim MesActual As Integer
        MesActual = Convert.ToInt32(Now.ToString("MM"))
        ddAnio.SelectedValue = AnioActual.ToString
        ddMes.SelectedValue = MesActual.ToString
        Dim AnioAnterior As Integer
        AnioAnterior = (AnioActual - 1).ToString
        ddAnio2.SelectedValue = AnioAnterior
        ddMes2.SelectedValue = MesActual.ToString
    End Sub

    Protected Function GetData() As DsComparacionGanancias
        Dim oBll As New BLL_Reportes
        Dim miDs As New DsComparacionGanancias
        Dim filtros As Hashtable = Me.GetFiltros
        Dim dt As DataTable = oBll.GetReporteComparacionGanancias(filtros)
        Dim fDesde As String = ""
        Dim fHasta As String = ""

        For Each dr As DataRow In dt.Rows
            miDs.DataTableComparacionGcias.AddDataTableComparacionGciasRow(GetNombreMes(dr("mes")), dr("anio"), IIf(IsDBNull(dr("monto")), 0, dr("monto")))
        Next

        Return miDs
        '
    End Function

    Protected Function GetNombreMes(ByVal nro As Integer) As String
        If nro = "0" Then
            Return "Todos"
        End If
        If nro = "1" Then
            Return "01-Enero"
        End If
        If nro = "2" Then
            Return "02-Febrero"
        End If
        If nro = "3" Then
            Return "03-Marzo"
        End If
        If nro = "4" Then
            Return "04-Abril"
        End If
        If nro = "5" Then
            Return "05-Mayo"
        End If
        If nro = "6" Then
            Return "06-Junio"
        End If
        If nro = "7" Then
            Return "07-Julio"
        End If
        If nro = "8" Then
            Return "08-Agosto"
        End If
        If nro = "9" Then
            Return "09-Septiembre"
        End If
        If nro = "10" Then
            Return "10-Octubre"
        End If
        If nro = "11" Then
            Return "11-Noviembre"
        End If
        If nro = "12" Then
            Return "12-Diciembre"
        End If
        Return ""
    End Function

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Me.RefrescarReporte()
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable

        
        filtros.Add("@Mes1", ddMes.SelectedValue)
        filtros.Add("@Anio1", ddAnio.SelectedValue)
        filtros.Add("@Mes2", ddMes2.SelectedValue)
        filtros.Add("@Anio2", ddAnio2.SelectedValue)

        Return filtros
    End Function

    Protected Sub RefrescarReporte()
        rvComparacionGcias.ProcessingMode = ProcessingMode.Local
        rvComparacionGcias.LocalReport.ReportPath = Server.MapPath("~/ReporteComparacionGcias.rdlc")
        Dim dsMio As DsComparacionGanancias = Me.GetData()
        Dim datasource As New ReportDataSource("DsComparacionGanancias", dsMio.Tables(0))
        rvComparacionGcias.LocalReport.DataSources.Clear()
        rvComparacionGcias.LocalReport.DataSources.Add(datasource)
    End Sub
End Class