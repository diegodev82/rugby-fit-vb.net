﻿Imports DAL
Imports BE
Public Class MPP_Chat
    Private oDatos As SQLHelper = SQLHelper.GetInstance
    Private ds As New DataSet
    Public Function GetChat(ByVal IdUsuario As Integer) As BE_Chat
        Dim hdatos As New Hashtable
        hdatos.Add("@IdEmisor", IdUsuario)
        Dim elChat As New BE_Chat
        elChat.Mensajes = New List(Of BE_MensajeChat)
        ds = oDatos.Leer("pa_GetChat", hdatos)
        Dim listado As New List(Of BE_Comentario)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                elChat.IdChat = ds.Tables(0).Rows(0).Item("id_chat")
                For Each item As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(item("id_mensaje")) Then
                        Dim unMsj As New BE_MensajeChat
                        unMsj.IdChat = item("id_chat")
                        unMsj.IdMensaje = item("id_mensaje")
                        unMsj.Mensaje = IIf(IsDBNull(item("mensaje")), "", item("mensaje"))
                        unMsj.Emisor = New BE_Usuario With {.IdUsuario = IIf(IsDBNull(item("id_emisor")), 0, item("id_emisor")), .Nombre = item("nombre_emisor")}
                        unMsj.IdMensaje = item("id_mensaje")
                        unMsj.FechaHora = item("fecha_hora")
                        unMsj.EsRespuesta = item("id_propietario") <> item("id_emisor")
                        unMsj.TiempoTranscurrido = CalcularTiempoTranscurrido(item("fecha_hora"))
                        elChat.Mensajes.Add(unMsj)
                    End If
                Next
            End If
        End If
        Return elChat
    End Function

    Public Function GetChats() As List(Of BE_Chat)
        Dim listado As New List(Of BE_Chat)
        ds = oDatos.Leer("pa_GetChats", Nothing)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unChat As New BE_Chat
                    unChat.Emisor = New BE_Usuario With {.IdUsuario = item("id_emisor"), .Nombre = item("nombre_emisor")}
                    unChat.IdChat = item("id_chat")
                    unChat.Mensajes = Me.GetMensajesChats(item("id_chat"))
                    listado.Add(unChat)
                Next
            End If
        End If
        Return listado
    End Function

    Public Function GetMensajesChats(ByVal idChat As Integer) As List(Of BE_MensajeChat)
        Dim listado As New List(Of BE_MensajeChat)
        Dim hdatos As New Hashtable
        hdatos.Add("@IdChat", idChat)


        ds = oDatos.Leer("pa_GetMensajesChat", hdatos)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(item("id_mensaje")) Then
                        Dim unMsj As New BE_MensajeChat
                        unMsj.IdChat = item("id_chat")
                        unMsj.IdMensaje = item("id_mensaje")
                        unMsj.Mensaje = IIf(IsDBNull(item("mensaje")), "", item("mensaje"))
                        unMsj.Emisor = New BE_Usuario With {.IdUsuario = IIf(IsDBNull(item("id_emisor")), 0, item("id_emisor")), .Nombre = item("nombre_emisor")}
                        unMsj.IdMensaje = item("id_mensaje")
                        unMsj.FechaHora = item("fecha_hora")
                        unMsj.EsRespuesta = item("id_propietario") <> item("id_emisor")
                        unMsj.TiempoTranscurrido = CalcularTiempoTranscurrido(item("fecha_hora"))
                        listado.Add(unMsj)
                    End If
                Next
            End If
        End If
        Return listado
    End Function

    Public Function CalcularTiempoTranscurrido(fechaHoraMsj As DateTime) As String
        Dim t As TimeSpan = DateTime.Now - fechaHoraMsj
        Dim mostrar As String = ""
        If t.TotalSeconds < 60 Then
            mostrar = "Hace menos de un minuto"
        ElseIf t.TotalMinutes < 60 Then
            mostrar = "Hace " + t.Minutes.ToString() + " minutos"
        ElseIf t.TotalHours < 24 Then
            mostrar = "Hace " + t.Hours.ToString() + " horas"
        ElseIf t.TotalHours >= 24 Then
            mostrar = fechaHoraMsj.ToShortDateString
        End If
        Return mostrar
    End Function

    Public Function IniciarChat(ByVal chat As BE_Chat) As Integer
        Dim hdatos As New Hashtable
        hdatos.Add("@IdEmisor", chat.Emisor.IdUsuario)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearChat", hdatos)
        If InsertedId > 0 = True Then
            Return InsertedId
        End If
        Return 0
    End Function

    Public Function AgregarMensaje(ByVal msj As BE_MensajeChat) As Integer
        Dim hdatos As New Hashtable
        hdatos.Add("@IdEmisor", msj.Emisor.IdUsuario)
        hdatos.Add("@IdChat", msj.IdChat)
        hdatos.Add("@Mensaje", msj.Mensaje)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_AgregarMensajeChat", hdatos)
        If InsertedId > 0 = True Then
            Return InsertedId 'se podría devolver un BE_ChatMensaje para hacer la carga async 
        End If
        Return 0
    End Function
End Class
