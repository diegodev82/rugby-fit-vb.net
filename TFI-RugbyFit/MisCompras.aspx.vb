﻿Imports BLL
Imports BE
Public Class MisCompras
    Inherits ACL
    Private oBll As New BLL_Ventas
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.ValidarAccesoCliente()
            div_tbl_vtas.Visible = False
            div_sin_vtas.Visible = False
            If Not IsPostBack Then
                Me._CargarData()
            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub

    Protected Sub _CargarData()
        Dim idCliente As Integer = MyBase.GetIdCliente()
        Dim vtas As List(Of BE_Venta) = oBll.GetVentasPorCliente(idCliente)
        If vtas.Count = 0 Then
            div_sin_vtas.Visible = True
        Else
            rptVtas.DataSource = Nothing
            rptVtas.DataBind()
            rptVtas.DataMember = "BE_Venta"
            rptVtas.DataSource = vtas
            rptVtas.DataBind()
            div_tbl_vtas.Visible = True
        End If
        
    End Sub

End Class