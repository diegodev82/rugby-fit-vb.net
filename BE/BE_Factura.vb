Public Class BE_Factura
    Public Const ESTADO_PAGADA = "PAGADA"
    Public Const ESTADO_NC_SIN_USAR = "NC_SIN_USAR"
    Public Const ESTADO_NC_USADA = "NC_USADA"
    Public Const ESTADO_ANULADA = "ANULADA"
    Public Const TIPO_FC = 1
    Public Const TIPO_NC = 2
    Public Property IdFacturaRel As Integer
    Public Property IdVenta As Integer
    Public Property Ciudad As BE_Ciudad
    Public Property Cliente As BE_Cliente
    Public Property Domicilio As String
    Public Property Estado As String ' PAGADA, PENDIENTE, ANULADA, (NC_SIN_USAR, NC_USADA) => PARA NC
    Public Property Fecha As DateTime
    Public Property FormaPago As Integer
    Public Property IdFactura As Integer
    ''' <summary>
    ''' NC = Nota de Cr�dito
    ''' FC = Factura
    ''' </summary>
    Public Property Items As List(Of BE_Detalle_Factura)
    Public Property Monto As Double
    Public Property Tipo As Integer ' 1 = FC, 2 = NC, 3 = ND

    Public Shared Function GetNombreFormaPago(ByVal idFormaPago As Integer) As String
        'forma pago 1 = TC 2 = CC 3 = Transferencia 4 = Cheque 5 = F
        If idFormaPago = 1 Then
            Return "Tarjeta de Cr�dito"
        End If
        If idFormaPago = 2 Then
            Return "Cuenta Corriente"
        End If
        If idFormaPago = 3 Then
            Return "Transferencia"
        End If
        If idFormaPago = 4 Then
            Return "Cheque"
        End If
        If idFormaPago = 5 Then
            Return "Efectivo"
        End If
        Return "Desconocido"
    End Function

End Class ' BE_Factura
