﻿Imports BE
Imports BLL
Public Class AdminBitacora
    Inherits ACL
    Const COD_PERMISO = "MOD_SEGURIDAD"
    Private oBllBit As New BLL_Bitacora

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.LlenarFiltroEventos()
            Me.LlenarDgv()
        End If

    End Sub

   

    Protected Sub LlenarFiltroEventos()

        Dim listadoEvt As List(Of BE_Evento) = oBllBit.ListarEventos()
        ddFiltroEvento.DataTextField = "Nombre"
        ddFiltroEvento.DataValueField = "IdEvento"
        ddFiltroEvento.DataSource = listadoEvt
        ddFiltroEvento.DataBind()
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Bitacora) = oBllBit.ListarBitacora(filtros)
        txtCantRegistros.Text = registros.Count

        DgvBitacora.AutoGenerateColumns = False
        DgvBitacora.DataSource = registros
        DgvBitacora.DataBind()
    End Sub


    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvBitacora_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvBitacora.PageIndexChanging
        DgvBitacora.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvBitacora_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvBitacora.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroPalabraClave.Text IsNot Nothing Then
            filtros.Add("@Observacion", filtroPalabraClave.Text)
        End If
        Dim idCat As Integer = CType(ddFiltroEvento.SelectedValue, Integer)
        If idCat > 0 Then
            filtros.Add("@IdEvento", idCat)
        End If
        If IsDate(filtroFechaDesde.Text) Then
            Dim fechaDesde As Date = CType(filtroFechaDesde.Text, Date)
            filtros.Add("@FechaDesde", fechaDesde)
        End If
        If IsDate(filtroFechaHasta.Text) Then
            Dim fechaHasta As Date = CType(filtroFechaHasta.Text, Date)
            filtros.Add("@FechaHasta", fechaHasta)
        End If
        Return filtros
    End Function

    Protected Sub DgvBitacora_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles DgvBitacora.RowDataBound
        Dim cssClass As String = ""
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim impac As String = e.Row.Cells(1).Text
            e.Row.CssClass = impac.ToLower
            e.Row.Cells(1).CssClass = "td_impacto"
        End If
    End Sub
End Class