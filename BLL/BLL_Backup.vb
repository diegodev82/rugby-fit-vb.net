Imports MPP
Imports BE
Imports System.IO

Public Class BLL_Backup


    Public oMpp As New MPP_Backup

    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Backup)
        Return oMpp.Listar(filtros)
    End Function

    Public Function Eliminar(ByVal bk As BE_Backup) As Boolean

    End Function

    Public Function RegistrarBackup(ByVal ruta As String) As Boolean
        Return oMpp.RegistrarBackup(ruta)
    End Function

    Public Function RealizarBackup(Optional rutaArchivo As Object = Nothing) As Boolean
        Dim fecha_backup As String = Format(Date.Today, "dd-MM-yyyy")
        Dim rutaArchivoPorDefecto As String = Environment.CurrentDirectory & "\Backups\"
        Dim rutaGenerada As String = rutaArchivoPorDefecto & fecha_backup & "\"
        Dim extension As String = ".bak"

        Dim nombreGenerado As String = "BK-" & fecha_backup & extension 'Falta concatenarle fecha y hora
        If Not rutaArchivo Is Nothing Then
            rutaGenerada = rutaArchivo
        End If
        'Me._CrearDirectorioBackup(rutaGenerada)

        Return oMpp.RealizarBackup(rutaGenerada)
    End Function
    Private Sub _CrearDirectorioBackup(ruta As String)
        If Not Directory.Exists(ruta) Then
            Directory.CreateDirectory(ruta)
        End If
    End Sub

    Public Function GetPorId(ByVal IdBackup As Integer) As BE_Backup
        Return oMpp.GetPorId(IdBackup)
    End Function
    Public Function RestaurarBackup(ByVal ruta As String) As Boolean
        Return oMpp.RestaurarBackup(ruta)
    End Function

End Class ' BLL_Backup
