Imports BE
Imports DAL

Public Class MPP_Categorias


    ''' 
    ''' <param name="objCategoria"></param>
    Public Function Crear(ByVal objCategoria As BE_Categoria) As BE_Categoria
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objCategoria"></param>
    Public Function Editar(ByVal objCategoria As BE_Categoria) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objCategoria"></param>
    Public Function Eliminar(ByVal objCategoria As BE_Categoria) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Categoria)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Categoria)
        DS = oDatos.Leer("pa_GetCategorias", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim unaCat As New BE_Categoria With {.IdCategoria = dr("id_categoria"), .Nombre = dr("nombre")}
                listado.Add(unaCat)
            Next
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objCategoria"></param>
    Public Function ValidarExistente(ByVal objCategoria As BE_Categoria) As Boolean
        ValidarExistente = False
    End Function


End Class ' MPP_Categorias

