﻿Imports MPP
Imports BE
Public Class BLL_Encuesta
    Private oMiMpp As New MPP_Encuesta

    Public Function Votar(ByVal idEnc As Integer, ByVal idRta As Integer) As Boolean
        Return oMiMpp.Votar(idEnc, idRta)
    End Function

    Public Function GetFichasOpinion() As List(Of BE_Encuesta)
        Return oMiMpp.GetFichasOpinion()
    End Function

    Public Function GetEncuestaActual() As BE_Encuesta
        Return oMiMpp.GetEncuestaActual()
    End Function

    Public Function GetPorId(ByVal id As Integer) As BE_Encuesta
        Return oMiMpp.GetPorId(id)
    End Function

    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Encuesta)
        Return oMiMpp.Listar(filtros)
    End Function

    Public Function ListarRespuestas(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_RespuestaEncuesta)
        Return oMiMpp.ListarRespuesta(filtros)
    End Function

    Public Function Crear(ByVal objEnc As BE_Encuesta) As BE_Encuesta
        If oMiMpp.ValidarEncuestaExistente(objEnc) = True Then
            Return Nothing
        End If
        Return oMiMpp.Crear(objEnc)
    End Function

    Public Function Editar(ByVal objEnc As BE_Encuesta) As Boolean
        If oMiMpp.ValidarEncuestaExistente(objEnc) = True Then
            Return Nothing
        End If
        Return oMiMpp.Editar(objEnc)
    End Function

    Public Function Eliminar(ByVal objEnc As BE_Encuesta) As Integer
        Return oMiMpp.Eliminar(objEnc)
    End Function

    Public Function CrearRespuesta(ByVal objRta As BE_RespuestaEncuesta) As BE_RespuestaEncuesta
        If oMiMpp.ValidarRespuestaExistente(objRta) = True Then
            Return Nothing
        End If
        Return oMiMpp.CrearRespuesta(objRta)
    End Function

    Public Function EditarRespuesta(ByVal objRta As BE_RespuestaEncuesta) As Object
        If oMiMpp.ValidarRespuestaExistente(objRta) = True Then
            Return Nothing
        End If
        Return oMiMpp.EditarRespuesta(objRta)
    End Function

    Public Function EliminarRespuesta(ByVal objRta As BE_RespuestaEncuesta) As Integer
        Return oMiMpp.EliminarRespuesta(objRta)
    End Function
End Class
