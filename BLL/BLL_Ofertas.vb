
Imports BE
Imports MPP


Public Class BLL_Ofertas


    Public m_MPP_Ofertas As MPP.MPP_Ofertas

    ''' 
    ''' <param name="objOferta"></param>
    Public Function Crear(ByVal objOferta As BE_Oferta) As BE.BE_Oferta
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objOferta"></param>
    Public Function Editar(ByVal objOferta As BE_Oferta) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objOferta"></param>
    Public Function Eliminar(ByVal objOferta As BE_Oferta) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Oferta)
        Listar = Nothing
    End Function


End Class ' BLL_Ofertas
