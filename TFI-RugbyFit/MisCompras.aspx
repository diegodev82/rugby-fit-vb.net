﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="MisCompras.aspx.vb" Inherits="TFI_RugbyFit.MisCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/css/miscompras.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <h2>Mis compras</h2>
    <% If Request.QueryString("anulado") = "ok" Then%>
    <div class="alert alert-success">
        <p>La factura fue anulada correctamente</p>
    </div>
    <% End If %>
    <div class="alert alert-info" runat="server" id="div_sin_vtas" visible="false">
        <p>A&uacute;n no ha realizado ninguna compra.<a href="Productos.aspx">&iquest;Desea comenzar ahora?</a> </p>
    </div>
    <div runat="server" id="div_tbl_vtas" visible="false">
        <table class="table table-bordered table-hover table-hover">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th>Nro Factura</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rptVtas" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><%# Container.DataItem.Fecha.ToString() %></td>
                            <td>$ <%# FormatNumber(Container.DataItem.MontoTotal, 2, TriState.True) %></td>
                            <td><%# IIf(Container.DataItem.IdFactura > 0, "FC " + Container.DataItem.IdFactura.ToString, "") %></td>
                            <td ><span class="label <%# Container.DataItem.Estado %>"><%# Container.DataItem.Estado %></span></td>
                            <td>
                                <asp:LinkButton ID="btnPagar"  runat="server" PostBackUrl="Pagar.aspx" CssClass="btn btn-sm btn-success" visible='<%# IIf(Container.DataItem.Estado = "ACTIVA" AND Container.DataItem.MontoTotal > 0 , True, False) %>'><i class="glyphicon glyphicon-piggy-bank"></i> Pagar</asp:LinkButton>
                                <asp:LinkButton ID="btnSeguirComprando"  runat="server" PostBackUrl="Productos.aspx" CssClass="btn btn-sm btn-info" visible='<%# IIf(Container.DataItem.Estado <> "PAGADA" And Container.DataItem.MontoTotal = 0, True, False)%>'><i class="glyphicon glyphicon-shopping-cart"></i> Seguir Comprando</asp:LinkButton>
                                <asp:LinkButton ID="btnAnular"  PostBackUrl='<%# "~/Factura.aspx?id="+ Container.DataItem.IdFactura.ToString %>'  runat="server" visible='<%# IIf(Container.DataItem.Estado = "PAGADA", True, False) %>' CssClass="btn btn-sm btn-danger"><i class="glyphicon glyphicon-ban-circle"></i> Anular</asp:LinkButton>
                                <asp:LinkButton ID="btnVer" PostBackUrl='<%# "~/Factura.aspx?id="+ Container.DataItem.IdFactura.ToString %>'  runat="server" CssClass="btn btn-sm btn-default" visible='<%# IIf(Container.DataItem.Estado = "PAGADA" OR Container.DataItem.Estado = "ENTREGADA", True, False) %>'><i class="glyphicon glyphicon-eye-open"></i> Ver</asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
