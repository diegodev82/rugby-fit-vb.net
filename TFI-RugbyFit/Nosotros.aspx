﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Nosotros.aspx.vb" Inherits="TFI_RugbyFit.Nosotros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="container">
        <div class="col-md-6">
            <h1>Acerca Nuestro</h1>
            <div>
                <p>
                    Rugby Fit es una empresa que se dedica a la comercialización de equipamiento para entrenamiento de rugby. Dentro del conjunto de productos ofrecidos se encuentran los siguientes: máquinas de scrum, máquinas de ruck, escudos de choque, bolsas de tackle, dispositivos de ruck, trineos de empuje, aros de line, dispositivos de pase.
                </p>
                <p>
                    Somos una empresa joven en pleno crecimiento y con muchas ganas de ayudar a perfeccionar los entrenamientos de los equipos de rugby. Gran parte del equipo está conformado por jugadores de este deporte que conocen las necesidades de los equipos y además logran trasladar los valores del rugby a la empresa.
                </p>
            </div>
            <h3>&iquest;Por qu&eacute; elegirnos?</h3>
            <div>
                <p>
                    Comercializamos productos que no se encuentran actualmente en el mercado, de excelente calidad y diseñados específicamente para trabajar los distintos aspectos claves del deporte, como por ejemplo: coordinación de scrum, limpieza de rucks, perfeccionamiento del pase, perfeccionamiento del tackle, asistencia al tackle, entre otros.
                </p>
                <p>
                    Además, gracias a los conocimientos en Educación Física y de la experiencia en el deporte con la que contamos, ofrecemos asesoramiento acerca de los beneficios de cada producto y qué aspectos del juego mejora cada uno, tanto física como técnicamente. En este aspecto nos diferenciaremos de nuestros competidores que solo ofrecen sus productos sin conocer las necesidades reales del cliente.
                </p>
                <p>
                    También ofrecemos la posibilidad de diseñar equipamiento personalizado de acuerdo a las necesidades no solo técnicas sino también de infraestructura del club.
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <img src="assets/img/Logo-RugbyFit.png" class="img-responsive center-block" alt="Rugby Fit" />
        </div>
    </div>
</asp:Content>
