﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports BE
Imports BLL

Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
<ScriptService()>
Public Class AjaxUtils
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function GetCiudadesPorProvincia(ByVal id_provincia As Integer) As List(Of BE_Ciudad)
        Dim oBll As New BLL_Ciudades()
        Dim hdatos As New Hashtable
        hdatos.Add("@IdProvincia", id_provincia)
        Return oBll.Listar(hdatos)
    End Function

    <WebMethod()> _
    Public Function Votar(ByVal id_encuesta As Integer, ByVal voto As Integer) As Boolean
        ' voto -> id_respuesta
        Dim oBll As New BLL_Encuesta()
        Return oBll.Votar(id_encuesta, voto)
    End Function


    <WebMethod()> _
    Public Function VerResultados() As List(Of BE_RespuestaEncuesta)
        ' voto -> id_respuesta
        Dim oBll As New BLL_Encuesta()
        Dim enc As BE_Encuesta = oBll.GetEncuestaActual
        Dim rtas As List(Of BE_RespuestaEncuesta) = enc.Respuestas
        Dim cantVotosTotal As Integer = 0
        For Each item As BE_RespuestaEncuesta In rtas
            cantVotosTotal = cantVotosTotal + item.CantVotos
        Next
        For Each rta As BE_RespuestaEncuesta In rtas
            If cantVotosTotal > 0 Then
                rta.Porcentaje = (rta.CantVotos / cantVotosTotal) * 100
            Else
                rta.Porcentaje = 0
            End If
        Next
        Return rtas
    End Function

    <WebMethod()> _
    Public Function ValorarProducto(ByVal idPrd As Integer, ByVal valoracion As Integer) As Integer
        Dim oBll As New BLL_Productos()
        Dim ok As Boolean = oBll.Valorar(idPrd, valoracion)
        If ok = True Then
            Dim prd As BE_Producto = oBll.GetPorId(idPrd)
            Return prd.Rating
        End If
        Return -1
    End Function

    <WebMethod()> _
    Public Function ValidarStock(ByVal IdProducto As Integer, ByVal Cant As Integer) As Boolean
        ' voto -> id_respuesta
        Dim oBllPrd As New BLL_Productos()
        Dim prd As BE_Producto = oBllPrd.GetPorId(IdProducto)
        If Not prd Is Nothing Then
            Return prd.Stock >= Cant
        End If
        Return False
    End Function

    <WebMethod()> _
    Public Function ActualizarCantidadCarrito(ByVal IdVtaDet As Integer, ByVal Cant As Integer) As Boolean
        ' voto -> id_respuesta
        Dim oBll As New BLL_Ventas()
        Dim vd As New BE_Detalle_Venta
        vd.IdDetalleVenta = IdVtaDet
        vd.Cantidad = Cant
        Return oBll.EditarItem(vd)
    End Function

    <WebMethod()> _
    Public Function EliminarItemCarrito(ByVal IdVtaDet As Integer) As Boolean
        Dim oBll As New BLL_Ventas()
        Dim vd As New BE_Detalle_Venta
        vd.IdDetalleVenta = IdVtaDet
        Return oBll.EliminarItem(vd)
    End Function

    <WebMethod()> _
    Public Function TCExistente(ByVal Numero As Int64) As BE_TarjetaCredito
        Dim oBll As New BLL_Ventas()
        Dim bllSeg As New BLL_Seguridad
        Dim tc As BE_TarjetaCredito = oBll.ValidarTCExistente(Numero)
        tc.CodigoDesencriptado = bllSeg.Desencriptar(tc.Codigo, True)
        Return tc
    End Function
End Class