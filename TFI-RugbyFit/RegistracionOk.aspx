﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="RegistracionOk.aspx.vb" Inherits="TFI_RugbyFit.RegistracionOk" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-thumbs-up"></span> 
        La registraci&oacute;n se realiz&oacute; con &eacute;xito. En breves estar&aacute; recibiendo un correo para confirmar la misma.
        <br /> Ingrese al link enviado para completar la acci&oacute;n
    </div> 
</asp:Content>
