﻿$(document).ready(function () {
    pintarEstrellas();
    $("body").on("click.Valorar", ".js-btn-star", function (e) {
        e.preventDefault();
        var idPrd = $("#idPrd").val();
        if (sessionStorage.getItem("valoracion_" + idPrd) != null) {
            alert("Disculpe, ya valoró este producto");
            return;
        }
        var value = $(this).data("value");
        $.ajax({
            "url": "/AjaxUtils.asmx/ValorarProducto",
            data: "{'idPrd':'" + idPrd + "', 'valoracion':'" + value + "'}",
            contentType: 'application/json; utf-8',
            dataType: 'json',
            type: 'post',
            success: function (data) {
                console.log(data);
                if (data.d >= 0) {
                    var cantOpiniones = $("#cantOpiniones").html()*1;
                    $("#cantOpiniones").html(cantOpiniones + 1)
                    $("#rating").html(data.d);
                    sessionStorage.setItem("valoracion_" + idPrd, value);
                    alert("Gracias por valorar nuestro producto");
                }
            },
        }
       );

    })
});

function pintarEstrellas() {
    var rating = $("#rating").html() * 1;
    $(".js-btn-star").each(function (index) {
        var ele = $(this);

        if (ele.data("value") <= rating) {
            ele.removeClass("btn-default");
            ele.addClass("btn-warning");
        }
    });
}