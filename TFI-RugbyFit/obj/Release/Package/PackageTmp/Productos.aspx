﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Productos.aspx.vb" Inherits="TFI_RugbyFit.Productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/productos.css" />
    <script src="assets/js/paginas/productos.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row" id="productos_page">
        <!-- Barra lateral -->
        <div class="col-md-3">
            <asp:Repeater ID="rptCategorias" runat="server">
                <ItemTemplate>
                    <a href="Productos.aspx?idCat=<%# Container.DataItem.IdCategoria %>"   class="list-group-item <%# IIf(Request.QueryString("idCat") =  Container.DataItem.IdCategoria.ToString, "active", "") %>"><%# Container.DataItem.Nombre %></a>
                    
                </ItemTemplate>
            </asp:Repeater>

        </div>
        <!-- /Barra lateral -->
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div id="filter-panel" class="filter-panel">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group col-md-4">
                                    <label class="filter-col" style="margin-right: 0;" for="pref-perpage">
                                        <asp:Literal ID="Literal5" runat="server" Text="Categorías"></asp:Literal></label>
                                         <asp:DropDownList CssClass="form-control" ID="ddFiltroCategoria" AppendDataBoundItems="true" runat="server">
                                        <asp:ListItem Value="0">Todas</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="filter-col" style="margin-right: 0;" for="pref-search">
                                        <asp:Literal ID="Literal6" runat="server" Text="Código"></asp:Literal></label>
                                    <asp:TextBox ID="filtroCodigo"  CssClass="form-control  input-sm" TextMode="Number" Max="50" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="filter-col" style="margin-right: 0;" for="pref-search">
                                        <asp:Literal ID="Literal7" runat="server" Text="Nombre"></asp:Literal></label>
                                    <asp:TextBox ID="filtroNombre"  CssClass="form-control input-sm" runat="server"></asp:TextBox>
                                </div>                             
                                    <br />
                                <a href="Productos.aspx" class="btn btn-default pull-right"><span class="glyphicon glyphicon-trash"></span>  </a>&nbsp;
                                    <asp:LinkButton OnClick="btnBuscar_Click" ID="btnBuscar" cssclass="btn btn-info pull-right" runat="server"><span class="glyphicon glyphicon-search"> </span></asp:LinkButton>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
            <div class="row carousel-holder ">
                <!-- ACA VAN LOS BANNERS -->
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="assets/media/banners/banner-1.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img class="slide-image" src="assets/media/banners/banner-2.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img class="slide-image" src="assets/media/banners/banner-3.jpg" alt="" />
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="well" id="barra_comparacion" style="display:none">
                    <div id="div_prd_agregados"></div>
                    <a href="javascript:goToComparacion();" class="btn btn-sm btn-success pull-right" id="btn_comparar">Comparar</a>
                    <a href="javascript:limpiarComparacion();" class="btn btn-sm btn-danger pull-right" id="btn_limpiar_comparacion"><i class="glyphicon glyphicon-trash"></i></a>
                </div>
                <p class="well" runat="server" id="MsjNoRtdos" visible="false">
                    No se han encontrado resultados para su b&uacute;squeda.
                </p>
                <asp:Repeater ID="prdRepeater" runat="server">
                    <ItemTemplate>
                        <div class="col-sm-4 col-lg-4 col-md-4">
                            <div class="thumbnail">
                                <asp:Image ID="imgPrd" data-imgprd=" <%# Container.DataItem.IdProducto %>" ImageUrl='<%# iif(Eval("LinkImagen")="","/assets/img/img-vacia.png",Eval("LinkImagen")) %>' Width="320" Height="150" CssClass="img-responsive" runat="server" />
                                <!-- <img src="http://placehold.it/320x150" alt="" /> -->
                                <div class="caption">
                                    <h4 class="pull-right">$ <%# Container.DataItem.Precio %></h4>
                                    <h4><a href="/ProductoAmpliado.aspx?id=<%# Container.DataItem.IdProducto %>" id="nombre_<%# Container.DataItem.IdProducto %>">
                                        <asp:Literal ID="Literal9" runat="server" Text="<%# Container.DataItem.Nombre %>"></asp:Literal></a></h4>
                                    <h6>
                                        <asp:Literal ID="Literal10" runat="server" Text="<%# Container.DataItem.Categoria.Nombre %>"></asp:Literal></h6>
                                    <p>
                                        <asp:Literal ID="Literal11" runat="server" Text="<%# Container.DataItem.DescripcionCorta %>"></asp:Literal>
                                        <br />
                                       <%-- <a target="_blank" href="/ProductoAmpliado.aspx?id=<%# Container.DataItem.IdProducto %>">
                                            <asp:Literal ID="Literal12" Text="Ver más" runat="server"></asp:Literal>
                                        </a>--%>
                                    </p>
                                </div>
                                <div class="ratings">
                                    <p class="pull-right"><%# Container.DataItem.CantVotos %> opiniones</p>
                                 
                                    <ajaxToolkit:Rating 
                                        ID="ratingPrd" 
                                        CssClass="ratingStars"
                                        runat="server" 
                                        CurrentRating="<%# IIf(Container.DataItem.Rating > 0,Container.DataItem.Rating, 2) %>"
                                        MaxRating="5"
                                        ReadOnly="true"
                                        StarCssClass="glyphicon glyphicon-star stars"
                                        WaitingStarCssClass="savedRatingStar"
                                        FilledStarCssClass="text-danger"
                                        EmptyStarCssClass="text-muted">
                                    </ajaxToolkit:Rating>
                                </div>
                                <div class="acciones">
                                    <%--  <span class="glyphicon glyphicon-pushpin"></span>
                            <span class="glyphicon glyphicon-shopping-cart"></span>--%>

                                    <a href="javascript:void(0)" class="btn btn-xs btn-default btn-comparar-prd js_add_comparar_prd" title="Agregar para comparar" data-toggle="tooltip" data-idPrd="<%# Container.DataItem.IdProducto %>">
                                        <span aria-hidden="true" class="glyphicon glyphicon-pushpin"></span></a>
                                    <a class="btn btn-xs btn-success btn-agregar-prd" title="Agregar a la compra" data-toggle="tooltip" href="javascript:void(0)">
                                        <span aria-hidden="true" class="glyphicon glyphicon-shopping-cart"></span>
                                    </a>
                                     <%--<asp:LinkButton ID="btnComparar" runat="server" Cssclass="btn btn-xs btn-default btn-comparar-prd" title="Agregar para comparar" data-toggle="tooltip" CommandName="AgregarParaComparar" CommandArgument="<%# Container.DataItem.IdProducto %>"> <span aria-hidden="true" class="glyphicon glyphicon-pushpin"></span></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Cssclass="btn btn-xs btn-success btn-agregar-prd" title="Agregar a la comprar" data-toggle="tooltip"  ><span aria-hidden="true" class="glyphicon glyphicon-shopping-cart"></span></asp:LinkButton>--%>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

        </div>
        <!-- /.row -->
    </div>
</asp:Content>

