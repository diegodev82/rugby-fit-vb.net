﻿Imports BLL
Imports BE
Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("UsuarioLogueado") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Protected Sub btn_login_LoginPage_Click(sender As Object, e As EventArgs) Handles btn_login_LoginPage.Click
        Dim oBLLSeg As New BLL_Seguridad
        Dim rta As Object = oBLLSeg.Login(iptNicknameLoginPage.Text, iptContraseniaLoginPage.Text)
        If rta.GetType() = GetType(BE_Usuario) Then
            Dim dataUsuario As BE_Usuario = rta
            Session("UsuarioLogueado") = dataUsuario
            If dataUsuario.EsEmpleado Then
                Response.Redirect("AdminDefault.aspx?rta=ok")
            Else
                Dim oBllVta As New BLL_Ventas
                dataUsuario.Cliente.VentaWeb = oBllVta.GetCarrito(dataUsuario.Cliente.IdCliente)
                Session("VentaWeb") = dataUsuario.Cliente.VentaWeb
                Response.Redirect("Default.aspx?rta=ok")
            End If

        End If
        Dim cod_error As String = "error-desconocido"
        If rta.GetType() = GetType(Integer) Then
            If rta = 1 Then
                cod_error = "usuario-incorrecto"
            End If
            If rta = 2 Then
                cod_error = "usuario-bloqueado"
            End If
            If rta = 3 Then
                cod_error = "contrasenia-incorrecta"
            End If
            If rta = 4 Then
                cod_error = "registracion-no-confirmada"
            End If
        End If
        Response.Redirect("Login.aspx?to=1&rta=" + cod_error)
    End Sub

  
End Class