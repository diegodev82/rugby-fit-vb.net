﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="MisPuntos.aspx.vb" Inherits="TFI_RugbyFit.MisPuntos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <h2>Mis Puntos Obtenidos</h2>
    <div class="alert alert-info" runat="server" id="div_sin_puntos" visible="false">
        <p>A&uacute;n no ha obtenido ning&uacute;n punto. <a href="Productos.aspx">&iquest;Desea comprar ahora?</a> </p>
    </div>
    <div runat="server" id="div_tbl_puntos" visible="false">
        <table class="table table-bordered table-hover table-hover">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Producto</th>
                    <th>Nro Factura</th>
                    <th>Puntos</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rptPuntos" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><%# Container.DataItem.FechaHora.ToString() %></td>
                            <td><%# Container.DataItem.Producto.Nombre %></td>
                            <td><%# IIf(Container.DataItem.Factura.IdFactura > 0, "FC " + Container.DataItem.Factura.IdFactura.ToString, "") %></td>
                            <td class="text-right"><%# Container.DataItem.Cantidad %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3" class="text-right">Total</td>
                    <td class="text-right"><asp:Literal ID="ltlTotalPuntos" runat="server">xxxxxx</asp:Literal></td>
                </tr>
            </tfoot>
        </table>
    </div>
</asp:Content>
