﻿Imports BLL
Imports BE
Public Class AdminChat
    Inherits ACL
    Private oBLL As New BLL_Chat
    Private chats As New List(Of BE_Chat)
    Const COD_PERMISO = "MOD_MKT"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ValidarAcceso(COD_PERMISO)
            If Not IsPostBack Then
                Me.CargarChats()
                If Not Request.QueryString("id") Is Nothing Then
                    CargarMensajesChat(Request.QueryString("id"))
                End If
            End If
            'Seteo el maxlength acá porque por html no lo toma
            txtRta.Attributes("MaxLength") = 150
        Catch ex As Exception

        End Try
        
      
    End Sub

    Public Sub CargarChats()
        Me.chats = oBLL.GetChats()
        rptChats.DataSource = Nothing
        rptChats.DataBind()
        rptChats.DataMember = "BE_Chat"
        rptChats.DataSource = chats
        rptChats.DataBind()
    End Sub

    Private Sub CargarMensajesChat(idChat As Integer)
        Dim chatEncontrado As BE_Chat = Nothing
        For Each unChat As BE_Chat In Me.chats
            If unChat.IdChat = idChat Then
                chatEncontrado = unChat
                Exit For
            End If
        Next
        If Not chatEncontrado Is Nothing Then
            iptIdChat.Value = chatEncontrado.IdChat
            rptChatAmpliado.DataSource = Nothing
            rptChatAmpliado.DataBind()
            rptChatAmpliado.DataMember = "BE_MensajeChat"
            rptChatAmpliado.DataSource = chatEncontrado.Mensajes
            rptChatAmpliado.DataBind()
            phRta.Visible = True
            mensajeInicial.Visible = False
        End If
    End Sub

    Protected Sub btnEnviarMensaje_Click(sender As Object, e As EventArgs) Handles btnEnviarMensaje.Click
        Try
            Dim msj As New BE_MensajeChat
            msj.IdChat = iptIdChat.Value
            msj.Mensaje = txtRta.Text
            msj.Emisor = New BE_Usuario With {.IdUsuario = Me.GetIdUsuario()}
            Dim agregado As Boolean = oBLL.AgregarMensaje(msj)
            If agregado = True Then
                Response.Redirect("AdminChat.aspx?id=" + msj.IdChat.ToString + "&send=ok")
            End If
        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
        
    End Sub
End Class