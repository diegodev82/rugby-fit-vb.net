SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_EsAdministrador
	@IdUsuario Integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT COUNT(*) AS cant FROM usuario_familia WHERE id_familia = 1 AND id_usuario = @IdUsuario

END
GO
