﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Novedades

    '''<summary>
    '''PlaceHolder1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolder1 As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''msjeRtaOK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msjeRtaOK As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''msjeRtaError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msjeRtaError As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''iptEmailNewsletter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iptEmailNewsletter As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RequiredFieldValidator_EmailNews control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator_EmailNews As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''btnSuscribirse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSuscribirse As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rptNovedades control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptNovedades As Global.System.Web.UI.WebControls.Repeater
End Class
