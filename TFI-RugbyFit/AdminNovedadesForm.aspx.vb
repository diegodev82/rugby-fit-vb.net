﻿Imports BLL
Imports BE
Public Class AdminNovedadesForm
    Inherits ACL
    Private oBll As New BLL_Novedad
    Const RUTA_NOVEDADES = "/assets/media/novedades/"
    Const COD_PERMISO = "MOD_MKT"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ValidarAcceso(COD_PERMISO)
            If Not IsPostBack Then
                Me.CargarData()
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            Dim oNov As New BE_Novedad

            oNov.IdNovedad = IIf(iptCodigo.Value <> "", iptCodigo.Value, 0)
            oNov.Titulo = iptTitulo.Text
            oNov.Copete = iptCopete.Text
            oNov.Cuerpo = iptCuerpo.Text
            oNov.FechaHora = iptFecha.Text
            Dim ruta_foto As String = SubirArchivo()
            If Not ruta_foto Is Nothing Then
                oNov.LinkImagen = ruta_foto
            Else
                oNov.LinkImagen = imgFotoSubida.ImageUrl
            End If

            Dim novedadGuardada As BE_Novedad = Nothing
            If oNov.IdNovedad > 0 Then
                Dim guardado As Boolean = oBll.Editar(oNov)
                If guardado = True Then
                    novedadGuardada = oNov
                End If
            Else
                novedadGuardada = oBll.Crear(oNov)
            End If
            If novedadGuardada Is Nothing Then
                MensajeRespuesta.Text = "Ya existe una novedad registrada con ese título. Por favor, ingrese otro título"
                PanelMensajeRespuesta.Visible = True
                Return
            End If
            If novedadGuardada.IdNovedad > 0 Then
                Response.Redirect("AdminNovedadesForm.aspx?guardado=ok&id=" + novedadGuardada.IdNovedad.ToString)
            Else
                MensajeRespuesta.Text = "Se produjo un error desconocido y la registación no pudo realizarse. Inténtelo nuevamente, por favor"
                PanelMensajeRespuesta.Visible = True
            End If
        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
       
    End Sub
    Protected Sub CargarData()
        If Request.QueryString("id") IsNot Nothing Then
            If Not IsNumeric(Request.QueryString("id")) Then
                Return
            End If
            Dim idNovedad As Integer = CType(Request.QueryString("id"), Integer)
            Dim dataNovedad As BE_Novedad = oBll.GetPorId(idNovedad)
            If dataNovedad IsNot Nothing Then
                iptCodigo.Value = dataNovedad.IdNovedad
                iptTitulo.Text = dataNovedad.Titulo
                iptCopete.Text = dataNovedad.Copete
                iptCuerpo.Text = dataNovedad.Cuerpo
                iptFecha.Text = dataNovedad.FechaHora.ToString("yyyy-MM-dd")

                If dataNovedad.LinkImagen IsNot Nothing And dataNovedad.LinkImagen.Trim.Length > 0 Then
                    imgFotoSubida.Visible = True
                    imgFotoSubida.ImageUrl = dataNovedad.LinkImagen
                End If
                btnEliminar.Visible = True
            Else
                iptFecha.Text = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
            End If
        End If
    End Sub
    Protected Function SubirArchivo() As String
        If iptFoto.HasFile Then
            Try
                iptFoto.SaveAs(MapPath(RUTA_NOVEDADES & iptFoto.FileName))
                Return RUTA_NOVEDADES & iptFoto.FileName
            Catch ex As Exception
                'Label1.Text = "ERROR: " & ex.Message.ToString()
            End Try
        End If
        Return Nothing
    End Function

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Try
            If IsNumeric(iptCodigo.Value) Then
                Dim idNov As Integer = CType(iptCodigo.Value, Integer)
                Dim objNov As New BE_Novedad With {.IdNovedad = idNov}
                Dim eliminado As Integer = oBll.Eliminar(objNov)
                If eliminado = 1 Then
                    Response.Redirect("AdminNovedadesList.aspx?eliminado=ok")
                Else
                    RtaErrorEliminar.Visible = True
                    msjErrorDB.Visible = True
                End If
            End If

        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
        
    End Sub
End Class