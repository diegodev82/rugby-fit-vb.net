﻿Imports BE

Public Class Utiles
    Inherits System.Web.UI.Page

    Public Shared Function TienePermiso(CodPermisoValidar As String) As Boolean
        If HttpContext.Current.Session("UsuarioLogueado") Is Nothing Then
            Return False
        End If
        Dim UsuarioLogueado As BE_Usuario = CType(HttpContext.Current.Session("UsuarioLogueado"), BE_Usuario)
        For Each permiso As BE_Permiso In UsuarioLogueado.Permisos
            If CodPermisoValidar = permiso.Codigo Then
                Return True
            End If
        Next
        Return False
    End Function

    


End Class
