﻿Public Class BE_Pago
    Public Const PAGO_CON_NC = "NC"
    Public Const PAGO_CON_TC = "TC"
    Public Const SALIDA = "S"
    Public Const ENTRADA = "E"

    Public Property IdPago As Integer
    Public Property IdCliente As Integer
    Public Property Monto As Decimal
    Public Property IdFacturaCancela As Integer
    Public Property IdFacturaUsa As Integer
    Public Property Fecha As DateTime
    Public Property TipoPago As String 'Con qué pagó con TC o con NC
    Public Property EsParcial As Boolean
    Public Property TipoMovimiento As Char
End Class
