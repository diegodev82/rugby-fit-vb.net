Imports DAL
Imports BE

Public Class MPP_Publicidades



    ''' 
    ''' <param name="objPubl"></param>
    Public Function Crear(ByVal objPubl As BE_Publicidad) As BE_Publicidad
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objPubl"></param>
    Public Function Editar(ByVal objPubl As BE_Publicidad) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objPubl"></param>
    Public Function Eliminar(ByVal objPubl As BE_Publicidad) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Publicidad)
        Listar = Nothing
    End Function

    ''' 
    ''' <param name="objPubl"></param>
    Public Function ValidarExistente(ByVal objPubl As BE_Publicidad) As Boolean
        ValidarExistente = False
    End Function


End Class ' MPP_Publicidades

