﻿Imports BLL
Imports BE
Public Class AdminModificarContrasenia
    Inherits ACL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAcceso()
        If Not IsPostBack Then

        End If
    End Sub
    Protected Sub btn_CambiarPwd_Click(sender As Object, e As EventArgs) Handles btn_CambiarPwd.Click
        Dim misDatos As BE_Usuario = Me.GetUsuarioLogueado
        Dim bllSeg As New BLL_Seguridad

        
        If Not misDatos.Contrasenia = bllSeg.Encriptar(iptContraseniaActual.Text) Then
            ltlMensajeError.Text = "La contraseña ingresada no coincide con la actual. Inténtelo nuevamente."
            phMsjeError.Visible = True
            phMsjeOk.Visible = False
            Return
        End If
        Dim miBll As New BLL_Usuarios

        Dim objUsuario As New BE_Usuario With {.Contrasenia = bllSeg.Encriptar(iptNuevaContrasenia.Text), .IdUsuario = Me.GetIdUsuario}
        Dim modificado As Boolean = miBll.ModificarContrasenia(objUsuario)
        If modificado = True Then
            Dim oBllCli As New BLL_Clientes
            misDatos.Contrasenia = bllSeg.Encriptar(iptNuevaContrasenia.Text)
            Dim enviado As Boolean = oBllCli.EnviarEmailResetPwd(misDatos.Email, iptNuevaContrasenia.Text)
            Session.Clear()
            Session.Abandon()
            Response.Redirect("Login.aspx?rtdo=contrasenia-modificada")
        End If
    End Sub

End Class