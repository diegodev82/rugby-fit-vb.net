﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminBackup.aspx.vb" Inherits="TFI_RugbyFit.AdminBackup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <div class="content-header">
            <% If Request.QueryString("generado") = "ok" Then%>
            <div class="block full">
                <div class="alert alert-success">
                    <p>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                        <asp:Literal ID="Literal1" runat="server" Text="El backup fue generado con éxito"></asp:Literal>
                    </p>
                </div>
            </div>
            <% End If%>
            <% If Request.QueryString("generado") = "error-db" Or Request.QueryString("generado") = "error-trx" Then%>
            <div class="block full">
                <div class="alert alert-danger">
                    <p>Disculpe, hubo un error en el sistema y la acci&oacute;n no pudo completarse. Int&eacute;ntelo nuevamente, por favor.</p>
                </div>
            </div>
            <% End If%>
            <% If Request.QueryString("eliminado") = "ok" Then%>
            <div class="block full">
                <div class="alert alert-success">
                    <p>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                        <asp:Literal ID="msjEliminadoOk" runat="server" Text="El backup fue eliminado con éxito"></asp:Literal>
                    </p>
                </div>
            </div>
            <% End If%>
            <% If Request.QueryString("restaurado") = "ok" Then%>
            <div class="block full">
                <div class="alert alert-success">
                    <p>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                        <asp:Literal ID="Literal2" runat="server" Text="El backup fue restaurado con éxito"></asp:Literal>
                    </p>
                </div>
            </div>
            <% End If%>
            <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
                <div class="alert alert-danger">
                    <asp:Literal ID="msjErrorDB" Visible="false" runat="server" Text="Se produjo un error en el sistema y el backup no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
                </div>
            </asp:PlaceHolder>
            <div class="header-section">
                <h3>
                    <strong>Backups</strong>
                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary pull-right" UseSubmitBehavior="false" Text="Generar nuevo bakcup" />
                </h3>
            </div>
        </div>

        <div class="block full">

            <div id="filter-panel" class="filter-panel">
                <div class="panel panel-default bg-panel">
                    <div class="panel-body">

                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">C&oacute;digo</div>
                                <asp:TextBox ID="filtroCodigo" runat="server" ClientIDMode="Static" MaxLength="3" max="100" TextMode="Number" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Desde</div>
                                <asp:TextBox ID="filtroDesde" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderDesde" TargetControlID="filtroDesde" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Hasta</div>
                                <asp:TextBox ID="filtroHasta" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderHasta" TargetControlID="filtroHasta" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <!-- form group [rows] -->
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-info" />
                            <a href="/AdminBackup.aspx" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>&nbsp;Limpiar</a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="block full">
            <div class="table-responsive">
                <span class="lead">Se han encontrado <strong class="text-danger">
                    <asp:Literal ID="txtCantRegistros" runat="server"></asp:Literal></strong> registros
                </span>
                <span class="help-block pull-right">Mostrando 15 registros por p&aacute;gina</span>
                <div style="border-top: 1px solid black;"></div>
                <asp:GridView ID="DgvBackup" runat="server" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-striped table-hover" AllowPaging="True" GridLines="None" PagerStyle-CssClass="pagination-dgv " PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="DgvBackup_PageIndexChanging" OnPageIndexChanged="DgvBackup_PageIndexChanged" PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="IdBackup" HeaderText="Codigo" />
                        <asp:BoundField DataField="Ruta" HeaderText="Ruta" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                        <asp:CommandField ShowEditButton="True" EditText="<i class='glyphicon glyphicon-wrench'></i>" ControlStyle-CssClass="btn btn-xs btn-default" HeaderText="Restaurar" />
                        <%--<asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" HeaderText="Eliminar" />--%>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
