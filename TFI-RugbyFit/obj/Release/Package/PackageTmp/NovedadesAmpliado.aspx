﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="NovedadesAmpliado.aspx.vb" Inherits="TFI_RugbyFit.NovedadesAmpliado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/novedades.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">

    <!-- Blog Post -->

    <!-- Title -->
    <h1 class="titulo">
        <asp:Literal ID="ltlTitulo" runat="server"></asp:Literal>
        <a href="Novedades.aspx" class="btn btn-default pull-right">Volver</a></h1>
        <!-- Date/Time -->
    <p>
        <span class="glyphicon glyphicon-time"></span>
        <asp:Literal ID="ltlFecha" runat="server"></asp:Literal>
    </p>

    <div id="divImg" runat="server" class="text-center" visible="false">
        <hr />
        <!-- Preview Image -->
        <asp:Image ID="img" runat="server"  Height="300" />
        <!--    <img class="img-responsive" style="margin: 0 auto;" src="http://placehold.it/900x300" alt="">-->

        <hr />
    </div>
    <!-- Post Content -->
    <p class="lead" runat="server" id="phCopete" visible="false">
        <asp:Literal ID="ltlCopete" runat="server"></asp:Literal>
    </p>
    <p>
        <asp:Literal ID="ltlCuerpo" runat="server"></asp:Literal>
    </p>




</asp:Content>
