﻿Imports BLL
Imports BE
Public Class AdminProductosList
    Inherits ACL
    Dim oBll As New BLL_Productos
    Const COD_PERMISO = "MOD_MKT"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.LlenarFiltroCategoria()
            Me.LlenarDgv()
        End If

    End Sub

    Protected Sub LlenarFiltroCategoria()
        Dim oBllCate As New BLL_Categorias
        Dim listadoCat As List(Of BE_Categoria) = oBllCate.Listar()
        ddFiltroCategoria.DataTextField = "Nombre"
        ddFiltroCategoria.DataValueField = "IdCategoria"
        ddFiltroCategoria.DataSource = listadoCat
        ddFiltroCategoria.DataBind()
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Producto) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count

        DgvProductos.AutoGenerateColumns = False
        DgvProductos.DataSource = registros
        DgvProductos.DataBind()
    End Sub

    

    Protected Sub DgvProductos_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvProductos.RowEditing
        Dim row As GridViewRow = DgvProductos.Rows(e.NewEditIndex)
        Dim IdProducto As String = row.Cells(0).Text

        Response.Redirect("/AdminProductosForm.aspx?id=" + IdProducto)
    End Sub

    Protected Sub DgvProductos_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvProductos.RowUpdating
        Dim a As Integer = 0

    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvProductos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvProductos.PageIndexChanging
        DgvProductos.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvProductos_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvProductos.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroNombre.Text IsNot Nothing Then
            filtros.Add("@NombreProducto", filtroNombre.Text)
        End If
        Dim idCat As Integer = CType(ddFiltroCategoria.SelectedValue, Integer)
        If idCat > 0 Then
            filtros.Add("@IdCategoria", idCat)
        End If
        If IsNumeric(filtroCodigo.Text) Then
            Dim idProducto As Integer = CType(filtroCodigo.Text, Integer)
            If idProducto > 0 Then
                filtros.Add("@IdProducto", idProducto)
            End If
        End If

        Return filtros
    End Function

    Protected Sub DgvProductos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles DgvProductos.RowDeleting
        Dim row As GridViewRow = DgvProductos.Rows(e.RowIndex)
        Dim idPrd As String = row.Cells(0).Text
        If IsNumeric(idPrd) Then
            Dim objPrd As New BE_Producto With {.IdProducto = idPrd}
            Dim eliminado As Integer = oBll.Eliminar(objPrd)
            If eliminado = 1 Then
                Response.Redirect("AdminProductosList.aspx?eliminado=ok")

            ElseIf eliminado = -1 Then
                RtaErrorEliminar.Visible = True
                msjErrorFK.Visible = True
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub
End Class