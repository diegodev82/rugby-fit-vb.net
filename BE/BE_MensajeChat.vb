﻿Public Class BE_MensajeChat
    Public Property IdMensaje As Integer
    Public Property IdChat As Integer

    Public Property Emisor As BE_Usuario
    Public Property Mensaje As String
    Public Property FechaHora As DateTime
    Public Property Leido As Boolean
    Public Property EsRespuesta As Boolean
    Public Property TiempoTranscurrido As String
End Class
