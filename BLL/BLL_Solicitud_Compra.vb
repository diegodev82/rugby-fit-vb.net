
Imports BE
Imports MPP

Public Class BLL_Solicitud_Compra


    ''' 
    ''' <param name="objDetSol"></param>
    Public Function AgregarItem(ByVal objDetSol As BE_Detalle_Solicitud_Compra) As BE.BE_Detalle_Solicitud_Compra
        AgregarItem = Nothing
    End Function

    ''' 
    ''' <param name="solicitudes"></param>
    Public Function Autorizar(ByVal solicitudes As List(Of BE_Solicitud_Compra)) As List(Of BE_Solicitud_Compra)
        Autorizar = Nothing
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Crear(ByVal objSolCpra As BE_Solicitud_Compra) As BE.BE_Solicitud_Compra
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Editar(ByVal objSolCpra As BE_Solicitud_Compra) As BE.BE_Solicitud_Compra
        Editar = Nothing
    End Function

    ''' 
    ''' <param name="objDetSol"></param>
    Public Function EditarItem(ByVal objDetSol As BE_Detalle_Solicitud_Compra) As Boolean
        EditarItem = False
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Eliminar(ByVal objSolCpra As BE_Solicitud_Compra) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="objDetSol"></param>
    Public Function EliminarItem(ByVal objDetSol As BE_Detalle_Solicitud_Compra) As Boolean
        EliminarItem = False
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Enviar(ByVal objSolCpra As BE_Solicitud_Compra) As Boolean
        Enviar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Solicitud_Compra)
        Listar = Nothing
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Rechazar(ByVal objSolCpra As BE_Solicitud_Compra) As Boolean
        Rechazar = False
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Ver(ByVal objSolCpra As BE_Solicitud_Compra) As Integer
        Ver = 0
    End Function


End Class ' BLL_Solicitud_Compra

