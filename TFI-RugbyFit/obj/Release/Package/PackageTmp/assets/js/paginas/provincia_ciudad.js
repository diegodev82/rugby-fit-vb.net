﻿$(document).ready(function () {
    $(".slt-provincia").on("change.CambiarProvincia", function (e) {
        var id_provincia = $(this).val();
        $.ajax(
            {
                "url": "/AjaxUtils.asmx/GetCiudadesPorProvincia",
                data: "{'id_provincia':'" + id_provincia + "'}",
                contentType: 'application/json; utf-8',
                dataType: 'json',
                type: 'post',
                success: function (data) {
                    if (data.d != null) {
                        var ciudades = data.d;
                        var options = []
                        console.log(ciudades.length)
                        var optionSeleccione = {}
                        optionSeleccione["value"] = 0;
                        optionSeleccione["text"] = "Seleccione...";
                        options.push(optionSeleccione);
                        for (var index in ciudades) {

                            var option = {}
                            option["value"] = ciudades[index].IdCiudad;
                            option["text"] = ciudades[index].Nombre;
                            options.push(option);
                        }
                        fill_select(".slt-ciudad", options);
                        
                    }
                },
            }
        );
    });
});