﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Error404.aspx.vb" Inherits="TFI_RugbyFit.Error404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rugby Fit</title>
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .error {
            margin: 0 auto;
            text-align: center;
        }

        .error-code {
            bottom: 60%;
            color: #2d353c;
            font-size: 96px;
            line-height: 100px;
        }

        .error-desc {
            font-size: 12px;
            color: #647788;
        }

        .m-b-10 {
            margin-bottom: 10px!important;
        }

        .m-b-20 {
            margin-bottom: 20px!important;
        }

        .m-t-20 {
            margin-top: 20px!important;
        }
    </style>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Cambiar navegacion</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Default.aspx">Rugby Fit</a>
                </div>
            </div>
        </nav>
        <form id="form1" runat="server">
            <div class="error">
                <div class="error-code m-b-10 m-t-20">404 <i class="fa fa-warning"></i></div>
                <h3 class="font-bold">No pudimos encontrar la p&aacute;gina..</h3>

                <div class="error-desc">
                    Disculpe, la p&aacute;gina que est&aacute; buscando no existe m&aacute;s.
                    <br />
                    Trate de refrescar la p&aacute;gina o clicke el bot&oacute;n de aqu&iacute; abajo para volver a la p&aacute;gina anterior.
                    <div>
                        <a class=" login-detail-panel-button btn" href="javascript: window.history.go(-1);">
                            <i class="glyphicon glyphicon-arrow-left"></i>
                            Volver
                    </a>
                    </div>
                </div>
            </div>
        </form>

        <footer>
            <p>
                <span><a href="#" class="js-ver-tyc">T&eacute;rminos y Condiciones</a></span>
                <span class="pull-right">&copy; 2016 Rugby Fit.</span>
            </p>
        </footer>
    </div>

</body>
</html>
