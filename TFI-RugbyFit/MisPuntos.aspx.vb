﻿Imports BLL
Imports BE
Public Class MisPuntos
    Inherits ACL
    Private oBll As New BLL_Clientes
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.ValidarAccesoCliente()
            div_tbl_puntos.Visible = False
            div_sin_puntos.Visible = False
            If Not IsPostBack Then
                Me._CargarData()
            End If
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try
        
    End Sub
    Protected Sub _CargarData()
        Dim idCliente As Integer = MyBase.GetIdCliente()
        Dim puntos As List(Of BE_Punto_Cliente) = oBll.GetPuntosBeneficioObtenido(idCliente)
        If puntos.Count = 0 Then
            div_sin_puntos.Visible = True
        Else
            rptPuntos.DataSource = Nothing
            rptPuntos.DataBind()
            rptPuntos.DataMember = "BE_Punto_Cliente"
            rptPuntos.DataSource = puntos
            rptPuntos.DataBind()
            div_tbl_puntos.Visible = True
        End If
        Dim total As Integer = CalcularTotal(puntos)
        ltlTotalPuntos.Text = total.toString()
    End Sub

    Protected Function CalcularTotal(ByVal listado As List(Of BE_Punto_Cliente)) As Integer
        Dim total As Integer = 0
        For Each item As BE_Punto_Cliente In listado
            If Not item Is Nothing Then
                If IsNumeric(item.Cantidad) Then
                    total = total + item.Cantidad
                End If
            End If
        Next
        Return total
    End Function
End Class