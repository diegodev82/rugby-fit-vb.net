
Imports MPP


Public Class BLL_Reportes


    Public oMpp As New MPP_Reportes

    Private Sub Generar()

    End Sub

    ''' 
    ''' <param name="filtros"></param>
    ''' <param name="nombre"></param>
    Public Function Listar(ByVal filtros As Hashtable, ByVal nombre As String) As List(Of Object)
        Listar = Nothing
    End Function


    Public Function GetReporteEncuestas(Optional ByVal filtros As Hashtable = Nothing) As DataTable
        Return oMpp.GetReporteEncuestas(filtros)
    End Function

    Public Function GetReporteFacturacion(Optional ByVal filtros As Hashtable = Nothing) As DataTable
        Return oMpp.GetReporteFacturacion(filtros)
    End Function
    Public Function GetReporteComparacionGanancias(Optional ByVal filtros As Hashtable = Nothing) As DataTable
        Return oMpp.GetReporteComparacionGanancias(filtros)
    End Function
    '
End Class ' BLL_Reportes

