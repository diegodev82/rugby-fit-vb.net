﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FacturaImprimir.aspx.vb" Inherits="TFI_RugbyFit.FacturaImprimir" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
    <style>
        /* =============================================================
   GENERAL STYLES
 ============================================================ */
        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 16px;
            line-height: 30px;
        }

        .container-fc {
            border-style: solid;
            padding: 15px;
        }

        .pad-top-botm {
            padding-bottom: 40px;
            padding-top: 60px;
        }

        h4 {
            text-transform: uppercase;
        }
        /* =============================================================
   PAGE STYLES
 ============================================================ */

        .contact-info span {
            font-size: 14px;
            padding: 0px 50px 0px 50px;
        }

        .contact-info hr {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .client-info {
            font-size: 15px;
        }

        .ttl-amts {
            text-align: right;
            padding-right: 50px;
        }

        .tipo_fc {
            border-style: solid;
            font-size: 26px;
            padding: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="container-fc">
                <div class="row pad-top-botm ">
                    <div class="col-lg-5 col-md-5 col-sm-5 ">
                        <img src="assets/img/Logo-RugbyFit.png" style="padding-bottom: 20px; height: 250px;" class="img-responsive" />
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <span class="tipo_fc">B</span>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5">

                        <strong style="font-size:40px;">Rugby Fit.</strong>
                        <br />
                        <i>Domicilio: </i> Avenida Rivadavia 2456 , Once,
              <br />
                        C.A.B.A. , Buenos Aires,
              <br />
                        Argentina.
              
                    </div>
                </div>
                <div class="row text-center contact-info">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <hr />
                        <span>
                            <strong>Email : </strong>info@rugbyit.com 
                        </span>
                        <span>
                            <strong>Tel. : </strong>0800-222-4567
                        </span>
                        <span>
                            <strong>Fb : </strong> www.facebook.com/RugbyFit
                        </span>
                        <hr />
                    </div>
                </div>
                <div class="row pad-top-botm client-info">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h4><strong>Informaci&oacute;n del Cliente</strong></h4>
                        <strong>Jhon Deo Chuixae</strong>
                        <br />
                        <b>Domicilio:</b> 145/908 , New York Lane,
              <br />
                        United States.
             <br />
                        <b>Tel. :</b> +90-908-567-0987
              <br />
                        <b>E-mail :</b> info@clientdomain.com
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">

                        <h4><strong>Detalles del Pago </strong></h4>
                        <b>Monto :  990 $ </b>
                        <br />
                        Fecha Pago:  01/11/2016
              <br />
                        <b>Forma de Pago :  TC </b>
                        
              <br />
                        Fecha Compra :  01/11/2016
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>&Iacute;tem</th>
                                        <th>Descirpci&oacute;n</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <th>Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Website Design</td>
                                        <td>1</td>
                                        <td>300 USD</td>
                                        <td>300 USD</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Plugin Dev.</td>
                                        <td>2</td>
                                        <td>200 USD</td>
                                        <td>400 USD</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Hosting Domains</td>
                                        <td>2</td>
                                        <td>100 $</td>
                                        <td>200 $</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <hr />
                        <div class="ttl-amts">
                            <h4><strong>Monto total : 990 $</strong> </h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <strong>Importante: </strong>
                        <ol>
                            <li>Esta factura fue generada electr&oacute;nicamente por lo que no requiere firma.</li>
                            <li>Por favor, lea los t&eacute;rminos y condiciones en   www.rugbyfit.com por devoluciones, reemplazos u otros inconvenientes.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row pad-top-botm">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <hr />
                    <a href="#" class="btn btn-primary btn-lg">Imprimir Factura</a>
                    &nbsp;&nbsp;&nbsp;
              <a href="#" class="btn btn-success btn-lg">Descargar PDF</a>

                </div>
            </div>
        </div>
    </form>
</body>
</html>
