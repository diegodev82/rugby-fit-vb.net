﻿$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip()
    setTimeout(function () {
        aplicarEstiloTraductor();
    }, 1000);
    $("body").on("click.VerTC", ".js-ver-tyc", function (e) {
        e.preventDefault();
        PopupCenter("TerminosCondiciones.aspx", "Terminos y Condiciones", 850,875);
    });
    $("body").on("click.VerPoliticas", ".js-ver-politicas", function (e) {
        e.preventDefault();
        PopupCenter("PoliticasPrivacidad.aspx", "Políticas de Privacidad", 850, 875);
    });
    $(".js-max-length").on("keypress", function (event) {
        var length = 9;
        if (typeof($(this).prop("maxlength")) != "undefined") {
            length = $(this).prop("maxlength");
        }
        return validarMaxLength(event, this, length);
    });
    $(".js-solo-numeros").on("keypress", function (event) {
        var keycode = event.which;
        if (!(event.shiftKey == false && ( keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
            event.preventDefault();
        }
    });
    $("input, textarea").on("blur", function (event) {
        var html = $(this).val();
        
        var htmlLimpio = $($.parseHTML(html)).text();
        //htmlLimpio = html.replace(/</g, "&lt;").replace(/>/g, "&gt;");
        $(this).val(htmlLimpio)
    });

});



function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes,location=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}
function aplicarEstiloTraductor() {
    $googleTranslate = $("#google_translate_element");
    $googleTranslate.find("select").addClass("form-control");
}


function ocultarTextoGoogle() {
    $googleTranslate = $("#google_translate_element");
    var textGoogle = $googleTranslate.find('.goog-te-gadget').first().contents().filter(function () {
        return this.nodeType == 3;
    });
    textGoogle.remove();
    var logoGoogle = $googleTranslate.find(".goog-te-gadget span");
    logoGoogle.remove();
}


function validarSoloNumeros(e, element) {
    if (e.charCode == 0 || e.keyCode == 8 || e.keyCode == 46) {
        return true;
    }
    var regex = new RegExp("[0-9]");
    return regex.test(element.value);
}
function validarMaxLength(e, element, length) {
    if(e.charCode == 0 || e.keyCode == 8 || e.keyCode == 46){
        return true;
    }
    return element.value.length <length;
}

/**
 * Rellena un select con options
 * @param {string} El id del select que hay que llenar
 * @param {array} data
 * @returns {void}
 */
function fill_select(selectorSelect, data) {
    //console.log(typeof id_select);
    var select = $(selectorSelect);
    select.empty();
    if (data.length > 0) {
        for (var index in data) {
            var opt = data[index];
            select.append('<option value="' + opt.value + '">' + opt.text + '</option>')
        }
    }
}

/**
 * Aplica la funcionalidad de "seleccionar/deseleccionar todo" a un link
 * @param {string} selector El selector jQuery de los checkbox que se van a checkear/descheckear
 * @returns {void}
 */
function select_all(selector) {
    $(selector).each(function (e) {
        var checkBoxes = $(selector);
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    })
}


function ellipsis(string, max_caracteres) {
    if (typeof (max_caracteres) === "undefined" || max_caracteres * 1 === 0) {
        max_caracteres = 50;
    }
    if (string.length > max_caracteres)
        return string.substring(0, max_caracteres) + '...';
    else
        return string;
}