Imports BE
Imports MPP
Imports System.Web

Public Class BLL_Clientes
    Private oBllBit As New BLL_Bitacora
    Private oMpp As New MPP_Clientes
    Public Const ERROR_CLIENTE_EXISTENTE = 1
    Public Const ERROR_DB = 2
    Public Const ERROR_SMTP = 3
    Public Const DOMINIO = "http://localhost:53255"

    Private HashRegistracionFallBack As String = ""

    Public Function GetPuntosBeneficioObtenido(ByVal idCliente As Integer) As List(Of BE.BE_Punto_Cliente)
        Return oMpp.GetPuntosBeneficioObtenido(idCliente)
    End Function
    Public Function DescontarSaldoFavor(ByVal idCliente As Integer, ByVal monto As Decimal) As Boolean
        Return oMpp.DescontarSaldoFavor(idCliente, monto)
    End Function
    Public Function IncrementarSaldoFavor(ByVal idCliente As Integer, ByVal monto As Decimal) As Boolean
        Return oMpp.IncrementarSaldoFavor(idCliente, monto)
    End Function

    Public Function ConfirmarRegistracion(ByVal hash As String, ByVal email As String) As Boolean
        Dim idConfirmado As Integer = oMpp.ValidarLinkConfirmacion(hash, email)
        If idConfirmado > 0 Then
            Dim evento As New BE_Evento
            evento.IdEvento = BE_Evento.CONFIRMAR_REGISTRACION
            oBllBit.RegistrarEvento(evento, "EMAIL: " + email)
            Return oMpp.ActivarUsuario(idConfirmado)
        End If
        Return False
    End Function

    Public Function ValidarUsuarioExistente(ByVal objCliente As BE_Cliente)
        Dim existe As Boolean = oMpp.ValidarExistente(objCliente)
        Return existe
    End Function

    Public Function ValidarUsuarioExistentePorMail(ByVal objCliente As BE_Cliente)
        Dim existe As Boolean = oMpp.ValidarExistentePorMail(objCliente)
        Return existe
    End Function

    Public Function Crear(ByVal objCliente As BE_Cliente) As Object
        Dim existeUsuario As Boolean = oMpp.ValidarExistente(objCliente)
        Dim existeMail As Boolean = oMpp.ValidarExistentePorMail(objCliente)
        If existeUsuario = True Or existeMail Then
            Return ERROR_CLIENTE_EXISTENTE
        End If
        Dim clienteCreado As Object = oMpp.Crear(objCliente)
        If clienteCreado.GetType() = GetType(BE_Cliente) Then
            Dim evento As New BE_Evento
            evento.IdEvento = BE_Evento.CREAR_CLIENTE
            oBllBit.RegistrarEvento(evento)

            Dim emailEnviado As Boolean = Me._EnviarEmailConfirmacion(clienteCreado)
            If emailEnviado = True Then
                Return True
            Else

                Return ERROR_SMTP
            End If
        End If
        Return ERROR_DB
    End Function

    Public Function GetHashRegistracionFallBack()
        Return Me.HashRegistracionFallBack
    End Function

    Public Function Editar(ByVal objCliente As BE_Cliente) As Boolean
        Dim evento As New BE_Evento
        evento.IdEvento = BE_Evento.EDITAR_CLIENTE
        oBllBit.RegistrarEvento(evento)
        Return oMpp.Editar(objCliente)
    End Function

    Public Function Eliminar(ByVal objCliente As BE_Cliente) As Boolean
        Eliminar = False
    End Function

    Private Function _EnviarEmailConfirmacion(ByVal clienteCreado As BE_Cliente) As Integer
        If Not clienteCreado.Email Is Nothing Then

            Dim hash As String = clienteCreado.HashRegistracion
            Dim email As String = clienteCreado.Email
            Dim host As String = DOMINIO
            Dim link As String = host + "/ConfirmarRegistracion.aspx?hash=" + hash + "&email=" + email
            Me.HashRegistracionFallBack = link
            Dim body As String = "<p>Por favor, ingrese al link para confirmar su registracion </p>" + _
            "<p><a href='" + link + "'>Confirmar</a>"
            Dim subject As String = "Confirmaci�n de Registro"
            Return Me._EnviarEmail(email, body, subject)
        End If
        Return False
    End Function

    Public Function EnviarEmailResetPwd(ByVal email As String, ByVal nuevaPwd As String) As Integer
        If Not email Is Nothing Then
            Dim host As String = DOMINIO
            Dim link As String = host + "/Login.aspx"
            Dim body As String = "<p>Usted est� recibiendo este correo porque solicit� la restauraci�n de su contrase�a. </p>" + _
            "<p>Su nueva contrase�a es: " + nuevaPwd + ". Puede intentar loguearse nuevamente desde <a href='" + link + "'>aqu�</a>"
            Dim subject As String = "Cambio de contrase�a"
            Return Me._EnviarEmail(email, body, subject)
        End If
        Return False
    End Function

    Public Function Listar(ByVal filtros As Hashtable) As Integer
        Listar = 0
    End Function

    Public Function ResetContrasenia(ByVal email As String) As Boolean
        Dim oBllSeg As New BLL_Seguridad
        Dim hash As String = oMpp.GenerarHashUnico()
        Dim nuevaPwd As String = oBllSeg.Encriptar(hash)
        Dim actualizado As Boolean = oMpp.ResetContrasenia(email, nuevaPwd)
        If actualizado = True Then
            Dim evento As New BE_Evento
            evento.IdEvento = BE_Evento.RESET_CONTRASENIA_CLIENTE
            oBllBit.RegistrarEvento(evento)
            Return Me.EnviarEmailResetPwd(email, hash)
        End If
        Return False
    End Function

    Public Function GetPorId(ByVal id As Integer, Optional ByVal activo As Boolean = True) As BE_Cliente
        Return oMpp.GetPorId(id, activo)
    End Function

    Private Function _EnviarEmail(email As String, body As String, ByVal subject As String)
        Dim _Message As New System.Net.Mail.MailMessage()
        Dim _SMTP As New System.Net.Mail.SmtpClient


        'CONFIGURACI�N DEL STMP
        _SMTP.Credentials = New System.Net.NetworkCredential("registracionrugbyfit@gmail.com", "regrugby")
        _SMTP.Host = "smtp.gmail.com"
        _SMTP.Port = 587
        _SMTP.EnableSsl = True

        ' CONFIGURACION DEL MENSAJE
        _Message.[To].Add(email) 'Cuenta de Correo al que se le quiere enviar el e-mail
        _Message.From = New System.Net.Mail.MailAddress("registracionrugbyfit@gmail.com", "Rugby Fit", System.Text.Encoding.UTF8) 'Quien lo env�a
        _Message.Subject = subject 'Sujeto del e-mail
        _Message.SubjectEncoding = System.Text.Encoding.UTF8 'Codificacion
        _Message.Body = body
        'contenido del mail
        _Message.BodyEncoding = System.Text.Encoding.UTF8
        _Message.Priority = System.Net.Mail.MailPriority.Normal
        _Message.IsBodyHtml = True

        'ENVIO
        Try
            _SMTP.Send(_Message)
            Console.Write("Mensaje enviado correctamene")
            Return True
        Catch ex As System.Net.Mail.SmtpException
            Console.Write(ex.ToString)
            Return False
        End Try
    End Function

End Class ' BLL_Clientes
