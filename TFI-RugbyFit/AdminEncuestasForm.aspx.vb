﻿Imports BLL
Imports BE
Public Class AdminEncuestaForm
    Inherits ACL
    Private oBll As New BLL_Encuesta
    Private encuestaEditando As BE_Encuesta
    Const COD_PERMISO = "MOD_MKT"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            If Request.QueryString("id") IsNot Nothing Then
                If Not IsNumeric(Request.QueryString("id")) Then
                    Return
                End If
                Dim idEnc As Integer = CType(Request.QueryString("id"), Integer)
                Me.CargarData(idEnc)
                Me.CargarRespuestas(Me.encuestaEditando.Respuestas)
                phRespuestas.Visible = True
                btnEliminar.Visible = True
            Else
                iptFechaDesde.Text = System.DateTime.Now.ToString("dd/MM/yyyy")
            End If
        End If
    End Sub



    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            Dim oEnc As New BE_Encuesta

            oEnc.IdEncuesta = IIf(iptCodigo.Value <> "", iptCodigo.Value, 0)
            oEnc.Pregunta = iptPregunta.Text
            oEnc.FechaDesde = CType(iptFechaDesde.Text, DateTime)
            oEnc.FechaHasta = CType(iptFechaHasta.Text, DateTime)
            oEnc.Tipo = ddTipo.SelectedValue
            Dim encGuardada As BE_Encuesta = Nothing
            If oEnc.IdEncuesta > 0 Then
                Dim guardado As Boolean = oBll.Editar(oEnc)
                If guardado = True Then
                    encGuardada = oEnc
                End If
            Else
                encGuardada = oBll.Crear(oEnc)
            End If
            If encGuardada Is Nothing Then
                MensajeRespuesta.Text = "Ya existe una encuesta registrada con ese título. Por favor, ingrese otra pregunta"
                PanelMensajeRespuesta.Visible = True
                Return
            End If
            If encGuardada.IdEncuesta > 0 Then
                Response.Redirect("AdminEncuestasForm.aspx?guardado=ok&id=" + encGuardada.IdEncuesta.ToString)
            Else
                MensajeRespuesta.Text = "Se produjo un error desconocido y la registación no pudo realizarse. Inténtelo nuevamente, por favor"
                PanelMensajeRespuesta.Visible = True
            End If
        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
       
    End Sub

    Protected Sub CargarData(ByVal idEnc As Integer)

        Me.encuestaEditando = oBll.GetPorId(idEnc)
        If Me.encuestaEditando IsNot Nothing Then
            iptCodigo.Value = Me.encuestaEditando.IdEncuesta
            iptPregunta.Text = Me.encuestaEditando.Pregunta
            iptFechaDesde.Text = Me.encuestaEditando.FechaDesde
            iptFechaHasta.Text = Me.encuestaEditando.FechaHasta
            ddTipo.SelectedValue = Me.encuestaEditando.Tipo
        End If
    End Sub

    Protected Sub CargarRespuestas(ByVal listadoRtas As List(Of BE_RespuestaEncuesta))

        gdvRespuestas.DataSource = Nothing
        gdvRespuestas.DataBind()
        gdvRespuestas.DataMember = "BE_RespuestaEncuesta"
        gdvRespuestas.DataSource = listadoRtas
        gdvRespuestas.DataBind()
    End Sub

    Protected Sub btnAgregaRta_Click(sender As Object, e As EventArgs) Handles btnAgregaRta.Click
        Dim rta As New BE_RespuestaEncuesta
        rta.IdEncuesta = iptCodigo.Value
        rta.Respuesta = iptRespuesta.Text
        rta.IdRespuesta = iptCodigoRta.Value
        error_rta_duplicada.Visible = False
        guardado_ok.Visible = False
        error_db.Visible = False
        If iptCodigoRta.Value = 0 Then
            Dim respuestaCreada As BE_RespuestaEncuesta = oBll.CrearRespuesta(rta)
            If respuestaCreada Is Nothing Then
                'Respuesta repetida
                error_rta_duplicada.Visible = True
            ElseIf respuestaCreada.IdRespuesta > 0 Then
                Me.CargarData(iptCodigo.Value)
                Me.CargarRespuestas(encuestaEditando.Respuestas)
                guardado_ok.Visible = True
                iptRespuesta.Text = ""
                iptCodigoRta.Value = 0
            Else
                error_db.Visible = True
                'error al insertar respuesta
            End If
        Else
            Dim guardada As Object = oBll.EditarRespuesta(rta)
            If guardada Is Nothing Then
                'Respuesta repetida
                error_rta_duplicada.Visible = True
            ElseIf guardada = True Then
                Me.CargarData(iptCodigo.Value)
                Me.CargarRespuestas(encuestaEditando.Respuestas)
                guardado_ok.Visible = True
                iptRespuesta.Text = ""
                iptCodigoRta.Value = 0
            Else
                error_db.Visible = True
                'error al insertar respuesta
            End If
        End If

      
        
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If IsNumeric(iptCodigo.Value) Then
            Dim idEnc As Integer = CType(iptCodigo.Value, Integer)
            Dim objEnc As New BE_Encuesta With {.IdEncuesta = idEnc}
            Dim eliminado As Integer = oBll.Eliminar(objEnc)
            If eliminado = 1 Then
                Response.Redirect("AdminEncuestasList.aspx?eliminado=ok")
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If

    End Sub

    Protected Sub gvdRespuestas_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gdvRespuestas.RowDeleting
        Dim row As GridViewRow = gdvRespuestas.Rows(e.RowIndex)
        Dim idRta As String = row.Cells(0).Text
        error_db.Visible = False
        If IsNumeric(idRta) Then
            Dim objRta As New BE_RespuestaEncuesta With {.IdRespuesta = idRta}
            Dim eliminado As Integer = oBll.EliminarRespuesta(objRta)
            If eliminado = 1 Then
                Me.CargarData(iptCodigo.Value)
                Me.CargarRespuestas(Me.encuestaEditando.Respuestas)
            Else
                error_db.Visible = False
            End If
        End If
    End Sub

    'Protected Sub gdvRespuestas_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gdvRespuestas.RowEditing
    '    Dim row As GridViewRow = gdvRespuestas.Rows(e.NewEditIndex)
    '    Dim IdRta As String = row.Cells(0).Text
    '    iptCodigoRta.Value = IdRta
    '    iptRespuesta.Text = row.Cells(1).Text
    '    Return
    'End Sub

    'Protected Sub gdvRespuestas_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles gdvRespuestas.RowUpdating
    '    Return
    '    '    Dim a As Integer = 0
    '    ' Dim row As GridViewRow = gdvRespuestas.Rows(e.RowIndex)
    '    ' Dim values = e.NewValues
    '    ' Dim IdRta As String = row.Cells(0).Text
    '    iptCodigoRta.Value = IdRta
    '    iptRespuesta.Text = row.Cells(1).Text
    'End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim btn As Button = sender
        Dim row As GridViewRow = btn.NamingContainer
        'Dim row As GridViewRow = gdvRespuestas.Rows(e.)
        Dim IdRta As String = row.Cells(0).Text
        iptCodigoRta.Value = IdRta
        iptRespuesta.Text = HttpUtility.HtmlDecode(row.Cells(1).Text)
    End Sub
End Class