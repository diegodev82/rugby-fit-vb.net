﻿Imports BLL
Imports BE
Public Class AdminNovedades
    Inherits ACL
    Private oBll As New BLL_Novedad
    Const COD_PERMISO = "MOD_MKT"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.LlenarDgv()
        End If
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Novedad) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count

        DgvNovedades.AutoGenerateColumns = False
        DgvNovedades.DataSource = registros
        DgvNovedades.DataBind()
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroTitulo.Text IsNot Nothing Then
            filtros.Add("@PalabraClave", filtroTitulo.Text)
        End If
        
        If IsNumeric(filtroCodigo.Text) Then
            Dim idNovedad As Integer = CType(filtroCodigo.Text, Integer)
            If idNovedad > 0 Then
                filtros.Add("@IdNovedad", idNovedad)
            End If
        End If

        Return filtros
    End Function

    Protected Sub DgvNovedades_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles DgvNovedades.RowDeleting
        Dim row As GridViewRow = DgvNovedades.Rows(e.RowIndex)
        Dim idNov As String = row.Cells(0).Text
        If IsNumeric(idNov) Then
            Dim objNov As New BE_Novedad With {.IdNovedad = idNov}
            Dim eliminado As Integer = oBll.Eliminar(objNov)
            If eliminado = 1 Then
                Response.Redirect("AdminNovedadesList.aspx?eliminado=ok")

            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub DgvNovedades_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvNovedades.RowEditing
        Dim row As GridViewRow = DgvNovedades.Rows(e.NewEditIndex)
        Dim IdProducto As String = row.Cells(0).Text

        Response.Redirect("/AdminNovedadesForm.aspx?id=" + IdProducto)
    End Sub

    Protected Sub DgvNovedades_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvNovedades.RowUpdating
        Dim a As Integer = 0

    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvNovedades_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvNovedades.PageIndexChanging
        DgvNovedades.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvNovedades_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvNovedades.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

End Class