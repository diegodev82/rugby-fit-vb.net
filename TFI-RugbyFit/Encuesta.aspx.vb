﻿Imports BLL
Imports BE
Public Class Encuesta
    Inherits System.Web.UI.Page
    Private oBll As New BLL_Encuesta
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.CargarData()
        Catch ex As Exception
            TryCast(Me.Master, Main).MostrarErrorSistema()
        End Try

    End Sub

    Public Sub CargarData()
        Dim enc As BE_Encuesta = oBll.GetEncuestaActual()
        If Not enc Is Nothing Then
            ltlPreguntaRtdo.Text = enc.Pregunta
            ltlPregunta.Text = enc.Pregunta
            rptRespuestas.DataSource = Nothing
            rptRespuestas.DataBind()
            rptRespuestas.DataSource = enc.Respuestas
            rptRespuestas.DataBind()

            rptRtasRtdos.DataSource = Nothing
            rptRtasRtdos.DataBind()
            rptRtasRtdos.DataSource = enc.Respuestas
            rptRtasRtdos.DataBind()

            phEncuesta.Visible = True
            phSinEncuesta.Visible = False
        Else
            phEncuesta.Visible = False
            phSinEncuesta.Visible = True
        End If
        

    End Sub

   
End Class