﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="FAQs.aspx.vb" Inherits="TFI_RugbyFit.FAQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="row" id="FAQs_page">
        <div class="col-md-12">
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Cerrar</span></button>
                Esta secci&oacute;n contiene las preguntas m&aacute;s frecuentes y sus respuestas. Si usted no encuentra la soluci&oacute;n a su problema en este listado, no dude en <a href="Contacto.aspx">contactarnos</a>
            </div>
            <div class="spacer"></div>
            <div class="panel-group" id="accordion">
                <div class="faqHeader">Preguntas m&aacute;s frecuentes</div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseGral1">&iquestC&oacute;mo me registro?</a>
                        </h4>
                    </div>
                    <div id="collapseGral1" class="panel-collapse collapse">
                        <div class="panel-body">
                            Sólo tenés que completar el siguiente formulario. De esta manera el sistema generará un usuario y contraseña con los que podrás ingresar en las visitas futuras. Una vez que seas usuario registrado de Rugby Fit podrás seleccionar productos como favoritos y recibir información sobre nuestros descuentos y promociones.
                           <br />
                            <i>* Sus datos personales se toman con los fines de cumplir con el giro comercial de la empresa, realizar acciones de marketing y publicidad. Ud. tiene derecho gratuito de ejercer los derechos de acceso, rectificación y supresión de los datos. La Dirección Nacional de Protección de Datos Personales, órgano de control de la Ley Nro 25.326, tiene la atribución de atender las denuncias y reclamos que se interpongan en relación a incumplimiento de las normas sobre protección de datos personales.</i>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseGral2">&iquestC&oacute;mo puedo editar mis datos?</a>
                        </h4>
                    </div>
                    <div id="collapseGral2" class="panel-collapse collapse">
                        <div class="panel-body">
                            Tenés la opción de modificar tus datos personales como email, teléfono de contacto y tu dirección principal de envío. Para ello tenés que seguir los siguientes pasos:
                            <ol>
                                <li>Inicia sesión  en RugbyFit.com</li>
                                <li>Ingresá a Mis datos</li>
                                <li>Modificá los datos que necesites corregir.</li>
                                <li>Hacé clic en Guardar.</li>
                                <li>Recibirás una confirmación de la actualización de tus datos.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseGral3">&iquestCuentan con Políticas de Seguridad que garanticen mi compra?</a>
                        </h4>
                    </div>
                    <div id="collapseGral3" class="panel-collapse collapse">
                        <div class="panel-body">
                            En RugbyFit.com te garantizamos la seguridad de todas tus compras online, manteniendo tus datos bajo la más estricta confidencialidad.
                            <br />
                            Gracias al respaldo de SSL (Secure Socket Layer) el sistema de seguridad utilizado por RugbyFit.com, te aseguramos cada transacción electrónica que realices en nuestra página web. Tu información personal será cifrada y no podrá ser leída ni utilizada por terceros mientras realices una compra. A su vez, RugbyFit te asegura que dichos datos no saldrán de la compañía, manejándolos con total responsabilidad, de manera absolutamente confidencial y conforme a lo dispuesto por la legislación vigente.
                            <br />
                            Para mayor información ver Términos y Condiciones de Garbarino.com

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseGral4">&iquestPuedo obtener una factura A de mi compra?</a>
                        </h4>
                    </div>
                    <div id="collapseGral4" class="panel-collapse collapse">
                        <div class="panel-body">
                            Las compras en RugbyFit.com se emiten con Factura B a consumidor final. Si necesitas realizar una compra donde la factura sea con comprobante A te recomendamos que te contactes con el centro de ventas telefónicas llamando al 0810-222-Rugby.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseGral5">&iquestQué incluye el envío a domicilio?</a>
                        </h4>
                    </div>
                    <div id="collapseGral5" class="panel-collapse collapse">
                        <div class="panel-body">
                            La ubicación del producto en el lugar indicado por el receptor, dentro del domicilio, cuando las instalaciones lo permitan. Incluido el ascenso por escaleras, salvo que el transportista detecte un riesgo para la seguridad de las personas o integridad del producto.
                            <br />
                            Cuando llegamos a tu casa, podemos desembalar el producto para que verifiques su integridad.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseGral6">Inconvenientes con la entrega</a>
                        </h4>
                    </div>
                    <div id="collapseGral6" class="panel-collapse collapse">
                        <div class="panel-body">
                            Si no podemos concretar la entrega, te contactaremos para pactar una nueva visita.
                            <br />
                            Recordá que si el producto no se encuentra en condiciones o no cumple con tus expectativas podés rechazarlo en el momento de la entrega. Va a ser necesario que firmes el remito indicando los motivos del rechazo, y luego te contactaremos para coordinar una nueva entrega.
                            <br />
                            Ante cualquier duda, te podés comunicar con nosotros al Centro de Atención a Clientes
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
