﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Login.aspx.vb" Inherits="TFI_RugbyFit.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <% 
        Dim mensaje As String = ""
        If Request.QueryString("rta") = "usuario-incorrecto" Then
            mensaje = "El usuario ingresado no se encuentra registrado en la base de datos."
        End If
        If Request.QueryString("rta") = "usuario-bloqueado" Then
            mensaje = "El usuario ingresado se encuentra bloqueado. Pongase en contacto con el administrador para desbloquearlo."
        End If
        If Request.QueryString("rta") = "contrasenia-incorrecta" Then
            mensaje = "La contraseña ingresada es incorrecta"
        End If
        If Request.QueryString("rta") = "registracion-no-confirmada" Then
            mensaje = "La registración aún no ha sido confirmada.<br> Por favor revise su email y complete la acción."
        End If
        
    If mensaje <> "" Then%>
    <div class="alert alert-danger">
        <p><%=mensaje %></p> 
    </div>
    <% End If %>
    <% If Request.QueryString("rtdo") = "contrasenia-modificada" Then%>
     <div class="alert alert-success">
        <p><i class="glyphicon glyphicon-thumbs-up"></i>  La contrase&ntilde;a ha sido modificada correctamente</p> 
         <p>Ingrese nuevamente con sus nuevas credenciales.</p>
    </div>
    <% End If %>
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="text-center">
                    <h4><b>Ingresar</b></h4>
                </div>

                <div class="form-group">
                    <label for="username">Usuario</label>
                    <asp:TextBox ID="iptNicknameLoginPage" runat="server" TabIndex="1" class="form-control" placeholder="Usuario" MaxLength="20" autocomplete="off"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label for="password">Contrase&ntilde;a</label>
                    <asp:TextBox ID="iptContraseniaLoginPage" runat="server" TabIndex="2" MaxLength="20" class="form-control" placeholder="Contrase&ntilde;a" autocomplete="off" TextMode="Password"></asp:TextBox>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                <asp:Button ID="btn_login_LoginPage" runat="server" Text="Ingresar" TabIndex="4" class="form-control btn btn-success" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="text-center">
                                <a href="Registracion.aspx" tabindex="5" class="registrarse">Registrarse</a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="text-center">
                                <a href="ResetContrasenia.aspx"  tabindex="5" class="olvido-password">&iquest;Olvid&oacute; su contrase&ntilde;a?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>



