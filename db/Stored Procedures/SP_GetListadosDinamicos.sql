 Set NoCount ON
    /* Variable Declaration */
    Declare @SQLQuery AS NVarchar(4000)
    Declare @ParamDefinition AS NVarchar(2000) 
    /* Build the Transact-SQL String with the input parameters */ 
    Set @SQLQuery = 'Select * From dbo.producto as prd INNER JOIN 
	dbo.categoria as cat on prd.id_categoria = cat.id_categoria
	where (1=1) ' 
    /* check for the condition and build the WHERE clause accordingly */
    If @NombreProducto Is Not Null 
         Set @SQLQuery = @SQLQuery + ' And (prd.nombre LIKE "% @NombreProducto"'

    If @IdProducto Is Not Null
         Set @SQLQuery = @SQLQuery + ' And (prd.id_producto = @IdProducto)' 
  
    
    /* If (@StartDate Is Not Null) AND (@EndDate Is Not Null)
         Set @SQLQuery = @SQLQuery + ' And (JoiningDate 
         BETWEEN @StartDate AND @EndDate)'*/
    /* Specify Parameter Format for all input parameters included 
     in the stmt */
    Set @ParamDefinition =      ' @NombreProducto NVarchar(100),
                @IdProducto Integer'
                
    /* Execute the Transact-SQL String with all parameter value's 
       Using sp_executesql Command */
    Execute sp_Executesql     @SQLQuery, 
                @ParamDefinition, 
                @NombreProducto, 
                @IdProducto
               
                
    If @@ERROR <> 0 GoTo ErrorHandler
    Set NoCount OFF
    Return(0)
  
ErrorHandler:
    Return(@@ERROR)