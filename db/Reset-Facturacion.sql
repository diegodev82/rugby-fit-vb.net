delete from venta_detalle;
delete from venta;
delete from pago;
update cliente set saldo_a_favor = 0;

update producto set stock = 15;
update cliente set puntos_acumulados = 0;
delete from punto_cliente;
delete from detalle_factura;
delete from factura;