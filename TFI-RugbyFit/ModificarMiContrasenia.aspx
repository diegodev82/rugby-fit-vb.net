﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ModificarMiContrasenia.aspx.vb" Inherits="TFI_RugbyFit.ModificarMiContrasenia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <h1 class="text-center">Modificar mi contrase&ntilde;a</h1>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <asp:PlaceHolder ID="phMsjeError" Visible="false" runat="server">
                            <p class="alert alert-danger">
                                <asp:Literal ID="ltlMensajeError" runat="server"></asp:Literal>
                            </p>
                        </asp:PlaceHolder>
                        <% If Request.QueryString("guardado") = "ok" Then%>
                        <asp:PlaceHolder ID="phMsjeOk" runat="server">
                            <p class="alert alert-success">La nueva contrase&ntilde;a se ha guardado correctamente.</p>
                        </asp:PlaceHolder>
                        <% End If%>
                        <p>Conteste la pregunta de seguridad que agreg&oacute; durante la registraci&oacute;n.</p>
                        <div class="panel-body">
                            <form class="form-inline">
                                <fieldset>
                                    <div class="form-group">
                                        <label>Pregunta de seguridad</label>
                                        <p class="form-control-static lead text-success">
                                            <asp:Literal ID="ltlPreguntaSeguridad" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label>Respuesta</label>
                                        <asp:TextBox ClientIDMode="Static" required="required" MaxLength="30"  ID="iptRespuesta" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Contrase&ntilde;a actual</label>
                                        <asp:TextBox ID="iptContraseniaActual" ClientIDMode="Static" required="required" CssClass="form-control" MaxLength="10" TextMode="Password" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>Nueva contrase&ntilde;a</label>
                                        <asp:TextBox ClientIDMode="Static" required="required" ID="iptNuevaContrasenia" CssClass="form-control" MaxLength="10" TextMode="Password" runat="server"></asp:TextBox>
                                        </div>
                                    <asp:Button class="btn btn-lg btn-primary btn-block" ID="btn_CambiarPwd" runat="server" Text="Restaurar Contraseña" ValidationGroup="ResetPwdValidation" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
