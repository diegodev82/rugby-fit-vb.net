Imports MPP
Imports BE
Public Class BLL_Productos


    Public oMpp As New MPP_Productos

    Public Function Valorar(ByVal idPrd As Integer, valoracion As Integer) As Boolean
        Return oMpp.Valorar(idPrd, valoracion)
    End Function

    Public Function AgregarComentario(ByVal objComen As BE_Comentario) As Boolean
        Dim comentarioAgregado As BE_Comentario = oMpp.AgregarComentario(objComen)
        If comentarioAgregado.IdComentario > 0 Then
            Return True
        End If
        Return False
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    Public Function Crear(ByVal objPrd As BE_Producto) As BE_Producto
        If oMpp.ValidarExistencia(objPrd) = True Then
            Return Nothing
        End If
        Return oMpp.Crear(objPrd)
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    ''' <param name="cant"></param>
    Public Function DescontarStock(ByVal objPrd As BE_Producto, ByVal cant As Integer) As Boolean
        DescontarStock = False
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    Public Function Editar(ByVal objPrd As BE_Producto) As Boolean
        If oMpp.ValidarExistencia(objPrd) = True Then
            Return Nothing
        End If
        Return oMpp.Editar(objPrd)
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    Public Function Eliminar(ByVal objPrd As BE_Producto) As Integer
        Dim resultado As Integer = oMpp.Eliminar(objPrd)
        Return resultado
    End Function

    ''' 
    ''' <param name="objPrd"></param>
    ''' <param name="cant"></param>
    Public Function IncrementarStock(ByVal objPrd As BE_Producto, ByVal cant As Integer) As Boolean
        IncrementarStock = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Producto)
        Return oMpp.Listar(filtros)
    End Function

    Public Function GetPorId(ByVal Id As Integer) As BE_Producto
        Return oMpp.GetPorId(Id)
    End Function

End Class ' BLL_Productos

