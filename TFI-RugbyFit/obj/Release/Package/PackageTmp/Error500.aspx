﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Error500.aspx.vb" Inherits="TFI_RugbyFit.Error500_100" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rugby Fit</title>
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .error {
        }
    </style>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Cambiar navegacion</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Default.aspx">Rugby Fit</a>
                </div>
            </div>
        </nav>
        <form id="form1" runat="server">
            <div class="alert alert-danger error">
                <p>
                    <strong>Error de Sistema</strong><br />
                    Disculpe, ocurri&oacute; un error inesperado en el sistema. Estamos trabajando para solucionarlo
                </p>
            </div>
            <p><a href="javascript: window.history.go(-1);" class="btn btn-default">Volver</a></p>
        </form>
        
        <footer>
            <p>
                <span><a href="#" class="js-ver-tyc">T&eacute;rminos y Condiciones</a></span>
                <span class="pull-right">&copy; 2016 Rugby Fit.</span>
            </p>
        </footer>
    </div>
</body>
</html>
