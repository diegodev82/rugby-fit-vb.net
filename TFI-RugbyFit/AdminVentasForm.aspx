﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminVentasForm.aspx.vb" Inherits="TFI_RugbyFit.AdminVentasForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <div class="col-md-8 col-md-offset-2" >
    <% If Request.QueryString("guardado") = "ok" Then%>
    <div class="alert alert-success">
        <p><span class="glyphicon glyphicon-thumbs-up"></span>El registro fue guardado con &eacute;xito</p>
    </div>
    <% End If%>
    <% If Request.QueryString("guardado") = "error-db" Then%>
    <div class="alert alert-danger">
        <p>Disculpe, hubo un error en el sistema y el registro no pudo ser actualizado. Int&eacute;ntelo nuevamente, por favor.</p>
    </div>
    <% End If%>
    
    <div class="form-group botonera clearfix">
        <a href="AdminVentasList.aspx" class="btn btn-info pull-right">Volver al listado</a>
    </div>
    <asp:HiddenField ID="iptCodigo" runat="server" />
    
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-2'>
                <label>Cliente:</label>
            </div>
            <div class='col-md-9'>
                <span class="form-control-static">
                    <asp:Literal ID="ltlCliente" runat="server"></asp:Literal></span>
            </div>
        </div>
        <!-- /row -->
    </div>
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-2'>
                <label>Monto Total:</label>
            </div>
            <div class='col-md-9'>
                <span class="form-control-static">
                    <asp:Literal ID="ltlMontoTotal" runat="server"></asp:Literal></span>
            </div>
        </div>
        <!-- /row -->
    </div>
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-2'>
                <label>Fecha:</label>
            </div>
            <div class='col-md-9'>
                <span class="form-control-static">
                    <asp:Literal ID="ltlFecha" runat="server"></asp:Literal></span>
            </div>
        </div>
        <!-- /row -->
    </div>
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-2'>
                <label>Estado:</label>
            </div>
            <div class='col-md-9'>
                <asp:DropDownList ID="ddEstado" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Value="PAGADA">Pagada</asp:ListItem>
                    <asp:ListItem Value="ENTREGADA">Entregada</asp:ListItem>
                    <asp:ListItem Value="PROCESANDO">Procesando</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    <div class="form-group botonera clearfix">
        <input type='reset' class="btn btn-default  pull-left" name='guardar' value='Cancelar' />
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar" />
    </div>
        </div>
</asp:Content>
