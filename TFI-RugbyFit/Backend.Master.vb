﻿Imports BLL
Imports BE
Public Class Backend
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ItemCompras.Visible = Utiles.TienePermiso("MOD_COMPRAS")
        ItemVentas.Visible = Utiles.TienePermiso("MOD_VENTAS")
        ItemMkt.Visible = Utiles.TienePermiso("MOD_MKT")
        ItemAdmFzas.Visible = Utiles.TienePermiso("MOD_ADM_FZAS")
        'ItemDeposito.Visible = Utiles.TienePermiso("MOD_DEPOSITO")
        ItemSeguridad.Visible = Utiles.TienePermiso("MOD_SEGURIDAD")
        'ItemCompras.Attributes.Add("class", (New String() {"SolicitudComprasList.aspx", "SolicitudComprasForm.aspx", "AdminComprasList.aspx", "AdminComprasForm.aspx", "AdminProveedoresList.aspx", "AdminProveedoresForm.aspx"}))
        ' Ventas: <%=isActive(New String(){"AdminVentasList.aspx","AdminVentasForm.aspx","AdminClientesList.aspx","AdminClientesForm.aspx"})%>
        'Administracion y Fzas: <%=isActive(New String(){"AdminFacturasList.aspx","AdminFacturasForm.aspx","AdminAnularFacturas.aspx","AdminAutorizacionComprasList.aspx","AdminAutorizacionComprasForm.aspx","AdminReportes.aspx"})%>
        'MKT:  <%=isActive(New String(){"AdminProductosList.aspx","AdminProductosForm.aspx","AdminOfertasList.aspx","AdminOfertasForm.aspx","AdminPublicidadesList.aspx","AdminPublicidadesForm.aspx","AdminCategoriasList.aspx","AdminCategoriasForm.aspx"})%>
        'Deposito: <%=isActive(New String(){"AdminStock.aspx","AdminRecepcionCompras.aspx","AdminCanjeVoucher.aspx"})%>
        'Seguridad: <%=isActive(New String(){"AdminUsuariosList.aspx","AdminUsuariosForm.aspx","AdminFamiliasList.aspx","AdminFamiliasForm.aspx","AdminAuditarBitacora.aspx","AdminBackup.aspx"})%>
    End Sub
    Public Sub MostrarErrorSistema()
        phErrorSistema.Visible = True
        phContenido.Visible = False
    End Sub
    Public Shared Function isActive(pag As String) As String
        Dim urlActual = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pagActual = urlActual.Substring(urlActual.LastIndexOf("/") + 1)
        If pag = pagActual Then
            Return "active"
        End If
        Return ""
    End Function

    Public Shared Function isActive(pags As String()) As String
        Dim urlActual = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pagActual = urlActual.Substring(urlActual.LastIndexOf("/") + 1)
        If pags.Contains(pagActual) Then
            Return "active"
        End If
        Return ""
    End Function

    Protected Sub btn_logout_Click(sender As Object, e As EventArgs) Handles btn_logout.Click
        Dim bllSeg As New BLL_Seguridad
        bllSeg.Logout()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("Login.aspx")
    End Sub
End Class