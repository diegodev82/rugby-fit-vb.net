﻿Imports BLL
Imports BE
Public Class AdminFamiliasList
    Inherits ACL
    Private oBll As New BLL_Familias
    Const COD_PERMISO = "MOD_SEGURIDAD"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            LlenarDgv()
        End If
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Familia) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count
        DgvFamilias.AutoGenerateColumns = False
        DgvFamilias.DataSource = registros
        DgvFamilias.DataBind()
    End Sub


    Protected Sub DgvFamilias_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvFamilias.RowEditing
        Dim row As GridViewRow = DgvFamilias.Rows(e.NewEditIndex)
        Dim IdFamilia As String = row.Cells(0).Text

        Response.Redirect("/AdminFamiliasForm.aspx?id=" + IdFamilia)
    End Sub

    Protected Sub DgvFamilias_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvFamilias.RowUpdating
        Dim a As Integer = 0

    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvFamilias_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvFamilias.PageIndexChanging
        DgvFamilias.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvFamilias_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvFamilias.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        If filtroNombre.Text IsNot Nothing Then
            filtros.Add("@Nombre", filtroNombre.Text)
        End If
        
        If IsNumeric(filtroCodigo.Text) Then
            Dim idFamilia As Integer = CType(filtroCodigo.Text, Integer)
            If idFamilia > 0 Then
                filtros.Add("@IdFamilia", idFamilia)
            End If
        End If

        Return filtros
    End Function

    Protected Sub DgvFamilias_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles DgvFamilias.RowDeleting
        Dim row As GridViewRow = DgvFamilias.Rows(e.RowIndex)
        Dim idFam As String = row.Cells(0).Text
        If IsNumeric(idFam) Then
            Dim objFam As New BE_Familia With {.IdFamilia = idFam}
            Dim eliminado As Integer = oBll.Eliminar(objFam)
            If eliminado = 1 Then
                Response.Redirect("AdminFamiliasList.aspx?eliminado=ok")

            ElseIf eliminado = -1 Then
                RtaErrorEliminar.Visible = True
                msjErrorFK.Visible = True
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

End Class