Imports BE
Imports DAL

Public Class MPP_Compras



    Public m_BE_Detalle_Compra As BE.BE_Detalle_Compra

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function AgregarProducto(ByVal objDetCpra As BE_Detalle_Compra) As BE.BE_Detalle_Compra
        AgregarProducto = Nothing
    End Function

    ''' 
    ''' <param name="objCompra"></param>
    Public Function Crear(ByVal objCompra As BE_Solicitud_Compra) As BE.BE_Compra
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objCompra"></param>
    Public Function Editar(ByVal objCompra As BE_Solicitud_Compra) As BE.BE_Compra
        Editar = Nothing
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function EditarItem(ByVal objDetCpra As BE_Detalle_Compra) As BE.BE_Detalle_Compra
        EditarItem = Nothing
    End Function

    ''' 
    ''' <param name="objCompra"></param>
    Public Function Eliminar(ByVal objCompra As BE_Compra) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function EliminarItem(ByVal objDetCpra As BE_Detalle_Compra) As Boolean
        EliminarItem = False
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Enviar(ByVal objSolCpra As BE_Solicitud_Compra) As Boolean
        Enviar = False
    End Function

    ''' 
    ''' <param name="IdCompra"></param>
    Public Function GetCompraDetallada(ByVal IdCompra As Integer) As BE.BE_Compra
        GetCompraDetallada = Nothing
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarCompras(ByVal filtros As Hashtable) As List(Of BE_Compra)
        ListarCompras = Nothing
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarProductosPendietnes(ByVal filtros As Hashtable) As List(Of BE_Detalle_Compra)
        ListarProductosPendietnes = Nothing
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarSolicitudes(ByVal filtros As Hashtable) As List(Of BE_Compra)
        ListarSolicitudes = Nothing
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function RegistrarIngreso(ByVal objDetCpra As BE_Detalle_Compra) As Boolean
        RegistrarIngreso = False
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function ValidarProductoAgregado(ByVal objDetCpra As BE_Detalle_Compra) As Boolean
        ValidarProductoAgregado = False
    End Function


End Class ' MPP_Compras
