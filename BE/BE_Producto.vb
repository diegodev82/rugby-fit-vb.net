Public Class BE_Producto


    Public Property Activo As Integer
    Public Property Categoria As BE_Categoria
    Public Property DescripcionCorta As String
    Public Property Descripcion As String
    Public Property EsCanjeable As Integer
    Public Property IdProducto As Integer
    Public Property LinkImagen As String = "/assets/img/img-vacia.png"
    Public Property Nombre As String
    Public Property OfrecePuntos As Integer
    Public Property Precio As Double
    Public Property Proveedor As BE_Proveedor
    Public Property PuntosOfrecidos As Integer
    Public Property PuntosRequeridos As Integer
    Public Property Stock As Integer
    Public Property StockMinimo As Integer
    Public Property Comentarios As New List(Of BE_Comentario)
    Public Property Valoracion As Integer = 0
    Public Property CantVotos As Integer = 0
    Public Property Rating As Integer = 0

End Class ' BE_Producto