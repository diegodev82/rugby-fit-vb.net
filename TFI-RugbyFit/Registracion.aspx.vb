﻿Imports BLL
Imports BE
Public Class Registracion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Try
                Me.CargarProvincias()
            Catch ex As Exception
                TryCast(Me.Master, Main).MostrarErrorSistema()
            End Try

        End If
    End Sub

    Protected Sub CargarProvincias()
        Dim oBLLProv As New BLL_Provincias()
        ddProvincia.DataTextField = "nombre"
        ddProvincia.DataValueField = "id_provincia"
        ddProvincia.DataSource = oBLLProv.Listar()
        ddProvincia.DataBind()
    End Sub

    Protected Sub btnRegistrarme_Click(sender As Object, e As EventArgs) Handles btnRegistrarme.Click
        Dim FormValido As Boolean = Me.ValidarForm()
        If FormValido = True Then
            Try
                Dim oBll As New BLL_Clientes
                Dim oBllSeg As New BLL_Seguridad
                Dim oBECliente As New BE_Cliente
                Dim oCiudad As New BE_Ciudad With {.IdCiudad = 0}
                Dim idCiudadRequest As String = Request(ddCiudad.UniqueID)
                If Not idCiudadRequest Is Nothing Then
                    oCiudad.IdCiudad = idCiudadRequest
                End If
                oBECliente.Nombre = iptNombre.Text
                oBECliente.Apellido = iptApellido.Text
                oBECliente.DNI = IIf(IsNumeric(iptDNI.Text) = False, 0, iptDNI.Text)
                oBECliente.CUIT = iptCUIT.Text
                oBECliente.Domicilio = iptDomicilio.Text
                oBECliente.Email = iptEmail.Text
                oBECliente.Origen = BE_Cliente.ORIGEN_WEB
                oBECliente.Telefono = iptTelefono.Text
                oBECliente.Ciudad = oCiudad
                oBECliente.Nickname = iptNickname.Text
                oBECliente.Contrasenia = oBllSeg.Encriptar(iptContrasenia.Text)
                oBECliente.PreguntaSeguridad = iptPreguntaSeguridad.Text
                oBECliente.RespuestaSeguridad = iptRtaPreguntaSeguridad.Text
                Dim creado As Object = oBll.Crear(oBECliente)
                If creado.GetType() = GetType(Integer) Then
                    If creado = BLL_Clientes.ERROR_CLIENTE_EXISTENTE Then
                        MensajeRespuesta.Text = "Ya existe un cliente registrado con ese usuario o email. Por favor, ingrese otro nombre de usuario"
                    End If
                    If creado = BLL_Clientes.ERROR_SMTP Then
                        phFallBack.Visible = True
                        ltlLinkConfirmacion.Text = oBll.GetHashRegistracionFallBack
                    End If
                    If creado = BLL_Clientes.ERROR_DB Then
                        MensajeRespuesta.Text = "Se produjo un error desconocido y la registación no pudo realizarse. Inténtelo nuevamente o pongase en <a href='Contacto.aspx'>contacto con nosotros</a>"
                    End If
                    PanelMensajeRespuesta.Visible = True
                Else
                    Response.Redirect("RegistracionOk.aspx")
                End If

            Catch ex As Exception
                TryCast(Me.Master, Main).MostrarErrorSistema()
            End Try
        End If

    End Sub

    Protected Function ValidarForm() As Boolean
        Dim huboError As Boolean = False

        EmailRequerido.Visible = False
        regexEmailValid.Visible = False
        NicknameRequerido.Visible = False
        ContraseniaRequerida.Visible = False
        preguntaRequerida.Visible = False
        rtaRequerida.Visible = False
        NombreRequerido.Visible = False
        ApellidoRequerido.Visible = False
        ErrorTyC.Visible = False

        If Validador.EstaVacio(iptEmail.Text) Then
            huboError = True
            EmailRequerido.Visible = True
        End If
        If Not Validador.EmailValido(iptEmail.Text) Then
            huboError = True
            regexEmailValid.Visible = True
        End If
        If Validador.EstaVacio_MaxLength(iptNickname.Text, 10) Then
            huboError = True
            NicknameRequerido.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            NicknameRequerido.Visible = True
        End If
       
        If Validador.EstaVacio_MaxLength(iptContrasenia.Text, 10) Then
            huboError = True
            ContraseniaRequerida.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            ContraseniaRequerida.Visible = True
        End If

        If Validador.EstaVacio_MaxLength(iptPreguntaSeguridad.Text) Then
            huboError = True
            preguntaRequerida.ErrorMessage = "* Campo requerido (Max 50 caracteres)"
            preguntaRequerida.Visible = True
        End If
        If Validador.EstaVacio_MaxLength(iptRtaPreguntaSeguridad.Text) Then
            huboError = True
            rtaRequerida.ErrorMessage = "* Campo requerido (Max 50 caracteres)"
            rtaRequerida.Visible = True
        End If
        If Validador.EstaVacio_MaxLength(iptNombre.Text, 10) Then

            huboError = True
            NombreRequerido.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            NombreRequerido.Visible = True
        End If
        If Validador.EstaVacio_MaxLength(iptApellido.Text, 10) Then
            huboError = True
            ApellidoRequerido.ErrorMessage = "* Campo requerido (Max 10 caracteres)"
            ApellidoRequerido.Visible = True
        End If
        If chkTyC.Checked = False Then
            huboError = True
            ErrorTyC.Visible = True
        End If
        If huboError = False Then
            Return True
        End If
        Return False
    End Function
End Class