SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_GetPermisosHeredadosPorFamilia
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT p.id_permiso, p.codigo FROM usuario_familia uf 
		INNER JOIN familia_permiso pf ON uf.id_familia = pf.id_familia 
		INNER JOIN permiso p ON p.id_permiso = pf.id_permiso  
		WHERE uf.id_usuario = @IdUsuario
END
GO

