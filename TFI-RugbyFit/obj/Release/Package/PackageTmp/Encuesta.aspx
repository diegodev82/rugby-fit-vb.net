﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Encuesta.aspx.vb" Inherits="TFI_RugbyFit.Encuesta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/paginas/encuesta.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="container">
        <asp:PlaceHolder ID="phSinEncuesta" Visible="false" runat="server">
            <p class="alert alert-info">No hay encuestas disponibles</p>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phEncuesta" runat="server">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                
                 <div class="panel panel-primary" id="panelRtdos" style="display:none;">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-arrow-right"></span>
                            <asp:Literal ID="ltlPreguntaRtdo" runat="server"></asp:Literal>
                            </h3>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptRtasRtdos" runat="server">
                                <ItemTemplate>
                                    <strong id="respuesta_<%# Container.DataItem.IdRespuesta %>"><%# Container.DataItem.Respuesta %></strong>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" id="porcentaje_<%# Container.DataItem.IdRespuesta %>" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
                                        
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                    </div>
                    <div class="panel-footer">
                        <a href="javascript:verVotacion();" id="linkVotar" class="btn btn-primary btn-sm" >Votar</a>
                    </div>
                </div>
                <div class="panel panel-primary" id="panelVotacion">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-arrow-right"></span>
                            <asp:Literal ID="ltlPregunta" runat="server"></asp:Literal>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                                <asp:Repeater ID="rptRespuestas" runat="server">
                                <ItemTemplate>
                                    <li class="list-group-item">
                                        <div class="radio">
                                            <label>
                                                <input type="radio"class="rta_encuesta" name="respuesta" data-id_encuesta="<%# Container.DataItem.IdEncuesta %>"" value="<%# Container.DataItem.IdRespuesta %>" /> <%# Container.DataItem.Respuesta %>
                                            </label>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary btn-sm" value="Votar" id="btn_votar">Votar</button>
                        <a href="javascript:verResultados();" >Ver Resultados</a>
                    </div>
                </div>
            </div> 
        </div>
        </asp:PlaceHolder>

    </div>
</asp:Content>
