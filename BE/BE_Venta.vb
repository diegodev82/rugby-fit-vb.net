Public Class BE_Venta

    Public Property IdVenta As Integer
    Public Property IdCliente As Integer
    Public Property Fecha As DateTime
    Public Property Items As New List(Of BE_Detalle_Venta)
    ''' <summary>
    ''' Puede ser:
    ''' WEB: si la venta proviene del e-commerce y la carga el cliente
    ''' LOCAL: si la venta la carga el vendedor en el local.
    ''' </summary>
    Public Property Origen As String
    Public Property IdVendedor As Integer
    Public Property MontoTotal As Decimal
    Public Property Estado As String
    Public Property IdFactura As Integer = 0
    Public Property NombreCliente As String


End Class ' BE_Venta
