Imports BE
Imports DAL
Public Class MPP_Usuarios

    Private ds As DataSet

    Public Function GetPorId(ByVal id As Integer) As BE_Usuario

        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetUsuarioPorId", hdatos)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                Dim clienteAsoc As New BE_Cliente With {.IdCliente = 0}
                Dim unUsuario As New BE_Usuario
                unUsuario.IdUsuario = item("id_usuario")
                unUsuario.Nombre = item("nombre")
                unUsuario.Apellido = item("apellido")
                unUsuario.Email = item("email")
                unUsuario.Telefono = IIf(IsDBNull(item("telefono")), "", item("telefono"))
                unUsuario.Nickname = item("nickname")
                unUsuario.Contrasenia = item("contrasenia")
                unUsuario.EsEmpleado = item("es_empleado")
                unUsuario.CII = IIf(IsDBNull(item("cii")), 0, item("cii"))
                unUsuario.DNI = IIf(IsDBNull(item("dni")), 0, item("dni"))
                unUsuario.PreguntaSeguridad = IIf(IsDBNull(item("pregunta_seguridad")), "", item("pregunta_seguridad"))
                unUsuario.RespuestaSeguridad = IIf(IsDBNull(item("respuesta_seguridad")), "", item("respuesta_seguridad"))
                unUsuario.Activo = item("activo")
                If Not IsDBNull(item("id_cliente")) And IsNumeric(item("id_cliente")) Then
                    clienteAsoc.IdCliente = item("id_cliente")
                End If
                unUsuario.Cliente = clienteAsoc
                unUsuario.Familias = Me._GetFamiliasPorUsuario(unUsuario.IdUsuario)
                unUsuario.Permisos = Me._GetPermisosPersonales(unUsuario.IdUsuario)
                Return unUsuario
            End If
        End If
        Return Nothing
    End Function


    Private Function _GetFamiliasPorUsuario(id_usuario) As List(Of BE_Familia)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id_usuario)
        Dim listadoFam As New List(Of BE_Familia)
        ds = oDatos.Leer("pa_GetFamiliasPorUsuario", hdatos)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim familia As New BE.BE_Familia
                familia.IdFamilia = dr.Item("id_familia")
                familia.Nombre = dr.Item("nombre")
                listadoFam.Add(familia)
            Next
        End If
        Return listadoFam
    End Function



    Private Function _GetPermisosPersonales(id_usuario) As List(Of BE_Permiso)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id_usuario)

        ds = oDatos.Leer("pa_GetPermisosHeredadosPorUsuario", hdatos)
        Dim listadoPerm As New List(Of BE_Permiso)
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim permiso As New BE.BE_Permiso
            permiso.IdPermiso = dr.Item("id_permiso")
            permiso.Descripcion = dr.Item("descripcion")
            permiso.Tipo = dr.Item("tipo")
            permiso.Codigo = dr.Item("codigo")
            listadoPerm.Add(permiso)
        Next
        Return listadoPerm
    End Function

    Public Function AgregarFamilia(ByVal IdFamilia As Integer, ByVal IdUsuario As Integer) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", IdUsuario)
        hdatos.Add("@IdFamilia", IdFamilia)
        Dim IdInsertado As Integer = oDatos.Insertar("pa_AgregarUsuarioFamilia", hdatos)
        Return IdInsertado
    End Function




    ''' 
    ''' <param name="IdUsuario"></param>
    ''' <param name="objPermiso"></param>
    Public Function AgregarPermiso(ByVal IdUsuario As Integer, ByVal objPermiso As BE_Permiso) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@IdUsuario", IdUsuario)
        hdatos.Add("@IdPermiso", objPermiso.IdPermiso)
        hdatos.Add("@Tipo", objPermiso.Tipo)
        
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim IdInsertado As Object = oDatos.Insertar("pa_AgregarUsuarioPermiso", hdatos)
        If IsNumeric(IdInsertado) = True Then
            Return IdInsertado
        End If
        Return 0
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Crear(ByVal objUsuario As BE_Usuario) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@Nombre", objUsuario.Nombre)
        hdatos.Add("@Apellido", objUsuario.Apellido)
        hdatos.Add("@Nickname", objUsuario.Nickname)
        hdatos.Add("@Contrasenia", objUsuario.Contrasenia)
        hdatos.Add("@Email", objUsuario.Email)
        hdatos.Add("@Telefono", objUsuario.Telefono)
        hdatos.Add("@DNI", objUsuario.DNI)
        hdatos.Add("@Activo", objUsuario.Activo)
        hdatos.Add("@EsEmpleado", objUsuario.EsEmpleado)
        
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim IdInsertado As Object = oDatos.Insertar("pa_CrearUsuario", hdatos)
        If IsNumeric(IdInsertado) = True Then
            Return IdInsertado
        End If
        Return 0
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Desbloquear(ByVal objUsuario As BE_Usuario) As Boolean
        Desbloquear = False
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Editar(ByVal objUsuario As BE_Usuario) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdUsuario", objUsuario.IdUsuario)
        hdatos.Add("@Nombre", objUsuario.Nombre)
        hdatos.Add("@Apellido", objUsuario.Apellido)
        hdatos.Add("@Email", objUsuario.Email)
        hdatos.Add("@Telefono", objUsuario.Telefono)
        hdatos.Add("@DNI", objUsuario.DNI)
        hdatos.Add("@Activo", objUsuario.Activo)
        hdatos.Add("@CII", objUsuario.CII)



        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ModificarUsuario", hdatos)
        Return guardado

    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function Eliminar(ByVal objUsuario As BE_Usuario) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", objUsuario.IdUsuario)
        Dim resultado As Object = oDatos.Eliminar("pa_EliminarUsuario", hdatos)
        Return resultado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Usuario)
        Dim hdatos As New Hashtable
        'hdatos.Add("@Email", objCliente.Email)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetUsuarios", filtros)
        Dim listado As New List(Of BE_Usuario)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim cliente As New BE_Cliente With {.IdCliente = 0}
                    Dim unUsu As New BE_Usuario
                    unUsu.IdUsuario = item("id_usuario")
                    unUsu.Nombre = item("nombre")
                    unUsu.Apellido = item("apellido")
                    unUsu.Nickname = item("nickname")
                    unUsu.Contrasenia = item("contrasenia")
                    unUsu.PreguntaSeguridad = item("pregunta_seguridad")
                    unUsu.RespuestaSeguridad = item("respuesta_seguridad")
                    unUsu.Telefono = IIf(IsDBNull(item("telefono")), "", item("telefono"))
                    unUsu.Email = item("email")
                    unUsu.EsEmpleado = item("es_empleado")
                    unUsu.CII = item("cii")
                    unUsu.Activo = item("activo")
                    If Not IsDBNull(item("id_cliente")) And IsNumeric(item("id_cliente")) Then
                        cliente.IdCliente = item("id_cliente")
                    End If
                    unUsu.Cliente = cliente
                    listado.Add(unUsu)
                Next
            End If
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    Public Function ModificarContrasenia(ByVal objUsuario As BE_Usuario) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", objUsuario.IdUsuario)
        hdatos.Add("@Contrasenia", objUsuario.Contrasenia)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ModificarContraseniaUsuario", hdatos)
        Return guardado
    End Function

    ''' 
    ''' 
    Public Function QuitarFamilia(ByVal IdUsuario As Integer, ByVal idFamilia As Integer) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", IdUsuario)
        hdatos.Add("@IdFamilia", idFamilia)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_QuitarUsuarioFamilia", hdatos)
        Return guardado
    End Function

    ''' 

    Public Function QuitarPermiso(ByVal IdUsuario As Integer, ByVal IdPermiso As Integer) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", IdUsuario)
        hdatos.Add("@IdPermiso", IdPermiso)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_QuitarUsuarioPermiso", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    ''' <summary>Devuelve true si aun queda alguien en la familia superadmin, false si es el �ltimo</summary>
    Public Function PuedeEliminarUsuario(ByVal objUsuario As BE_Usuario) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", objUsuario.IdUsuario)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_EsAdmin", hdatos)
        If ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
            Dim dsCant As New DataSet
            dsCant = oDatos.Leer("pa_HayMasAdmins", Nothing)
            Dim dr As DataRow = dsCant.Tables(0).Rows(0)
            Dim cant As Integer = CType(dr("cant"), Integer)
            Return cant > 1
        End If
        Return True 'si no es admin, se puede eliminar tranquilamente
    End Function

    ''' 
    ''' <param name="objUsuario"></param>
    ''' <summary>Devuelve true si existe, false si no existe</summary>
    Public Function ValidarExistente(ByVal objUsuario As BE_Usuario) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Nickname", objUsuario.Nickname)
        hdatos.Add("@IdUsuario", objUsuario.IdUsuario)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarUsuarioExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function


End Class ' MPP_Usuarios
