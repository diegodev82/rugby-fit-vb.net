Imports DAL
Imports BE

Public Class MPP_Ciudades

    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Ciudad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim list As New List(Of BE_Ciudad)
        ds = oDatos.Leer("pa_GetCiudadesPorProvincia", filtros)
        If ds.Tables.Count > 0 Then
            For Each item As DataRow In ds.Tables(0).Rows
                Dim unaCiudad As New BE_Ciudad
                unaCiudad.IdCiudad = item("id_ciudad")
                unaCiudad.Nombre = item("nombre")
                list.Add(unaCiudad)
            Next
        End If
        Return list
    End Function


End Class ' MPP_Ciudades

