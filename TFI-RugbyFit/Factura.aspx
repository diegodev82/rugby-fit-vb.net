﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Factura.aspx.vb" Inherits="TFI_RugbyFit.Factura" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="assets/css/factura.css" rel="stylesheet" />
    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <asp:HiddenField ID="iptIdFactura" runat="server" />
    <div class="col-xs-12" runat="server" id="div_error" visible="false">
        <div class="alert alert-danger">
            <p runat="server" id="msj_error"></p>
        </div>
    </div>
    <div class="container-fc" runat="server" id="div_fc" visible="false">
        <div class="row pad-top-botm ">
            <div class="col-lg-5 col-md-5 col-sm-5 ">
                <img src="assets/img/Logo-RugbyFit.png" style="padding-bottom: 20px; height: 250px;" class="img-responsive" />
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <span class="tipo_fc">B</span>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5">
                <h3>Factura Nro: <asp:Literal ID="ltlNroFc" runat="server">123456</asp:Literal></h3>
                <strong style="font-size: 20px;">Rugby Fit.</strong>
                <br />
                Avenida Coronel Roca 5601 , Villa Lugano,
              <br />
                C.A.B.A. , Buenos Aires,
              <br />
                Argentina.
              
            </div>
        </div>
        <div class="row text-center contact-info">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <hr />
                <span>
                    <strong>Email : </strong>info@rugbyit.com 
                </span>
                <span>
                    <strong>Tel. : </strong>0800-222-4567
                </span>
                <span>
                    <strong>Fb : </strong>www.facebook.com/RugbyFit
                </span>
                <hr />
            </div>
        </div>
        <div class="row pad-top-botm client-info">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h4><strong>Informaci&oacute;n del Cliente</strong></h4>
                <strong><asp:Literal ID="ltlNombreCliente" runat="server">Juan Carlos Sanchez</asp:Literal></strong>
                <br />
                <b>Domicilio:</b> <asp:Literal ID="ltlDomicilioCliente" runat="server">Chiclana 2456</asp:Literal>,  <asp:Literal ID="ltlCiudad" runat="server">Lanus</asp:Literal>, <asp:Literal ID="ltlProvincia" runat="server">Buenos Aires</asp:Literal>
              <br />
                Argentina.
             <br />
                <b>Tel. :</b> <asp:Literal ID="ltlTelefonoCliente" runat="server">4566-8999</asp:Literal>
              <br />
                <b>E-mail :</b>  <asp:Literal ID="ltlMailCliente" runat="server">example@example.com</asp:Literal>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">

                <h4><strong>Detalles del Pago </strong></h4>
                <b>Monto :  $ <asp:Literal ID="ltlMontoCab" runat="server">000.00</asp:Literal></b>
                <br />
                Fecha Pago:  <asp:Literal ID="ltlFecha" runat="server">13 / 04 / 2016</asp:Literal>
              <br />
                <b>Forma de Pago :  <asp:Literal ID="ltlMetodoPago" runat="server">Efectivo</asp:Literal> </b>

                <br />
                Fecha Compra :  <asp:Literal ID="ltlFecha2" runat="server">13 / 04 / 2016</asp:Literal>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Descirpci&oacute;n</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                                <th>Sub Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptItems" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="td-producto"><%# Container.DataItem.Producto.Nombre %></td>
                                            <td class="td-cantidad"><%# Container.DataItem.Cantidad %></td>
                                            <td class="td-precio">$<%# Container.DataItem.Monto %></td>
                                            <td class="td-subtotal">$<%# Container.DataItem.Monto * Container.DataItem.Cantidad %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
     <%--                       <tr>
                                <td>1</td>
                                <td>Website Design</td>
                                <td>1</td>
                                <td>300 USD</td>
                                <td>300 USD</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Plugin Dev.</td>
                                <td>2</td>
                                <td>200 USD</td>
                                <td>400 USD</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Hosting Domains</td>
                                <td>2</td>
                                <td>100 $</td>
                                <td>200 $</td>
                            </tr>--%>

                        </tbody>
                    </table>
                </div>
                <hr />
                <div class="ttl-amts">
                    <h4><strong>Monto total : $ <asp:Literal ID="ltlTotal" runat="server">000.00</asp:Literal></strong> </h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <strong>Importante: </strong>
                <ol>
                    <li>Esta factura fue generada electr&oacute;nicamente por lo que no requiere firma.</li>
                    <li>Por favor, lea los t&eacute;rminos y condiciones en   www.rugbyfit.com por devoluciones, reemplazos u otros inconvenientes.</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row pad-top-botm">
         <% If Request.QueryString("imprimir") Is Nothing Then%>
        <div class="col-md-6 text-left">
            <asp:Button ID="btnAnular" Visible="false" CssClass="btn btn-danger btn-lg" runat="server" Text="Anular" />
        </div>
        <div class="col-md-6 text-right">
            <asp:Button ID="btnImprimir" CssClass="btn btn-success btn-lg" runat="server" Text="Imprimir" />
        </div>
        <% End If %>
    </div>
    <%--    <div class="row">
        <div class="col-xs-12" runat="server" id="div_error" visible="false">
            <div class="alert alert-danger">
                <p runat="server" id="msj_error"></p>
            </div>
            </div>
        <div class="col-xs-12" runat="server" id="div_fc" visible="false">
            <div class="invoice-title">
                <h2>Factura</h2>
                <h3 class="pull-right">Nro:
                    <asp:Literal ID="ltlNroFc" runat="server">123456</asp:Literal></h3>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>Datos del cliente:</strong><br />
                        <span class="nombre-cliente">
                            <asp:Literal ID="ltlNombreCliente" runat="server">Juan Carlos Sanchez</asp:Literal></span>,<br />
                        <span class="domicilio-cliente">
                            <asp:Literal ID="ltlDomicilioCliente" runat="server">Chiclana 3195</asp:Literal></span>,<br />
                        <span class="ciudad-cliente">
                            <asp:Literal ID="ltlCiudad" runat="server">Lanus</asp:Literal></span>,<br />
                        <span class="provincia-cliente">
                            <asp:Literal ID="ltlProvincia" runat="server">Buenos Aires</asp:Literal></span>
                    </address>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>M&eacute;todo de pago:</strong><br />
                        <asp:Literal ID="ltlMetodoPago" runat="server">Efectivo</asp:Literal><br />
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Fecha: </strong>
                        <br />
                        <asp:Literal ID="ltlFecha" runat="server">13 / 04 / 2016</asp:Literal><br />
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Descripci&oacute;n</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Producto</strong></td>
                                    <td class="text-center"><strong>Precio</strong></td>
                                    <td class="text-center"><strong>Cantidad</strong></td>
                                    <td class="text-right"><strong>Subtotal</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptItems" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="td-producto"><%# Container.DataItem.Producto.Nombre %></td>
                                            <td class="td-precio text-center">$<%# Container.DataItem.Monto %></td>
                                            <td class="td-cantidad text-center"><%# Container.DataItem.Cantidad %></td>
                                            <td class="td-subtotal text-right">$<%# Container.DataItem.Monto * Container.DataItem.Cantidad %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>Total</strong></td>
                                    <td class="thick-line text-right">$<asp:Literal ID="ltlTotal" runat="server">000.00</asp:Literal></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <% If Request.QueryString("imprimir") Is Nothing Then%>
        <div class="col-md-6 text-left">
            <asp:Button ID="btnAnular" Visible="false" CssClass="btn btn-danger" runat="server" Text="Anular" />
        </div>
        <div class="col-md-6 text-right">
            <asp:Button ID="btnImprimir" CssClass="btn btn-info" runat="server" Text="Imprimir" />
        </div>
        <% End If %>
    </div>--%>
</asp:Content>
