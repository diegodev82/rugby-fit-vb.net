﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Novedades.aspx.vb" Inherits="TFI_RugbyFit.Novedades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/novedades.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">Novedades</h1>
                <p class="help-block">Estas son las &uacute;ltimas novedades que tenemos para contarle.</p>
            </div>

            <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                <div class="col-md-12">
                       <div runat="server" id="msjeRtaOK" class="alert alert-success"  visible="false">
                           Gracias por suscribirse a nuestro newsletter
                       </div>
                    <div runat="server" id="msjeRtaError"  class="alert alert-danger" visible="false"></div>
                </div>
            </asp:PlaceHolder>
            <div class="col-md-12">
                <h4>Puede suscribirse a nuestro newsletter ingresando su email aqu&iacute;</h4>
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="iptEmailNewsletter" TextMode="Email" ClientIDMode="Static" required="required" CssClass="form-control" runat="server" ValidationGroup="SuscripcionNews"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_EmailNews" ControlToValidate="iptEmailNewsletter" ForeColor="Red" runat="server" ErrorMessage="Campo requerido" ValidationGroup="SuscripcionNews"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnSuscribirse" CssClass="btn btn-info" runat="server" Text="Suscribirme" ValidationGroup="SuscripcionNews" />
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row-eq-height ">
            <asp:Repeater ID="rptNovedades" runat="server">
                <ItemTemplate>
                    <div class="col-xs-12 col-md-12 col-lg-12">
                        <h2 class="titulo"><%# Container.DataItem.Titulo %></h2>
                        <p class="copete"><%# Container.DataItem.Copete %></p>
                        <p><a class="btn btn-default" href="/NovedadesAmpliado.aspx?id=<%# Container.DataItem.IdNovedad %>" role="button">Leer m&aacute;s &raquo;</a></p>
                    </div>
                    <hr />
                </ItemTemplate>
            </asp:Repeater>        
        </div>
        <!--/row-->
    </div>

</asp:Content>
