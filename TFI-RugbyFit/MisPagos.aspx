﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="MisPagos.aspx.vb" Inherits="TFI_RugbyFit.MisPagos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <h2>Cuenta Corriente</h2>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Descripci&oacute;n</th>
                <th>Medio de Pago</th>
                <th>Saldo a Favor</th>
                <th>Pago</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rptPagos" runat="server">
                <ItemTemplate>
                    <tr>
                        <td><%# Container.DataItem.Fecha %></td>
                        <td><%# IIf(Container.DataItem.TipoMovimiento = "S", "Cancela Factura ","Nota de crédito por Factura") %> <%# Container.DataItem.IdFacturaCancela %></td>
                        <td><%# IIf(Container.DataItem.TipoMovimiento = "S", IIf(Container.DataItem.TipoPago = "TC", "Tarjeta de Crédito", "Usa Nota de Crédito"), "")%></td>
                        <td class="text-right"><%# IIf(Container.DataItem.TipoMovimiento = "S", "", "$ " + FormatNumber(Container.DataItem.Monto, 2, TriState.True).ToString)%></td>
                        <td class="text-right"><%# IIf(Container.DataItem.TipoMovimiento = "S", "$ " + FormatNumber(Container.DataItem.Monto,2,TriState.True).ToString, "")%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</asp:Content>
