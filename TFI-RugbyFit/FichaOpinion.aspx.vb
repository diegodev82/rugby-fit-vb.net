﻿Imports BLL
Imports BE
Public Class FichaOpinion
    Inherits ACL
    Private oBll As New BLL_Encuesta
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            MyBase.ValidarAccesoCliente()
            If Not IsPostBack Then
                Me.CargarData()
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub CargarData()
        iptIdFc.Value = Request.QueryString("idFc")
        Dim encs As List(Of BE_Encuesta) = oBll.GetFichasOpinion()
        rptFichas.DataSource = encs
        rptFichas.DataBind()
        'ltlPreguntaRtdo.Text = enc.Pregunta
        'ltlPregunta.Text = enc.Pregunta
        'rptRespuestas.DataSource = Nothing
        'rptRespuestas.DataBind()
        'rptRespuestas.DataSource = enc.Respuestas
        'rptRespuestas.DataBind()

        'rptRtasRtdos.DataSource = Nothing
        'rptRtasRtdos.DataBind()
        'rptRtasRtdos.DataSource = enc.Respuestas
        'rptRtasRtdos.DataBind()
    End Sub

    Protected Sub rptFichas_ItemDataBound(sender As Object, args As RepeaterItemEventArgs)
        If args.Item.ItemType = ListItemType.Item Or args.Item.ItemType = ListItemType.AlternatingItem Then

            Dim childRepeater As Repeater = args.Item.FindControl("rptRespuestas")
            childRepeater.DataSource = args.Item.DataItem.Respuestas
            childRepeater.DataBind()
        End If
    End Sub
End Class