SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_GetCategorias
	@Nombre as nvarchar(100) = NULL --queda preparada para el build dynamic
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET NOCOUNT ON;
	SELECT * FROM dbo.categoria	order by nombre
END
GO
