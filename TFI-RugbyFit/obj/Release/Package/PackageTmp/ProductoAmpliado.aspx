﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ProductoAmpliado.aspx.vb" Inherits="TFI_RugbyFit.ProductoAmpliado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="assets/css/producto_ampliado.css" />
    <script type="text/javascript" src="assets/js/paginas/producto_ampliado.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    
    <!-- VER https://blackrockdigital.github.io/startbootstrap-blog-post/  -->
    <!-- Title -->
    <asp:HiddenField ID="iptCodigo"  runat="server" />
    <div class="container-fluid">
        <div class="content-wrapper">
            <div class="item-container">
                <div class="container">
                    <% If Request.QueryString("rtdo") = "ok" Then%>
                    <div class="alert alert-success" id="MsgProductoAgregado" >
                        <span class="glyphicon glyphicon-thumbs-up"></span> El producto fue agregado a su compra.
                    </div>
                    <% End If %>
                    <% If Request.QueryString("rtdo") = "error" Then%>
                    <div class="alert alert-danger" id="MsgErrorDb" >
                        <span class="glyphicon glyphicon-info"></span> Disculpe, hubo un error en el sistema y no se pudo completar la operaci&oacute;n. Int&eacute;ntelo nuevamente, por favor.
                    </div>
                    <% End If %>
                    <% If Request.QueryString("rtdo") = "error-stock" Then%>
                    <div class="alert alert-danger" id="MsgErrorStock" >
                        <span class="glyphicon glyphicon-info"></span> Disculpe, no hay stock suficiente para este producto. Int&eacute;ntelo nuevamente m&aacute;s tarde, por favor.
                    </div>
                    <% End If %>
                    <div class="col-md-5">
                        <div class="product service-image-left text-center">                        
                                <asp:Image ID="imgPrd" CssClass="img-responsive" ImageUrl="#" runat="server"></asp:Image>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <a href="Productos.aspx" class="pull-right">Volver al cat&aacute;logo</a>
                        <div class="product-title">
                            <asp:Literal ID="ltlTitulo" runat="server"></asp:Literal></div>
                        <div class="product-desc">
                            <asp:Literal ID="ltlDescCorta" runat="server"></asp:Literal></div>
                        <div class="product-rating"><i class="fa fa-star gold"></i><i class="fa fa-star gold"></i><i class="fa fa-star gold"></i><i class="fa fa-star gold"></i><i class="fa fa-star-o"></i></div>
                        <hr>
                        <div class="product-price">$
                            <asp:Literal ID="ltlPrecio" runat="server"></asp:Literal></div>
                        <div class="product-stock" runat="server" id="divEnStock" visible="false">En Stock</div>
                        <div class="product-sin-stock" runat="server" id="divSinStock" visible="false">Sin Stock</div>
                        <hr>
                        <div class="btn-group cart">                          
                            <asp:Button ID="btnAgregarAlCarrito" CausesValidation="false" CssClass="btn btn-success" runat="server" Text="Agregar al carrito" />    
                        </div>
                        <div class="ratings">
                            <p><span id="cantOpiniones"><asp:Literal ID="ltlCantVotos" runat="server"></asp:Literal></span> opiniones</p>
                            <div class="rating-block">
                                <input type="hidden" id="idPrd" value="<%=Request.QueryString("id") %>" />
                                <h2 class="bold padding-bottom-7"><span id="rating"><asp:Literal ID="ltlRating" runat="server"></asp:Literal></span> <small>/ 5</small></h2>
                                <button type="button" class="btn btn-default btn-sm js-btn-star" data-value="1">
                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-sm js-btn-star" data-value="2">
                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-sm js-btn-star" data-value="3">
                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-sm js-btn-star" data-value="4" >
                                    <span class="glyphicon glyphicon-star" aria-hidden="true" ></span>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-sm js-btn-star" data-value="5">
                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-md-12 product-info">
                    <ul id="myTab" class="nav nav-tabs nav_tabs">
                        <li><a href="#service-1" data-toggle="tab">DESCRIPCI&Oacute;N</a></li>
                        <li class="active"><a href="#service-2" data-toggle="tab">COMENTARIOS</a></li>

                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade" id="service-1">
                            <section class="container product-info">
                                <asp:Literal ID="ltlDescripcion" runat="server"></asp:Literal>
                            </section>

                        </div>
                        <div class="tab-pane fade  in active" id="service-2">
                            <% If Request.QueryString("guardado") = "ok" Then%>
                            <div class="alert alert-success">
                                <span class="glyphicon glyphicon-thumbs-up"></span>&iexcl;Gracias por dejar su comentario!
                            </div>
                            <% End If%>
                            <% If Request.QueryString("guardado") = "error-db" Then%>
                            <div class="alert alert-danger">
                                Disculpe, hubo un error en el sistema y no pudimos registrar su comentario. Por favor, int&eacute;ntelo nuevamente.
                            </div>
                            <% End If%>
                            <section class="container">
                                <!-- Comments Form -->
                                <div class="well">
                                    <h4>Dejar un comentario:</h4>
                                    <form role="form">
                                        <div class="form-group">
                                            <label>Autor:</label>
                                            <asp:TextBox ID="iptAutor" CssClass="form-control js-evitar-codigo-malicioso" runat="server" MaxLength="50"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>Comentario *:</label>
                                            <p class="help-block">M&iacute;nimo 20 car&aacute;cteres, m&aacute;ximo 250</p>
                                            <asp:TextBox ID="txtComentario" pattern="^[^<>]+$" ClientIDMode="Static" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control js-evitar-codigo-malicioso" onkeypress="return validarMaxLength(event, this, 250)" runat="server"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator_txtComentario" runat="server" ForeColor="Red" ErrorMessage="Por favor, deje un comentario mínimo  de 50 caracteres, máximo 250" ControlToValidate="txtComentario" ValidationExpression=".{50,250}"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtComentario" ControlToValidate="txtComentario" runat="server" ForeColor="Red" ErrorMessage="* Campo requerido"></asp:RequiredFieldValidator>
                                        </div>
                                        <asp:Button ID="btnGuardarComentario" CssClass="btn btn-primary" runat="server" Text="Enviar" />
                                    </form>
                                </div>

                                <hr>

                                <div class="" id="div5mentarios" runat="server" visible="false">
                                    <p class="lead">No hay comentarios para este producto.</p>
                                </div>
                                <!-- Posted Comments -->
                                <asp:Repeater ID="rptComentarios" runat="server" Visible="false">
                                    <ItemTemplate>
                                        <div class="media">
                                            <%--<a class="pull-left" href="#"><img class="media-object" src="http://placehold.it/64x64" alt=""></a>--%>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <asp:Literal ID="ltlAutor" runat="server" Text="<%# Container.DataItem.Autor %>"></asp:Literal>
                                                    <small class="fecha-comentario">
                                                        <asp:Literal ID="ltlFechaHora" runat="server" Text="<%# Container.DataItem.FechaHora.ToLongDateString() %>"></asp:Literal></small>
                                                </h4>
                                                <asp:Literal ID="ltlComentario" runat="server" Text="<%# Container.DataItem.Comentario %>"></asp:Literal>
                                            </div>
                                        </div>
                                        <hr />
                                    </ItemTemplate>
                                </asp:Repeater>
                                <!-- Comment -->



                            </section>

                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <%--    <!-- Author -->
    <p class="lead">
        by <a href="#">Start Bootstrap</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p><span class="glyphicon glyphicon-time"></span>Posted on August 24, 2013 at 9:00 PM</p>

    <hr>

    <div class="col-md-8">
        <!-- Preview Image -->
        <img class="img-responsive" src="http://placehold.it/900x300" alt="">
    </div>
    <div class="col-md-4">
        	<div class="product-title">Corsair GS600 600 Watt PSU</div>
					<div class="product-desc">The Corsair Gaming Series GS600 is the ideal price/performance choice for mid-spec gaming PC</div>
					<div class="product-rating"><i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star-o"></i> </div>
					<hr>
					<div class="product-price">$ 1234.00</div>
					<div class="product-stock">In Stock</div>
					<hr>
					<div class="btn-group cart">
						<button class="btn btn-success" type="button">
							Add to cart 
						</button>
					</div>
					<div class="btn-group wishlist">
						<button class="btn btn-danger" type="button">
							Add to wishlist 
						</button>
					</div>
    </div>
    <hr>

    <!-- Post Content -->
    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

    <hr>

    <!-- Blog Comments -->

    <!-- Comments Form -->
    <div class="well">
        <h4>Leave a Comment:</h4>
        <form role="form">
            <div class="form-group">
                <textarea class="form-control" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <hr>

    <!-- Posted Comments -->

    <!-- Comment -->
    <div class="media">
        <a class="pull-left" href="#">
            <img class="media-object" src="http://placehold.it/64x64" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Start Bootstrap
                            <small>August 25, 2014 at 9:30 PM</small>
            </h4>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
        </div>
    </div>

    <!-- Comment -->
    <div class="media">
        <a class="pull-left" href="#">
            <img class="media-object" src="http://placehold.it/64x64" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Start Bootstrap
                            <small>August 25, 2014 at 9:30 PM</small>
            </h4>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        <!-- Nested Comment -->
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">Nested Start Bootstrap
                                    <small>August 25, 2014 at 9:30 PM</small>
                    </h4>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
            </div>
            <!-- End Nested Comment -->
        </div>
    </div>--%>
</asp:Content>
