﻿Imports DAL
Imports BE

Public Class MPP_Encuesta
    Private oDatos As SQLHelper = SQLHelper.GetInstance

    Public Function Votar(ByVal idEnc As Integer, ByVal idRta As Integer) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@IdEncuesta", idEnc)
        hdatos.Add("@IdRespuesta", idRta)
        

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_SumarVotoEncuesta", hdatos)
        Return guardado
    End Function

    Public Function GetFichasOpinion() As List(Of BE_Encuesta)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetFichasOpinion", Nothing)
        Dim listado As New List(Of BE_Encuesta)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unaEnc As New BE_Encuesta
                    unaEnc.IdEncuesta = item("id_encuesta")
                    unaEnc.Pregunta = item("pregunta")
                    unaEnc.FechaDesde = item("fecha_desde")
                    unaEnc.FechaHasta = item("fecha_hasta")
                    Dim filtros As New Hashtable
                    filtros.Add("@IdEncuesta", unaEnc.IdEncuesta)
                    unaEnc.Respuestas = Me.ListarRespuesta(filtros)
                    listado.Add(unaEnc)
                Next
            End If
        End If
        Return listado
    End Function

    Public Function GetEncuestaActual() As BE_Encuesta

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetEncuestaActual", Nothing)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                Dim unaEnc As New BE_Encuesta
                unaEnc.IdEncuesta = item("id_encuesta")
                unaEnc.Pregunta = item("pregunta")
                unaEnc.FechaDesde = item("fecha_desde")
                unaEnc.FechaHasta = item("fecha_hasta")
                Dim filtros As New Hashtable
                filtros.Add("@IdEncuesta", unaEnc.IdEncuesta)
                unaEnc.Respuestas = Me.ListarRespuesta(filtros)
                Return unaEnc
            End If
        End If
        Return Nothing
    End Function

    Public Function GetPorId(ByVal id As Integer) As BE_Encuesta
        Dim hdatos As New Hashtable
        hdatos.Add("@IdEncuesta", id)

        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetEncuestaPorId", hdatos)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim item As DataRow = ds.Tables(0).Rows(0)
                Dim unaEnc As New BE_Encuesta
                unaEnc.IdEncuesta = item("id_encuesta")
                unaEnc.Pregunta = item("pregunta")
                unaEnc.FechaDesde = item("fecha_desde")
                unaEnc.FechaHasta = item("fecha_hasta")
                unaEnc.Tipo = item("tipo")
                Dim filtros As New Hashtable
                filtros.Add("@IdEncuesta", unaEnc.IdEncuesta)
                unaEnc.Respuestas = Me.ListarRespuesta(filtros)
                Return unaEnc
            End If
        End If
        Return Nothing
    End Function


    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Encuesta)
        Dim hdatos As New Hashtable
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetEncuestas", filtros)
        Dim listado As New List(Of BE_Encuesta)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unEnc As New BE_Encuesta
                    unEnc.IdEncuesta = item("id_encuesta")
                    unEnc.Pregunta = item("pregunta")
                    unEnc.FechaDesde = item("fecha_desde")
                    unEnc.FechaHasta = item("fecha_hasta")
                    unEnc.Tipo = item("tipo")
                    listado.Add(unEnc)
                Next
            End If
        End If
        Return listado
    End Function

    Public Function Crear(ByVal objEnc As BE_Encuesta) As BE.BE_Encuesta
        Dim hdatos As New Hashtable

        hdatos.Add("@Pregunta", objEnc.Pregunta)
        hdatos.Add("@FechaDesde", objEnc.FechaDesde)
        hdatos.Add("@FechaHasta", objEnc.FechaHasta)
        hdatos.Add("@Tipo", objEnc.Tipo)
        

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearEncuesta", hdatos)
        If InsertedId > 0 = True Then
            objEnc.IdEncuesta = InsertedId
        End If
        Return objEnc
    End Function

    Public Function Editar(ByVal objEnc As BE_Encuesta) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@IdEncuesta", objEnc.IdEncuesta)
        hdatos.Add("@Pregunta", objEnc.Pregunta)
        hdatos.Add("@FechaDesde", objEnc.FechaDesde)
        hdatos.Add("@FechaHasta", objEnc.FechaHasta)
        hdatos.Add("@Tipo", objEnc.Tipo)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_EditarEncuesta", hdatos)
        Return guardado
    End Function

    Public Function Eliminar(ByVal objEnc As BE_Encuesta) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@IdEncuesta", objEnc.IdEncuesta)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim eliminado As Integer = oDatos.Eliminar("pa_EliminarEncuesta", hdatos)
        Return eliminado
    End Function

    Public Function CrearRespuesta(ByVal objRtaEnc As BE_RespuestaEncuesta) As BE.BE_RespuestaEncuesta
        Dim hdatos As New Hashtable

        hdatos.Add("@Respuesta", objRtaEnc.Respuesta)
        hdatos.Add("@IdEncuesta", objRtaEnc.IdEncuesta)


        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearRespuestaEncuesta", hdatos)
        If InsertedId > 0 = True Then
            objRtaEnc.IdRespuesta = InsertedId
        End If
        Return objRtaEnc
    End Function

    Public Function EditarRespuesta(ByVal objRtaEnc As BE_RespuestaEncuesta) As Boolean
        Dim hdatos As New Hashtable

        hdatos.Add("@Respuesta", objRtaEnc.Respuesta)
        hdatos.Add("@IdRespuesta", objRtaEnc.IdRespuesta)


        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ModificarRespuestaEncuesta", hdatos)
        Return guardado
    End Function

    Public Function EliminarRespuesta(ByVal objRtaEnc As BE_RespuestaEncuesta) As Integer
        Dim hdatos As New Hashtable

        hdatos.Add("@IdRespuesta", objRtaEnc.IdRespuesta)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim eliminado As Integer = oDatos.Eliminar("pa_EliminarRespuestaEncuesta", hdatos)
        Return eliminado
    End Function

    Public Function ListarRespuesta(ByVal filtros As Hashtable) As List(Of BE_RespuestaEncuesta)
        Dim hdatos As New Hashtable
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_GetRespuestasEncuesta", filtros)
        Dim listado As New List(Of BE_RespuestaEncuesta)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each item As DataRow In ds.Tables(0).Rows
                    Dim unaRtaEnc As New BE_RespuestaEncuesta
                    unaRtaEnc.IdRespuesta = item("id_respuesta")
                    unaRtaEnc.IdEncuesta = item("id_encuesta")
                    unaRtaEnc.Respuesta = item("respuesta")
                    unaRtaEnc.IdEncuesta = item("id_encuesta")
                    unaRtaEnc.CantVotos = IIf(IsDBNull(item("cant_votos")), 0, item("cant_votos"))
                    listado.Add(unaRtaEnc)
                Next
            End If
        End If
        Return listado
    End Function

    Public Function ValidarEncuestaExistente(ByVal objEnc As BE_Encuesta) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdEncuesta", objEnc.IdEncuesta)
        hdatos.Add("@Pregunta", objEnc.Pregunta)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarEncuestaExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function

    Public Function ValidarRespuestaExistente(ByVal objRtaEnc As BE_RespuestaEncuesta) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@Idrespuesta", objRtaEnc.IdRespuesta)
        hdatos.Add("@IdEncuesta", objRtaEnc.IdEncuesta)
        hdatos.Add("@Respuesta", objRtaEnc.Respuesta)
        Dim ds As New DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        ds = oDatos.Leer("pa_ValidarRespuestaExistente", hdatos)
        If ds.Tables.Count > 0 Then
            Return ds.Tables(0).Rows.Count > 0
        End If
        Return False
    End Function
End Class
