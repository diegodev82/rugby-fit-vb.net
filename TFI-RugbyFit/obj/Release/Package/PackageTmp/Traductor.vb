﻿Imports System.Net

Public Class Traductor

    Public Shared Sub TraducirControlesIdioma(ByVal pagina As Page, ByVal idiomaAntiguo As String, ByVal idiomaNuevo As String)
        Try
            If idiomaAntiguo <> idiomaNuevo Then
                For Each dr As Label In GetControlList(Of Label)(pagina.Form.Controls)
                    dr.Text = RetornarTexto(DirectCast(dr, Label).Text.Trim(), idiomaAntiguo, idiomaNuevo)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Iterator Function GetControlList(Of T As Control)(controlCollection As ControlCollection) As IEnumerable(Of T)
        For Each control As Control In controlCollection
            If TypeOf control Is T Then
                Yield control
            End If

            If control.HasControls() Then
                For Each childControl As T In GetControlList(Of T)(control.Controls)
                    Yield childControl
                Next
            End If
        Next
    End Function

    Public Shared Function Traducir(ByVal texto As String, ByVal IdiomaAntiguo As String, ByVal IdiomaNuevo As String) As String
        Return RetornarTexto(texto.Trim(), IdiomaAntiguo, IdiomaNuevo)
    End Function

    Public Shared Function RetornarTexto(ByVal texto As String, ByVal IdiomaAntiguo As String, ByVal IdiomaNuevo As String) As String
        Dim url As String = [String].Format("http://www.google.com/translate_t?hl=en&ie=UTF8&text={0}&langpair={1}", texto, IdiomaAntiguo + "|" + IdiomaNuevo)
        Dim webClient As New WebClient()
        webClient.Encoding = System.Text.Encoding.UTF8
        Dim result As String = webClient.DownloadString(url)
        result = result.Substring(result.IndexOf("<span title=""") + "<span title=""".Length)
        result = result.Substring(result.IndexOf(">") + 1)
        result = result.Substring(0, result.IndexOf("</span>"))
        Return result.Trim()
    End Function
End Class

