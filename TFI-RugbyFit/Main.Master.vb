﻿Imports System.Web
Imports BLL
Imports BE

Public Class Main
    Inherits System.Web.UI.MasterPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarDataCarrito()
    End Sub

    Public Sub MostrarErrorSistema()
        phErrorSistema.Visible = True
        phContenido.Visible = False
    End Sub

    Public Shared Function isActive(pag As String) As String
        Dim urlActual = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pagActual = urlActual.Substring(urlActual.LastIndexOf("/") + 1)
        If pag = pagActual Then
            Return "active"
        End If
        Return ""
    End Function

    Public Shared Function isActive(pags As String()) As String
        Dim urlActual = HttpContext.Current.Request.Url.AbsoluteUri
        Dim pagActual = urlActual.Substring(urlActual.LastIndexOf("/") + 1)
        If pags.Contains(pagActual) Then
            Return "active"
        End If
        Return ""
    End Function


    Protected Sub CargarDataCarrito()
        Dim acl As New ACL
        If acl.GetIdCliente() > 0 Then
            Dim oBllVta As New BLL_Ventas
            Dim carrito As BE_Venta = oBllVta.GetCarrito(acl.GetIdCliente())
            If Not carrito Is Nothing Then

                If carrito.Items.Count > 0 Then
                    ltlCantItems.Text = carrito.Items.Count
                    rptItemsCarrito.DataSource = Nothing
                    rptItemsCarrito.DataBind()
                    rptItemsCarrito.DataMember = "BE_Detalle_Venta"
                    rptItemsCarrito.DataSource = carrito.Items
                    rptItemsCarrito.DataBind()
                    liEcommerce.Visible = True
                Else
                    liEcommerce.Visible = False
                End If

            Else
                liEcommerce.Visible = False
            End If
        Else

        End If

    End Sub


    Protected Sub btn_logout_Click(sender As Object, e As EventArgs) Handles btn_logout.Click
        Dim bllSeg As New BLL_Seguridad
        bllSeg.Logout()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("Default.aspx")
    End Sub
End Class