SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [pa_QuitarUsuarioFamilia]
	@IdUsuario integer,
	@IdFamilia integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DELETE from usuario_familia WHERE id_usuario = @IdUsuario and id_familia = @IdFamilia
END
GO

