﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Backend

    '''<summary>
    '''HeadBackendMasterPager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HeadBackendMasterPager As Global.System.Web.UI.HtmlControls.HtmlHead

    '''<summary>
    '''HeadBackend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HeadBackend As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ItemVentas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ItemVentas As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ItemAdmFzas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ItemAdmFzas As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ItemMkt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ItemMkt As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ItemSeguridad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ItemSeguridad As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''btn_logout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_logout As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''phErrorSistema control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phErrorSistema As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''phContenido control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phContenido As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''ContenidoBackendMasterPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ContenidoBackendMasterPage As Global.System.Web.UI.WebControls.ContentPlaceHolder
End Class
