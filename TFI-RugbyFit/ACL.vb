﻿Imports BLL
Imports BE
Public Class ACL
    Inherits System.Web.UI.Page
    Protected UsuarioLogueado As BE_Usuario
    Public Function EstaLogueado()
        UsuarioLogueado = CType(Session("UsuarioLogueado"), BE_Usuario)
        If UsuarioLogueado Is Nothing Then
            Return False
        End If
        Return True
    End Function

    Public Function ValidarEmpleado()
        If EstaLogueado() Then
            UsuarioLogueado = CType(Session("UsuarioLogueado"), BE_Usuario)
            If Not UsuarioLogueado.EsEmpleado = True Then
                Return False
            End If
            Return True
        End If
        Return False
    End Function

    Public Function ValidarPermiso(Optional ByVal codPermiso As String = Nothing)
        If Not codPermiso Is Nothing Then
            Return Utiles.TienePermiso(codPermiso)
        End If
        Return False
    End Function

    Public Function GetUsuarioLogueado() As BE_Usuario
        If EstaLogueado() Then
            Return UsuarioLogueado
        End If
        Return Nothing
    End Function

    Public Function GetIdUsuario() As Integer
        Dim dataUsuario As BE_Usuario = Me.GetUsuarioLogueado
        Return dataUsuario.IdUsuario
    End Function

    Public Function GetIdCliente() As Integer
        Dim dataUsuario As BE_Usuario = Me.GetUsuarioLogueado
        If Not dataUsuario Is Nothing Then
            If Not dataUsuario.Cliente Is Nothing Then
                Return dataUsuario.Cliente.IdCliente
            End If
        End If
        Return 0
    End Function

    Public Sub ValidarAcceso(Optional ByVal codPermiso As String = Nothing)
        If EstaLogueado() = False Then
            Response.Redirect("Login.aspx")
        ElseIf GetIdCliente() > 0 Then
            Session.Clear()
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If
        If Not codPermiso Is Nothing Then
            If Me.ValidarPermiso(codPermiso) = False Then
                Response.Redirect("PermisoDenegado.aspx")
            End If
        End If
    End Sub
    Public Sub ValidarAccesoCliente()
        If EstaLogueado() = False Then
            Response.Redirect("Login.aspx")
        End If
        Dim id_cliente As Integer = GetIdCliente()
        If Not IsNumeric(id_cliente) Or id_cliente <= 0 Then
            Session.Clear()
            Session.Abandon() ' es un empleado que no tiene cliente. Le borro la sesion y que se loguee de nuevo
            Response.Redirect("Login.aspx?e=sin-cliente")
        End If
    End Sub
End Class
