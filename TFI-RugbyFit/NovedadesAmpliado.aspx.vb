﻿Imports BLL
Imports BE
Public Class NovedadesAmpliado
    Inherits System.Web.UI.Page
    Private oBll As New BLL_Novedad

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.CargarNovedad()
    End Sub


    Public Sub CargarNovedad()
        If Not Request.QueryString("id") Is Nothing Then
            Dim id As Integer = Request.QueryString("id")
            Dim nove As BE_Novedad = oBll.GetPorId(id)
            ltlTitulo.Text = nove.Titulo
            ltlCuerpo.Text = nove.Cuerpo
            ltlFecha.Text = nove.FechaHora.ToLongDateString
            If nove.Copete.Length > 1 Then
                phCopete.Visible = True
                ltlCopete.Text=nove.Copete
            End If
            If nove.LinkImagen.Length > 1 Then

                img.ImageUrl = nove.LinkImagen
                divImg.Visible = True
            End If
        End If
    End Sub
End Class