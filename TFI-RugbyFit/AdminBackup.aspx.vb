﻿Imports BLL
Imports BE
Public Class AdminBackup
    Inherits ACL
    Public Const RUTA = "/assets/bk/"
    Const COD_PERMISO = "MOD_SEGURIDAD"
    Private oBll As New BLL_Backup
    Private oBllBit As New BLL_Bitacora
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MyBase.ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            LlenarDgv()
        End If
    End Sub
    Protected Sub LlenarDgv(Optional ByVal filtros As Hashtable = Nothing)
        Dim registros As List(Of BE_Backup) = oBll.Listar(filtros)
        txtCantRegistros.Text = registros.Count
        DgvBackup.AutoGenerateColumns = False
        DgvBackup.DataSource = registros
        DgvBackup.DataBind()
    End Sub

    Protected Sub DgvBackup_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles DgvBackup.RowEditing
        Dim row As GridViewRow = DgvBackup.Rows(e.NewEditIndex)
        Dim IdBackup As Integer = CType(row.Cells(0).Text, Integer)
        Dim bk As BE_Backup = oBll.GetPorId(IdBackup)
        Dim restaurado As Boolean = oBll.RestaurarBackup(Server.MapPath(bk.Ruta))
        If restaurado = True Then
            Dim evt As New BE_Evento
            evt.IdEvento = BE_Evento.RESTAURAR_BACKUP
            oBllBit.RegistrarEvento(evt, "fecha: " + Date.Now.ToString, Me.GetIdUsuario)
            Response.Redirect("AdminBackup.aspx?restaurado=ok")
        Else
            Response.Redirect("AdminBackup.aspx?restaurado=error-db")
        End If

    End Sub

    Protected Sub DgvBackup_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles DgvBackup.RowUpdating
        Dim a As Integer = 0

    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
    End Sub

    Protected Sub DgvBackup_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles DgvBackup.PageIndexChanging
        DgvBackup.PageIndex = e.NewPageIndex
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim a As String = ""
    End Sub

    Protected Sub DgvBackup_PageIndexChanged(sender As Object, e As EventArgs) Handles DgvBackup.PageIndexChanged
        Dim filtros As Hashtable = GetFiltros()
        LlenarDgv(filtros)
        Dim b As String = ""
    End Sub

    Protected Function GetFiltros() As Hashtable
        Dim filtros As New Hashtable
        

        If IsNumeric(filtroCodigo.Text) Then
            Dim idBackup As Integer = CType(filtroCodigo.Text, Integer)
            If idBackup > 0 Then
                filtros.Add("@IdBackup", idBackup)
            End If
        End If

        If IsDate(filtroDesde.Text) Then
            Dim fDesde As DateTime = CType(filtroDesde.Text, DateTime)
            filtros.Add("@FechaDesde", fDesde)
        End If

        If IsDate(filtroHasta.Text) Then
            Dim fHasta As DateTime = CType(filtroHasta.Text, DateTime)
            filtros.Add("@FechaHasta", fHasta)
        End If

        Return filtros
    End Function

    Protected Sub DgvBackup_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles DgvBackup.RowDeleting
        Dim row As GridViewRow = DgvBackup.Rows(e.RowIndex)
        Dim idBK As String = row.Cells(0).Text
        If IsNumeric(idBK) Then
            Dim objBK As New BE_Backup With {.IdBackup = idBK}
            Dim eliminado As Integer = oBll.Eliminar(objBK)
            If eliminado = 1 Then
                Response.Redirect("AdminBackup.aspx?eliminado=ok")
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim hoy As DateTime = Date.Now
        Dim rutaRel As String = RUTA + "/" + hoy.ToString("yyyy-MM-dd-HH-mm-ss") + ".bak"
        Dim rutaAbs As String = Server.MapPath(rutaRel)
        Try
            Dim generado As Boolean = oBll.RealizarBackup(rutaAbs)
            If generado = True Then
                oBll.RegistrarBackup(rutaRel)
                Dim evt As New BE_Evento
                evt.IdEvento = BE_Evento.GENERAR_BACKUP
                oBllBit.RegistrarEvento(evt, "fecha: " + Date.Now.ToString, Me.GetIdUsuario)
                Response.Redirect("AdminBackup.aspx?generado=ok")
            Else
                Response.Redirect("AdminBackup.aspx?generado=error-db")
            End If
        Catch ex As Exception
            ' Response.Redirect("AdminBackup.aspx?generado=error-trx")
            Dim a As Integer = 0
        End Try
        
    End Sub
End Class