SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE pa_GetPermisosHeredadosPorUsuario
	@IdUsuario nchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT p.id_permiso, p.descripcion, up.tipo, p.codigo FROM usuario_permiso AS up 
	  INNER JOIN permiso AS p ON p.id_permiso = up.id_permiso 
	  WHERE up.id_usuario = @IdUsuario
END
GO