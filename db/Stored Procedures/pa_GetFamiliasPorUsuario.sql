USE [RugbyFit]
GO
/****** Object:  StoredProcedure [dbo].[pa_GetFamiliasPorUsuario]    Script Date: 29/08/2016 16:42:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[pa_GetFamiliasPorUsuario]
	@IdUsuario integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT f.id_familia, f.nombre FROM usuario_familia AS uf  
	INNER JOIN familia AS f ON f.id_familia = uf.id_familia 
	WHERE uf.id_usuario = @IdUsuario
END
