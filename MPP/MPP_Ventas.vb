Imports BE
Imports DAL


Public Class MPP_Ventas

    Public Function GetPorId(ByVal idVta As Integer) As BE_Venta
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Venta)
        Dim filtros As New Hashtable
        filtros.Add("@IdVenta", idVta)
        DS = oDatos.Leer("pa_GetVentaPorId", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = DS.Tables(0).Rows(0)
            Dim unaVta As New BE_Venta
            unaVta.IdVenta = IIf(IsDBNull(dr("id_venta")), 0, dr("id_venta"))
            unaVta.IdFactura = IIf(IsDBNull(dr("id_factura")), 0, dr("id_factura"))
            unaVta.IdCliente = IIf(IsDBNull(dr("id_cliente")), 0, dr("id_cliente"))
            unaVta.NombreCliente = IIf(IsDBNull(dr("apellido_cliente")), "", dr("apellido_cliente")) + ", " + IIf(IsDBNull(dr("apellido_cliente")), "", dr("nombre_cliente"))
            unaVta.Fecha = IIf(IsDBNull(dr("fecha")), Date.Now, dr("fecha"))
            unaVta.Estado = IIf(IsDBNull(dr("estado")), "", dr("estado"))
            unaVta.MontoTotal = IIf(IsDBNull(dr("monto_total")), 0, dr("monto_total"))

            Return unaVta
        End If
        Return New BE_Venta
    End Function

    Public Function ActualizarEstado(ByVal IdVta As Integer, ByVal estado As String) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVenta", IdVta)
        hdatos.Add("@Estado", estado)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_ActualizarEstadoVenta", hdatos)
        Return guardado
    End Function

    Public Function GetVentasPorCliente(ByVal idCliente As Integer) As List(Of BE_Venta)
        'pa_GetVentasPorCliente
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", idCliente)

        Dim listado As New List(Of BE_Venta)
        ds = oDatos.Leer("pa_GetVentasPorCliente", hdatos)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim vta As New BE_Venta
                vta.IdVenta = IIf(IsDBNull(dr("id_venta")), 0, dr("id_venta"))
                vta.Fecha = IIf(IsDBNull(dr("fecha")), Date.Now, dr("fecha"))
                vta.MontoTotal = IIf(IsDBNull(dr("monto_total")), 0, dr("monto_total"))
                vta.Estado = IIf(IsDBNull(dr("estado")), "", dr("estado"))
                vta.IdFactura = IIf(IsDBNull(dr("id_factura")), 0, dr("id_factura"))
                listado.Add(vta)
            Next
            Return listado
        End If
        Return Nothing
    End Function
    Public Function GetTC() As List(Of BE_TarjetaCredito)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet

        Dim listado As New List(Of BE_TarjetaCredito)
        ds = oDatos.Leer("pa_GetTC", Nothing)
        Dim vta As BE_Venta = Nothing
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim tc As New BE_TarjetaCredito
                tc.IdTC = IIf(IsDBNull(dr("id_tc")), 0, dr("id_tc"))
                tc.Numero = IIf(IsDBNull(dr("numero")), 0, dr("numero"))
                tc.Codigo = IIf(IsDBNull(dr("codigo")), 0, dr("codigo"))
                tc.MesAnioExpiracion = IIf(IsDBNull(dr("mes_anio_expiracion")), "", dr("mes_anio_expiracion"))
                tc.Tipo = New BE_TipoTC With {.IdTipoTC = dr("id_tipo_tc"), .Nombre = dr("tipotc_nombre"), .CantDigitos = dr("tipotc_cant_digitos")}
                listado.Add(tc)
            Next
            Return listado
        End If
        Return Nothing
    End Function

    Public Function ValidarTCExistente(ByVal numero As Int64) As BE_TarjetaCredito
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@Numero", numero)


        ds = oDatos.Leer("pa_ValidarTCExistente", hdatos)
        Dim vta As BE_Venta = Nothing
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Dim tc As New BE_TarjetaCredito
            tc.IdTC = dr("id_tc")
            tc.Numero = dr("numero")
            tc.Codigo = dr("codigo")
            tc.MesAnioExpiracion = dr("mes_anio_expiracion")
            tc.Tipo = New BE_TipoTC With {.IdTipoTC = dr("id_tipo_tc"), .Nombre = dr("tipotc_nombre"), .CantDigitos = dr("tipotc_cant_digitos")}
            Return tc
        End If
        Return Nothing
    End Function

    ''' 
    ''' <param name="IdVenta"></param>
    ''' <param name="objProducto"></param>
    Public Function AgregarProducto(ByVal IdVenta As Integer, ByVal dv As BE_Detalle_Venta) As BE_Detalle_Venta
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVenta", IdVenta)
        hdatos.Add("@IdProducto", dv.Producto.IdProducto)
        hdatos.Add("@Cantidad", dv.Cantidad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_AgregarVentaDetalle", hdatos)
        If InsertedId > 0 = True Then
            dv.IdDetalleVenta = InsertedId
        End If
        Return dv
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function AnularVenta(ByVal objVenta As BE_Venta) As Integer
        AnularVenta = 0
    End Function

    ''' 
    ''' <param name="cliente"></param>
    Public Function CrearCarrito(ByVal cliente As BE_Cliente) As BE.BE_Venta
        Dim vta As New BE_Venta
        vta.IdVendedor = 0
        vta.Origen = "W"
        vta.IdCliente = cliente.IdCliente
        Dim hdatos As New Hashtable

        hdatos.Add("@IdCliente", vta.IdCliente)

        hdatos.Add("@Origen", vta.Origen)
        hdatos.Add("@IdVendedor", vta.IdVendedor)

        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim InsertedId As Integer = oDatos.Insertar("pa_CrearVenta", hdatos)
        If InsertedId > 0 = True Then
            vta.IdVenta = InsertedId
        End If
        Return vta
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function EditarItem(ByVal objDetVta As BE_Detalle_Venta) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVentaDetalle", objDetVta.IdDetalleVenta)
        hdatos.Add("@Cantidad", objDetVta.Cantidad)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_EditarDetalleVenta", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function EliminarItem(ByVal objDetVta As BE_Detalle_Venta) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVentaDetalle", objDetVta.IdDetalleVenta)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim eliminado As Boolean = oDatos.Eliminar("pa_EliminarDetalleVenta", hdatos)
        Return eliminado >= 0
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function EliminarVenta(ByVal objVenta As BE_Venta) As Integer
        EliminarVenta = 0
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function FinalizarVenta(ByVal objVenta As BE_Venta) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVenta", objVenta.IdVenta)
        Dim guardado As Boolean = oDatos.Escribir("pa_CerrarVenta", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function GetDetalleVenta(ByVal objVenta As BE_Venta) As List(Of BE.BE_Detalle_Venta)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVenta", objVenta.IdVenta)


        ds = oDatos.Leer("pa_GetVentaDetalles", hdatos)
        Dim listado As New List(Of BE_Detalle_Venta)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim vd As New BE_Detalle_Venta
                vd.IdVenta = dr("id_venta")
                vd.IdDetalleVenta = dr("id_venta_detalle")
                vd.Monto = dr("precio_unitario")
                vd.Producto = New BE_Producto With {
                    .IdProducto = dr("id_producto"),
                    .Nombre = dr("prd_nombre"),
                    .DescripcionCorta = dr("prd_desc"),
                    .LinkImagen = dr("prd_link_imagen")
                }
                vd.Cantidad = dr("cantidad")
                listado.Add(vd)
            Next
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objVenta"></param>
    Public Function Guardar(ByVal objVenta As BE_Venta) As BE.BE_Venta
        Guardar = Nothing
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function IncrementarCantidad(ByVal objDetVta As BE_Detalle_Venta) As Boolean
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVentaDetalle", objDetVta.IdDetalleVenta)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance
        Dim guardado As Boolean = oDatos.Escribir("pa_IncrementarCantProductoAgregado", hdatos)
        Return guardado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Venta)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Venta)
        DS = oDatos.Leer("pa_GetVentas", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim unaVta As New BE_Venta
                unaVta.IdVenta = dr("id_venta")
                unaVta.IdFactura = dr("id_factura")
                unaVta.IdCliente = IIf(IsDBNull(dr("id_cliente")), "", dr("id_cliente"))
                unaVta.NombreCliente = dr("apellido_cliente") + ", " + dr("nombre_cliente")
                unaVta.Fecha = dr("fecha")
                unaVta.Estado = dr("estado")
                unaVta.MontoTotal = dr("monto_total")
                listado.Add(unaVta)
            Next
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objDetVta"></param>
    Public Function ProductoAgregado(ByVal objDetVta As BE_Detalle_Venta) As Integer

        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdVenta", objDetVta.IdVenta)
        hdatos.Add("@IdProducto", objDetVta.Producto.IdProducto)

        ds = oDatos.Leer("pa_ValidarProductoAgregado", hdatos)
        Dim vta As BE_Venta = Nothing
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Return CType(dr("id_venta_detalle"), Integer)
        End If
        Return 0
    End Function

    ''' 
    ''' <param name="objProducto"></param>
    Public Function ValidarStock(ByVal objProducto As BE_Producto) As Boolean
        ValidarStock = False
    End Function

    ''' 
    ''' <param name="idCliente"></param>
    Public Function VerificarCarritoAbierto(ByVal idCliente As Integer) As BE.BE_Venta
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdCliente", idCliente)

        ds = oDatos.Leer("pa_VerificarVentaWebAbierta", hdatos)
        Dim vta As BE_Venta = Nothing
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                vta = New BE_Venta
                vta.IdVenta = dr("id_venta")
                vta.Fecha = dr("fecha")
                vta.Origen = dr("origen")
                vta.IdCliente = dr("id_cliente")
                vta.IdVendedor = dr("id_vendedor")
                Dim detalles As List(Of BE_Detalle_Venta) = Me.GetDetalleVenta(vta)
                vta.Items = detalles
                Return vta
            Next
        End If
        Return Nothing
    End Function


End Class ' MPP_Ventas
