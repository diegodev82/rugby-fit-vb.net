﻿Imports BE
Imports BLL
Public Class Comparacion
    Inherits System.Web.UI.Page
    Private productos As List(Of BE_Producto)
    Private productosComparacion As New List(Of BE_Producto)
    Private oBllPrd As New BLL_Productos
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.productos = oBllPrd.Listar(Nothing)
        End If
        If Not Request.QueryString("ids") Is Nothing Then
            Dim ids As String = Request.QueryString("ids")
            If Not String.IsNullOrEmpty(ids) Then
                Dim vector() As String = Split(Request.QueryString("ids"), ",")
                If vector.Length < 2 Or vector.Length > 3 Then
                    errorCant.Visible = True
                    Return
                End If
                For Each id As String In vector
                    If IsNumeric(id) Then
                        Me.AgregarProducto(CType(id, Integer))
                    End If
                Next
                errorCant.Visible = False
                tblComparacion.Visible = True
                rptComparacion.DataMember = "BE_Producto"
                rptComparacion.DataSource = Me.productosComparacion
                rptComparacion.DataBind()
            End If
        End If
    End Sub


    Protected Sub AgregarProducto(id As Integer)
        For Each prd As BE_Producto In productos
            If prd.IdProducto = CType(id, Integer) Then
                Me.productosComparacion.Add(prd)
                Exit For
            End If
        Next
    End Sub
End Class