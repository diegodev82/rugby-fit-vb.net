﻿Imports BLL
Imports BE

Public Class AdminUsuariosForm
    Inherits ACL
    Private oMiBll As New BLL_Usuarios
    Private oBllSeg As New BLL_Seguridad
    Private dataUsuario As BE_Usuario
    Const COD_PERMISO = "MOD_SEGURIDAD"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.CargarData()
            Me._CargarDropdownFamilia()
            Me._CargarDropdownPermisos()
        End If
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click


        Dim oBEUsuario As New BE_Usuario

        oBEUsuario.IdUsuario = IIf(iptCodigo.Value <> "", iptCodigo.Value, 0)
        oBEUsuario.Nombre = iptNombre.Text
        oBEUsuario.Apellido = iptApellido.Text
        oBEUsuario.DNI = IIf(IsNumeric(iptDNI.Text) = True, iptDNI.Text, 0)
        oBEUsuario.Email = iptEmail.Text
        oBEUsuario.Telefono = iptTelefono.Text
        oBEUsuario.Nickname = iptNickname.Text
        oBEUsuario.Contrasenia = oBllSeg.Encriptar(iptContrasenia.Text)
        oBEUsuario.Activo = IIf(ddActivo.SelectedValue = "1", True, False)
        oBEUsuario.CII = IIf(ddBloqueado.SelectedValue = "0", 0, iptCII.Text)
        oBEUsuario.EsEmpleado = True 'como se crea del backend es empleado
        Dim usuarioGuardado As BE_Usuario = Nothing
        If oBEUsuario.IdUsuario > 0 Then
            Dim guardado As Boolean = oMiBll.Editar(oBEUsuario)
            If guardado = True Then
                usuarioGuardado = oBEUsuario
            End If
        Else
            usuarioGuardado = oMiBll.Crear(oBEUsuario)
        End If
        If usuarioGuardado Is Nothing Then
            MensajeRespuesta.Text = "Ya existe una persona registrado con ese usuario. Por favor, ingrese otro nombre de usuario"
            PanelMensajeRespuesta.Visible = True
            Return
        End If
        If usuarioGuardado.IdUsuario > 0 Then
            Response.Redirect("AdminUsuariosForm.aspx?guardado=ok&id=" + usuarioGuardado.IdUsuario.ToString)
        Else
            MensajeRespuesta.Text = "Se produjo un error desconocido y la registación no pudo realizarse. Inténtelo nuevamente, por favor"
            PanelMensajeRespuesta.Visible = True
        End If
    End Sub

    Protected Sub CargarData()
        If Request.QueryString("id") IsNot Nothing Then
            If Not IsNumeric(Request.QueryString("id")) Then
                Return
            End If
            Dim idUsuario As Integer = CType(Request.QueryString("id"), Integer)
            dataUsuario = oMiBll.GetPorId(idUsuario)

            If dataUsuario IsNot Nothing Then
                iptNickname.Text = dataUsuario.Nickname
                iptCodigo.Value = dataUsuario.IdUsuario
                iptNombre.Text = dataUsuario.Nombre
                iptApellido.Text = dataUsuario.Apellido
                iptEmail.Text = dataUsuario.Email
                iptTelefono.Text = dataUsuario.Telefono
                iptCII.Text = dataUsuario.CII
                iptDNI.Text = dataUsuario.DNI
                ddActivo.SelectedValue = IIf(dataUsuario.Activo = True, 1, 0)
                ddBloqueado.SelectedValue = IIf(dataUsuario.CII > BLL_Seguridad.MAX_CII, 1, 0)
                btnEliminar.Visible = True
                iptContrasenia.Visible = False
                txtPwdGuardada.Visible = True
                RequiredFieldValidator_iptContrasenia.Visible = False
                'Cargar Permisos Guardados

                Me._CargarDgvFamilias(dataUsuario.Familias)
                Me._CargarDgvPermisos(dataUsuario.Permisos)
                'Cargar Familias Guardadas
                phAsignarFamiliasPermisos.Visible = True
                ' lnkCambiarContrasenia.Visible = True
            End If
        End If
    End Sub

    Protected Sub _CargarDgvFamilias(ByVal familias As List(Of BE_Familia))
        If Not familias Is Nothing Then
            gdvFamilias.DataSource = Nothing
            gdvFamilias.DataSource = familias
            gdvFamilias.DataBind()
        End If
    End Sub

    Protected Sub _CargarDgvPermisos(ByVal permisos As List(Of BE_Permiso))
        If Not permisos Is Nothing Then
            gdvPermisos.DataSource = Nothing
            gdvPermisos.DataSource = permisos
            gdvPermisos.DataBind()
        End If
    End Sub

    Protected Sub _CargarDropdownFamilia()
        Dim bllFamilia As New BLL_Familias

        ddFamilias.DataMember = "BE_Familia"
        ddFamilias.DataValueField = "IdFamilia"
        ddFamilias.DataTextField = "nombre"
        ddFamilias.DataSource = bllFamilia.Listar
        ddFamilias.DataBind()
    End Sub

    Protected Sub _CargarDropdownPermisos()
        Dim bllPermisos As New BLL_Permisos
        ddPermisos.DataMember = "BE_Permiso"
        ddPermisos.DataValueField = "IdPermiso"
        ddPermisos.DataTextField = "descripcion"
        ddPermisos.DataSource = bllPermisos.Listar()
        ddPermisos.DataBind()
    End Sub

    Protected Sub GdvPermisos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvPermisos.RowDataBound
        Dim cssClass As String = ""
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim tipo As Integer = e.Row.Cells(2).Text
            If tipo = 1 Then
                e.Row.Cells(2).Text = "<span class='glyphicon glyphicon-ok text-success'></span>"
            Else
                e.Row.Cells(2).Text = "<span class='glyphicon glyphicon-remove text-danger'></span>"
            End If
            ' e.Row.CssClass = impac.ToLower

        End If
    End Sub

    Protected Sub btnAgregarPermiso_Click(sender As Object, e As EventArgs)
        Dim idPermiso As Integer = ddPermisos.SelectedValue
        Dim tipo As Integer = ddTipoPermiso.SelectedValue
        Dim idUsuario As Integer = iptCodigo.Value
        If idUsuario > 0 And idPermiso > 0 And tipo >= 0 Then
            Dim permisoAgregado As New BE_Permiso With {.IdPermiso = idPermiso, .Tipo = tipo}
            Dim agregado = oMiBll.AgregarPermiso(idUsuario, permisoAgregado)
            If agregado = True Then
                Me.CargarData()
                Me._CargarDgvPermisos(Me.dataUsuario.Permisos)
            End If
        End If
    End Sub

    Protected Sub btnAgregarFamilia_Click(sender As Object, e As EventArgs)
        Dim idFamilia As Integer = ddFamilias.SelectedValue
        Dim idUsuario As Integer = iptCodigo.Value
        If idUsuario > 0 And idFamilia > 0 Then

            Dim agregado = oMiBll.AgregarFamilia(idFamilia, idUsuario)
            If agregado = True Then
                Me.CargarData()
                Me._CargarDgvFamilias(Me.dataUsuario.Familias)
            End If
        End If
    End Sub

    Protected Sub gdvFamilias_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gdvFamilias.RowDeleting
        Dim row As GridViewRow = gdvFamilias.Rows(e.RowIndex)
        Dim idFam As String = row.Cells(0).Text
        Dim idUsuario As Integer = iptCodigo.Value
        If IsNumeric(idFam) Then

            Dim eliminado As Boolean = oMiBll.QuitarFamilia(idUsuario, idFam)
            If eliminado = True Then
                Me.CargarData()
                Me._CargarDgvFamilias(Me.dataUsuario.Familias)
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub gdvPermisos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gdvPermisos.RowDeleting
        Dim row As GridViewRow = gdvPermisos.Rows(e.RowIndex)
        Dim idPerm As String = row.Cells(0).Text
        Dim idUsuario As Integer = iptCodigo.Value
        If IsNumeric(idPerm) Then

            Dim eliminado As Boolean = oMiBll.QuitarPermiso(idUsuario, idPerm)
            If eliminado = True Then
                Me.CargarData()
                Me._CargarDgvPermisos(Me.dataUsuario.Permisos)
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Try
            If IsNumeric(iptCodigo.Value) Then
                If iptCodigo.Value = MyBase.GetIdUsuario Then
                    RtaErrorEliminar.Visible = True
                    msjErrorEsMiUsuario.Visible = True
                    Return
                End If
                Dim idUsuario As Integer = CType(iptCodigo.Value, Integer)
                Dim objUsu As New BE_Usuario With {.IdUsuario = idUsuario}
                Dim eliminado As Integer = oMiBll.Eliminar(objUsu)
                If eliminado = 1 Then
                    Response.Redirect("AdminUsuariosList.aspx?eliminado=ok", False)
                    Return
                ElseIf eliminado = -1 Then
                    RtaErrorEliminar.Visible = True
                    msjErrorFK.Visible = True
                ElseIf eliminado = -2 Then
                    RtaErrorEliminar.Visible = True
                    msjErrorEsAdmin.Visible = True
                Else
                    RtaErrorEliminar.Visible = True
                    msjErrorDB.Visible = True
                End If
            End If
        Catch ex As Exception
            TryCast(Me.Master, Backend).MostrarErrorSistema()
        End Try
        
    End Sub
End Class