Imports BE
Imports DAL


Public Class MPP_Proveedores

    ''' 
    ''' <param name="objProv"></param>
    Public Function Crear(ByVal objProv As BE_Proveedor) As BE.BE_Proveedor
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objProv"></param>
    Public Function Editar(ByVal objProv As BE_Proveedor) As Boolean
        Editar = False
    End Function

    ''' 
    ''' <param name="objProv"></param>
    Public Function Eliminar(ByVal objProv As BE_Proveedor) As Boolean
        Eliminar = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Proveedor)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim listado As New List(Of BE_Proveedor)
        DS = oDatos.Leer("pa_GetProveedores", filtros)

        If DS.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In DS.Tables(0).Rows
                Dim unProve As New BE_Proveedor With {
                    .IdProveedor = dr("id_proveedor"),
                    .Nombre = dr("nombre"),
                    .Email = IIf(IsDBNull(dr("email")), "", dr("email")),
                    .Domiclio = IIf(IsDBNull(dr("domicilio")), "", dr("domicilio")),
                    .Ciudad = New BE_Ciudad With {.IdCiudad = IIf(IsDBNull(dr("id_ciudad")), 0, dr("id_ciudad"))},
                    .Telefono = IIf(IsDBNull(dr("telefono")), "", dr("telefono"))
                }
                listado.Add(unProve)
            Next
        End If
        Return listado
    End Function

    ''' 
    ''' <param name="objProv"></param>
    Public Function ValidarExistente(ByVal objProv As BE_Proveedor) As Boolean
        ValidarExistente = False
    End Function


End Class ' MPP_Proveedores
