﻿$(document).ready(function () {
    $('body').on('click.EliminarComparacion', '.js-eliminar-comparacion', function (e) {
        e.preventDefault();
        var parent = $(this).parents('.columna').first();
        parent.remove();
        if ($('.columna').length <= 1) {
            location.href = 'Productos.aspx';
        }
    });
});
