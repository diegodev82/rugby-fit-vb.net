﻿Imports BLL
Imports BE
Public Class ConfirmarRegistracion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("hash") Is Nothing And Not Request.QueryString("email") Is Nothing Then

            Dim oBll As New BLL_Clientes
            Dim hash As String = Request.QueryString("hash")
            Dim email As String = Request.QueryString("email")
            Dim activado As Boolean = oBll.ConfirmarRegistracion(hash, email)
            If activado = True Then
                Response.Redirect("ConfirmarRegistracion.aspx?rta=ok")
            Else
                Response.Redirect("ConfirmarRegistracion.aspx?rta=error-validacion")

            End If
        End If
    End Sub

End Class