
Imports BE
Imports DAL

Namespace MPP
	Public Class MPP_Solicitud_Compra

        ''' 
        ''' <param name="objDetSol"></param>
		Public Function AgregarItem(ByVal objDetSol As BE_Detalle_Solicitud_Compra) As BE.BE_Detalle_Solicitud_Compra
			AgregarItem = Nothing
		End Function

		''' 
		''' <param name="solicitudes"></param>
        Public Function Autorizar(ByVal solicitudes As List(Of BE_Solicitud_Compra)) As List(Of BE_Solicitud_Compra)
            Autorizar = Nothing
        End Function

		''' 
		''' <param name="objSolCpra"></param>
		Public Function Crear(ByVal objSolCpra As BE_Solicitud_Compra) As BE.BE_Solicitud_Compra
			Crear = Nothing
		End Function

		''' 
		''' <param name="objSolCpra"></param>
		Public Function Editar(ByVal objSolCpra As BE_Solicitud_Compra) As BE.BE_Solicitud_Compra
			Editar = Nothing
		End Function

		''' 
		''' <param name="objDetSol"></param>
		Public Function EditarItem(ByVal objDetSol As BE_Detalle_Solicitud_Compra) As boolean
			EditarItem = False
		End Function

		''' 
		''' <param name="objSolCpra"></param>
		Public Function Eliminar(ByVal objSolCpra As BE_Solicitud_Compra) As boolean
			Eliminar = False
		End Function

		''' 
		''' <param name="objDetSol"></param>
		Public Function EliminarItem(ByVal objDetSol As BE_Detalle_Solicitud_Compra) As boolean
			EliminarItem = False
		End Function

		''' 
		''' <param name="objSolCpra"></param>
		Public Function Enviar(ByVal objSolCpra As BE_Solicitud_Compra) As boolean
			Enviar = False
		End Function

		''' 
		''' <param name="objSolCpra"></param>
		Public Function GetSolicitudDetallad(ByVal objSolCpra As BE_Solicitud_Compra) As BE.BE_Solicitud_Compra
			GetSolicitudDetallad = Nothing
		End Function

		''' 
		''' <param name="filtros"></param>
        Public Function Listar(ByVal filtros As Hashtable) As List(Of BE_Solicitud_Compra)
            Listar = Nothing
        End Function

		''' 
		''' <param name="objSolCpra"></param>
		Public Function Rechazar(ByVal objSolCpra As BE_Solicitud_Compra) As boolean
			Rechazar = False
		End Function

		''' 
		''' <param name="objSolDet"></param>
        Public Function ValidarProductoAgregado(ByVal objSolDet As BE_Detalle_Solicitud_Compra) As Boolean
            ValidarProductoAgregado = False
        End Function


	End Class ' MPP_Solicitud_Compra

End Namespace ' MPP