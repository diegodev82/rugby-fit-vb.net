﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AdminChat

    '''<summary>
    '''rptChats control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptChats As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''mensajeInicial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mensajeInicial As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rptChatAmpliado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptChatAmpliado As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''phRta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phRta As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''iptIdChat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iptIdChat As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''txtRta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RequiredFieldValidator_iptMsje control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator_iptMsje As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''btnEnviarMensaje control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEnviarMensaje As Global.System.Web.UI.WebControls.Button
End Class
