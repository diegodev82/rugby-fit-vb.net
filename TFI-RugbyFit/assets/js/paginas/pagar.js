﻿$(document).ready(function (e) {
    // tester();
    $(".js-usar").on("click", function (e) {
        e.preventDefault();
        var tr = $(this).parent().parent("tr");
        var numero = tr.find(".js-nro-usar").html();
        var fecha = tr.find(".js-fecha-usar").html();
        var codigo = tr.find(".js-codigo-usar").html();
        $(".js-numero-tc").val(numero);
        $(".js-codigo-seguridad").val(codigo);
        $(".js-fecha-expiracion").val(fecha);
    });

    $(".js-btn-pagar").on("click", function (e) {
        if (ValidarPago() == false) {
            e.preventDefault();
            return false;
        }
       // e.preventDefault();
       // alert("TODO OK");
    });
});


function ValidarPago() {
    var $chkUsarNC = $(".js-chkUsarNC input[type=checkbox]");
    var $chkPagarConTC = $(".js-chkPagarConTC input[type=checkbox]");
    var usaSaldo = $chkUsarNC.prop("checked") == true;
    var usaTC = $chkPagarConTC.prop("checked") == true;
    var saldoFavor = parseFloat($("#saldoFavor").html());
    var montoVenta = parseFloat($("#montoVta").html());
    

    if (!usaSaldo && !usaTC) {
        alert("Seleccone una forma de pago");
        return false;
    }
    if (usaSaldo && !usaTC) {
        if (saldoFavor < montoVenta) {
            alcanza = false;
            alert("Disculpe, el monto a favor no alcanza para pagar el total de la venta");
            return false;
        } else {
            $chkPagarConTC.prop("checked", false);
        }
    }

    if (usaTC == true) {       
        if (!validarDatosTC()) {
            return false;
        }
    }

    var r = confirm("¿Confirma realizar el pago?");
    return r;

}

function validarDatosTC() {
    var val = new ValidadorTC();
    var numero = $(".js-numero-tc").val();
    var codigo = $(".js-codigo-seguridad").val();
    var fecha_exp = $(".js-fecha-expiracion").val();
    $(".js-error").hide();
    if (val.validarNumero(numero) == false) {
        $(".js-error-nro").show();
        return false;
    }
    if (val.validarCodigo(codigo) == false) {
        $(".js-error-codigo").show();
        return false;
    }
    if (val.validarFechaExpiracion(fecha_exp) == false) {
        $(".js-error-fecha").show();
        return false;
    }
    return true;
}