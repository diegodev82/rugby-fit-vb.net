Imports BE
Imports MPP

Public Class BLL_Familias


    Public oMiMpp As New MPP.MPP_Familias

    Public Function GetPorId(ByVal id As Integer) As BE_Familia
        Return oMiMpp.GetPorId(id)
    End Function

    ''' 
    ''' <param name="objPermiso"></param>
    ''' <param name="IdFamilia"></param>
    Public Function AgregarPermiso(ByVal objPermiso As BE_Permiso, ByVal IdFamilia As Integer) As Boolean
        Dim agregado As Integer = oMiMpp.AgregarPermiso(objPermiso, IdFamilia)
        Return agregado > 0
    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function Crear(ByVal objFam As BE_Familia) As BE.BE_Familia
        If oMiMpp.ValidarExistente(objFam) = True Then
            Return Nothing
        End If
        objFam.IdFamilia = 0 'Asumo que va a fallar
        Dim IdInsertado As Integer = oMiMpp.Crear(objFam)
        If IdInsertado > 0 Then ' Si no falla, sobrescribo el valor por def.
            objFam.IdFamilia = IdInsertado
        End If
        Return objFam
    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function Editar(ByVal objFam As BE_Familia) As Boolean
        If oMiMpp.ValidarExistente(objFam) = True Then
            Return Nothing
        End If
        Dim actualizado As Boolean = oMiMpp.Editar(objFam)
        Return actualizado
    End Function

    ''' 
    ''' <param name="objFam"></param>
    Public Function Eliminar(ByVal objFam As BE_Familia) As Integer
        Dim eliminado As Integer = oMiMpp.Eliminar(objFam)
        Return eliminado
    End Function

    ''' 
    ''' <param name="IdFamiliaPermiso"></param>
    Public Function QuitarPermiso(ByVal IdFamilia As Integer, ByVal IdPermiso As Integer) As Boolean
        Dim eliminado As Boolean = oMiMpp.QuitarPermiso(IdFamilia, IdPermiso)
        Return eliminado
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function Listar(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Familia)
        Return oMiMpp.Listar(filtros)
    End Function


End Class ' BLL_Familias

