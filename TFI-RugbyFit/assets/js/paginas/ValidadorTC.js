﻿function ValidadorTC() {
    this.numero = null;
    this.tipoTc = null;
    this.dataTC = {}
    this.validarNumero = function (nro) {
        //return true;
        this.numero = nro;
        this.tipoTc = this.getTipoTcSeleccionada();
        if (typeof (this.tipoTc) == "undefined") {
            return false;
        }
        if (this.tipoTc == "visa" && !this.esVisa()) {
            return false;
        }
        if (this.tipoTc == "mastercard" && !this.esMastercard()) {
            return false;
        }
        if (this.tipoTc == "amex" && !this.esAmex()) {
            return false;
        }

        if (this.esNumeroValido() == false) {
            return false;
        }
        this.dataTC = this.getDataTc();
        if (this.dataTC == false) {
            return false;
        }

        return true;
    }
    this.validarCodigo = function (codigoIngresado) {
        console.info(this.dataTC);
        return codigoIngresado == this.dataTC.CodigoDesencriptado
    }
    this.validarFechaExpiracion = function (fechaExpiracion) {
        console.info(this.dataTC);
        return fechaExpiracion == this.dataTC.MesAnioExpiracion
    }
    this.getTipoTcSeleccionada = function () {
        var tipoTc = $('.js-tipo-tc input[type=radio]:checked').val();
        return tipoTc;
    }
    this.esVisa = function () {
        return this.numero[0] == "4"
    }

    this.esMastercard = function () {
        var num = this.numero[0] + "" + this.numero[1];
        if (num == "51" || num == "55" || num == "53" || num == "52") {
            return true;
        }
        return false;
    }

    this.esAmex = function () {
        var num = this.numero[0] + "" + this.numero[1];
        if (num == "34" || num == "37") {
            return true;
        }
        return false;
    }

    this.getDataTc = function () {

        var info = false;
        $.ajax({
            "url": "/AjaxUtils.asmx/TCExistente",
            data: "{Numero:" + this.numero + "}",
            contentType: 'application/json; utf-8',
            dataType: 'json',
            type: 'post',
            async: false,
            success: function (data) {
                if (data.d != null) {
                    info = data.d;
                }

            },
        });

        return info;
    }


    /**
    * Algoritmo MOD10:
    http://www.getcreditcardnumbers.com/
    * Otro algoritmo: 
    //Ref: http://ricardogeek.com/como-validar-tarjetas-de-credito/
    */
    this.esNumeroValido = function () {
        //Uso algoritmo MOD10
        var ccNum = this.numero;
        var charCount = ccNum.length
        if (charCount < 13 || charCount > 16) {
            return false;
        }
        var dv = ccNum[ccNum.length - 1];
        var numArr = [];
        var pos = 0;
        for (i = charCount - 2; i >= 0 ; i--) {
            var digit = parseInt(ccNum.charAt(i));
            var nro;
            if (pos % 2 == 0) {
                nro = digit * 2
                nro = this.toSingle(nro);
            } else {
                nro = digit
            }
            //console.log(nro);
            numArr.push(nro)
            pos++;
        }
        sumTotal = 0;
        for (i = 0; i < numArr.length; i++) {
            sumTotal += numArr[i];
        }
        var resultado = sumTotal * 9;
        var strRtdo = resultado.toString()
        var digitoCalculado = strRtdo[strRtdo.length - 1] * 1;
        return digitoCalculado == dv;

    }

    this.toSingle = function (digit) {
        if (digit > 9) {
            var tmp = digit.toString();
            var d1 = parseInt(tmp.charAt(0));
            var d2 = parseInt(tmp.charAt(1));
            return (d1 + d2);
        } else {
            return digit;
        }
    }

}
/**
// Otro algoritmo: 
//Ref: http://ricardogeek.com/como-validar-tarjetas-de-credito/
function isValid(ccNum, charCount) {
    var double = true;
    var numArr = [];
    var sumTotal = 0;
    for (i = 0; i < charCount; i++) {
        var digit = parseInt(ccNum.charAt(i));

        if (double) {
            digit = digit * 2;
            digit = toSingle(digit);
            double = false;
        } else {
            double = true;
        }
        numArr.push(digit);
    }

    for (i = 0; i < numArr.length; i++) {
        sumTotal += numArr[i];
    }
    var diff = eval(sumTotal % 10);
    console.log(diff);
    console.log(diff == "0");
    return (diff == "0");
}

*/