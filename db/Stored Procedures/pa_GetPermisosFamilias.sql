USE [RugbyFit]
GO
/****** Object:  StoredProcedure [dbo].[pa_GetPermisosFamilias]    Script Date: 29/08/2016 16:43:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Diego Olmedo
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[pa_GetPermisosFamilias]
	@IdsFamilias nchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT p.id_permiso, p.descripcion, p.codigo FROM permiso p
	 INNER JOIN familia_permiso pf ON pf.id_permiso = p.id_permiso 
	  WHERE pf.id_familia IN (@IdsFamilias)

END
