﻿Imports BLL
Imports BE
Public Class AdminFamiliasForm
    Inherits ACL
    Private oMiBll As New BLL_Familias
    Private dataFamilia As BE_Familia
    Const COD_PERMISO = "MOD_SEGURIDAD"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ValidarAcceso(COD_PERMISO)
        If Not IsPostBack Then
            Me.CargarData()
            Me._CargarDropdownPermisos()
        End If
    End Sub
    Protected Sub CargarData()
        If Request.QueryString("id") IsNot Nothing Then
            If Not IsNumeric(Request.QueryString("id")) Then
                Return
            End If
            Dim idFamilia As Integer = CType(Request.QueryString("id"), Integer)
            Me.dataFamilia = oMiBll.GetPorId(idFamilia)
            If dataFamilia IsNot Nothing Then
                iptCodigo.Value = dataFamilia.IdFamilia
                iptNombre.Text = dataFamilia.Nombre
                btnEliminar.Visible = True
                Me._CargarDgvPermisos(dataFamilia.Permisos)
                phAsignarFamiliasPermisos.Visible = True
            End If
        End If
    End Sub

    Protected Sub _CargarDgvPermisos(ByVal permisos As List(Of BE_Permiso))
        If Not permisos Is Nothing Then
            gdvPermisos.DataSource = Nothing
            gdvPermisos.DataSource = permisos
            gdvPermisos.DataBind()
        End If
    End Sub

    
    Protected Sub _CargarDropdownPermisos()
        Dim bllPermisos As New BLL_Permisos
        ddPermisos.DataSource = Nothing
        ddPermisos.DataMember = "BE_Permiso"
        ddPermisos.DataValueField = "IdPermiso"
        ddPermisos.DataTextField = "descripcion"
        ddPermisos.DataSource = bllPermisos.Listar()
        ddPermisos.DataBind()
    End Sub


    Protected Sub btnAgregarPermiso_Click(sender As Object, e As EventArgs) Handles btnAgregarPermiso.Click
        Dim idPermiso As Integer = ddPermisos.SelectedValue
        Dim idFamilia As Integer = iptCodigo.Value
        If idFamilia > 0 And idPermiso > 0 Then
            Dim permisoAgregado As New BE_Permiso With {.IdPermiso = idPermiso, .Tipo = 1}
            Dim agregado = oMiBll.AgregarPermiso(permisoAgregado, idFamilia)
            If agregado = True Then
                Me.CargarData()
                Me._CargarDgvPermisos(Me.dataFamilia.Permisos)
            End If
        End If
    End Sub

    Protected Sub gdvPermisos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gdvPermisos.RowDeleting
        Dim row As GridViewRow = gdvPermisos.Rows(e.RowIndex)
        Dim idPerm As String = row.Cells(0).Text
        Dim idFamilia As Integer = iptCodigo.Value
        If IsNumeric(idPerm) Then

            Dim eliminado As Boolean = oMiBll.QuitarPermiso(idFamilia, idPerm)
            If eliminado = True Then
                Me.CargarData()
                Me._CargarDgvPermisos(Me.dataFamilia.Permisos)
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If IsNumeric(iptCodigo.Value) Then
            Dim idFamilia As Integer = CType(iptCodigo.Value, Integer)
            Dim objFam As New BE_Familia With {.IdFamilia = idFamilia}
            Dim eliminado As Integer = oMiBll.Eliminar(objFam)
            If eliminado = 1 Then
                Response.Redirect("AdminFamiliasList.aspx?eliminado=ok")
            ElseIf eliminado = -1 Then
                RtaErrorEliminar.Visible = True
                msjErrorFK.Visible = True
            Else
                RtaErrorEliminar.Visible = True
                msjErrorDB.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim oFami As New BE_Familia
        Dim familiaGuardada As BE_Familia = Nothing
        oFami.IdFamilia = IIf(iptCodigo.Value <> "", iptCodigo.Value, 0)
        oFami.Nombre = iptNombre.Text
        

        If oFami.IdFamilia > 0 Then
            Dim guardado As Boolean = oMiBll.Editar(oFami)
            If guardado = True Then
                familiaGuardada = oFami
            End If
        Else
            familiaGuardada = oMiBll.Crear(oFami)
        End If
        If familiaGuardada Is Nothing Then
            MensajeRespuesta.Text = "Ya existe un rol registrado con ese nombre. Por favor, ingrese otro nombre distinto"
            PanelMensajeRespuesta.Visible = True
            Return
        End If
        If familiaGuardada.IdFamilia > 0 Then
            Response.Redirect("AdminFamiliasForm.aspx?guardado=ok&id=" + familiaGuardada.IdFamilia.ToString)
        Else
            MensajeRespuesta.Text = "Se produjo un error desconocido y no pudo realizarse la acción. Inténtelo nuevamente, por favor"
            PanelMensajeRespuesta.Visible = True
        End If
    End Sub
End Class