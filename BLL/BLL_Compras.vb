Imports BE
Imports MPP


Public Class BLL_Compras


    Public m_MPP_Compras As MPP.MPP_Compras
    Public m_MPP_Productos As MPP.MPP_Productos

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function AgregarProducto(ByVal objDetCpra As BE_Detalle_Compra) As BE.BE_Detalle_Compra
        AgregarProducto = Nothing
    End Function

    ''' 
    ''' <param name="objCompra"></param>
    Public Function Crear(ByVal objCompra As BE_Compra) As BE.BE_Compra
        Crear = Nothing
    End Function

    ''' 
    ''' <param name="objCompra"></param>
    Public Function Editar(ByVal objCompra As BE_Solicitud_Compra) As BE.BE_Compra
        Editar = Nothing
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function EditarItem(ByVal objDetCpra As BE_Detalle_Compra) As BE.BE_Detalle_Compra
        EditarItem = Nothing
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function EliminarItem(ByVal objDetCpra As BE_Detalle_Compra) As Boolean
        EliminarItem = False
    End Function

    ''' 
    ''' <param name="objSolCpra"></param>
    Public Function Enviar(ByVal objSolCpra As BE_Solicitud_Compra) As Boolean
        Enviar = False
    End Function

    Public Function GenerarOC() As Boolean
        GenerarOC = False
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarCompras(ByVal filtros As Hashtable) As List(Of BE_Compra)
        ListarCompras = Nothing
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarProductosPendientes(ByVal filtros As Hashtable) As List(Of BE_Detalle_Compra)
        ListarProductosPendientes = Nothing
    End Function

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarSolicitudes(ByVal filtros As Hashtable) As List(Of BE_Compra)
        ListarSolicitudes = Nothing
    End Function

    ''' 
    ''' <param name="objDetCpra"></param>
    Public Function RegistrarIngreso(ByVal objDetCpra As BE_Detalle_Compra) As Boolean
        RegistrarIngreso = False
    End Function


End Class ' BLL_Compras

