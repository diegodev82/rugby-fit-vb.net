﻿Imports BE
Imports MPP
Imports System.Security.Cryptography

Public Class BLL_Seguridad
    Private oBllBit As New BLL_Bitacora
    Private Const tablaVarchar = "abcdefghijklmnñopqrstuvwxyz0123456789ABCDEFGHIJLKMNÑOPQRSTUVWXYZ,.-;:_{}[]+*\¿?!¡#$&/()=áéíóúÁÉÍÓÚ " + Chr(13)
    Private Const tablaNumeros = "0123456789"
    Public Const MAX_CII = 5
    Private usuarioLogueado As BE_Usuario = New BE_Usuario()
    Private clienteLogueado As BE_Cliente = New BE_Cliente()

    Public Function Login(ByVal nickname As String, ByVal password As String) As Object
        Dim passwordEnc = Encriptar(password, False)
        Dim oSeguridad As MPP_Seguridad = New MPP_Seguridad()
        Dim rtaLogin As Object = oSeguridad.Login(nickname, passwordEnc)
        Dim nroError As Integer
        Dim evento As New BE.BE_Evento
        'Si el usuario existe en la db, me traigo los datos del mismo para poder realizar distintas acciones  (aumentar cii, verificar cii, etc)
        If rtaLogin.GetType() = GetType(BE_Usuario) Then
            Me.usuarioLogueado = oSeguridad.GetUsuarioLogueado()
        End If
        If rtaLogin.GetType() = GetType(Integer) Then
            If rtaLogin <> MPP_Seguridad.ERROR_USUARIO_INCORRECTO Then
                Me.usuarioLogueado = oSeguridad.GetUsuarioLogueado()
            End If
        End If
        Dim idUsuario = usuarioLogueado.IdUsuario

        Dim retorno As Integer = -1
        usuarioLogueado.EsAdministrador = oSeguridad.esAdministrador(idUsuario)

        If rtaLogin.GetType() = GetType(BE.BE_Usuario) Then ' Login ok
            idUsuario = rtaLogin.IdUsuario
            oSeguridad.ResetearCII(idUsuario)
            oSeguridad.CargarPermisos(idUsuario) 'cargar permisos

            evento.IdEvento = BE_Evento.LOGIN_OK
            oBllBit.RegistrarEvento(evento, "Usuario Logueado OK: " & usuarioLogueado.Nickname & "(" & usuarioLogueado.IdUsuario.ToString & ")")
            
            Return rtaLogin
        End If

        If rtaLogin.GetType() = GetType(Integer) Then
            nroError = rtaLogin
            If nroError = MPP_Seguridad.ERROR_USUARIO_INCORRECTO Then 'Nickname incorrecto
                evento.IdEvento = BE_Evento.NICKNAME_INCORRECTO
                oBllBit.RegistrarEvento(evento, "Nickname Ingresado: " & nickname)
                retorno = 1
            End If
            If nroError = MPP_Seguridad.ERROR_USUARIO_BLOQUEADO Then 'Usuario bloqueado previamente
                evento.IdEvento = BE_Evento.USUARIO_BLOQUEADO
                Dim observacion As String = ""
                observacion = "Usuario Ya Bloqueado previamente: "
                oBllBit.RegistrarEvento(evento, observacion & usuarioLogueado.Nickname & "(" & usuarioLogueado.IdUsuario.ToString & ")")
                retorno = 2
            End If
            If nroError = MPP_Seguridad.ERROR_CONTRASENIA_INCORRECTA Then 'Password incorrecto
                oSeguridad.AumentarCII(idUsuario)
                Dim cii As Integer = oSeguridad.VerificarCII(idUsuario)
                If cii < MAX_CII Then
                    retorno = 3 'Password incorrecto
                    evento.IdEvento = BE_Evento.PASSWORD_INCORRECTO
                    Dim observacion As String = ""
                    observacion = "Contraseña ingresada: " + password + " - Usuario intentando ingresar: "
                    oBllBit.RegistrarEvento(evento, observacion & usuarioLogueado.Nickname & "(" & usuarioLogueado.IdUsuario.ToString & ")")
                Else 'Usuario bloqueado
                    evento.IdEvento = BE_Evento.USUARIO_BLOQUEADO
                    Dim observacion As String = ""
                    observacion = "Usuario Bloqueado: "
                    oBllBit.RegistrarEvento(evento, observacion & usuarioLogueado.Nickname & "(" & usuarioLogueado.IdUsuario.ToString & ")")
                    retorno = 2 'Muestra mensaje de usuario bloqueado
                End If
            End If
            Return retorno
        End If
        Return -1
    End Function


    Public Sub Logout()
        Dim evento As New BE_Evento
        Dim oBllBit As New BLL_Bitacora
        evento.IdEvento = BE_Evento.LOGOUT_OK
        Dim observacion As String = "Usuario: " + Me.usuarioLogueado.Nickname
        oBllBit.RegistrarEvento(evento, observacion)
    End Sub

    Function Desencriptar(ByVal texto As String, Optional esNumero As Boolean = False) As String
        If esNumero = True Then
            Return Me._DesencriptarNumero(texto)
        End If
        Dim resultado As String
        Dim pos As Integer
        Dim tabla As String
        resultado = ""
        texto = limpiar(texto, esNumero)
        Dim key As Integer = 3
        If esNumero = True Then
            tabla = tablaNumeros
        Else
            tabla = tablaVarchar
        End If
        For i As Integer = 0 To (texto.Length - 1)
            If texto.Chars(i) = "," And esNumero = True Then 'Si estoy encriptando un numero, la coma (,) no lo encripto
                resultado = resultado + "."
            Else
                pos = tabla.IndexOf(texto.Chars(i))
                If ((pos - key) < 0) Then
                    resultado = resultado + tabla.Chars((pos - key) + tabla.Length)
                Else
                    resultado = resultado + tabla.Chars(pos - key)
                End If
            End If
        Next
        resultado = resultado.Replace("{.13}", vbCrLf)
        Return resultado
    End Function

    Private Function _DesencriptarNumero(nro As Decimal) As String
        Dim numeroEnc As String = nro.ToString
        Dim tabla As String = tablaNumeros
        Dim resultado As String = ""
        Dim key As Integer = 3
        Dim pos As Integer
        For i As Integer = 0 To (numeroEnc.Length - 1)
            If numeroEnc.Chars(i) = "," Then
                resultado = resultado + ","
            Else
                pos = tabla.IndexOf(numeroEnc.Chars(i))
                If ((pos - key) < 0) Then
                    resultado = resultado + tabla.Chars((pos - key) + tabla.Length)
                Else
                    resultado = resultado + tabla.Chars(pos - key)
                End If
            End If
        Next
        Return (resultado)
    End Function

    Public Function EncriptarNumero(ByVal texto As String, Optional ByVal reversible As Boolean = False) As String
        If reversible = False Then
            ' Encriptar con MD5
            Return _EncriptarMD5(texto)
        End If

        Dim resultado As String
        Dim pos As Integer
        resultado = ""
        'texto = limpiar(texto, esPrecio)
        Dim key As Integer = 3

        For i As Integer = 0 To (texto.Length - 1)
            pos = tablaNumeros.IndexOf(texto.Chars(i)) 'buscar la posicion del caracter 
            If (pos + key) < tablaNumeros.Length Then
                resultado = resultado + tablaNumeros.Chars(pos + key) 'realiza el reemplazo de caracteres
            Else
                resultado = resultado + tablaNumeros.Chars((pos + key) - tablaNumeros.Length) 'vuelve a empezar
            End If
        Next
        'reconstruye el mensaje con los retornos de carro
        resultado = resultado.Replace("{.13}", vbCrLf)
        Return resultado
    End Function

    Public Function Encriptar(ByVal texto As String, Optional ByVal reversible As Boolean = False) As String
        If reversible = False Then
            ' Encriptar con MD5
            Return _EncriptarMD5(texto)
        End If

        Dim resultado As String
        Dim pos As Integer
        resultado = ""
        'texto = limpiar(texto, esPrecio)
        Dim key As Integer = 3

        For i As Integer = 0 To (texto.Length - 1)
            pos = tablaVarchar.IndexOf(texto.Chars(i)) 'buscar la posicion del caracter 
            If (pos + key) < tablaVarchar.Length Then
                resultado = resultado + tablaVarchar.Chars(pos + key) 'realiza el reemplazo de caracteres
            Else
                resultado = resultado + tablaVarchar.Chars((pos + key) - tablaVarchar.Length) 'vuelve a empezar
            End If
        Next
        'reconstruye el mensaje con los retornos de carro
        resultado = resultado.Replace("{.13}", vbCrLf)
        Return resultado
    End Function

    Private Function _EncriptarMD5(ByVal texto) As String
        Dim textoEncriptado As String

        Dim md5 As MD5CryptoServiceProvider
        Dim bytValue() As Byte
        Dim bytHash() As Byte
        Dim strPassOutput As String
        Dim i As Integer
        strPassOutput = ""

        md5 = New MD5CryptoServiceProvider

        bytValue = System.Text.Encoding.UTF8.GetBytes(texto)

        bytHash = md5.ComputeHash(bytValue)
        md5.Clear()

        For i = 0 To bytHash.Length - 1
            strPassOutput &= bytHash(i).ToString("x").PadLeft(2, "0")
        Next

        textoEncriptado = strPassOutput
        Return textoEncriptado
    End Function

    Private Function limpiar(ByVal t As String, Optional esPrecio As Boolean = False) As String
        Dim pos As Integer
        Dim tabla As String
        If esPrecio Then
            tabla = tablaNumeros
        Else
            tabla = tablaVarchar
        End If
        ' reemplaza el retorno de carro con un simbolo
        If Not t Is Nothing Then
            t = t.Replace(vbCrLf, "{.13}")
            ' reemplaza los caracteres que no se encuentran en la tabla por un simbolo
            For i As Integer = 0 To (t.Length - 1)
                pos = tabla.IndexOf(t.Chars(i))
                If (pos = -1) Then
                    t = t.Replace(t.Chars(i), "{.d}")
                End If
            Next
        End If
        Return t
    End Function

    Private Function _Deprecated_Login(ByVal email As String, ByVal password As String) As Object
        Dim passwordEnc = Encriptar(password)
        Dim oSeguridad As MPP_Seguridad = New MPP_Seguridad()
        Dim rtaLogin As Object = oSeguridad.Login(email, passwordEnc)
        Dim nroError As Integer
        Dim evento As New BE.BE_Evento
        'Si el usuario existe en la db, me traigo los datos del mismo para poder realizar distintas acciones  (aumentar cii, verificar cii, etc)
        If rtaLogin.GetType() = GetType(Integer) Then
            If rtaLogin <> MPP_Seguridad.ERROR_USUARIO_INCORRECTO Then
                Me.clienteLogueado = oSeguridad.GetClienteLogueado()
            End If
        End If
        Dim idUsuario = clienteLogueado.IdCliente
        Dim retorno As Integer = -1

        If rtaLogin.GetType() = GetType(BE.BE_Cliente) Then ' Login ok
            idUsuario = rtaLogin.IdCliente
            evento.IdEvento = 14
            oBllBit.RegistrarEvento(evento, "Cliente Logueado: " & clienteLogueado.Email & "(" & clienteLogueado.IdCliente.ToString & ")")
            Return rtaLogin
        End If

        If rtaLogin.GetType() = GetType(Integer) Then
            nroError = rtaLogin
            If nroError = MPP_Seguridad.ERROR_USUARIO_INCORRECTO Then 'Nickname incorrecto
                evento.IdEvento = 15
                oBllBit.RegistrarEvento(evento, "Email Ingresado: " & email)
                retorno = 1
            End If
            If nroError = MPP_Seguridad.ERROR_CONTRASENIA_INCORRECTA Then 'Password incorrecto
                retorno = 3 'Password incorrecto
            End If
            If nroError = MPP_Seguridad.ERROR_REGISTRACION_NO_CONFIRMADA Then 'Aun no confirmo la registracion
                retorno = 4 'registracion sin confirmar
            End If
        End If
        Return retorno
        Return -1
    End Function



End Class
