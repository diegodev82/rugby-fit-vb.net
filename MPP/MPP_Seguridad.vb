﻿Imports DAL
Imports BE
Public Class MPP_Seguridad
    Private _usuarioLogueado As BE.BE_Usuario = New BE.BE_Usuario
    Private _clienteLogueado As BE.BE_Cliente = New BE.BE_Cliente
    Public Const ERROR_USUARIO_BLOQUEADO = 2
    Public Const ERROR_USUARIO_INCORRECTO = 1
    Public Const ERROR_CONTRASENIA_INCORRECTA = 3
    Public Const ERROR_REGISTRACION_NO_CONFIRMADA = 4
    Public Const MAX_CII = 10


    Public Function esAdministrador(ByVal idUsuario As Integer) As Boolean
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet

        Dim dt As New DataTable
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", idUsuario)

        DS = oDatos.Leer("pa_EsAdministrador", hdatos)

        If DS.Tables(0).Rows.Count > 0 Then
            Dim item As DataRow = DS.Tables(0).Rows(0)
            Dim cant As Integer = CInt(item("cant"))
            Return cant > 0
        End If
        Return False
    End Function

    Public Sub CargarPermisos(ByVal id_usuario As Integer)
        Me._cargarPermisosPersonales(id_usuario)
        Me._cargarPermisosDeFamilia(id_usuario)
    End Sub

    Public Sub ResetearCII(ByVal id_usuario As Integer)
        Dim oDal As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        Dim resultado As Boolean
        hdatos.Add("@IdUsuario", id_usuario)

        resultado = oDal.Escribir("pa_ResetearCII", hdatos)
    End Sub
    Public Sub AumentarCII(ByVal id_usuario As Integer)
        Dim oDal As SQLHelper = SQLHelper.GetInstance
        Dim hdatos As New Hashtable
        Dim resultado As Boolean
        hdatos.Add("@IdUsuario", id_usuario)

        resultado = oDal.Escribir("pa_AumentarCII", hdatos)
    End Sub

    Public Function VerificarCII(ByVal id_usuario As Integer) As Integer
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id_usuario)

        DS = oDatos.Leer("pa_VerificarCII", hdatos)

        If DS.Tables(0).Rows.Count > 0 Then
            Dim item As DataRow = DS.Tables(0).Rows(0)
            Dim cii As Integer = CInt(item("cii"))
            Return cii
        End If
        Return 99 ' por las dudas mando un numero alto para que no pase la verificacion
        'Dim sql As String = String.Format("SELECT TOP 1 cii FROM usuario WHERE id_usuario = {0}", id_usuario)
        'Return SQLHelper.GetInstance.ExecuteScalar(sql)
    End Function
    'Devuelve los datos del usuario que está intentando loguearse si es que existe

    Public Function GetUsuarioLogueado() As BE_Usuario
        Return Me._usuarioLogueado
    End Function

    Public Function GetClienteLogueado() As BE_Cliente
        Return Me._clienteLogueado
    End Function

    Private Function _CargarDatosCliente(ByVal dr As DataRow)
        Me._usuarioLogueado.Cliente = Nothing
        If Not IsDBNull(dr("id_cliente")) Then
            If dr("id_cliente") > 0 Then
                Dim cliente As New BE_Cliente
                cliente.Nombre = dr("nombre")
                cliente.Apellido = dr("apellido")
                cliente.Nickname = dr("nickname")
                cliente.IdCliente = dr("id_cliente")
                cliente.Origen = dr("origen")
                cliente.DNI = dr("dni")
                cliente.Email = dr("email")
                cliente.Domicilio = dr("domicilio")
                cliente.Ciudad = New BE_Ciudad With {.IdCiudad = dr("id_ciudad")}
                cliente.Activo = dr("activo")
                cliente.CUIT = dr("cuit")
                cliente.Contrasenia = dr("contrasenia")
                cliente.HashRegistracion = dr("hash_registracion")
                cliente.PuntosAcumulados = dr("puntos_acumulados")
                cliente.RegistracionConfirmada = dr("registracion_confirmada")
                cliente.Telefono = dr("telefono")
                Me._usuarioLogueado.Cliente = cliente
            End If
        End If
    End Function

    Public Function Login(ByVal nickname As String, ByVal password As String) As Object

        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet

        Dim dt As New DataTable
        Dim hdatos As New Hashtable
        hdatos.Add("@Usuario", nickname)

        DS = oDatos.Leer("pa_Login", hdatos)

        If DS.Tables(0).Rows.Count = 0 Then
            Return ERROR_USUARIO_INCORRECTO
        Else
            Dim item As DataRow = DS.Tables(0).Rows(0)

            Me._usuarioLogueado.IdUsuario = item("id_usuario")
            Me._usuarioLogueado.Nombre = item("nombre")
            Me._usuarioLogueado.Apellido = item("apellido")
            Me._usuarioLogueado.Nickname = item("nickname")
            Me._usuarioLogueado.Contrasenia = item("contrasenia")
            Me._usuarioLogueado.Email = item("email")
            Me._usuarioLogueado.Telefono = item("telefono")
            Me._usuarioLogueado.DNI = item("dni")
            Me._usuarioLogueado.Activo = item("activo")
            Me._usuarioLogueado.CII = item("cii")
            Me._usuarioLogueado.EsEmpleado = item("es_empleado")
            Me._usuarioLogueado.PreguntaSeguridad = item("pregunta_seguridad")
            Me._usuarioLogueado.RespuestaSeguridad = item("respuesta_seguridad")
            Dim passDB As String = Me._usuarioLogueado.Contrasenia
            Dim cii As Integer = Me._usuarioLogueado.CII
            If String.IsNullOrEmpty(passDB) Then
                Return ERROR_CONTRASENIA_INCORRECTA
            Else
                If cii >= MAX_CII Then
                    Return ERROR_USUARIO_BLOQUEADO
                End If
                If passDB = password Then
                    Me._CargarDatosCliente(item)

                    Return Me._usuarioLogueado
                Else
                    Return ERROR_CONTRASENIA_INCORRECTA
                End If
            End If
        End If
    End Function

    Private Sub _cargarPermisosDeFamilia(id_usuario)
        Me._cargarFamilias(id_usuario)
        If Me._usuarioLogueado.Familias.Count > 0 Then
            Dim idsFamilias As ArrayList = New ArrayList
            For Each familia As BE.BE_Familia In Me._usuarioLogueado.Familias
                idsFamilias.Add(familia.IdFamilia)
            Next

            Dim oDatos As SQLHelper = SQLHelper.GetInstance()
            Dim ds As New DataSet
            Dim hdatos As New Hashtable
            hdatos.Add("@IdsFamilias", String.Join(",", idsFamilias.ToArray))
            'Para multipermisos pa_DynamicGetPermisosFamilias
            ds = oDatos.Leer("pa_GetPermisosFamilias", hdatos)
            If Not ds Is Nothing Then
                Dim dt As DataTable = ds.Tables(0)
                If Not dt Is Nothing Then
                    For Each dr As DataRow In dt.Rows
                        Dim permiso As New BE.BE_Permiso
                        permiso.IdPermiso = dr.Item("id_permiso")
                        permiso.Descripcion = dr.Item("descripcion")
                        permiso.Codigo = dr.Item("codigo")
                        permiso.Tipo = 1
                        If Not _permisoYaCargada(permiso.IdPermiso) Then
                            Me._usuarioLogueado.Permisos.Add(permiso)
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub _cargarPermisosPersonales(id_usuario)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id_usuario)

        ds = oDatos.Leer("pa_GetPermisosHeredadosPorUsuario", hdatos)
        Me._usuarioLogueado.Permisos.Clear()
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim permiso As New BE.BE_Permiso
            permiso.IdPermiso = dr.Item("id_permiso")
            permiso.Descripcion = dr.Item("descripcion")
            permiso.Tipo = dr.Item("tipo")
            permiso.Codigo = dr.Item("codigo")
            Me._usuarioLogueado.Permisos.Add(permiso)
        Next
    End Sub

    Private Function _permisoYaCargada(id_permiso) As Boolean
        For Each permiso As BE.BE_Permiso In Me._usuarioLogueado.Permisos
            If permiso.IdPermiso = id_permiso Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function DEPRECATED_obtenerPermisosPorFamilia(id_usuario) As DataSet
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id_usuario)

        DS = oDatos.Leer("pa_GetPermisosHeredadosPorFamilias", hdatos)
        Return DS

        'Dim sql As String = String.Format("SELECT p.id_permiso, p.codigo FROM usuario_familia uf " _
        '            & " INNER JOIN familia_permiso pf ON uf.id_familia = pf.id_familia " _
        '            & " INNER JOIN permiso p ON p.id_permiso = fp.id_permiso " _
        '            & " WHERE uf.id_usuario = {0}", id_usuario)
        'Return DAL.SQLHelper.GetInstance.GetRecordset(sql)
    End Function

    Private Sub _cargarFamilias(id_usuario)
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim ds As New DataSet
        Dim hdatos As New Hashtable
        hdatos.Add("@IdUsuario", id_usuario)

        ds = oDatos.Leer("pa_GetFamiliasPorUsuario", hdatos)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim familia As New BE.BE_Familia
                familia.IdFamilia = dr.Item("id_familia")
                familia.Nombre = dr.Item("nombre")
                Me._usuarioLogueado.Familias.Add(familia)
            Next
        End If
    End Sub



    Public Function _DEPRECATED_Login(ByVal email As String, ByVal password As String) As Object
        Dim oDatos As SQLHelper = SQLHelper.GetInstance()
        Dim DS As New DataSet

        Dim dt As New DataTable
        Dim hdatos As New Hashtable
        hdatos.Add("@Email", email)

        DS = oDatos.Leer("pa_Login", hdatos)

        If DS.Tables(0).Rows.Count = 0 Then
            Return ERROR_USUARIO_INCORRECTO
        Else
            Dim item As DataRow = DS.Tables(0).Rows(0)

            Me._clienteLogueado.IdCliente = item("id_cliente")
            Me._clienteLogueado.Nombre = item("nombre")
            Me._clienteLogueado.Apellido = item("apellido")
            Me._clienteLogueado.Contrasenia = item("contrasenia")
            Me._clienteLogueado.Email = item("email")
            Me._clienteLogueado.Telefono = item("telefono")
            Me._clienteLogueado.DNI = item("dni")
            Me._clienteLogueado.CUIT = item("cuit").ToString
            Me._clienteLogueado.Domicilio = item("domicilio")
            Me._clienteLogueado.Origen = item("origen")
            Me._clienteLogueado.PuntosAcumulados = item("puntos_acumulados")
            Me._clienteLogueado.Activo = item("activo")
            Me._clienteLogueado.RegistracionConfirmada = CType(item("registracion_confirmada"), Integer)
            Me._clienteLogueado.Ciudad = New BE_Ciudad With {.IdCiudad = item("id_ciudad")}


            If Me._clienteLogueado.RegistracionConfirmada = 0 Then
                Return ERROR_REGISTRACION_NO_CONFIRMADA
            End If
            Dim passDB As String = Me._clienteLogueado.Contrasenia
            If String.IsNullOrEmpty(passDB) Then
                Return ERROR_CONTRASENIA_INCORRECTA
            Else
                If passDB = password Then
                    Return Me._clienteLogueado
                Else
                    Return ERROR_CONTRASENIA_INCORRECTA
                End If
            End If
        End If
    End Function

End Class
