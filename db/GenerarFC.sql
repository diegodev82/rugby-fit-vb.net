declare @fecha date = '2016-10-12';
declare @cantidad int;
select @cantidad = ABS(Checksum(NewID()) % 10)+1

insert into dbo.factura (id_factura_rel, id_venta, id_cliente, fecha, domicilio, id_ciudad, tipo, id_forma_pago, estado) 
values(0, 99, 6, @fecha, 'Example 123', 1242, 1, 1, 'PAGADA');

declare @InsertdId int = 0;
select  @InsertdId = SCOPE_IDENTITY();
insert into dbo.detalle_factura (id_factura, id_producto, cantidad, precio_unitario, precio_final) 
values(@InsertdId, 1, @cantidad, 10000, @cantidad*10000);
