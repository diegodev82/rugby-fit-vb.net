﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminModificarContrasenia.aspx.vb" Inherits="TFI_RugbyFit.AdminModificarContrasenia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <h1 class="text-center">Modificar mi contrase&ntilde;a</h1>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <asp:PlaceHolder ID="phMsjeError" Visible="false" runat="server">
                            <p class="alert alert-danger">
                                <asp:Literal ID="ltlMensajeError" runat="server"></asp:Literal>
                            </p>
                        </asp:PlaceHolder>
                        <% If Request.QueryString("guardado") = "ok" Then%>
                        <asp:PlaceHolder ID="phMsjeOk" runat="server">
                            <p class="alert alert-success">La nueva contrase&ntilde;a se ha guardado correctamente.</p>
                        </asp:PlaceHolder>
                        <% End If%>

                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <label>Contrase&ntilde;a actual</label>
                                    <asp:TextBox ID="iptContraseniaActual" ClientIDMode="Static" required="required" CssClass="form-control" MaxLength="10" TextMode="Password" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Nueva contrase&ntilde;a</label>
                                    <asp:TextBox ID="iptNuevaContrasenia" CssClass="form-control" ClientIDMode="Static" required="required" MaxLength="10" TextMode="Password" runat="server"></asp:TextBox>
                                </div>
                                <asp:Button class="btn btn-lg btn-primary btn-block" ID="btn_CambiarPwd" runat="server" Text="Restaurar Contraseña" ValidationGroup="ResetPwdValidation" />
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
