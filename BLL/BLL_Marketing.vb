Imports BE
Imports MPP

Public Class BLL_Marketing


    Public m_MPP_Marketing As MPP.MPP_Marketing

    ''' 
    ''' <param name="objProducto"></param>
    Public Function Canjear(ByVal objProducto As BE_Producto) As Boolean
        Canjear = False
    End Function

    Public Sub GenerarVoucher()

    End Sub

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarProductosCanjeables(ByVal filtros As Hashtable) As List(Of BE_Producto)
        ListarProductosCanjeables = Nothing
    End Function

    ''' 
    ''' <param name="IdCliente"></param>
    Public Function VerMisProductosCanjeados(ByVal IdCliente As Integer) As List(Of BE_Voucher)
        VerMisProductosCanjeados = Nothing
    End Function

    ''' 
    ''' <param name="IdCliente"></param>
    Public Function VerMisPuntos(ByVal IdCliente As Integer) As List(Of BE_Punto_Cliente)
        VerMisPuntos = Nothing
    End Function


End Class ' BLL_Marketing
