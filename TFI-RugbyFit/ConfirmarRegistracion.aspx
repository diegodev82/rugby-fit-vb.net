﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ConfirmarRegistracion.aspx.vb" Inherits="TFI_RugbyFit.ConfirmarRegistracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">
    <% If Request.QueryString("rta") = "ok" Then%>
    <div class="alert alert-success">
        <p>Su usuario fue activado correctamente.</p>
    </div>
    <% End If%>
    <% If Request.QueryString("rta") = "error-validacion" Then%>
    <div class="alert alert-warning">
        <p>Los datos de confirmación no son válidos o el usuario ya se encuentra activado.</p>
    </div>
    <% End If%>
    <% If Request.QueryString("rta") = "error-datos" Then%>
    <div class="alert alert-danger">
        <p>Los datos de confirmacion no son validos.</p>
    </div>
    <% End If%>
</asp:Content>
