
Imports BE
Imports MPP
Imports System.Web.HttpContext

Public Class BLL_Bitacora

    Private miMpp As New MPP_Bitacora

    ''' 
    ''' <param name="filtros"></param>
    Public Function ListarEventos(Optional ByVal filtros As Hashtable = Nothing) As List(Of BE_Evento)
        Return miMpp.ListarEventos(filtros)
    End Function

    Public Function ListarBitacora(ByVal filtros As Hashtable) As List(Of BE_Bitacora)
        Return miMpp.ListarBitacora(filtros)
    End Function

    ''' 
    ''' <param name="objEvento"></param>
    Public Function RegistrarEvento(ByVal objEvento As BE_Evento, Optional ByVal obs As String = "", Optional ByVal IdUsuario As Integer = 0) As Boolean
        Dim objBitacora As New BE_Bitacora
        Dim id_usuario_accion As Integer = GetIdUsuarioLogueado()
        If IdUsuario > 0 Then
            id_usuario_accion = IdUsuario
        End If

        objBitacora.Evento = objEvento

        objBitacora.Usuario = New BE_Usuario With {.IdUsuario = id_usuario_accion}
        objBitacora.Observacion = obs
        Dim registrado As Boolean = miMpp.RegistrarEvento(objBitacora)
        Return registrado
    End Function


    ' @TODO: reimplementar porque no funciona
    Protected Function GetIdUsuarioLogueado() As Integer
        Dim usuario As BE_Usuario = CType(System.Web.HttpContext.Current.Session("UsuarioLogueado"), BE_Usuario)
        If Not usuario Is Nothing Then
            Return usuario.IdUsuario
        End If
        Return 0
    End Function

End Class ' BLL_Bitacora

