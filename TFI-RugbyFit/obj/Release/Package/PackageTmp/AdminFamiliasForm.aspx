﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminFamiliasForm.aspx.vb" Inherits="TFI_RugbyFit.AdminFamiliasForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
     <% If Request.QueryString("guardado") = "ok" Then%>
    <div class="alert alert-success">
        <p><span class="glyphicon glyphicon-thumbs-up"></span> El rol fue guardado con &eacute;xito</p>
    </div>
    <% End If%>
    <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
        <div class="alert alert-danger">
            <asp:Literal ID="msjErrorFK" Visible="false" runat="server" Text="El rol no puede ser eliminado ya que existen usuarios asociados a él."></asp:Literal>
            <asp:Literal ID="msjErrorDB" Visible="false" runat="server" Text="Se produjo un error en el sistema y el familia no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
        </div>
    </asp:PlaceHolder>
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <div class="form-group botonera clearfix">
        <a href="AdminFamiliasList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <a href="AdminFamiliasForm.aspx" class="btn btn-default pull-left">Nuevo</a>
        <asp:Button ID="btnEliminar" runat="server" CssClass="btn btn-danger pull-right" Visible="false" Text="Eliminar" />
    </div>
    <asp:HiddenField ID="iptCodigo" runat="server" />

   
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-6'>
                <label>Nombre:</label>
                <asp:TextBox ID="iptNombre" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_iptNombre" runat="server" ErrorMessage="Campo requerido" ControlToValidate="iptNombre"></asp:RequiredFieldValidator>
            </div>
               <asp:PlaceHolder ID="phAsignarFamiliasPermisos" Visible="false" runat="server">       

        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12"><label>Asignar Permisos</label></div>
                <div class="col-md-9">
                    <asp:DropDownList ID="ddPermisos" CssClass="form-control" AppendDataBoundItems="true" runat="server" ValidationGroup="vgAgregarPermiso">
                        <asp:ListItem Enabled="true" Selected="True" Value="">Seleccione...</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_ddPermisos" runat="server" ErrorMessage="Campo Requerido" ControlToValidate="ddPermisos" ValidationGroup="vgAgregarPermiso"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-3">
                    <asp:Button ID="btnAgregarPermiso" OnClick="btnAgregarPermiso_Click" runat="server" CssClass="btn btn-sm btn-warning" Text="+" ValidationGroup="vgAgregarPermiso" />
                </div>
                <div class="col-md-12">
                    <asp:GridView ID="gdvPermisos"  CssClass="table table-hover table-bordered" BorderStyle="None" ItemType="BE_Permisio" runat="server" AutoGenerateColumns="False">

                        <Columns>
                            <asp:BoundField DataField="IdPermiso" HeaderText="Codigo" ReadOnly="True" SortExpression="IdPermiso"></asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Nombre" ReadOnly="True" SortExpression="Descripcion"></asp:BoundField>
                            <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="Eliminar" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
                        </asp:PlaceHolder>
        </div>
        <!-- /row -->
    </div>

    <!-- Placeholders de asignación de familias y permisos -->
    <hr />
 


    
    <div class="form-group botonera clearfix">
        <a href="AdminFamiliasList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <input type='reset' class="btn btn-default  pull-left" name='guardar' value='Cancelar' />
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar" />
    </div>
</asp:Content>
