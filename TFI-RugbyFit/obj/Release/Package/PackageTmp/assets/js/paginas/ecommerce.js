﻿$(document).ready(function () {
    calcularTotal();
    $("body").on("click.EditarItem", ".js-editar-item", function (e) {
        e.preventDefault();
        var idVtaDet = $(this).data("idvtadet");
        var cant = $("#cant_" + idVtaDet).val();
        
        if (idVtaDet == 0) {
            return false;
        }
        if (cant <= 0 || cant > 10) {
            alert("Debe ingresar un valor mayor a 0 y menor 10");
            return false;
        }
        $.ajax(
  {
      "url": "/AjaxUtils.asmx/ActualizarCantidadCarrito",
      data: "{IdVtaDet:" + idVtaDet + ", Cant:" + cant + "}",
      contentType: 'application/json; utf-8',
      dataType: 'json',
      type: 'post',
      success: function (data) {
          if (data.d != null) {
              if (data.d = true) {
                  location.href = "ECommerce.aspx?guardado=ok"
              } else {
                  location.href = "ECommerce.aspx?guardado=error"
              }

          }
      },
  }
);
    });
    $("body").on("click.EliminarItem", ".js-eliminar-item", function (e) {
        e.preventDefault();
        var idVtaDet = $(this).data("idvtadet");
        if (idVtaDet == 0) {
            return false;
        }
        $.ajax(
  {
      "url": "/AjaxUtils.asmx/EliminarItemCarrito",
      data: "{IdVtaDet:" + idVtaDet + "}",
      contentType: 'application/json; utf-8',
      dataType: 'json',
      type: 'post',
      success: function (data) {
          if (data.d != null) {
              if (data.d = true) {
                  location.href = "ECommerce.aspx?eliminado=ok"
              } else {
                  location.href = "ECommerce.aspx?eliminado=error"
              }

          }
      },
  }
);
    });
});

function calcularTotal() {
    var total = 0;
    $(".js-subtotal").each(function () {
        var valor = $(this).data("value") * 1;
        total = total + valor;
    });
    $("#total").html(total.toLocaleString());
}