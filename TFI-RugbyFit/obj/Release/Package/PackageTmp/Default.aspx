﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Default.aspx.vb" Inherits="TFI_RugbyFit._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoMasterPage" runat="server">

    <div class="jumbotron">
        <h1>Bienvenidos a Rugby Fit</h1>
        <p>Somos una empresa que se dedica a la comercialización de equipamiento para entrenamiento de rugby.</p>
        <p>Nuestro aporte será, a través de la comercialización y distribución de máquinas de scrum, ruck, escudos de choque y distintos dispositivos para perfeccionar las destrezas de rugby y elevar la calidad de la competencia interna.</p>
        <p>Productos de alta calidad y con diseños tecnológicamente innovadores que nos permitirán crecer y consolidarnos como la principal empresa de Buenos Aires proveedora de este tipo de equipamiento, aumentando nuestra rentabilidad al ritmo del crecimiento profesional y social del deporte. </p>
        <p>
          <a class="btn btn-lg btn-primary" href="Productos.aspx" role="button">Vea nuestros productos &raquo;</a>
        </p>
      </div>
    
</asp:Content>
