Public Class BE_Cliente

    Public Const ORIGEN_WEB = "WEB"
    Public Const ORINGEN_LOCAL = "LOCAL"

    Public Property Apellido As String = ""
    Public Property Ciudad As New BE_Ciudad
    Public Property Contrasenia As String = ""
    Public Property CUIT As String = ""
    Public Property DNI As Integer = 0
    Public Property Domicilio As String = ""
    Public Property Email As String = ""
    Public Property IdCliente As Integer = 0
    Public Property Nombre As String = ""
    Public Property Origen As String = ""
    Public Property PuntosAcumulados As Integer = 0
    Public Property Telefono As String = ""
    Public Property Activo As Integer = 0
    Public Property IdUsuario As Integer = 0
    Public Property HashRegistracion As String = ""
    Public Property RegistracionConfirmada As Integer = 0
    Public Property Nickname = ""
    Public Property PreguntaSeguridad = ""
    Public Property RespuestaSeguridad = ""
    Public Property VentaWeb As BE_Venta = Nothing
    Public Property SaldoFavor As Decimal = 0
End Class ' BE_Cliente

