﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Backend.Master" CodeBehind="AdminUsuariosForm.aspx.vb" Inherits="TFI_RugbyFit.AdminUsuariosForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadBackend" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoBackendMasterPage" runat="server">
    <% If Request.QueryString("guardado") = "ok" Then%>
    <div class="alert alert-success">
        <p><span class="glyphicon glyphicon-thumbs-up"></span>El usuario fue guardado con &eacute;xito</p>
    </div>
    <% End If%>
    <asp:PlaceHolder ID="RtaErrorEliminar" Visible="false" runat="server">
        <div class="alert alert-danger">
            <asp:Literal ID="msjErrorEsMiUsuario" Visible="false" runat="server" Text="No puede elimiar su propio usuario."></asp:Literal>
            <asp:Literal ID="msjErrorEsAdmin" Visible="false" runat="server" Text="El usuario no puede ser eliminado ya que es el último usuario administrador del sistema."></asp:Literal>
            <asp:Literal ID="msjErrorFK" Visible="false" runat="server" Text="El usuario no puede ser eliminado ya que está asignado a una venta o a una compra"></asp:Literal>
            <asp:Literal ID="msjErrorDB" Visible="false" runat="server" Text="Se produjo un error en el sistema y el usuario no pudo ser eliminado. Intentelo nuevamente por favor"></asp:Literal>
        </div>
    </asp:PlaceHolder>
    <asp:Panel ID="PanelMensajeRespuesta" Visible="false" runat="server">
        <div class="alert alert-danger">
            <span class="glyphicon glyphicon-remove-circle"></span>
            <asp:Literal ID="MensajeRespuesta" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <div class="form-group botonera clearfix">
        <a href="AdminUsuariosList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <a href="AdminUsuariosForm.aspx" class="btn btn-default pull-left">Nuevo</a>
        <asp:Button ID="btnEliminar" runat="server" CssClass="btn btn-danger pull-right" UseSubmitBehavior="false" Visible="false" Text="Eliminar" />
    </div>
    <asp:HiddenField ID="iptCodigo" runat="server" />

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Usuario:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptNickname" runat="server" ClientIDMode="Static" required="required" MaxLength="10" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptNickname" runat="server" ErrorMessage="Campo requerido" ControlToValidate="iptNickname"></asp:RequiredFieldValidator>
            </div>
            <div class='col-md-1'>
                <label>Contrase&ntilde;a:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptContrasenia" runat="server" ClientIDMode="Static" required="required" MaxLength="10" CssClass="form-control" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptContrasenia" runat="server" ErrorMessage="Campo requerido" ControlToValidate="iptContrasenia"></asp:RequiredFieldValidator>
                <!-- <asp:LinkButton ID="lnkCambiarContrasenia" Visible="false" CssClass="btn btn-warning" runat="server">Cambiar Contrase&ntilde;a</asp:LinkButton>-->
                <asp:TextBox ID="txtPwdGuardada" Visible="false" Enabled="false" CssClass="form-control" ReadOnly="true" value="**********" runat="server"></asp:TextBox>
            </div>
        </div>
        <!-- /row -->
    </div>

    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Nombre:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptNombre" runat="server" MaxLength="20" ClientIDMode="Static" required="required" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptNombre" runat="server" ErrorMessage="Campo requerido" ControlToValidate="iptNombre"></asp:RequiredFieldValidator>
            </div>
              <div class='col-md-1'>
                <label>Apellido:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptApellido" runat="server" MaxLength="30" ClientIDMode="Static" required="required" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_iptApellido" runat="server" ErrorMessage="Campo requerido" ControlToValidate="iptApellido"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- /row -->
    </div>
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Tel&eacute;fono:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptTelefono" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
            </div>
              <div class='col-md-1'>
                <label>Email:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptEmail" runat="server" MaxLength="20" ClientIDMode="Static" CssClass="form-control" TextMode="Email"></asp:TextBox>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>DNI:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptDNI" runat="server" onkeypress="return validarMaxLength(e, this,8)"  ClientIDMode="Static" MaxLength="8" Max="99999999" CssClass="form-control" TextMode="Number"></asp:TextBox>
            </div>
            <div class='col-md-1'>
                <label>C.I.I.:</label>
            </div>
            <div class='col-md-5'>
                <asp:TextBox ID="iptCII" runat="server" MaxLength="50" CssClass="form-control" ReadOnly="true" Enabled="false" TextMode="Number"></asp:TextBox>
                <p class="help-block">Cantidad de Ingresos Incorrectos</p>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    
    <div class='form-group clearfix'>
        <div class='row'>
            <div class='col-md-1'>
                <label>Activo:</label>
            </div>
            <div class='col-md-5'>
                <asp:DropDownList ID="ddActivo" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="1">Sí</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class='col-md-1'>
                <label>Bloqueado:</label>
            </div>
            <div class='col-md-5'>
                <asp:DropDownList ID="ddBloqueado" runat="server" CssClass="form-control" AutoPostBack="True">
                    <asp:ListItem Value="1">Sí</asp:ListItem>
                    <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /from-group -->
    
    <!-- Placeholders de asignación de familias y permisos -->
    <hr />
    <asp:PlaceHolder ID="phAsignarFamiliasPermisos" Visible="false" runat="server">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12"><span class="lead">Asignar Roles</span></div>
                <div class="col-md-10">
                    <asp:DropDownList ID="ddFamilias" CssClass="form-control" runat="server" AppendDataBoundItems="true" ValidationGroup="vgAgregarFamilia">
                        <asp:ListItem Enabled="true" Selected="True" Value="">Seleccione...</asp:ListItem>
                    </asp:DropDownList>
                <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_ddFamilias" runat="server" ErrorMessage="Campo Requerido" ControlToValidate="ddFamilias" ValidationGroup="vgAgregarFamilia"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                    <asp:Button OnClick="btnAgregarFamilia_Click" ID="btnAgregarFamilia" runat="server" CssClass="btn btn-sm btn-warning" Text="+" ValidationGroup="vgAgregarFamilia" />
                </div>
                <div class="col-md-12">
                    <asp:GridView  ID="gdvFamilias" runat="server" CssClass="table table-hover table-bordered" BorderStyle="None" ItemType="BE_Familia" AutoGenerateColumns="False" BorderWidth="0">
                        <Columns>
                            <asp:BoundField DataField="IdFamilia" HeaderText="Codigo" ReadOnly="True" SortExpression="IdFamilia" ControlStyle-BorderStyle="None"></asp:BoundField>
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" ReadOnly="True" SortExpression="Nombre" ControlStyle-BorderStyle="None"></asp:BoundField>
                            <asp:CommandField  ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="Eliminar" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12"><span class="lead">Asignar Permisos</span></div>
                <div class="col-md-6">
                    <asp:DropDownList ID="ddPermisos" CssClass="form-control" AppendDataBoundItems="true" runat="server" ValidationGroup="vgAgregarPermiso">
                        <asp:ListItem Enabled="true" Selected="True" Value="">Seleccione...</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_ddPermisos" runat="server" ErrorMessage="Campo Requerido" ControlToValidate="ddPermisos" ValidationGroup="vgAgregarPermiso"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddTipoPermiso" CssClass="form-control" runat="server" ValidationGroup="vgAgregarPermiso">
                        <asp:ListItem Value="">Seleccione...</asp:ListItem>
                        <asp:ListItem Value="1">Permitir</asp:ListItem>
                        <asp:ListItem Value="0">Denegar</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator_ddTipoPermiso" runat="server" ErrorMessage="Campo Requerido" ControlToValidate="ddTipoPermiso" ValidationGroup="vgAgregarPermiso"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                    <asp:Button ID="btnAgregarPermiso" OnClick="btnAgregarPermiso_Click" runat="server" CssClass="btn btn-sm btn-warning" Text="+" ValidationGroup="vgAgregarPermiso" />
                </div>
                <div class="col-md-12">
                    <asp:GridView ID="gdvPermisos"  CssClass="table table-hover table-bordered" BorderStyle="None" ItemType="BE_Permisio" runat="server" AutoGenerateColumns="False">

                        <Columns>
                            <asp:BoundField DataField="IdPermiso" HeaderText="Codigo" ReadOnly="True" SortExpression="IdPermiso"></asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Nombre" ReadOnly="True" SortExpression="Descripcion"></asp:BoundField>
                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" ReadOnly="True" SortExpression="Tipo"></asp:BoundField>
                            <asp:CommandField ShowDeleteButton="True" DeleteText="<i class='glyphicon glyphicon-trash'></i>" ControlStyle-CssClass="btn btn-xs btn-danger" ControlStyle-BorderStyle="None" HeaderText="Eliminar" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
        </asp:PlaceHolder>



    
    <div class="form-group botonera clearfix">
        <a href="AdminUsuariosList.aspx" class="btn btn-info pull-left">Volver al listado</a>
        <input type='reset' class="btn btn-default  pull-left" name='guardar' value='Cancelar' />
        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar" />
    </div>
</asp:Content>
